package com.zkzj.pojo.query.hrm;

import java.util.List;

import com.zkzj.pojo.query.base.BaseQuery;

public class EmployeeQuery extends BaseQuery {

	private static final long serialVersionUID = 1L;

	private Long userId;
	private String employeeName;
	private Integer sex;
	private String nativePlace;
	private String address;
	private String mobile;
	private Long cityId;
    private Long merchantsId;
	private List<Long> empIds;

	public Long getMerchantsId() {
		return merchantsId;
	}

	public void setMerchantsId(Long merchantsId) {
		this.merchantsId = merchantsId;
	}

	public Long getCityId() {
		return cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getNativePlace() {
		return nativePlace;
	}

	public void setNativePlace(String nativePlace) {
		this.nativePlace = nativePlace;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<Long> getEmpIds() {
		return empIds;
	}

	public void setEmpIds(List<Long> empIds) {
		this.empIds = empIds;
	}

}