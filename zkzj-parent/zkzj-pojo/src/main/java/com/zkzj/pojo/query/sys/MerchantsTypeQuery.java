package com.zkzj.pojo.query.sys;

import com.zkzj.pojo.query.base.BaseQuery;

public class MerchantsTypeQuery extends BaseQuery {

	private static final long serialVersionUID = 1L;

	private String typeName;

	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

}