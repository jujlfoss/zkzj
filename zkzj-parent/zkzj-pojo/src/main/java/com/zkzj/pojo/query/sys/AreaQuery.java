package com.zkzj.pojo.query.sys;

import com.zkzj.pojo.query.base.BaseQuery;

public class AreaQuery extends BaseQuery {

	private static final long serialVersionUID = 1L;
	private Long id;

	private String areaName;
	private Long parentId;
	private Long countyId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public Long getCountyId() {
		return countyId;
	}
	public void setCountyId(Long countyId) {
		this.countyId = countyId;
	}
	

}