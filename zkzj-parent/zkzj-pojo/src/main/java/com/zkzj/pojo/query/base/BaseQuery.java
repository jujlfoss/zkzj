package com.zkzj.pojo.query.base;

import java.io.Serializable;

public abstract class BaseQuery implements Serializable {

	private static final long serialVersionUID = 1L;

	protected String teamIds;

	protected String startDate;// 开始日期
	protected String endDate;// 结束日期
	protected Integer status;

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getTeamIds() {
		return teamIds;
	}

	public void setTeamIds(String teamIds) {
		this.teamIds = teamIds;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}
