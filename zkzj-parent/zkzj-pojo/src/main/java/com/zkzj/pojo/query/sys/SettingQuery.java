package com.zkzj.pojo.query.sys;

import com.zkzj.pojo.query.base.BaseQuery;

public class SettingQuery extends BaseQuery {

	private static final long serialVersionUID = 1L;

	private String settingKey;
	private String settingValue;
	private String description;
	

	public String getSettingKey() {
		return settingKey;
	}
	public void setSettingKey(String settingKey) {
		this.settingKey = settingKey;
	}
	public String getSettingValue() {
		return settingValue;
	}
	public void setSettingValue(String settingValue) {
		this.settingValue = settingValue;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	

}