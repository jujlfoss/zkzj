package com.zkzj.pojo.vos.pay;

/**
 * 微信支付：统一下单
 * @author Heguoyong
 * 相关字段详解见： https://pay.weixin.qq.com/wiki/doc/api/native.php?chapter=9_1
 */
public class WeChatUnifiedOrder extends WeChatPayCommon {
	
	/**统一下单URL*/
	public static String URL = "https://api.mch.weixin.qq.com/pay/unifiedorder";
	/**商品描述*/
	private String body;
	/**商户订单号*/
	private String outTradeNo;
	/**标价金额*/
	private Integer totalFee;
	/**终端IP*/
	private String spbillCreateIp;
	/**异步通知地址*/
	private String notifyUrl;
	/**交易类型*/
	private String tradeType;
	/**附加数据*/
	private String attach;
	
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getOutTradeNo() {
		return outTradeNo;
	}
	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}
	public Integer getTotalFee() {
		return totalFee;
	}
	public void setTotalFee(Integer totalFee) {
		this.totalFee = totalFee;
	}
	public String getSpbillCreateIp() {
		return spbillCreateIp;
	}
	public void setSpbillCreateIp(String spbillCreateIp) {
		this.spbillCreateIp = spbillCreateIp;
	}
	public String getNotifyUrl() {
		return notifyUrl;
	}
	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}
	public String getTradeType() {
		return tradeType;
	}
	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}
	public String getAttach() {
		return attach;
	}
	public void setAttach(String attach) {
		this.attach = attach;
	}
}
