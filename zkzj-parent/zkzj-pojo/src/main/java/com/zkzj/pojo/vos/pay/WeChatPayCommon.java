package com.zkzj.pojo.vos.pay;

/**
 * 微信公共量
 * @author Heguoyong
 */
public class WeChatPayCommon {
	
	/**公众账号ID*/
	private String appid;
	/**商户号*/
	private String mchId;
	/**密钥*/
	private String key;
	/**随机字符串*/
	private String nonceStr;
	/**签名*/
	private String sign; 
	
	public String getAppid() {
		return appid;
	}
	public void setAppid(String appid) {
		this.appid = appid;
	}
	public String getMchId() {
		return mchId;
	}
	public void setMchId(String mchId) {
		this.mchId = mchId;
	}
	public String getNonceStr() {
		return nonceStr;
	}
	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	
}
