package com.zkzj.pojo.vos.pay;

/**
 * 微信用户申请退款
 * @author Heguoyong
 * 请求需要双向证书。 详见证书使用
 * 相关字段详解见： https://pay.weixin.qq.com/wiki/doc/api/native.php?chapter=9_4
 */
public class WeChatRefund extends WeChatPayCommon {
	
	/**申请退款URL*/
	public static String URL = "https://api.mch.weixin.qq.com/secapi/pay/refund";
	/**微信订单号*/
	private String transactionId;
	/**商户订单号*/
	private String outTradeNo;
	/**退款订单号*/
	private String outRefundNo;
	/**订单金额*/
	private Integer totalFee;
	/**退款金额*/
	private Integer refundFee;
	/**退款原因*/
	private String refundDesc;
	
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getOutTradeNo() {
		return outTradeNo;
	}
	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}
	public String getOutRefundNo() {
		return outRefundNo;
	}
	public void setOutRefundNo(String outRefundNo) {
		this.outRefundNo = outRefundNo;
	}
	public Integer getTotalFee() {
		return totalFee;
	}
	public void setTotalFee(Integer totalFee) {
		this.totalFee = totalFee;
	}
	public Integer getRefundFee() {
		return refundFee;
	}
	public void setRefundFee(Integer refundFee) {
		this.refundFee = refundFee;
	}
	public String getRefundDesc() {
		return refundDesc;
	}
	public void setRefundDesc(String refundDesc) {
		this.refundDesc = refundDesc;
	}
	
}
