package com.zkzj.pojo.query.sys;

import com.zkzj.pojo.query.base.BaseQuery;

public class HomePageImgQuery extends BaseQuery {

	private static final long serialVersionUID = 1L;

	private String path;
	private String name;
	private String linkAddr;
	private String h5name;

	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLinkAddr() {
		return linkAddr;
	}
	public void setLinkAddr(String linkAddr) {
		this.linkAddr = linkAddr;
	}
	public String getH5name() {
		return h5name;
	}
	public void setH5name(String h5name) {
		this.h5name = h5name;
	}

}