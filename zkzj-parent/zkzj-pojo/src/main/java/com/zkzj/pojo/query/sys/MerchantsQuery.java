package com.zkzj.pojo.query.sys;

import com.zkzj.pojo.query.base.BaseQuery;

public class MerchantsQuery extends BaseQuery {

	private static final long serialVersionUID = 1L;

	private String name;
	private Long merchantsTypeId;
	private Long areaId;

	public Long getAreaId() {
		return areaId;
	}

	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getMerchantsTypeId() {
		return merchantsTypeId;
	}
	public void setMerchantsTypeId(Long merchantsTypeId) {
		this.merchantsTypeId = merchantsTypeId;
	}
	

}