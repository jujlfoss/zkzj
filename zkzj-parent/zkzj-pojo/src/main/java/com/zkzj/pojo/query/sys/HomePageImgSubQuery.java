package com.zkzj.pojo.query.sys;

import com.zkzj.pojo.query.base.BaseQuery;

public class HomePageImgSubQuery extends BaseQuery {

	private static final long serialVersionUID = 1L;

	private Long homePageImgId;
	private String name;

	public Long getHomePageImgId() {
		return homePageImgId;
	}
	public void setHomePageImgId(Long homePageImgId) {
		this.homePageImgId = homePageImgId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}