package com.zkzj.pojo.query.sys;

import com.zkzj.pojo.query.base.BaseQuery;
import java.math.BigDecimal;
import java.math.BigDecimal;
public class CityQuery extends BaseQuery {

	private static final long serialVersionUID = 1L;

	private String areaName;
	private Integer areaType;
	private Long parentId;
	private String baiduCode;
	private String cityCode;
	private String addressCode;
	private BigDecimal latitude;
	private BigDecimal longitude;

	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public Integer getAreaType() {
		return areaType;
	}
	public void setAreaType(Integer areaType) {
		this.areaType = areaType;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public String getBaiduCode() {
		return baiduCode;
	}
	public void setBaiduCode(String baiduCode) {
		this.baiduCode = baiduCode;
	}
	public String getCityCode() {
		return cityCode;
	}
	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	public String getAddressCode() {
		return addressCode;
	}
	public void setAddressCode(String addressCode) {
		this.addressCode = addressCode;
	}
	public BigDecimal getLatitude() {
		return latitude;
	}
	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}
	public BigDecimal getLongitude() {
		return longitude;
	}
	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

}