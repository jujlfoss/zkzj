package com.zkzj.service.hrm;

import com.zkzj.entity.hrm.Employee;
import com.zkzj.entity.sys.User;
import com.zkzj.pojo.query.hrm.EmployeeQuery;
import com.zkzj.pojo.vos.common.PageVo;
import com.zkzj.service.base.BaseService;

public interface EmployeeService extends BaseService<Employee> {

	/**
	 * 分页
	 * 
	 * @param employee
	 * @param start
	 * @param limit
	 * @return
	 */
	PageVo pageEmployee(EmployeeQuery employeeQuery, int start, int limit);

	/**
	 * 新增
	 * 
	 * @param employee
	 */
	void saveEmployee(Employee employee);

	/**
	 * 更新
	 *
	 * @param employee
	 */
	void updateEmployee(Employee employee);

	/**
	 * 删除
	 * 
	 * @param id
	 */
	void removeEmployee(long id);

	/**
	 * 恢复
	 * 
	 * @param id
	 */
	void recover(long id);

	/**
	 * 保存员工账户
	 * 
	 * @param user
	 */
	void saveEmployeeUser(User user);

	/**
	 * 冻结用户
	 * 
	 * @param id
	 */
	void freeze(long id);

	/**
	 * 解冻用户
	 * 
	 * @param id
	 */
	void unfreeze(long id);
	


	Employee getEmployeeByUserId(long userId);

}
