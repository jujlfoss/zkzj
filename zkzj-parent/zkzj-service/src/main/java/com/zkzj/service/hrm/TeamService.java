package com.zkzj.service.hrm;

import com.zkzj.entity.hrm.Team;
import com.zkzj.pojo.vos.common.PageVo;
import com.zkzj.service.base.BaseService;

public interface TeamService extends BaseService<Team> {

	/**
	 * 分页显示
	 * 
	 * @param team
	 * @param start
	 * @param limit
	 * @return
	 */
	PageVo pageTeam(Team team, int start, int limit);

	/**
	 * 保存
	 * 
	 * @param team
	 */
	void saveTeam(Team team);

	/**
	 * 更新
	 * 
	 * @param team
	 */
	void updateTeam(Team team);

	/**
	 * 删除
	 * 
	 * @param id
	 */
	boolean removeTeam(long id);

	/**
	 * 恢复
	 * 
	 * @param id
	 */
	void recover(long id);

	/**
	 * 获取团队树
	 * 
	 * @return
	 */
	Team listTeam();

	/**
	 * 获取选择团队树
	 * 
	 * @return
	 */
	Team listChecked();

	/**
	 * 移动团队树节点
	 * 
	 * @param id
	 * @param parentId
	 * @return
	 */
	Team move(long id, long parentId);

}
