package com.zkzj.service.sys.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zkzj.dao.sys.MerchantsTypeDao;
import com.zkzj.entity.enums.StatusEnum;
import com.zkzj.entity.sys.MerchantsType;
import com.zkzj.pojo.query.sys.MerchantsTypeQuery;
import com.zkzj.pojo.vos.common.PageVo;
import com.zkzj.service.base.impl.BaseServiceImpl;
import com.zkzj.service.sys.MerchantsTypeService;
import com.zkzj.utils.DateUtil;

@Service("merchantsTypeService")
public class MerchantsTypeServiceImpl extends BaseServiceImpl<MerchantsType> implements MerchantsTypeService {

	@Autowired
	private MerchantsTypeDao merchantsTypeDao;

	@Override
	public PageVo pageMerchantsType(MerchantsTypeQuery merchantsTypeQuery, Integer start, Integer limit) {
		return new PageVo(start, limit, this.merchantsTypeDao.countMerchantsType(merchantsTypeQuery), this.merchantsTypeDao.listMerchantsType(merchantsTypeQuery, start, limit));
	}

	@Override
	public void saveMerchantsType(MerchantsType merchantsType) {
		long time = DateUtil.getTime();

		this.merchantsTypeDao.save(merchantsType);
	}
	
	@Override
	public void updateMerchantsType(MerchantsType merchantsType) {
		this.merchantsTypeDao.updateByPrimaryKeySelective(merchantsType);
	}

	@Override
	public void removeMerchantsType(long id) {
		try {
			this.merchantsTypeDao.removeByPrimaryKey(id);
		} catch (Exception e) {
			log.info(e.getMessage(),e);
			MerchantsType merchantsType = new MerchantsType();
			merchantsType.setId(id);
			this.merchantsTypeDao.updateByPrimaryKeySelective(merchantsType);
		}
	}

	@Override
	public void recover(long id) {
		MerchantsType merchantsType = new MerchantsType();

		this.merchantsTypeDao.updateByPrimaryKeySelective(merchantsType);
	}

}
