package com.zkzj.service.sys;

import java.util.List;
import java.util.Set;

import com.zkzj.entity.sys.Resource;
import com.zkzj.pojo.vos.sys.PermitVo;
import com.zkzj.service.base.BaseService;

/**
 * ResourceService
 * 
 * @author liu
 * @date 2016年8月21日
 */
public interface ResourceService extends BaseService<Resource> {

	/**
	 * 获取菜单树
	 * 
	 * @return
	 */
	Resource listMenu();

	/**
	 * 获取权限vo
	 * 
	 * @param permissions
	 * @return
	 */
	List<PermitVo> listPermit(Set<String> permissions);

	/**
	 * 获取资源树
	 * 
	 * @return
	 */
	Resource listResource();

	/**
	 * 保存资源
	 * 
	 * @param resource
	 * @return
	 */
	void saveResource(Resource resource);

	/**
	 * 编辑资源
	 * 
	 * @return
	 */
	void updateResource(long id, Resource resource);

	/**
	 * 删除资源
	 * 
	 * @param id
	 * @return
	 */
	boolean removeResource(long id);

	/**
	 * 恢复资源
	 * 
	 * @param id
	 */
	void recover(long id);

	/**
	 * 冻结资源
	 * 
	 * @param id
	 */
	void freeze(long id);

	/**
	 * 解冻资源
	 * 
	 * @param id
	 */
	void unfreeze(long id);

	/**
	 * 移动资源
	 * 
	 * @param id
	 * @param parentId
	 * @return
	 */
	Resource move(long id, long parentId);

	/**
	 * 获取授权树
	 * 
	 * @param roleId
	 * @return
	 */
	Resource listGrant(long roleId);

}
