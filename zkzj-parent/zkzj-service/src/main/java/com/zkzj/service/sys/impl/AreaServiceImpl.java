package com.zkzj.service.sys.impl;

import com.zkzj.utils.tree.AreaTree;
import com.zkzj.utils.tree.BaseTree;
import com.zkzj.utils.tree.TreeUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zkzj.dao.sys.AreaDao;
import com.zkzj.entity.enums.StatusEnum;
import com.zkzj.entity.sys.Area;
import com.zkzj.pojo.query.sys.AreaQuery;
import com.zkzj.pojo.vos.common.PageVo;
import com.zkzj.service.base.impl.BaseServiceImpl;
import com.zkzj.service.sys.AreaService;
import com.zkzj.utils.DateUtil;

import java.util.ArrayList;
import java.util.List;

@Service("areaService")
public class AreaServiceImpl extends BaseServiceImpl<Area> implements AreaService {

	@Autowired
	private AreaDao areaDao;

	@Override
	public PageVo pageArea(AreaQuery areaQuery, Integer start, Integer limit) {
		return new PageVo(start, limit, this.areaDao.countArea(areaQuery), this.areaDao.listArea(areaQuery, start, limit));
	}

	@Override
	public void saveArea(Area area) {
		long time = DateUtil.getTime();
		area.setCreateTime(time);
		area.setModifyTime(time);
		area.setStatus(StatusEnum.NORMAL.getValue());
		this.areaDao.save(area);
	}
	
	@Override
	public void updateArea(Area area) {
		area.setModifyTime(DateUtil.getTime());
		this.areaDao.updateByPrimaryKeySelective(area);
	}

	@Override
	public void removeArea(long id) {
		try {
			this.areaDao.removeByPrimaryKey(id);
		} catch (Exception e) {
			log.info(e.getMessage(),e);
			Area area = new Area();
			area.setId(id);
			area.setModifyTime(DateUtil.getTime());
			area.setStatus(StatusEnum.DELETE.getValue());
			this.areaDao.updateByPrimaryKeySelective(area);
		}
	}

	@Override
	public void recover(long id) {
		Area area = new Area();
		area.setId(id);
		area.setModifyTime(DateUtil.getTime());
		area.setStatus(StatusEnum.NORMAL.getValue());
		this.areaDao.updateByPrimaryKeySelective(area);
	}

	@Override
	public BaseTree areaTree() {
		BaseTree root = new BaseTree();
		root.setId("0");
		root.setParentId("-1");
		List<Area> list = areaDao.areaTree();
		List<AreaTree> rootNode = new ArrayList<AreaTree>();
		list.forEach(area -> {
			AreaTree tree= new AreaTree();
			BeanUtils.copyProperties(area,tree);
			tree.setId(area.getId()+"");
            if (area.getParentId() == null) {
                tree.setParentId("0");
            } else {
                tree.setParentId(area.getParentId() + "");
            }
//            tree.setSuperName(area.getSuperName());
			rootNode.add(tree);


		});
		TreeUtil.appendChildren(root, rootNode);
		return root;
	}

}
