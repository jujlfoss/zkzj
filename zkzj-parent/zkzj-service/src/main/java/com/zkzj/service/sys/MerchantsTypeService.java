package com.zkzj.service.sys;

import com.zkzj.entity.sys.MerchantsType;
import com.zkzj.pojo.query.sys.MerchantsTypeQuery;
import com.zkzj.pojo.vos.common.PageVo;
import com.zkzj.service.base.BaseService;

public interface MerchantsTypeService extends BaseService<MerchantsType> {

	/**
	 * 分页
	 * 
	 * @param merchantsType
	 * @param start
	 * @param limit
	 * @return
	 */
	PageVo pageMerchantsType(MerchantsTypeQuery merchantsTypeQuery, Integer start, Integer limit);

	/**
	 * 新增
	 * 
	 * @param merchantsType
	 */
	void saveMerchantsType(MerchantsType merchantsType);
	
	/**
	 * 更新
	 *
	 * @param merchantsType
	 */
	void updateMerchantsType(MerchantsType merchantsType);

	/**
	 * 删除
	 * 
	 * @param id
	 */
	void removeMerchantsType(long id);

	/**
	 * 恢复
	 * 
	 * @param id
	 */
	void recover(long id);

}
