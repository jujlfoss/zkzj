package com.zkzj.service.sys;

import com.zkzj.entity.sys.City;
import com.zkzj.pojo.query.sys.CityQuery;
import com.zkzj.pojo.vos.common.PageVo;
import com.zkzj.service.base.BaseService;

import java.util.List;

public interface CityService extends BaseService<City> {

	/**
	 * 分页
	 * 
	 * @param city
	 * @param start
	 * @param limit
	 * @return
	 */
	PageVo pageCity(CityQuery cityQuery, Integer start, Integer limit);

	/**
	 * 新增
	 * 
	 * @param city
	 */
	void saveCity(City city);
	
	/**
	 * 更新
	 *
	 * @param city
	 */
	void updateCity(City city);

	/**
	 * 删除
	 * 
	 * @param id
	 */
	void removeCity(long id);

	/**
	 * 恢复
	 * 
	 * @param id
	 */
	void recover(long id);

	List<City> getList(Long id);

}
