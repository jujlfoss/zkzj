package com.zkzj.service.sys;

import com.zkzj.entity.sys.Area;
import com.zkzj.pojo.query.sys.AreaQuery;
import com.zkzj.pojo.vos.common.PageVo;
import com.zkzj.service.base.BaseService;
import com.zkzj.utils.tree.BaseTree;

public interface AreaService extends BaseService<Area> {

	/**
	 * 分页
	 * 
	 * @param area
	 * @param start
	 * @param limit
	 * @return
	 */
	PageVo pageArea(AreaQuery areaQuery, Integer start, Integer limit);

	/**
	 * 新增
	 * 
	 * @param area
	 */
	void saveArea(Area area);
	
	/**
	 * 更新
	 *
	 * @param area
	 */
	void updateArea(Area area);

	/**
	 * 删除
	 * 
	 * @param id
	 */
	void removeArea(long id);

	/**
	 * 恢复
	 * 
	 * @param id
	 */
	void recover(long id);
	BaseTree areaTree();

}
