package com.zkzj.service.sys.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zkzj.dao.sys.MerchantsDao;
import com.zkzj.entity.enums.StatusEnum;
import com.zkzj.entity.sys.Merchants;
import com.zkzj.pojo.query.sys.MerchantsQuery;
import com.zkzj.pojo.vos.common.PageVo;
import com.zkzj.service.base.impl.BaseServiceImpl;
import com.zkzj.service.sys.MerchantsService;
import com.zkzj.utils.DateUtil;

@Service("merchantsService")
public class MerchantsServiceImpl extends BaseServiceImpl<Merchants> implements MerchantsService {

	@Autowired
	private MerchantsDao merchantsDao;

	@Override
	public PageVo pageMerchants(MerchantsQuery merchantsQuery, Integer start, Integer limit) {
		return new PageVo(start, limit, this.merchantsDao.countMerchants(merchantsQuery), this.merchantsDao.listMerchants(merchantsQuery, start, limit));
	}

	@Override
	public void saveMerchants(Merchants merchants) {
		long time = DateUtil.getTime();
		merchants.setCreateTime(time);
		merchants.setModifyTime(time);
		merchants.setStatus(StatusEnum.NORMAL.getValue());
		merchants.setId(null);
		this.merchantsDao.save(merchants);
	}
	
	@Override
	public void updateMerchants(Merchants merchants) {
		merchants.setModifyTime(DateUtil.getTime());
		this.merchantsDao.updateByPrimaryKeySelective(merchants);
	}

	@Override
	public void removeMerchants(long id) {
		try {
			this.merchantsDao.removeByPrimaryKey(id);
		} catch (Exception e) {
			log.info(e.getMessage(),e);
			Merchants merchants = new Merchants();
			merchants.setId(id);
			merchants.setModifyTime(DateUtil.getTime());
			merchants.setStatus(StatusEnum.DELETE.getValue());
			this.merchantsDao.updateByPrimaryKeySelective(merchants);
		}
	}

	@Override
	public void recover(long id) {
		Merchants merchants = new Merchants();
		merchants.setId(id);
		merchants.setModifyTime(DateUtil.getTime());
		merchants.setStatus(StatusEnum.NORMAL.getValue());
		this.merchantsDao.updateByPrimaryKeySelective(merchants);
	}

}
