package com.zkzj.service.fileutil;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

/**
 * 文件上传接口
 * 
 * @author liujialin
 *
 */
public interface FileUploadService {
	/**
	 * 上传并切图
	 * 
	 * @param files
	 *            图片
	 * @param height
	 *            高
	 * @param width
	 *            宽
	 * @return 路径
	 * @throws IOException
	 */

	String uploadWithThumbnails(List<MultipartFile> files, Integer height, Integer width, String desPath)
			throws IOException;

	/**
	 * 上传图片
	 * 
	 * @param files
	 *            图片
	 * @param thumbnails
	 *            是否切图
	 * @return 路径
	 * @throws IOException
	 */
	String multipleUpload(List<MultipartFile> files, String desPath) throws IOException;

	/**
	 * 删除图片
	 * 
	 * @param img
	 * @throws IOException
	 */
	void deleteImage(String img) throws IOException;

	/**
	 * 切图
	 * 
	 * @param file
	 * @param rows
	 *            横向块数
	 * @param cols
	 *            纵向块数
	 * @param savePath
	 *            切图后存放路径
	 * @return
	 */
	String cutImage(File file, int rows, int cols, String savePath);
	
	String multipleUploadHtm(List<MultipartFile> files, String desPath) throws IOException;

}
