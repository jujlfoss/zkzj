package com.zkzj.service.jpush;

public interface JpushManager {

	void sendEmployeeMsg(String mobile, String title, String msg, Integer type);

	void sendCustomerMsg(String mobile, String title, String msg, Integer type);

}
