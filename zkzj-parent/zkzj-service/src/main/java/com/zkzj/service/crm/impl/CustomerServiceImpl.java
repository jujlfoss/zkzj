package com.zkzj.service.crm.impl;

import java.math.BigDecimal;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zkzj.dao.crm.CustomerDao;
import com.zkzj.dao.redis.RedisRepository;
import com.zkzj.dao.sys.UserDao;
import com.zkzj.dao.sys.UserRoleDao;
import com.zkzj.entity.crm.Customer;
import com.zkzj.entity.enums.StatusEnum;
import com.zkzj.entity.sys.User;
import com.zkzj.entity.sys.UserRole;
import com.zkzj.pojo.constants.Constant;
import com.zkzj.pojo.exceptions.ControllerException;
import com.zkzj.pojo.exceptions.ExceptionEnum;
import com.zkzj.pojo.exceptions.ServiceException;
import com.zkzj.pojo.query.crm.CustomerQuery;
import com.zkzj.pojo.vos.common.PageVo;
import com.zkzj.service.base.impl.BaseServiceImpl;
import com.zkzj.service.crm.CustomerService;
import com.zkzj.service.sys.UserService;
import com.zkzj.utils.DateUtil;
import com.zkzj.utils.EncodeUtil;
import com.zkzj.utils.MD5Util;
import com.zkzj.utils.ValidUtil;

import tk.mybatis.mapper.entity.Example;

@Service("customerService")
public class CustomerServiceImpl extends BaseServiceImpl<Customer> implements CustomerService {

	@Autowired
	private CustomerDao customerDao;
	@Autowired
	private UserService userService;

	@Autowired
	private UserRoleDao userRoleDao;
	@Autowired
	private RedisRepository redisRepository;
	@Autowired
	private UserDao userDao;

	@Override
	public PageVo pageCustomer(CustomerQuery customerQuery, int start, int limit) {
		return new PageVo(start, limit, this.customerDao.countCustomer(customerQuery),
				this.customerDao.listCustomer(customerQuery, start, limit));
	}

	@Override
	public void saveCustomer(Customer customer) {
		long time = DateUtil.getTime();
		customer.setCreateTime(time);
		customer.setModifyTime(time);
		customer.setStatus(StatusEnum.NORMAL.getValue());
		this.customerDao.save(customer);
	}

	@Override
	public void removeCustomer(long id) {
		try {
			this.customerDao.removeByPrimaryKey(id);
		} catch (Exception e) {
			log.info(e.getMessage(), e);
			Customer customer = new Customer();
			customer.setId(id);
			customer.setModifyTime(DateUtil.getTime());
			customer.setStatus(StatusEnum.DELETE.getValue());
			this.customerDao.updateByPrimaryKeySelective(customer);
		}
	}

	@Override
	public void recover(long id) {
		Customer customer = new Customer();
		customer.setId(id);
		customer.setModifyTime(DateUtil.getTime());
		customer.setStatus(StatusEnum.NORMAL.getValue());
		this.customerDao.updateByPrimaryKeySelective(customer);
	}

	@Override
	public String getCustomerName(Long id) {
		return customerDao.getByPrimaryKey(id).getCustomerName();
	}

	@Override
	public void updateCustomer(Customer customer) {
		customer.setModifyTime(DateUtil.getTime());
		this.customerDao.updateByPrimaryKeySelective(customer);
	}

	@Override
	public void saveCustomerUser(User user, String referrerMobile, String inviteMobile) {

	}

	@Override
	public Customer getCustomerByUserId(long userId) {
		return this.customerDao.getCustomerByUserId(userId);
	}

	@Override
	public void updatePayPsw(Long id, String newPsw, String pswRe, String mobileCode) {
		if (!MD5Util.getMD5(newPsw).equals(MD5Util.getMD5(pswRe))) {
			throw new ServiceException("输入支付密码不一致");
		}
		Customer customer = getByPrimaryKey(id);
		String psw = customer.getPayPsw();
		if (!StringUtils.isEmpty(psw)) {
			if (psw.equals(MD5Util.getMD5(newPsw))) {
				throw new ServiceException("与原支付密码相同");
			}
		}
		User user = userDao.getByPrimaryKey(customer.getUserId());
		String mobileCodeKey = Constant.VALIDCODE + user.getMobile();
		if (!this.redisRepository.exist(mobileCodeKey)) {
			throw new ServiceException(ExceptionEnum.MOBILE_VALID_NOT_EXISTS);
		}
		if (!((String) this.redisRepository.get(mobileCodeKey)).equalsIgnoreCase(mobileCode)) {
			throw new ControllerException(ExceptionEnum.MOBILE_VALID_CODE_ERROR);
		}
		customer.setPayPsw(MD5Util.getMD5(newPsw));
		customerDao.updateByPrimaryKey(customer);

	}

	@Override
	public boolean whetherSetPayPsw(Long id) {
		Customer customer = customerDao.getByPrimaryKey(id);
		if (!StringUtils.isEmpty(customer.getPayPsw())) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isCheckPass(Long customerId, String moilde, String payPsw, String code) {
		Customer customer = customerDao.getByPrimaryKey(customerId);
		User user = userDao.getByPrimaryKey(customer.getUserId());
		if (user.getMobile().equals(moilde)) {
			throw new ServiceException("手机号未发生变动");
		}
		Example ex = new Example(User.class);
		ex.createCriteria().andEqualTo("mobile", moilde);
		if (userDao.getCountByExample(ex) > 0) {
			throw new ServiceException("该手机号已注册");
		}
		String mobileCodeKey = Constant.VALIDCODE + moilde;
		if (!this.redisRepository.exist(mobileCodeKey)) {
			throw new ServiceException(ExceptionEnum.MOBILE_VALID_NOT_EXISTS);
		}
		if (!((String) this.redisRepository.get(mobileCodeKey)).equalsIgnoreCase(code)) {
			throw new ControllerException(ExceptionEnum.MOBILE_VALID_CODE_ERROR);
		}

		if (!MD5Util.getMD5(payPsw).equals(customer.getPayPsw())) {
			throw new ServiceException("支付密码不正确");
		}
		redisRepository.delete(mobileCodeKey);
		return true;
	}

	@Override
	public Customer webGetCustomer(Long id) {
		return customerDao.getByPrimaryKey(id);
	}

	@Override
	public Customer getCurrentCustomerInfo(long id) {
		return customerDao.getCurrentCustomerInfo(id);
	}

}
