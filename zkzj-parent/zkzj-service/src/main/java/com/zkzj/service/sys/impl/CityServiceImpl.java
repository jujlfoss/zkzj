package com.zkzj.service.sys.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zkzj.dao.sys.CityDao;
import com.zkzj.entity.enums.StatusEnum;
import com.zkzj.entity.sys.City;
import com.zkzj.pojo.query.sys.CityQuery;
import com.zkzj.pojo.vos.common.PageVo;
import com.zkzj.service.base.impl.BaseServiceImpl;
import com.zkzj.service.sys.CityService;
import com.zkzj.utils.DateUtil;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service("cityService")
public class CityServiceImpl extends BaseServiceImpl<City> implements CityService {

	@Autowired
	private CityDao cityDao;

	@Override
	public PageVo pageCity(CityQuery cityQuery, Integer start, Integer limit) {
		return new PageVo(start, limit, this.cityDao.countCity(cityQuery), this.cityDao.listCity(cityQuery, start, limit));
	}

	@Override
	public void saveCity(City city) {
		long time = DateUtil.getTime();
		city.setCreateTime(time);
		city.setModifyTime(time);
		city.setStatus(StatusEnum.NORMAL.getValue());
		this.cityDao.save(city);
	}
	
	@Override
	public void updateCity(City city) {
		city.setModifyTime(DateUtil.getTime());
		this.cityDao.updateByPrimaryKeySelective(city);
	}

	@Override
	public void removeCity(long id) {
		try {
			this.cityDao.removeByPrimaryKey(id);
		} catch (Exception e) {
			log.info(e.getMessage(),e);
			City city = new City();
			city.setId(id);
			city.setModifyTime(DateUtil.getTime());
			city.setStatus(StatusEnum.DELETE.getValue());
			this.cityDao.updateByPrimaryKeySelective(city);
		}
	}

	@Override
	public void recover(long id) {
		City city = new City();
		city.setId(id);
		city.setModifyTime(DateUtil.getTime());
		city.setStatus(StatusEnum.NORMAL.getValue());
		this.cityDao.updateByPrimaryKeySelective(city);
	}

	@Override
	public List<City> getList(Long id) {
		Example ex = new Example(City.class);
		Example.Criteria cr = ex.createCriteria();
		cr.andEqualTo("parentId", id);
		List<City> list = cityDao.listByExample(ex);
		return list;
	}

}
