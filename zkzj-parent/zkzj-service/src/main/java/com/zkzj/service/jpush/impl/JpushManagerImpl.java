package com.zkzj.service.jpush.impl;

import java.io.Serializable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.zkzj.entity.enums.StatusEnum;
import com.zkzj.service.jpush.JpushManager;
import com.zkzj.utils.DateUtil;

import cn.jiguang.common.ClientConfig;
import cn.jiguang.common.resp.APIConnectionException;
import cn.jiguang.common.resp.APIRequestException;
import cn.jpush.api.JPushClient;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.Message;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;

@Component("jpushManager")
public class JpushManagerImpl implements JpushManager {

	private static final Logger LOG = LogManager.getLogger(JpushManager.class);

	private static final String MASTER_SECRET_EMPLOYEE = "384a1d1ed1257acafdbac580";

	private static final String APP_KEY_EMPLOYEE = "5233e1c73ecef507f495c912";

	private static final String MASTER_SECRET_CUSTOMER = "f601c23557ebb36830e49bdd";

	private static final String APP_KEY_CUSTOMER = "0d63eebfefe597acb51cb79c";

//	@Autowired
//	private MessageService messageService;

	private void sendMsg(String mobile, String title, String msg, Integer type, String masterSecret, String appKey) {
//		com.zkzj.entity.eb.Message message = new com.zkzj.entity.eb.Message(title, msg, mobile,
//				DateUtil.getTime(), StatusEnum.NORMAL.getValue(), false, false, type);
//		this.messageService.save(message);
//		try {
//			JPushClient jpushClient = new JPushClient(masterSecret, appKey, null, ClientConfig.getInstance());
//			PushPayload payload = PushPayload.newBuilder().setPlatform(Platform.all())
//					.setAudience(Audience.alias(mobile))
//					.setMessage(
//							Message.content(JSONObject.toJSONString(new JpushVo(message.getId(), msg, title, type))))
//					.build();
//			PushResult result = jpushClient.sendPush(payload);
//			LOG.debug("Got result - " + result);
//		} catch (APIConnectionException e) {
//			LOG.error("Connection error, should retry later", e);
//		} catch (APIRequestException e) {
//			LOG.error("Should review the error, and fix the request", e);
//			LOG.info("HTTP Status: " + e.getStatus());
//			LOG.info("Error Code: " + e.getErrorCode());
//			LOG.info("Error Message: " + e.getErrorMessage());
//		}
	}

	@Override
	public void sendEmployeeMsg(String mobile, String title, String msg, Integer type) {
		this.sendMsg(mobile, title, msg, type, MASTER_SECRET_EMPLOYEE, APP_KEY_EMPLOYEE);
	}

	@Override
	public void sendCustomerMsg(String mobile, String title, String msg, Integer type) {
		this.sendMsg(mobile, title, msg, type, MASTER_SECRET_CUSTOMER, APP_KEY_CUSTOMER);
	}

	@SuppressWarnings("unused")
	private static class JpushVo implements Serializable {

		private static final long serialVersionUID = 6044165112088698834L;

		private Long id;
		private String msg;
		private Integer type;
		private String title;

		public JpushVo(Long id, String msg, String title, Integer type) {
			this.id = id;
			this.msg = msg;
			this.title = title;
			this.type = type;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getMsg() {
			return msg;
		}

		public void setMsg(String msg) {
			this.msg = msg;
		}

		public Integer getType() {
			return type;
		}

		public void setType(Integer type) {
			this.type = type;
		}

	}
}
