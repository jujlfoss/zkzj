package com.zkzj.service.sys;

import com.zkzj.entity.sys.Merchants;
import com.zkzj.pojo.query.sys.MerchantsQuery;
import com.zkzj.pojo.vos.common.PageVo;
import com.zkzj.service.base.BaseService;

public interface MerchantsService extends BaseService<Merchants> {

	/**
	 * 分页
	 * 
	 * @param merchants
	 * @param start
	 * @param limit
	 * @return
	 */
	PageVo pageMerchants(MerchantsQuery merchantsQuery, Integer start, Integer limit);

	/**
	 * 新增
	 * 
	 * @param merchants
	 */
	void saveMerchants(Merchants merchants);
	
	/**
	 * 更新
	 *
	 * @param merchants
	 */
	void updateMerchants(Merchants merchants);

	/**
	 * 删除
	 * 
	 * @param id
	 */
	void removeMerchants(long id);

	/**
	 * 恢复
	 * 
	 * @param id
	 */
	void recover(long id);

}
