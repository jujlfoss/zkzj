package com.zkzj.service.crm;

import org.apache.ibatis.annotations.Param;

import com.zkzj.entity.crm.Customer;
import com.zkzj.entity.sys.User;
import com.zkzj.pojo.query.crm.CustomerQuery;
import com.zkzj.pojo.vos.common.PageVo;
import com.zkzj.service.base.BaseService;

public interface CustomerService extends BaseService<Customer> {

	/**
	 * 分页
	 * 
	 * @param customer
	 * @param start
	 * @param limit
	 * @return
	 */
	PageVo pageCustomer(CustomerQuery customerQuery, int start, int limit);

	/**
	 * 新增
	 * 
	 * @param customer
	 */
	void saveCustomer(Customer customer);

	/**
	 * 更新
	 *
	 * @param customer
	 */
	void updateCustomer(Customer customer);

	/**
	 * 删除
	 * 
	 * @param id
	 */
	void removeCustomer(long id);

	/**
	 * 恢复
	 * 
	 * @param id
	 */
	void recover(long id);

	/**
	 * 获取客户名
	 * 
	 * @param id
	 * @return
	 */
	String getCustomerName(Long id);

	/**
	 * 保存客户账户
	 * 
	 */
	void saveCustomerUser(User user, String referrerMobile, String inviteMobile);

	/**
	 * 获取客户通过userId
	 * 
	 * @param userId
	 * @return
	 */
	Customer getCustomerByUserId(@Param("userId") long userId);

	/**
	 * 
	 * @param newPsw
	 *            新密码
	 * @param pswRe
	 *            重新输入密码
	 */
	void updatePayPsw(Long id, String newPsw, String pswRe, String mobileCode);

	/**
	 * 难是否设置支付密码
	 * 
	 * @param id
	 * @return
	 */
	boolean whetherSetPayPsw(Long id);

	boolean isCheckPass(Long customerId, String moilde, String payPsw, String code);

	Customer webGetCustomer(Long id);

	Customer getCurrentCustomerInfo(long id);

}
