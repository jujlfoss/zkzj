package com.zkzj.service.hrm.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zkzj.dao.hrm.EmployeeDao;
import com.zkzj.dao.sys.UserDao;
import com.zkzj.entity.enums.StatusEnum;
import com.zkzj.entity.hrm.Employee;
import com.zkzj.entity.sys.User;
import com.zkzj.pojo.exceptions.ExceptionEnum;
import com.zkzj.pojo.exceptions.ServiceException;
import com.zkzj.pojo.query.hrm.EmployeeQuery;
import com.zkzj.pojo.vos.common.PageVo;
import com.zkzj.service.base.impl.BaseServiceImpl;
import com.zkzj.service.hrm.EmployeeService;
import com.zkzj.service.sys.UserService;
import com.zkzj.utils.DateUtil;

@Service("employeeService")
public class EmployeeServiceImpl extends BaseServiceImpl<Employee> implements EmployeeService {

	@Autowired
	private EmployeeDao employeeDao;
	@Autowired
	private UserService userService;
	@Autowired
	private UserDao userDao;

	@Override
	public PageVo pageEmployee(EmployeeQuery employeeQuery, int start, int limit) {
		return new PageVo(start, limit, this.employeeDao.countEmployee(employeeQuery),
				this.employeeDao.listEmployee(employeeQuery, start, limit));
	}

	@Override
	public Employee getEmployeeByUserId(long userId) {
		Employee search = new Employee();
		search.setUserId(userId);
		return this.employeeDao.get(search);
	}

	@Override
	public void saveEmployee(Employee employee) {
		long time = DateUtil.getTime();
		employee.setCreateTime(time);
		employee.setModifyTime(time);
		employee.setStatus(StatusEnum.NORMAL.getValue());
		this.employeeDao.save(employee);
	}

	@Override
	public void updateEmployee(Employee employee) {
		employee.setModifyTime(DateUtil.getTime());
		Long userId = this.employeeDao.getByPrimaryKey(employee.getId()).getUserId();
		if (userId != null) {
			User user = this.userService.getByPrimaryKey(userId);
			user.setMobile(employee.getMobile());
			this.userDao.updateByPrimaryKey(user);
		}
		this.employeeDao.updateByPrimaryKeySelective(employee);

	}

	@Override
	public void removeEmployee(long id) {
		try {
			this.employeeDao.removeByPrimaryKey(id);
			this.userService.removeUser(this.employeeDao.getByPrimaryKey(id).getUserId());
		} catch (Exception e) {
			log.info(e.getMessage(), e);
			Employee employee = new Employee();
			employee.setId(id);
			employee.setModifyTime(DateUtil.getTime());
			employee.setStatus(StatusEnum.DELETE.getValue());
			this.employeeDao.updateByPrimaryKeySelective(employee);
		}
	}

	@Override
	public void recover(long id) {
		Employee employee = new Employee();
		employee.setId(id);
		employee.setModifyTime(DateUtil.getTime());
		employee.setStatus(StatusEnum.NORMAL.getValue());
		this.employeeDao.updateByPrimaryKeySelective(employee);
	}

	@Override
	public void saveEmployeeUser(User user) {
		this.userService.saveUser(user);
		Employee employee = this.employeeDao.getByPrimaryKey(user.getEmployeeId());
		if (employee.getUserId() != null) {
			throw new ServiceException(ExceptionEnum.EMPLOYEE_USER_NAME_EXISTED);
		}
		employee.setUserId(user.getId());
		employee.setModifyTime(DateUtil.getTime());
		this.employeeDao.updateByPrimaryKey(employee);
	}

	@Override
	public void freeze(long id) {
		Employee employee = new Employee();
		employee.setId(id);
		employee.setModifyTime(DateUtil.getTime());
		employee.setStatus(StatusEnum.FREEZE.getValue());
		this.employeeDao.updateByPrimaryKeySelective(employee);
		Long userId = this.employeeDao.getByPrimaryKey(id).getUserId();
		if (userId != null) {
			this.userService.freeze(userId);
		}
	}

	@Override
	public void unfreeze(long id) {
		Employee employee = new Employee();
		employee.setId(id);
		employee.setModifyTime(DateUtil.getTime());
		employee.setStatus(StatusEnum.NORMAL.getValue());
		this.employeeDao.updateByPrimaryKeySelective(employee);
		Long userId = this.employeeDao.getByPrimaryKey(id).getUserId();
		if (userId != null) {
			this.userService.unfreeze(userId);
		}
	}

}
