package com.zkzj.dao.sys;

import java.util.Set;

import org.apache.ibatis.annotations.Param;

import com.zkzj.dao.base.BaseDao;
import com.zkzj.entity.sys.UserRole;

/**
 * UserRoleDao
 * 
 * @author liu
 * @date 2016年9月1日
 *
 */
public interface UserRoleDao extends BaseDao<UserRole> {

	void saveUserRoleList(@Param("userId") long userId, @Param("roleIds") Set<Long> roleIds);

}