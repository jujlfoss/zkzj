package com.zkzj.dao.crm;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.zkzj.dao.base.BaseDao;
import com.zkzj.entity.crm.Customer;
import com.zkzj.pojo.query.crm.CustomerQuery;

public interface CustomerDao extends BaseDao<Customer> {

	List<Customer> listCustomer(@Param("customerQuery") CustomerQuery customerQuery, @Param("start") Integer start,
                                @Param("limit") Integer limit);

	Long countCustomer(@Param("customerQuery") CustomerQuery customerQuery);

	Customer getCustomerByUserId(@Param("userId") long userId);

	Customer getCurrentCustomerInfo(@Param("id") long id);

}