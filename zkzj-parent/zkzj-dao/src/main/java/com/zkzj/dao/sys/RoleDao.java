package com.zkzj.dao.sys;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.zkzj.dao.base.BaseDao;
import com.zkzj.entity.sys.Role;

/**
 * 
 * @author liu
 * @date 2016年8月6日
 *
 */
public interface RoleDao extends BaseDao<Role> {

	List<Role> listByUserId(@Param("userId") long userId);

	List<Long> listIdByUserId(@Param("userId") long userId);

}
