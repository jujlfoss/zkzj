package com.zkzj.dao.sys;

import java.util.List;
import org.apache.ibatis.annotations.Param;

import com.zkzj.dao.base.BaseDao;
import com.zkzj.entity.sys.MerchantsType;
import com.zkzj.pojo.query.sys.MerchantsTypeQuery;

public interface MerchantsTypeDao extends BaseDao<MerchantsType> {

	List<MerchantsType> listMerchantsType(@Param("merchantsTypeQuery") MerchantsTypeQuery merchantsTypeQuery, @Param("start") Integer start, @Param("limit") Integer limit);

	long countMerchantsType(@Param("merchantsTypeQuery") MerchantsTypeQuery merchantsTypeQuery);

}