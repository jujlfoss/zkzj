package com.zkzj.dao.sys;

import java.util.List;
import org.apache.ibatis.annotations.Param;

import com.zkzj.dao.base.BaseDao;
import com.zkzj.entity.sys.City;
import com.zkzj.pojo.query.sys.CityQuery;

public interface CityDao extends BaseDao<City> {

	List<City> listCity(@Param("cityQuery") CityQuery cityQuery, @Param("start") Integer start, @Param("limit") Integer limit);

	long countCity(@Param("cityQuery") CityQuery cityQuery);

}