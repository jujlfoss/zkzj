package com.zkzj.dao.sys;

import java.util.List;
import org.apache.ibatis.annotations.Param;

import com.zkzj.dao.base.BaseDao;
import com.zkzj.entity.sys.Merchants;
import com.zkzj.pojo.query.sys.MerchantsQuery;

public interface MerchantsDao extends BaseDao<Merchants> {

	List<Merchants> listMerchants(@Param("merchantsQuery") MerchantsQuery merchantsQuery, @Param("start") Integer start, @Param("limit") Integer limit);

	long countMerchants(@Param("merchantsQuery") MerchantsQuery merchantsQuery);

}