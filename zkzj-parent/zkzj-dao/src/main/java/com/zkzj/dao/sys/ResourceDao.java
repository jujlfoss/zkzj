package com.zkzj.dao.sys;

import java.util.List;
import java.util.Set;

import org.apache.ibatis.annotations.Param;

import com.zkzj.dao.base.BaseDao;
import com.zkzj.entity.sys.Resource;
import com.zkzj.pojo.vos.sys.PermitVo;

/**
 * ResourceDao
 * 
 * @author liu
 * @date 2016年8月21日
 */
public interface ResourceDao extends BaseDao<Resource> {

	List<Resource> listByRoleId(@Param("roleId") long roleId);

	List<Long> listIdByRoleId(@Param("roleId") long roleId);

	List<PermitVo> listPermit(@Param("permissions") Set<String> permissions);

}
