package com.zkzj.dao.sys;

import java.util.Set;

import org.apache.ibatis.annotations.Param;

import com.zkzj.dao.base.BaseDao;
import com.zkzj.entity.sys.RoleResource;


/**
 * RoleResourceDao
 * 
 * @author liu
 * @date 2016年8月30日
 *
 */
public interface RoleResourceDao extends BaseDao<RoleResource> {

	void saveRoleResourceList(@Param("roleId") long roleId, @Param("resourceIds") Set<Long> resourceIds);

}