package com.zkzj.dao.sys;

import java.util.List;
import org.apache.ibatis.annotations.Param;

import com.zkzj.dao.base.BaseDao;
import com.zkzj.entity.sys.Area;
import com.zkzj.pojo.query.sys.AreaQuery;

public interface AreaDao extends BaseDao<Area> {

	List<Area> listArea(@Param("areaQuery") AreaQuery areaQuery, @Param("start") Integer start, @Param("limit") Integer limit);

	long countArea(@Param("areaQuery") AreaQuery areaQuery);

	List<Area> areaTree();

}