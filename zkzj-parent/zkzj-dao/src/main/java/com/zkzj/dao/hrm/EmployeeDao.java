package com.zkzj.dao.hrm;

import java.util.List;
import java.util.Set;

import org.apache.ibatis.annotations.Param;

import com.zkzj.dao.base.BaseDao;
import com.zkzj.entity.hrm.Employee;
import com.zkzj.pojo.query.hrm.EmployeeQuery;

public interface EmployeeDao extends BaseDao<Employee> {

	List<Employee> listEmployee(@Param("employeeQuery") EmployeeQuery employeeQuery, @Param("start") Integer start,
                                @Param("limit") Integer limit);

	Long countEmployee(@Param("employeeQuery") EmployeeQuery employeeQuery);



}