package com.zkzj.dao.hrm;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.zkzj.dao.base.BaseDao;
import com.zkzj.entity.hrm.Team;

public interface TeamDao extends BaseDao<Team> {

	List<Team> listTeam(@Param("team") Team team, @Param("start") Integer start, @Param("limit") Integer limit);

	Long countTeam(@Param("team") Team team);

	Long getTeamIdByUserId(@Param("userId") long userId);

	Team getTeamByUserId(@Param("userId") long userId);

}