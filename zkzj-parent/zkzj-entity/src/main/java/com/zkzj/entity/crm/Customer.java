package com.zkzj.entity.crm;

import javax.persistence.Table;
import javax.persistence.Transient;

import com.zkzj.entity.base.BaseEntity;
import com.zkzj.pojo.annotation.Comment;

@Table(name = "crm_customer")
public class Customer extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Comment("用户Id")
	private Long userId;
	@Comment("昵称")
	private String customerName;
	@Comment("头像路径")
	private String avatarUrl;
	@Comment("推荐人手机号")
	private String referrerMobile;
	@Comment("邀请人手机号")
	private String inviteMobile;

	@Transient
	@Comment("手机号码")
	private String mobile;
	// 支付密码
	@Comment("支付密码")
	private String payPsw;

	public String getInviteMobile() {
		return inviteMobile;
	}

	public void setInviteMobile(String inviteMobile) {
		this.inviteMobile = inviteMobile;
	}

	public Customer() {
		super();
	}

	public Customer(Long userId) {
		super();
		this.userId = userId;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public String getReferrerMobile() {
		return referrerMobile;
	}

	public void setReferrerMobile(String referrerMobile) {
		this.referrerMobile = referrerMobile;
	}

	public String getPayPsw() {
		return payPsw;
	}

	public void setPayPsw(String payPsw) {
		this.payPsw = payPsw;
	}

}