package com.zkzj.entity.sys;

import javax.persistence.Table;
import com.zkzj.entity.base.BaseEntity;
import com.zkzj.entity.base.IdEntity;
import com.zkzj.pojo.annotation.Comment;
import com.zkzj.entity.base.BaseEntity;
import com.zkzj.pojo.annotation.Comment;

@Table(name = "sys_merchants_type")
public class MerchantsType extends IdEntity {

	private static final long serialVersionUID = 1L;

	@Comment("类型名")
	private String typeName;

	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

}