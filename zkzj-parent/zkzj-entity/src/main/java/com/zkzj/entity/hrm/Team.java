package com.zkzj.entity.hrm;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Table;
import javax.persistence.Transient;

import com.zkzj.entity.base.BaseEntity;
import com.zkzj.pojo.annotation.Comment;

/**
 * 团队
 * 
 * @author liu
 *
 */
@Table(name = "hrm_team")
public class Team extends BaseEntity {

	private static final long serialVersionUID = 5003315558985259191L;

	@Comment("团队名")
	private String teamName;
	@Comment("备注")
	private String remark;
	@Comment("父Id")
	private Long parentId;
	@Comment("排序值")
	private Double sort;

	@Transient
	@Comment("是否展开")
	private Boolean expanded;
	@Transient
	@Comment("是否选中")
	private Boolean checked;
	@Transient
	@Comment("子节点")
	private Set<Team> children = new HashSet<Team>();

	public Boolean getExpanded() {
		return expanded;
	}

	public void setExpanded(Boolean expanded) {
		this.expanded = expanded;
	}

	public Double getSort() {
		return sort;
	}

	public void setSort(Double sort) {
		this.sort = sort;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Boolean getChecked() {
		return checked;
	}

	public void setChecked(Boolean checked) {
		this.checked = checked;
	}

	public Set<Team> getChildren() {
		return children;
	}

	public void setChildren(Set<Team> children) {
		this.children = children;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}