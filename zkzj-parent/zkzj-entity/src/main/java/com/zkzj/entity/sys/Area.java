package com.zkzj.entity.sys;

import javax.persistence.Table;
import javax.persistence.Transient;

import com.zkzj.entity.base.BaseEntity;
import com.zkzj.pojo.annotation.Comment;

@Table(name = "sys_area")
public class Area extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Comment("区域")
	private String areaName;
	@Comment("上级ID")
	private Long parentId;
	@Comment("所属县")
	private Long countyId;
	@Transient
	private String countyName;
	@Transient
	private String cityName;
	@Transient
	private String pName;
	@Transient
	private Long pId;
	@Transient
	private Long cityId;
	@Transient
	private String superName;

	public String getSuperName() {
		return superName;
	}

	public void setSuperName(String superName) {
		this.superName = superName;
	}

	public Long getpId() {
		return pId;
	}

	public void setpId(Long pId) {
		this.pId = pId;
	}

	public Long getCityId() {
		return cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	public String getCountyName() {
		return countyName;
	}

	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getpName() {
		return pName;
	}

	public void setpName(String pName) {
		this.pName = pName;
	}

	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public Long getCountyId() {
		return countyId;
	}
	public void setCountyId(Long countyId) {
		this.countyId = countyId;
	}
	

}