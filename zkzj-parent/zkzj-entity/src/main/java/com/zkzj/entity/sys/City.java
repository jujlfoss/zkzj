package com.zkzj.entity.sys;

import javax.persistence.Table;
import com.zkzj.entity.base.BaseEntity;
import com.zkzj.pojo.annotation.Comment;
import java.math.BigDecimal;
import java.math.BigDecimal;
@Table(name = "sys_city")
public class City extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Comment("")
	private String areaName;
	@Comment("")
	private Integer areaType;
	@Comment("")
	private Long parentId;
	@Comment("")
	private String baiduCode;
	@Comment("")
	private String cityCode;
	@Comment("")
	private String addressCode;
	@Comment("")
	private BigDecimal latitude;
	@Comment("")
	private BigDecimal longitude;

	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public Integer getAreaType() {
		return areaType;
	}
	public void setAreaType(Integer areaType) {
		this.areaType = areaType;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public String getBaiduCode() {
		return baiduCode;
	}
	public void setBaiduCode(String baiduCode) {
		this.baiduCode = baiduCode;
	}
	public String getCityCode() {
		return cityCode;
	}
	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	public String getAddressCode() {
		return addressCode;
	}
	public void setAddressCode(String addressCode) {
		this.addressCode = addressCode;
	}
	public BigDecimal getLatitude() {
		return latitude;
	}
	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}
	public BigDecimal getLongitude() {
		return longitude;
	}
	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

}