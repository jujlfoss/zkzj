package com.zkzj.entity.sys;

import javax.persistence.Table;
import javax.persistence.Transient;

import com.zkzj.entity.base.BaseEntity;
import com.zkzj.pojo.annotation.Comment;

@Table(name = "sys_merchants")
public class Merchants extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Comment("商户名")
	private String name;
	@Comment("商户类型")
	private Long merchantsTypeId;
	private Long areaId;
	@Transient
	private String areaName;
	private Integer status;
	@Transient
	private String typeName;
	@Transient
	private String countyName;
	@Transient
	private String cityName;
	@Transient
	private String pName;
	@Transient
	private Long pId;
	@Transient
	private Long cityId;
	@Transient
	private Long countyId;

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getCountyName() {
		return countyName;
	}

	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getpName() {
		return pName;
	}

	public void setpName(String pName) {
		this.pName = pName;
	}

	public Long getpId() {
		return pId;
	}

	public void setpId(Long pId) {
		this.pId = pId;
	}

	public Long getCityId() {
		return cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	public Long getCountyId() {
		return countyId;
	}

	public void setCountyId(Long countyId) {
		this.countyId = countyId;
	}

	@Override
	public Integer getStatus() {
		return status;
	}

	@Override
	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public Long getAreaId() {
		return areaId;
	}

	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getMerchantsTypeId() {
		return merchantsTypeId;
	}
	public void setMerchantsTypeId(Long merchantsTypeId) {
		this.merchantsTypeId = merchantsTypeId;
	}
	

}