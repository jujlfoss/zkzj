package com.zkzj.entity.enums.sys;

/**
 * 用户状态枚举
 * 
 * @author liu
 *
 */
public enum UserTypeEnum {

	/** 员工 0 **/
	EMPLOYEE(0, "员工"),
	/** 客户 1 **/
	CUSTOMER(1, "客户");

	private final String text;
	private final int value;

	private UserTypeEnum(int value, String text) {
		this.value = value;
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public int getValue() {
		return value;
	}

}
