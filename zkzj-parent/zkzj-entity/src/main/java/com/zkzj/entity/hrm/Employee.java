package com.zkzj.entity.hrm;

import javax.persistence.Table;
import javax.persistence.Transient;

import com.zkzj.entity.base.BaseEntity;
import com.zkzj.pojo.annotation.Comment;

@Table(name = "hrm_employee")
public class Employee extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Comment("用户ID")
	private Long userId;
	@Comment("员工姓名")
	private String employeeName;
	@Comment("性别")
	private Integer sex;
	@Comment("籍贯")
	private String nativePlace;
	@Comment("地址")
	private String address;
	@Comment("手机号码")
	private String mobile;
	@Comment("商户")
	private Long merchantsId;
	@Transient
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getMerchantsId() {
		return merchantsId;
	}

	public void setMerchantsId(Long merchantsId) {
		this.merchantsId = merchantsId;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getNativePlace() {
		return nativePlace;
	}

	public void setNativePlace(String nativePlace) {
		this.nativePlace = nativePlace;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}