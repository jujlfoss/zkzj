package com.zkzj.manager.auth;

import java.io.IOException;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authz.AuthorizationFilter;
import org.apache.shiro.web.util.WebUtils;

import com.zkzj.pojo.constants.Constant;
import com.zkzj.utils.AjaxUtil;
import com.zkzj.utils.SessionUtil;

public class WebAuthorizationFilter extends AuthorizationFilter {

	@Override
	protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue)
			throws IOException {
		Subject subject = super.getSubject(request, response);
		String servletPath = WebUtils.toHttp(request).getServletPath().substring(1);
		if (subject.isAuthenticated() || subject.isRemembered()) {
			if (subject.hasRole(Constant.ADMIN) || subject.isPermitted(servletPath)
					|| servletPath.indexOf(Constant.HTML) != -1) {
				return true;
			}
		}
		return false;
	}

	@Override
	protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws IOException {
		Subject subject = super.getSubject(request, response);
		if (subject.isAuthenticated() || subject.isRemembered()) {
			HttpServletResponse hResponse = WebUtils.toHttp(response);
			if (AjaxUtil.isAjax(WebUtils.toHttp(request))) {
				hResponse.setHeader(Constant.REQUESTURL, WebUtils.toHttp(request).getServletPath().substring(1));
				hResponse.setStatus(Constant.AUTH_ERROR_STATUS);
			} else {
				hResponse.sendError(Constant.AUTH_ERROR_STATUS);
			}
		} else {
			if (AjaxUtil.isAjax(WebUtils.toHttp(request))) {
				HttpServletResponse hResponse = WebUtils.toHttp(response);
				hResponse.setHeader(Constant.REFERER, WebUtils.toHttp(request).getHeader(Constant.REFERER));
				hResponse.setStatus(Constant.SESSION_ERROR_STATUS);
			} else {
				SessionUtil.setSessionAttribute(Constant.REQUESTURL, WebUtils.toHttp(request).getServletPath());
				super.saveRequestAndRedirectToLogin(request, response);
			}
		}
		return false;
	}

}
