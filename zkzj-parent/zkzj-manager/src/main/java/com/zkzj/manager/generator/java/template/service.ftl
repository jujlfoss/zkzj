package com.zkzj.service.${moduleName};

import com.zkzj.entity.${moduleName}.${entityName};
import com.zkzj.pojo.query.${moduleName}.${entityName}Query;
import com.zkzj.pojo.vos.common.PageVo;
import com.zkzj.service.base.BaseService;

public interface ${entityName}Service extends BaseService<${entityName}> {

	/**
	 * 分页
	 * 
	 * @param ${packageName}
	 * @param start
	 * @param limit
	 * @return
	 */
	PageVo page${entityName}(${entityName}Query ${packageName}Query, Integer start, Integer limit);

	/**
	 * 新增
	 * 
	 * @param ${packageName}
	 */
	void save${entityName}(${entityName} ${packageName});
	
	/**
	 * 更新
	 *
	 * @param ${packageName}
	 */
	void update${entityName}(${entityName} ${packageName});

	/**
	 * 删除
	 * 
	 * @param id
	 */
	void remove${entityName}(long id);

	/**
	 * 恢复
	 * 
	 * @param id
	 */
	void recover(long id);

}
