package com.zkzj.service.${moduleName}.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zkzj.dao.${moduleName}.${entityName}Dao;
import com.zkzj.entity.enums.StatusEnum;
import com.zkzj.entity.${moduleName}.${entityName};
import com.zkzj.pojo.query.${moduleName}.${entityName}Query;
import com.zkzj.pojo.vos.common.PageVo;
import com.zkzj.service.base.impl.BaseServiceImpl;
import com.zkzj.service.${moduleName}.${entityName}Service;
import com.zkzj.utils.DateUtil;

@Service("${packageName}Service")
public class ${entityName}ServiceImpl extends BaseServiceImpl<${entityName}> implements ${entityName}Service {

	@Autowired
	private ${entityName}Dao ${packageName}Dao;

	@Override
	public PageVo page${entityName}(${entityName}Query ${packageName}Query, Integer start, Integer limit) {
		return new PageVo(start, limit, this.${packageName}Dao.count${entityName}(${packageName}Query), this.${packageName}Dao.list${entityName}(${packageName}Query, start, limit));
	}

	@Override
	public void save${entityName}(${entityName} ${packageName}) {
		long time = DateUtil.getTime();
		${packageName}.setCreateTime(time);
		${packageName}.setModifyTime(time);
		${packageName}.setStatus(StatusEnum.NORMAL.getValue());
		this.${packageName}Dao.save(${packageName});
	}
	
	@Override
	public void update${entityName}(${entityName} ${packageName}) {
		${packageName}.setModifyTime(DateUtil.getTime());
		this.${packageName}Dao.updateByPrimaryKeySelective(${packageName});
	}

	@Override
	public void remove${entityName}(long id) {
		try {
			this.${packageName}Dao.removeByPrimaryKey(id);
		} catch (Exception e) {
			log.info(e.getMessage(),e);
			${entityName} ${packageName} = new ${entityName}();
			${packageName}.setId(id);
			${packageName}.setModifyTime(DateUtil.getTime());
			${packageName}.setStatus(StatusEnum.DELETE.getValue());
			this.${packageName}Dao.updateByPrimaryKeySelective(${packageName});
		}
	}

	@Override
	public void recover(long id) {
		${entityName} ${packageName} = new ${entityName}();
		${packageName}.setId(id);
		${packageName}.setModifyTime(DateUtil.getTime());
		${packageName}.setStatus(StatusEnum.NORMAL.getValue());
		this.${packageName}Dao.updateByPrimaryKeySelective(${packageName});
	}

}
