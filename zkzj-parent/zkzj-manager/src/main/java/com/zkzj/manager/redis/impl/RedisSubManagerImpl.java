package com.zkzj.manager.redis.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.zkzj.dao.redis.RedisRepository;
import com.zkzj.manager.redis.RedisSubManager;
import com.zkzj.pojo.constants.Constant;
import com.zkzj.pojo.vos.common.MsgTypeEnum;

import com.zkzj.service.jpush.JpushManager;
import com.zkzj.service.sys.UserService;

public class RedisSubManagerImpl implements RedisSubManager {

	private static final Logger LOG = LogManager.getLogger(RedisRepository.class);

	private static final String SPLIT = ":";



	@Autowired
	private UserService userService;
	@Autowired
	private JpushManager jpushManager;


	@Override
	public void handleMessage(String message) {

	}

	// @Override
	// public void handleMessage(Map<?, ?> message) {
	// log.info("handleMessage2:" + message);
	// }
	//
	// @Override
	// public void handleMessage(byte[] message) {
	// log.info("handleMessage3:" + message);
	// }
	//
	// @Override
	// public void handleMessage(Serializable message) {
	// log.info("handleMessage4:" + message);
	// }
	//
	// @Override
	// public void handleMessage(Serializable message, String channel) {
	// log.info("handleMessage5:" + message + channel);
	// }

}
