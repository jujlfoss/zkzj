package com.zkzj.manager.cache.shiro;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.UnknownSessionException;
import org.apache.shiro.session.mgt.eis.AbstractSessionDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zkzj.dao.redis.RedisRepository;

/**
 * @author liu
 *
 */
public class ShiroRedisSessionDao extends AbstractSessionDAO {

	private static final Logger LOG = LoggerFactory.getLogger(ShiroRedisSessionDao.class);

	private RedisRepository redisRepository;

	private String keyPrefix = "shiro_session:";

	private String getRedisKey(Serializable sessionId) {
		return new StringBuffer(this.keyPrefix).append(sessionId).toString();
	}

	@Override
	public void update(Session session) throws UnknownSessionException {
		this.saveSession(session);
	}

	/**
	 * save session
	 * 
	 * @param session
	 * @throws UnknownSessionException
	 */
	private void saveSession(Session session) throws UnknownSessionException {
		if (session == null || session.getId() == null) {
			LOG.error("session or session id is null");
			return;
		}
		Long expireTime = this.redisRepository.getDefaultExpireTime();
		session.setTimeout(expireTime * 1000);
		redisRepository.set(this.getRedisKey(session.getId()), session, expireTime);
	}

	@Override
	public void delete(Session session) {
		if (session == null || session.getId() == null) {
			LOG.error("session or session id is null");
			return;
		}
		redisRepository.delete(this.getRedisKey(session.getId()));
	}

	@Override
	public Collection<Session> getActiveSessions() {
		Set<Serializable> keys = this.redisRepository.keys(this.keyPrefix + "*");
		if (CollectionUtils.isNotEmpty(keys)) {
			Set<Session> sessions = new HashSet<Session>();
			for (Serializable key : keys) {
				Session session = (Session) this.redisRepository.get(key);
				sessions.add(session);
			}
			return sessions;
		}
		return Collections.emptySet();
	}

	@Override
	protected Serializable doCreate(Session session) {
		Serializable sessionId = this.generateSessionId(session);
		this.assignSessionId(session, sessionId);
		this.saveSession(session);
		return sessionId;
	}

	@Override
	protected Session doReadSession(Serializable sessionId) {
		if (sessionId == null) {
			LOG.error("session id is null");
			return null;
		}
		Session session = (Session) this.redisRepository.get(this.getRedisKey(sessionId));
		return session;
	}

	public String getKeyPrefix() {
		return keyPrefix;
	}

	public void setKeyPrefix(String keyPrefix) {
		this.keyPrefix = keyPrefix;
	}

	public RedisRepository getredisRepository() {
		return redisRepository;
	}

	public void setredisRepository(RedisRepository redisRepository) {
		this.redisRepository = redisRepository;
	}

}
