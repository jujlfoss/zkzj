package com.zkzj.dao.${moduleName};

import java.util.List;
import org.apache.ibatis.annotations.Param;

import com.zkzj.dao.base.BaseDao;
import com.zkzj.entity.${moduleName}.${entityName};
import com.zkzj.pojo.query.${moduleName}.${entityName}Query;

public interface ${entityName}Dao extends BaseDao<${entityName}> {

	List<${entityName}> list${entityName}(@Param("${packageName}Query") ${entityName}Query ${packageName}Query, @Param("start") Integer start, @Param("limit") Integer limit);

	long count${entityName}(@Param("${packageName}Query") ${entityName}Query ${packageName}Query);

}