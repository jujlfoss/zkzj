package com.dy.pojo.query.${moduleName};

import com.zkzj.pojo.query.base.BaseQuery;
${imports}
public class ${entityName}Query extends BaseQuery {

	private static final long serialVersionUID = 1L;

	${qfields}

	${getSets}

}