package com.zkzj.web.controller.${moduleName};

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zkzj.entity.${moduleName}.${entityName};
import com.zkzj.pojo.query.${moduleName}.${entityName}Query;
import com.zkzj.pojo.vos.common.PageVo;
import com.zkzj.pojo.vos.common.ResultVo;
import com.zkzj.service.${moduleName}.${entityName}Service;
import com.zkzj.web.controller.base.BaseController;
import com.zkzj.web.utils.ResultUtil;

@RequestMapping("${moduleName}/${packageName}")
@Controller
public class ${entityName}Controller extends BaseController {

	@Autowired
	private ${entityName}Service ${packageName}Service;

	/**
	 * 分页
 	 * 
 	 * @param ${packageName}Query
	 * @param start
	 * @param limit
	 * @return
	 */
	@RequestMapping("page${entityName}")
	@ResponseBody
	public PageVo page${entityName}(${entityName}Query ${packageName}Query, Integer start, Integer limit) {
		return this.${packageName}Service.page${entityName}(${packageName}Query, start, limit);
	}

	/**
	 * 新增
	 * 
	 * @param ${packageName}
	 * @return
	 */
	@RequestMapping(value = "save${entityName}", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo save${entityName}(${entityName} ${packageName}) {
		try {
			this.${packageName}Service.save${entityName}(${packageName});
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}
	
	/**
	 * 更新
	 * 
	 * @param ${packageName}
	 * @return
	 */
	@RequestMapping(value = "update${entityName}", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo update${entityName}(${entityName} ${packageName}) {
		try {
			this.${packageName}Service.update${entityName}(${packageName});
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 删除
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "remove${entityName}", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo remove${entityName}(long id) {
		try {
			this.${packageName}Service.remove${entityName}(id);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 恢复
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "recover", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo recover(long id) {
		try {
			this.${packageName}Service.recover(id);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

}