Ext.define('App.view.${packageName}.${entityName}Window', {
	extend : 'App.config.BaseWindow',
	requires : [ 'App.controller.${packageName}.${entityName}WindowController'],
	controller : '${packageName}WindowController',
	xtype : '${packageName}Window',
	id : '${packageName}Window',
	buttons : [ {
		text : '保存',
		action : 'save'
	}, {
		text : '关闭',
		handler : function() {
			this.up('window').close();
		}
	} ],
	items : [ {
		xtype : 'basewindowform',
		items : [{
			xtype:'hidden',
			name:'id'
		},${columns} ]
	} ]
});