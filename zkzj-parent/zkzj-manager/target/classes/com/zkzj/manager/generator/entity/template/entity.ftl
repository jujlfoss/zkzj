package com.dy.entity.${moduleName};

import javax.persistence.Table;
import com.zkzj.entity.base.BaseEntity;
import com.zkzj.pojo.annotation.Comment;
${imports}
@Table(name = "${tableName}")
public class ${entityName} extends BaseEntity {

	private static final long serialVersionUID = 1L;

	${efields}

	${getSets}

}