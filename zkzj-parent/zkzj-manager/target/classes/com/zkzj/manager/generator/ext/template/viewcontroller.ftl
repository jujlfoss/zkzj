Ext.define('App.controller.${packageName}.${entityName}ViewController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.${packageName}ViewController',
	menu : Ext.widget('menu'),
	control : {
		'${packageName}View ${packageName}List' : {
			beforerender : function(grid) {
				var buttons = {
					'${moduleName}/${packageName}/save${entityName}' : function(permit) {
						BaseUtil.createView('${packageName}.${entityName}Window',permit,{storeId:'${packageName}.${entityName}Store'}).show();
					}
				};
				BaseUtil.createPermitTbar(grid, buttons);
			},
			itemcontextmenu : function(view, record, item, index, e) {
				var id = record.get('id');
				var menuitems = {
					'${moduleName}/${packageName}/update${entityName}' : function(permit) {
						var win = BaseUtil.createView('${packageName}.${entityName}Window',permit,{storeId:'${packageName}.${entityName}Store'});
						win.down('form').loadRecord(record);
						win.show();
					}
				};
				var status = record.get('status'),store = Ext.StoreMgr.get('${packageName}.${entityName}Store');
				if(status === StatusEnum.DELETE.getValue()){
					menuitems['${moduleName}/${packageName}/recover'] = function(permit) {
						BaseUtil.statusConfirm('确认恢复此记录吗?',permit.url,id,store);
					}
				}else{
					menuitems['${moduleName}/${packageName}/remove${entityName}'] = function(permit) {
						BaseUtil.statusConfirm('确认删除此记录吗?',permit.url,id,store);
					}
				}
				BaseUtil.createPermitMenu(view, this.menu, e, menuitems);
			},
			render : function() {
				BaseUtil.loadStore(Ext.StoreMgr.get('${packageName}.${entityName}Store'),Ext.getCmp('${packageName}View').down('${packageName}Search form').getForm().getFieldValues());
			}
		},
		'${packageName}View ${packageName}Search [action=search] ' : {
			click : function() {
				BaseUtil.loadStore(Ext.StoreMgr.get('${packageName}.${entityName}Store'),Ext.getCmp('${packageName}View').down('${packageName}Search form').getForm().getFieldValues());
			}
		}
	}
});