require.async([ 'Constant', 'ValidUtil', 'JSEncrypt','UrlUtil' ], function() {
	var changeImageCode = function(mobile){
		$("#login-imageCodeImg").attr("src",basePath+"/imageCode?mobile="+mobile+"&dc="+Math.random());
	}
	var bindImageCodeClick = function(mobile){
        $("#login-imageCodeImg").unbind("click");
        $("#login-imageCodeImg").bind("click", function(){
        	changeImageCode(mobile);
        });
	}
	// 登录
	$("#login-loginDiv [type=button]").click(function() {
		var userName = ValidUtil.trim($("#login-signin-username").val());
		if (!userName) {
			layer.msg('手机号码不能为空！')
			return;
		}
		var userPwd = ValidUtil.trim($("#login-signin-password").val());
		if (!userPwd) {
			layer.msg('密码不能为空！')
			return;
		}
		// var imageCode = null;
		// if($("#login-imageCodeP:hidden").length<1){
		// 	imageCode = ValidUtil.trim($("#login-imageCode").val());
		// 	if (!imageCode) {
		// 		layer.msg('验证码不能为空！')
		// 		return;
		// 	}
		// }
		var encrypt = new JSEncrypt();
		encrypt.setPublicKey(BaseUtil.getTextData(Constant.URL_PUBLICKEY));
		userPwd = encrypt.encrypt(userPwd);
		if (!userPwd) {
			$("#login-signin-password").val('');
			layer.msg('您的操作太快了,请重试！');
			return;
		}
		// $("#login-signin-password").val(userPwd);
		BaseUtil.post('login', function(data) {
			if (data.success) {
				var dataUrl = data.data;
				if (dataUrl && dataUrl.indexOf('.html') != -1) {
					location.href = basePath + dataUrl;
					return;
				}
				var refererUrl = UrlUtil.getUrlParam('referer');
				if (UrlUtil.getHost(refererUrl) == baseHost&& refererUrl.indexOf('.html') != -1) {
					location.href = refererUrl;
					return;
				}
				location.href = basePath + '/index.html';
				return;
			}
			$("#login-signin-password").val('');
			// if(data.code=="pwd_error_too_many"){
			// 	layer.msg("请输入图片验证码");
			// 	bindImageCodeClick(userName);
			// 	changeImageCode(userName);
			// 	$("#login-imageCode").val('');
			// 	$("#login-imageCodeP").show();
			// 	return;
			// }
			// if($("#login-imageCodeP:hidden").length>0){
			// 	bindImageCodeClick(userName);
			// 	changeImageCode(userName);
			// }
			layer.msg(data.msg);
		}, {
			userName : userName,
			userPwd : userPwd,
			remember : false,
			// imageCode : imageCode
		});

	});
});
