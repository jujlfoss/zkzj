<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/assets/config/jquery-config.jsp"%>

<!DOCTYPE html>
<html lang="en" class="no-js">

<head>

	<meta charset="utf-8">
	<title>重卡之家</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- CSS -->
	<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans:400,700'>
	<link rel="stylesheet" href="<%=basePath%>/front/css/reset.css">
	<link rel="stylesheet" href="<%=basePath%>/front/css/supersized.css">
	<link rel="stylesheet" href="<%=basePath%>/front/css/login.css">

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</head>

<body>

<div class="page-container" id="login-loginDiv">
	<h1>Login</h1>
	<form>
		<input type="text" name="username" class="username" id="login-signin-username">
		<input type="password" name="password" class="password" id="login-signin-password" placeholder="输入密码，密码由6-20位字符组成" id="">

		<button type="button">Sign me in</button>
		<div class="error"><span>+</span></div>
	</form>
</div>
<!-- Javascript -->
<script src="<%=basePath%>/assets/front/login/jquery-1.8.2.min.js"></script>
<script src="<%=basePath%>/assets/front/login/supersized.3.2.7.min.js"></script>
<script src="<%=basePath%>/assets/front/login/supersized-init.js"></script>
<script src="<%=basePath%>/assets/front/login/scripts.js"></script>
<script type="text/javascript" src="<%=basePath%>/login/front/login.js"></script>

</body>

</html>

