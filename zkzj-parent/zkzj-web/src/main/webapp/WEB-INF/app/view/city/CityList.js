Ext.define('App.view.city.CityList', {
	extend : 'App.config.BaseGrid',
	xtype : 'cityList',
	store : Ext.create('App.store.city.CityStore'),
	bbar : {
		xtype : 'pagingtoolbar',
		store : this.store,
		scrollable : true,
		displayInfo : true
	},
	columns : [ {
		xtype : 'rownumberer'
	},{
			text : '',
			dataIndex : 'areaName'
		},{
			text : '',
			dataIndex : 'areaType'
		},{
			text : '',
			dataIndex : 'parentId'
		},{
			text : '',
			dataIndex : 'baiduCode'
		},{
			text : '',
			dataIndex : 'cityCode'
		},{
			text : '',
			dataIndex : 'addressCode'
		},{
			text : '',
			dataIndex : 'latitude'
		},{
			text : '',
			dataIndex : 'longitude'
		},{
			text : '创建时间',
			dataIndex : 'createTime',
			renderer : function(value) {
				return DateUtil.timeToString(value,DateUtil.TIME);
			}
		},{
			text : '状态',
			dataIndex : 'status',
			renderer : function(value) {
				return StatusEnum.getHtml(value);
			}
		}]
});