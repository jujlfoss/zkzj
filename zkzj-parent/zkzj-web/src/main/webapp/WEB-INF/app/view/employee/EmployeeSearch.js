Ext.define('App.view.employee.EmployeeSearch', {
	requires : [ 'App.lib.TeamMultiSelect',"App.lib.MerchantsSingleSelect" ],
	extend : 'App.config.BaseFieldSet',
	xtype : 'employeeSearch',
	items : [ {
		xtype : 'basesearchform',
		items : [ {
			items : [ {
				xtype:"merchantsSingleSelect",
                allowBlank : true,
                beforeLabelTextTpl : [ '' ],
                columnWidth : .25,
			},{
				fieldLabel : '员工姓名',
				name : 'employeeName',
                columnWidth : .2,
			}, {
				fieldLabel : '手机号码',
				name : 'mobile',
                columnWidth : .2,
			},{
                xtype : 'button',
                text : '查询',
                action : 'search',
                columnWidth : .15,
                style : 'margin-left:15px'
            }, {
                xtype : 'button',
                text : '重置',
                columnWidth : .15,
                style : 'margin-left:15px',
                handler : function(me) {
                    me.up('form').getForm().reset();
                }
            }]
		},  ]
	} ]
});
