Ext.define('App.view.area.AreaView', {
	extend : 'App.config.BasePanel',
	requires : [ 'App.controller.area.AreaViewController',
			'App.view.area.AreaSearch', 'App.view.area.AreaList' ],
	controller : 'areaViewController',
	xtype : 'areaView',
	id : 'areaView',
	items : [
	// 	{
	// 	xtype : 'areaSearch'
	// },
		{
		xtype : 'areaList'
	} ]
});