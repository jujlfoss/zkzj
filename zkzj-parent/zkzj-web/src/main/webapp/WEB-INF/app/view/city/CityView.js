Ext.define('App.view.city.CityView', {
	extend : 'App.config.BasePanel',
	requires : [ 'App.controller.city.CityViewController',
			'App.view.city.CitySearch', 'App.view.city.CityList' ],
	controller : 'cityViewController',
	xtype : 'cityView',
	id : 'cityView',
	items : [ {
		xtype : 'citySearch'
	}, {
		xtype : 'cityList'
	} ]
});