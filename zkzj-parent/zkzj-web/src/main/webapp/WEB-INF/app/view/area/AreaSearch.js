Ext.define('App.view.area.AreaSearch', {
	extend : 'App.config.BaseFieldSet',
	xtype : 'areaSearch',
	items : [ {
		xtype : 'basesearchform',
		items : [ {
			items : [ {
				fieldLabel : '区域',
				name : 'areaName'
			} ]
		}, {
			items : [ {
				xtype : 'button',
				text : '查询',
				action : 'search',
				columnWidth : .15,
				style : 'margin-left:15px'
			}, {
				xtype : 'button',
				text : '重置',
				columnWidth : .15,
				style : 'margin-left:15px',
				handler : function(me) {
					me.up('form').getForm().reset();
				}
			} ]
		} ]
	} ]
});
