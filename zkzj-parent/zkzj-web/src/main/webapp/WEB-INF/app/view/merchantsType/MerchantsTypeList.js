Ext.define('App.view.merchantsType.MerchantsTypeList', {
	extend : 'App.config.BaseGrid',
	xtype : 'merchantsTypeList',
	store : Ext.create('App.store.merchantsType.MerchantsTypeStore'),
	bbar : {
		xtype : 'pagingtoolbar',
		store : this.store,
		scrollable : true,
		displayInfo : true
	},
	columns : [ {
		xtype : 'rownumberer'
	},{
			text : '类型名',
			dataIndex : 'typeName'
		}]
});