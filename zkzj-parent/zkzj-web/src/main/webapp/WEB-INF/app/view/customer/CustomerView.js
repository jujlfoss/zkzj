Ext.define('App.view.customer.CustomerView', {
	extend : 'App.config.BasePanel',
	requires : [ 'App.controller.customer.CustomerViewController',
			'App.view.customer.CustomerSearch', 'App.view.customer.CustomerList' ],
	controller : 'customerViewController',
	xtype : 'customerView',
	id : 'customerView',
	items : [ {
		xtype : 'customerSearch'
	}, {
		xtype : 'customerList'
	} ]
});