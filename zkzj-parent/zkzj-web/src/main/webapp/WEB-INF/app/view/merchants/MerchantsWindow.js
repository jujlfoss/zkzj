Ext.define('App.view.merchants.MerchantsWindow', {
	extend : 'App.config.BaseWindow',
	requires : [ 'App.controller.merchants.MerchantsWindowController'],
	controller : 'merchantsWindowController',
	xtype : 'merchantsWindow',
	id : 'merchantsWindow',
	buttons : [ {
		text : '保存',
		action : 'save'
	}, {
		text : '关闭',
		handler : function() {
			this.up('window').close();
		}
	} ],
	items : [ {
		xtype : 'basewindowform',
		items : [{
			xtype:'hidden',
			name:'id'
		},{
			allowBlank : false,
			beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
			fieldLabel : '商户名',
			name : 'name'
		},{
			allowBlank : false,
			beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
			fieldLabel : '商户类型',
			name : 'merchantsTypeId'
		} ]
	} ]
});