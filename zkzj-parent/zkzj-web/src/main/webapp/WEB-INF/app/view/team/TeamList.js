Ext.define('App.view.team.TeamList', {
	extend : 'App.config.BaseTree',
	xtype : 'teamList',
	mixins : [ 'App.lib.TreeFilter' ],
	store : Ext.create('App.store.team.TeamStore'),
	columns : [ {
		dataIndex : 'id',
		hidden : true
	}, {
		xtype : 'treecolumn',
		text : '名称',
		dataIndex : 'teamName'
	}, {
		text : '备注',
		dataIndex : 'remark'
	}, {
		text : '排序值',
		dataIndex : 'sort'
	}, {
		text : '状态',
		dataIndex : 'status',
		renderer : function(value) {
			return StatusEnum.getHtml(value);
		}
	} ]
});