Ext.define('App.view.customer.CustomerList', {
	extend : 'App.config.BaseGrid',
	xtype : 'customerList',
	store : Ext.create('App.store.customer.CustomerStore'),
	bbar : {
		xtype : 'pagingtoolbar',
		store : this.store,
		scrollable : true,
		displayInfo : true
	},
	columns : [ {
		xtype : 'rownumberer'
	},{
			text : '客户手机号码',
			dataIndex : 'mobile'
		},{
			text : '昵称',
			dataIndex : 'customerName'
		},{
			text : '推荐人手机号',
			dataIndex : 'referrerMobile'
		},{
			text : '创建时间',
			dataIndex : 'createTime',
			renderer : function(value) {
				return DateUtil.timeToString(value,DateUtil.TIME);
			}
		},{
			text : '状态',
			dataIndex : 'status',
			renderer : function(value) {
				return StatusEnum.getHtml(value);
			}
		}]
});