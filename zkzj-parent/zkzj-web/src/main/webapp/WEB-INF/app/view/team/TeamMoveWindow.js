Ext.define('App.view.team.TeamMoveList', {
	extend : 'App.config.BaseTree',
	xtype : 'teamMoveList',
	rootVisible : true,
	tbar : [ {
		xtype : 'form',
		border : 0,
		layout : 'hbox',
		defaults : {
			labelWidth : 50
		},
		items : [ {
			fieldLabel : '名称',
			name : 'teamName',
			xtype : 'textfield'
		}, {
			xtype : 'button',
			text : '查询',
			style : 'margin-left:15px',
			handler : 'tbarSearch'
		} ]
	} ],
	columns : [ {
		dataIndex : 'id',
		hidden : true
	}, {
		xtype : 'treecolumn',
		text : '名称',
		dataIndex : 'teamName'
	},{
		text : '状态',
		dataIndex : 'state',
		renderer : function(value) {
			return StateEnum.getHtml(value);
		}
	} ],
	initComponent : function() {
		this.store = Ext.create('App.store.team.TeamStore', {
			storeId : 'team.TeamMoveStore',
			autoDestroy : true,
			autoLoad : true
		});
		this.callParent(arguments);
	}
});
Ext.define('App.view.team.TeamMoveWindow', {
	extend : 'App.config.BaseWindow',
	requires : [ 'App.controller.team.TeamMoveWindowController'],
	controller : 'teamMoveWindowController',
	xtype : 'teamMoveWindow',
	id:'teamMoveWindow',
	title : '选择团队',
	buttons : [ {
		text : '确认',
		action : 'move'
	}, {
		text : '关闭',
		handler : function() {
			this.up('window').close();
		}
	} ],
	items : [ {
		xtype : 'teamMoveList'
	} ]
});