Ext.define('App.view.customer.CustomerWindow', {
	extend : 'App.config.BaseWindow',
	requires : [ 'App.controller.customer.CustomerWindowController'],
	controller : 'customerWindowController',
	xtype : 'customerWindow',
	id : 'customerWindow',
	buttons : [ {
		text : '保存',
		action : 'save'
	}, {
		text : '关闭',
		handler : function() {
			this.up('window').close();
		}
	} ],
	items : [ {
		xtype : 'basewindowform',
		items : [{
			xtype:'hidden',
			name:'id'
		},{
			allowBlank : false,
			beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
			fieldLabel : '用户Id',
			name : 'userId'
		},{
			allowBlank : false,
			beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
			fieldLabel : '昵称',
			name : 'customerName'
		},{
			allowBlank : false,
			beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
			fieldLabel : '头像路径',
			name : 'avatarUrl'
		},{
			allowBlank : false,
			beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
			fieldLabel : '推荐人手机号',
			name : 'referrerMobile'
		} ]
	} ]
});