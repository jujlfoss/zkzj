Ext.define('App.view.customer.CustomerSearch', {
	extend : 'App.config.BaseFieldSet',
	xtype : 'customerSearch',
	items : [ {
		xtype : 'basesearchform',
		items : [ {
			items : [ {
				fieldLabel : '客户手机号码',
				name : 'mobile'
			},{
				fieldLabel : '推荐人手机号',
				name : 'referrerMobile'
			} ]
		}, {
			items : [ {
				xtype : 'basestartdate',
				columnWidth : .25,
			}, {
				xtype : 'baseenddate',
				columnWidth : .2,
				labelWidth : 10
			}, {
				xtype : 'button',
				text : '查询',
				action : 'search',
				columnWidth : .15,
				style : 'margin-left:15px'
			}, {
				xtype : 'button',
				text : '重置',
				columnWidth : .15,
				style : 'margin-left:15px',
				handler : function(me) {
					me.up('form').getForm().reset();
				}
			} ]
		} ]
	} ]
});
