Ext.define('App.view.role.RoleGrantWindow', {
	extend : 'App.config.BaseWindow',
	requires : [ 'App.controller.role.RoleGrantWindowController',
			'App.view.role.RoleGrantList' ],
	controller : 'roleGrantWindowController',
	xtype : 'roleGrantWindow',
	id : 'roleGrantWindow',
	buttons : [ {
		text : '保存',
		action : 'save'
	}, {
		text : '关闭',
		handler : function() {
			this.up('window').close();
		}
	} ],
	items : [ {
		xtype : 'roleGrantList'
	} ]
});