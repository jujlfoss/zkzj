Ext.define('App.store.employee.EmployeeNotInCarStore', {
	extend : 'Ext.data.Store',
	model : 'App.model.employee.EmployeeModel',
	storeId : 'employee.EmployeeNotInCarStore',
	pageSize : Constant.PAGESIZE,
	proxy : {
		type : 'ajax',
		url : basePath + '/hrm/employee/pageEmployeeNotInCar',
		reader : {
			type : 'json',
			rootProperty : 'items',
			totalProperty : 'total'
		}
	}
});