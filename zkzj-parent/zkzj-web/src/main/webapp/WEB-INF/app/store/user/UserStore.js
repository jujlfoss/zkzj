Ext.define('App.store.user.UserStore', {
	extend : 'Ext.data.Store',
	storeId : 'user.UserStore',
	model : 'App.model.user.UserModel',
	pageSize : Constant.PAGESIZE,
	proxy : {
		type : 'ajax',
		url : basePath + '/sys/user/page',
		reader : {
			type : 'json',
			rootProperty : 'items',
			totalProperty : 'total'
		}
	}
});