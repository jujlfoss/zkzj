Ext.define('App.store.role.RoleStore', {
	extend : 'Ext.data.Store',
	model : 'App.model.role.RoleModel',
	storeId : 'role.RoleStore',
	pageSize : Constant.PAGESIZE,
	proxy : {
		type : 'ajax',
		url : basePath + '/sys/role/page',
		reader : {
			type : 'json',
			rootProperty : 'items',
			totalProperty : 'total'
		}
	}
});