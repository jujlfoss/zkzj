Ext.define('App.store.employee.EmployeeStore', {
	extend : 'Ext.data.Store',
	model : 'App.model.employee.EmployeeModel',
	storeId : 'employee.EmployeeStore',
	pageSize : Constant.PAGESIZE,
	proxy : {
		type : 'ajax',
		url : basePath + '/hrm/employee/pageEmployee',
		reader : {
			type : 'json',
			rootProperty : 'items',
			totalProperty : 'total'
		}
	}
});