Ext.define('App.store.merchants.MerchantsStore', {
	extend : 'Ext.data.Store',
	model : 'App.model.merchants.MerchantsModel',
	storeId : 'merchants.MerchantsStore',
	pageSize : Constant.PAGESIZE,
	proxy : {
		type : 'ajax',
		url : basePath + '/sys/merchants/pageMerchants',
		reader : {
			type : 'json',
			rootProperty : 'items',
			totalProperty : 'total'
		}
	}
});