Ext.define('App.controller.merchants.MerchantsWindowController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.merchantsWindowController',
	control : {
		'merchantsWindow [action=save]': {
			click : function(btn) {
				var win = btn.up('window'),params = win.params;
				BaseUtil.submit(win.down('form'), params.url,function(data) {
					BaseUtil.toast(data.msg);
					Ext.StoreMgr.get(params.storeId).reload();
					win.close();
				});
			}
		}
	}
});