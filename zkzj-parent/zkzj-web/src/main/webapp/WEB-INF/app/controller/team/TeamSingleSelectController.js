Ext.define('App.controller.team.TeamSingleSelectController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.teamSingleSelectController',
	control : {
		'teamSingleSelectWindow [action=save]':{
			click:function(me){
				var win = me.up('window'),id,teamName;
				var checkeds = win.down('teamSingleSelectList').getView().getChecked();
				if(checkeds.length > 0){
					var record = checkeds[0];
					id = record.get('id');
					teamName = record.get('teamName');
				}
				var select = win.teamSingleSelect;
				select.down('[itemId=teamId]').setValue(id);
				select.down('[itemId=teamName]').setValue(teamName);
				win.close();
			}
		},
		'teamSingleSelectWindow teamSingleSelectList':{
			checkchange : function(node, checked, obj) {
				var checkeds = Ext.getCmp('teamSingleSelectWindow').down('teamSingleSelectList').getView().getChecked();
				Ext.each(checkeds, function(c) {
					c.set('checked',false);
				});
				if(node.get("status")==StatusEnum.DELETE.getValue()){
					BaseUtil.toast('不能选择已删除记录');
					return;
				}
				node.set('checked',checked);
			}
		}
	},
	tbarSearch:function(){
		var tree = Ext.getCmp('teamSingleSelectWindow').down('teamSingleSelectList');
		tree.filterByTeamName(tree.down('[name=teamName]').getValue());
	}
});