Ext.define('App.controller.team.TeamViewController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.teamViewController',
	menu : Ext.widget('menu'),
	control : {
		'teamView teamList' : {
			beforerender : function(tree) {
				var buttons = {
					'hrm/team/saveFrist' : function(permit) {
						BaseUtil.createView('team.TeamWindow',permit).show();
					}
				};
				BaseUtil.createPermitTbar(tree, buttons);
			},
			itemcontextmenu : function(view, record, item, index, e) {
				var id = record.get('id'),teamName = record.get('teamName');
				var menuitems = {
					'hrm/team/saveChild' : function(permit) {
						var win = BaseUtil.createView('team.TeamWindow',permit);
						win.down('form').getForm().findField('parentId').setValue(id);
						win.show();
					},
					'hrm/team/updateTeam' : function(permit) {
						var win = BaseUtil.createView('team.TeamWindow',permit);
						win.down('form').loadRecord(record);
						win.show();
					},
					'hrm/team/seeEmployee' : function() {
						BaseUtil.initView(BaseUtil.getPermit('employeeView'),function(view){
							var search = view.down('employeeSearch');
							search.down('[name=teamIds]').setValue(record.get('id'));
							search.down('[name=teamNames]').setValue(teamName);
							BaseUtil.loadStore(Ext.StoreMgr.get('employee.EmployeeStore'),search.down('form').getForm().getFieldValues());
						});
					},
					'hrm/team/move' : function(permit) {
						var win = BaseUtil.createView('team.TeamMoveWindow',permit);
						win.params.id = id;
						win.show();
					}
				};
				var status = record.get('status'),store = Ext.StoreMgr.get('team.TeamStore'),selected = view.getSelection()[0];
				if(status === StatusEnum.DELETE.getValue()){
					menuitems['hrm/team/recover'] = function(permit) {
						BaseUtil.statusConfirm('确认恢复<font color=red> '+teamName+' </font>吗?',permit.url,id,null,function(){
							selected.set({status:StatusEnum.NORMAL.getValue()});
							selected.commit();
						});
					}
				}else{
					menuitems['hrm/team/removeTeam'] = function(permit) {
						BaseUtil.statusConfirm('确认删除<font color=red> '+teamName+' </font>吗?',permit.url,id,null,function(data){
							if(data.data){
								selected.remove();
							}else{
								selected.set({status:StatusEnum.DELETE.getValue()});
								selected.commit();
							}
						});
					}
				}
				BaseUtil.createPermitMenu(view, this.menu, e, menuitems);
			},
			render : function(tree) {
				tree.getStore().load();
			}
		},
		'teamView teamSearch [action=search] ' : {
			click : function(me) {
				var view = me.up('teamView'); 
				view.down('teamList').filterByTeamName(view.down('[name=teamName]').getValue());
			}
		}
	}
});
Ext.ns('App.controller.team.util');
App.controller.team.util.refreshNode = function(url,data) {
	var tree = Ext.getCmp('teamView').down('teamList');
	switch (url) {
	case 'hrm/team/saveChild':
		var node = tree.getSelection()[0];
		node.appendChild(data);
		node.expand();
		tree.getSelectionModel().select(tree.getStore().getNodeById(data.id));
		break;
	case 'hrm/team/updateTeam':
		var selected = tree.getSelection()[0];
		selected.set(data);
		selected.commit();
		break;
	case 'hrm/team/saveFrist':
		tree.getRootNode().appendChild(data);
		tree.getSelectionModel().select(tree.getStore().getNodeById(data.id));
		break;
	case 'hrm/team/move':
		var store = tree.getStore();
		store.getNodeById(data.id).remove();
		var node = store.getNodeById(data.parentId);
		node.appendChild(data);
		node.expand();
		tree.getSelectionModel().select(store.getNodeById(data.id));
		break;
	}
};