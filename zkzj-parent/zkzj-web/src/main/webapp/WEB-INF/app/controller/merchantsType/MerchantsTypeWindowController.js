Ext.define('App.controller.merchantsType.MerchantsTypeWindowController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.merchantsTypeWindowController',
	control : {
		'merchantsTypeWindow [action=save]': {
			click : function(btn) {
				var win = btn.up('window'),params = win.params;
				BaseUtil.submit(win.down('form'), params.url,function(data) {
					BaseUtil.toast(data.msg);
					Ext.StoreMgr.get(params.storeId).reload();
					win.close();
				});
			}
		}
	}
});