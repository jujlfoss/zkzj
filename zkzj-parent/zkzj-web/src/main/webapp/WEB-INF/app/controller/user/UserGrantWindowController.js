Ext.define('App.controller.user.UserGrantWindowController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.userGrantWindowController',
	control : {
		'userGrantWindow itemselector':{
			render:function(itemselector){
				itemselector.getStore().load();
			}
		},
		'userGrantWindow [action=save]' : {
			click : function(btn) {
				var win = btn.up('window');
				BaseUtil.submit(win.down('form'),win.params.url, function(data) {
					BaseUtil.toast(data.msg);
					win.close();
				});
			}
		}
	}
});