Ext.define('App.controller.customer.CustomerViewController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.customerViewController',
	menu : Ext.widget('menu'),
	control : {
		'customerView customerList' : {
			itemcontextmenu : function(view, record, item, index, e) {
				var id = record.get('id');
				var menuitems = {
					'crm/customer/cashAccountView' : function(permit) {
						BaseUtil.initView(BaseUtil.getPermit('cashAccountView'),function(view){
							var search = view.down('cashAccountSearch');
							search.down('[name=mobile]').setValue(record.get('mobile'));
							BaseUtil.loadStore(Ext.StoreMgr.get('cashAccount.CashAccountStore'),search.down('form').getForm().getFieldValues());
						});
					},
					'crm/customer/pointAccountView' : function(permit) {
						BaseUtil.initView(BaseUtil.getPermit('pointAccountView'),function(view){
							var search = view.down('pointAccountSearch');
							search.down('[name=mobile]').setValue(record.get('mobile'));
							BaseUtil.loadStore(Ext.StoreMgr.get('pointAccount.PointAccountStore'),search.down('form').getForm().getFieldValues());
						});
					}
				};
				var status = record.get('status'),store = Ext.StoreMgr.get('customer.CustomerStore');
				if(status === StatusEnum.DELETE.getValue()){
					menuitems['crm/customer/recover'] = function(permit) {
						BaseUtil.statusConfirm('确认恢复此记录吗?',permit.url,id,store);
					}
				}else{
					menuitems['crm/customer/removeCustomer'] = function(permit) {
						BaseUtil.statusConfirm('确认删除此记录吗?',permit.url,id,store);
					}
				}
				BaseUtil.createPermitMenu(view, this.menu, e, menuitems);
			},
			render : function() {
				BaseUtil.loadStore(Ext.StoreMgr.get('customer.CustomerStore'),Ext.getCmp('customerView').down('customerSearch form').getForm().getFieldValues());
			}
		},
		'customerView customerSearch [action=search] ' : {
			click : function() {
				BaseUtil.loadStore(Ext.StoreMgr.get('customer.CustomerStore'),Ext.getCmp('customerView').down('customerSearch form').getForm().getFieldValues());
			}
		}
	}
});