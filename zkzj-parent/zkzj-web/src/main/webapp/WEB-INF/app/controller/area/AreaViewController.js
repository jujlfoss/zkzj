Ext.define('App.controller.area.AreaViewController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.areaViewController',
	menu : Ext.widget('menu'),
	control : {
		'areaView areaList' : {
			beforerender : function(grid) {
				var buttons = {

                    'refresh': function (permit) {
                        Ext.StoreMgr.get('area.AreaStore').reload();
                    },
                    'expand':function(){
                        grid.getRootNode().expand(true)
                    },
                    'collapse':function(){
                        grid.getRootNode().collapseChildren(true);
                    }
				};
				BaseUtil.createPermitTbar(grid, buttons);
			},
			itemcontextmenu : function(view, record, item, index, e) {
			    console.log(record);
				var id = record.get('id');
				var menuitems = {
					'sys/area/updateArea' : function(permit) {
						var win = BaseUtil.createView('area.AreaWindow',permit,{storeId:'area.AreaStore'});
                        win.down("cityCombox").getStore().load({
                            params : {
                                id : record.get("pId")
                            }
                        })
                        win.down("cityCombox").setValue(record.get("cityId"));

                        win.down("countyCombox").getStore().load({
                            params : {
                                id : record.get("cityId")
                            }
                        })
                        win.down("countyCombox").setValue(record.get("countyId"));


						win.down('form').loadRecord(record);
						console.log(record);
						if(record.data.id!=1){
                            win.down("[itemId=parentName]").hidden =false;
                            win.down("[name=parentId]").setValue(record.data.parentId);
                            win.down("[itemId=parentName]").setValue(record.get("superName"));
                        }else{
                            win.down("[itemId=parentName]").hidden =true;
                            win.down("[name=parentId]").setValue(0);
                        }

						win.show();
					},


				};
                if(id==1) {
                    menuitems['sys/area/saveArea'] = function (permit) {
                        var win = BaseUtil.createView('area.AreaWindow', permit, {storeId: 'area.AreaStore'});
                        win.down("[name=parentId]").setValue(record.data.id);
                        win.down("[itemId=parentName]").setValue(record.get("areaName"));
                        win.show();
                    }
                }
				var status = record.get('status'),store = Ext.StoreMgr.get('area.AreaStore');
				if(status === StatusEnum.DELETE.getValue()){
					menuitems['sys/area/recover'] = function(permit) {
						BaseUtil.statusConfirm('确认恢复此记录吗?',permit.url,id,store);
					}
				}else{
					menuitems['sys/area/removeArea'] = function(permit) {
						BaseUtil.statusConfirm('确认删除此记录吗?',permit.url,id,store);
					}
				}
				BaseUtil.createPermitMenu(view, this.menu, e, menuitems);
			},
			render : function() {
				BaseUtil.loadStore(Ext.StoreMgr.get('area.AreaStore'));
			}
		},
	}
});