Ext.define('App.controller.city.CityViewController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.cityViewController',
	menu : Ext.widget('menu'),
	control : {
		'cityView cityList' : {
			beforerender : function(grid) {
				var buttons = {
					'sys/city/saveCity' : function(permit) {
						BaseUtil.createView('city.CityWindow',permit,{storeId:'city.CityStore'}).show();
					}
				};
				BaseUtil.createPermitTbar(grid, buttons);
			},
			itemcontextmenu : function(view, record, item, index, e) {
				var id = record.get('id');
				var menuitems = {
					'sys/city/updateCity' : function(permit) {
						var win = BaseUtil.createView('city.CityWindow',permit,{storeId:'city.CityStore'});
						win.down('form').loadRecord(record);
						win.show();
					}
				};
				var status = record.get('status'),store = Ext.StoreMgr.get('city.CityStore');
				if(status === StatusEnum.DELETE.getValue()){
					menuitems['sys/city/recover'] = function(permit) {
						BaseUtil.statusConfirm('确认恢复此记录吗?',permit.url,id,store);
					}
				}else{
					menuitems['sys/city/removeCity'] = function(permit) {
						BaseUtil.statusConfirm('确认删除此记录吗?',permit.url,id,store);
					}
				}
				BaseUtil.createPermitMenu(view, this.menu, e, menuitems);
			},
			render : function() {
				BaseUtil.loadStore(Ext.StoreMgr.get('city.CityStore'),Ext.getCmp('cityView').down('citySearch form').getForm().getFieldValues());
			}
		},
		'cityView citySearch [action=search] ' : {
			click : function() {
				BaseUtil.loadStore(Ext.StoreMgr.get('city.CityStore'),Ext.getCmp('cityView').down('citySearch form').getForm().getFieldValues());
			}
		}
	}
});