Ext.define('App.controller.user.UserWindowController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.userWindowController',
	control : {
		'userWindow [action=save]' : {
			click : function(btn) {
				var win = btn.up('window');
				BaseUtil.submit(win.down('form'), win.params.url,function(data) {
					BaseUtil.toast("操作成功，您的初始密码是："+Constant.USER_RESETPWD);
					Ext.StoreMgr.get('user.UserStore').reload();
					win.close();
				});
			}
		},
		'userWindow [itemId=employeeMobile]' : {
			change : function(me) {
				var win = me.up('window');
				var value = me.getValue();
				win.down('[name=mobile]').setValue(value);
			}
		},
		'userWindow [name=userType]' : {
			select : function(me, record) {
				var win = me.up('window'), viewModel = win.getViewModel();
				if (me.getValue() == UserTypeEnum.EMPLOYEE.getValue()) {
					viewModel.set({
						employeeSingleSelectHidden : false,
						employeeSingleSelectDisabled : false
					});
				} else {
					viewModel.set({
						employeeSingleSelectHidden : true,
						employeeSingleSelectDisabled : true
					});
				}
			}
		}
	}
});