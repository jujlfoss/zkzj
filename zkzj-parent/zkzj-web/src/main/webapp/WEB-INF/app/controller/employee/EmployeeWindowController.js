Ext.define('App.controller.employee.EmployeeWindowController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.employeeWindowController',
	control : {
		'employeeWindow [action=save]': {
			click : function(btn) {
				var win = btn.up('window'),params = win.params;
				BaseUtil.submit(win.down('form'), params.url,function(data) {
					BaseUtil.toast(data.msg);
					Ext.StoreMgr.get(params.storeId).reload();
					win.close();
				});
			}
		}
	}
});