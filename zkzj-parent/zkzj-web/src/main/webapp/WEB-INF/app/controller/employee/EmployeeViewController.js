Ext.define('App.controller.employee.EmployeeViewController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.employeeViewController',
	menu : Ext.widget('menu'),
	control : {
		'employeeView employeeList' : {
			beforerender : function(grid) {
				var buttons = {
					'hrm/employee/saveEmployee' : function(permit) {
						var win = BaseUtil.createView('employee.EmployeeWindow',permit,{storeId:'employee.EmployeeStore'}).show();
					}
				};
				BaseUtil.createPermitTbar(grid, buttons);
			},
			itemcontextmenu : function(view, record, item, index, e) {
				var id = record.get('id');
				var menuitems = {
					'hrm/employee/updateEmployee' : function(permit) {
						var win = BaseUtil.createView('employee.EmployeeWindow',permit,{storeId:'employee.EmployeeStore'});
						win.down('form').loadRecord(record);
						win.show();
					}
				};
				var status = record.get('status'),store = Ext.StoreMgr.get('employee.EmployeeStore');
				// if (status === StatusEnum.NORMAL.getValue()) {
				// 	menuitems['hrm/employee/freeze'] = function(permit) {
				// 		BaseUtil.statusConfirm('确认冻结该员工吗?',permit.url,id,store);
				// 	}
				// }else if (status === StatusEnum.FREEZE.getValue()) {
				// 	menuitems['hrm/employee/unfreeze'] = function(permit) {
				// 		BaseUtil.statusConfirm('确认解冻该员工吗?',permit.url,id,store);
				// 	}
				// }
				// if(status === StatusEnum.DELETE.getValue()){
				// 	menuitems['hrm/employee/recover'] = function(permit) {
				// 		BaseUtil.statusConfirm('确认恢复此记录吗?',permit.url,id,store);
				// 	}
				// }else{
					menuitems['hrm/employee/removeEmployee'] = function(permit) {
						BaseUtil.statusConfirm('确认删除此记录吗?',permit.url,id,store);
					}
				// }
				BaseUtil.createPermitMenu(view, this.menu, e, menuitems);
			},
			render : function() {
				BaseUtil.loadStore(Ext.StoreMgr.get('employee.EmployeeStore'),Ext.getCmp('employeeView').down('employeeSearch form').getForm().getFieldValues());
			}
		},
		'employeeView employeeSearch [action=search] ' : {
			click : function() {
				BaseUtil.loadStore(Ext.StoreMgr.get('employee.EmployeeStore'),Ext.getCmp('employeeView').down('employeeSearch form').getForm().getFieldValues());
			}
		}
	}
});