Ext.define('App.controller.uploadFile.UploadMultiViewSelectController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.uploadMultiViewSelectController',
	control : {
		"uploadMultiViewSelectWindow" : {
			'afterrender' : function(me, eOpts) {
				me.down("panel").removeAll();
				var data = me.uploadMultiViewSelect.down("[itemId=files]")
						.getValue();
				App.controller.uploadFile.show(data, null, me);

			}
		}
	},
});
/***
 * 图片显示
 * @param img 图片路径
 * @param btn 当前按钮
 * @param win 窗口
 */
App.controller.uploadFile.show = function(image, btn, win) {
	if (image) {
		if (image.indexOf(",") > -1) {
			var files = image.split(",");
			for (var i = 0; i < files.length; i++) {
				var imgs = App.controller.uploadFile.create(files[i]);
				App.controller.uploadFile.add(btn,win,imgs);
			}

		} else {
			var img = App.controller.uploadFile.create(image);
			App.controller.uploadFile.add(btn,win,img);
		}

	}

}
/**
 * 创建加载图片组件
 *@param image 图片路径
 *
 */
App.controller.uploadFile.create = function(image) {
	return Ext.create('Ext.Component', {
		html : 'Hello world!',
		width : 100,
		height : 100,
		padding : 20,
		autoEl : {
			tag : "img",
			src : basePath + "/file/display/" + image,
			onMouseMove:"App.controller.uploadFile.big(this)",
			onMouseOut:"App.controller.uploadFile.small(this)"
		}
	});
}
/**
 * 添加图片
 */
App.controller.uploadFile.add = function(btn,win,img) {
	if (!btn) {
		win.down("panel").add(img);
	} else {
		btn.up("window").down("panel").add(img);
	}
}

App.controller.uploadFile.big = function(img){
	var id = img.getAttribute("id");
	document.getElementById(id).style.width = '300px';
	document.getElementById(id).style.height = '300px';
	
}
App.controller.uploadFile.small = function(img){
	var id = img.getAttribute("id");
	document.getElementById(id).style.width = '100px';
	document.getElementById(id).style.height = '100px';
	
}