Ext.define('App.controller.employee.EmployeeSingleSelectController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.employeeSingleSelectController',
	control : {
		'employeeSingleSelectWindow [action=save]' : {
			click : function(me) {
				var win = me.up('window'),id,employeeName;
				var selection = win.down('employeeSingleSelectList').getSelectionModel().getSelection();
				if(selection.length > 0){
					var record = selection[0];
					id = record.get('id');
					employeeName = record.get('employeeName');
					mobile = record.get('mobile');
				}
				var select = win.employeeSingleSelect;
				select.down('[itemId=employeeId]').setValue(id);
				select.down('[itemId=employeeName]').setValue(employeeName);
				select.down('[itemId=employeeMobile]').setValue(mobile);
				win.close();
			}
		},
		'employeeSingleSelectWindow employeeSingleSelectList':{
			'select':function(me,record){
				if(record.get("status")==StatusEnum.DELETE.getValue()){
					BaseUtil.toast('不能选择已删除记录');
					me.deselect(record);
				}
			}
		}
	},
	tbarSearch : function() {
		BaseUtil.loadStore(Ext.StoreMgr
				.get('employee.EmployeeSingleSelectStore'), Ext.getCmp(
				'employeeSingleSelectWindow').down('form').getForm()
				.getFieldValues());
	}
});