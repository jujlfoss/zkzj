Ext.define('App.controller.resource.ResourceWindowController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.resourceWindowController',
	control:{
		'resourceWindow  [action=save]' : {
			click : function(me) {
				var win = me.up('window');
				var url = win.params.url;
				BaseUtil.submit(win.down('form'), url, function(data) {
					App.controller.resource.util.refreshNode(url, data.data);
					BaseUtil.toast(data.msg);
					win.close();
				});
			}
		}
	}
});