Ext.define('App.model.merchants.MerchantsModel', {
	extend : 'Ext.data.Model',
	fields : [ 'id','name','merchantsTypeId','createTime','modifyTime','status',"areaName",'typeName','countyName','cityName','pName','cityId','countyId']

});