require.sync('BespeakEnum');
Ext.define('App.lib.BespeakStatusCombox', {
	extend : 'Ext.form.field.ComboBox',
	alias : 'widget.bespeakStatusCombox',
	fieldLabel : '订单状态',
	allowBlank : false,
	beforeLabelTextTpl : "",
	fieldStyle : 'cursor:pointer',
	name : 'bespeakStatus',
	editable : false,
	store : Ext.create('Ext.data.Store', {
		fields : [ 'name', 'value' ],
		data : [ {
			'name' : BespeakEnum.PAYWAIT.getText(),
			'value' : BespeakEnum.PAYWAIT.getValue()
		}, {
			'name' : BespeakEnum.PAYSUCCESS.getText(),
			'value' : BespeakEnum.PAYSUCCESS.getValue()
		},{
			'name' : BespeakEnum.ORDERS.getText(),
			'value' : BespeakEnum.ORDERS.getValue()
		} ,{
			'name' : BespeakEnum.FINISH.getText(),
			'value' : BespeakEnum.FINISH.getValue()
		},{
				'name' : BespeakEnum.CANCEL.getText(),
				'value' : BespeakEnum.CANCEL.getValue()
		}]
	}),
	queryMode : 'local',
	displayField : 'name',
	valueField : 'value',
});
