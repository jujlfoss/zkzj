Ext.define('App.lib.MerchantsSingleSelectList', {
	extend : 'App.config.BaseGrid',
	requires : [ 'Ext.selection.CheckboxModel' ],
	xtype : 'merchantsSingleSelectList',
	selModel: { 
        selType: 'checkboxmodel',
        mode : 'SINGLE',
		allowDeselect : true
    },
    features: [{
        groupHeaderTpl: Ext.create('Ext.XTemplate',
            '商户类型:',
            '<span>{name:this.formatName}</span>',
            {
                formatName: function (name) {
                    return name;
                }
            }
        ),
        ftype: 'groupingsummary'
    }],
	tbar : [ {
		xtype : 'form',
		border : 0,
		layout : 'hbox',
		defaults : {
			labelWidth : 60
		},
		items : [ {
			fieldLabel : '商户名',
			name : 'name',
			xtype : 'textfield'
		}, {
			xtype : 'button',
			text : '查询',
			style : 'margin-left:15px',
			handler : 'tbarSearch'
		} ]
	} ],
	columns : [ {
		dataIndex : 'id',
		hidden : true
	}, {
        text : '省',
        dataIndex : 'pName'
    },{
        text : '市',
        dataIndex : 'cityName'
    },{
        text : '区/县',
        dataIndex : 'countyName'
    },{
        text : '所属区域',
        dataIndex : 'areaName'
    },{
        text : '商户类型',
        dataIndex : 'typeName'
    },{
        text : '商户名',
        dataIndex : 'name',
        summaryType: 'count',
        summaryRenderer: function (value) {

            return value ? value : 0;
        }
    }],
	initComponent : function() {
		this.store = Ext.create('App.store.merchants.MerchantsStore', {
            groupField: 'typeName',
			storeId : 'merchants.merchantsSingleSelectStore',
			autoDestroy : true,
			autoLoad : true,
			listeners : {
				load : function() {
					var win = Ext.getCmp('merchantsSingleSelectWindow'),list = win.down('merchantsSingleSelectList'),store = list.getStore(),count = store.getCount(),id = win.merchantsSingleSelect.down('[itemId=merchantsId]').getValue();
					for (var i = 0; i < count; i++) {
						var record = store.getAt(i);
						if(record.get('id')==id){
							list.getSelectionModel().select(record);
							return;
						}
					}
				}
			}
		});
		this.bbar = Ext.widget('pagingtoolbar',{
			store : this.store,
			scrollable : true,
			displayInfo : true
		});
		this.callParent(arguments);
	}
});
Ext.define('App.lib.MerchantsSingleSelectWindow', {
	extend : 'App.config.BaseWindow',
	requires : [ 'App.controller.merchants.MerchantsSingleSelectController'],
	controller : 'merchantsSingleSelectController',
	xtype : 'merchantsSingleSelectWindow',
	id : 'merchantsSingleSelectWindow',
	title : '请选择商户',
	buttons : [ {
		text : '确认',
		action : 'save'
	},{
		text : '清空',
		handler : function() {
			this.up('window').down('grid').getSelectionModel().deselectAll();
		}
	},{
		text : '关闭',
		handler : function() {
			this.up('window').close();
		}
	} ],
	items : [ {
		xtype : 'merchantsSingleSelectList'
	} ]
});
Ext.define('App.lib.MerchantsSingleSelect', {
	extend : 'Ext.form.FieldSet',
	requires : ['App.controller.merchants.MerchantsSingleSelectController','App.lib.SelectField'],
	controller : 'merchantsSingleSelectController',
	alias : 'widget.merchantsSingleSelect',
	layout : 'fit',
	border : false,
	padding:0,
	margin:0,
	config:{
		margin : 0,
		beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
		allowBlank : false,
		submitName:'name',
		submitValue:'merchantsId',
		fieldLabel : '商户名'
	},
	updateMargin: function(value){
		this.margin = value;
	},
    updateAllowBlank: function(value) {
    	this.items[1].allowBlank = value;
    },
    updateBeforeLabelTextTpl: function(value) {
    	this.items[1].beforeLabelTextTpl = value;
    },
    updateSubmitName: function(value) {
    	this.items[1].name = value;
    },
    updateSubmitValue: function(value) {
    	this.items[0].name = value;
    },
    updateFieldLabel: function(value) {
    	this.items[1].fieldLabel = value;
    },
	defaults : {
		msgTarget : 'under',
		labelWidth : 100,
		labelAlign : 'right'
	},
	items : [ {
		xtype : 'hidden',
		itemId:'merchantsId'
	}, {
		xtype : 'selectfield',
		itemId:'name',
		onClick : function(me) {
            var viewXtype = me.up('merchantsSingleSelect').viewXtype;
			Ext.widget('merchantsSingleSelectWindow', {merchantsSingleSelect : me.up('merchantsSingleSelect'),viewXtype: viewXtype}).show();
		}
	} ]
});
