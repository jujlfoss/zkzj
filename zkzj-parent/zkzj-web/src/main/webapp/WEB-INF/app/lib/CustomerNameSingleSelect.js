Ext.define('App.lib.CustomerNameSingleSelectList', {
	extend : 'App.config.BaseGrid',
	requires : [ 'Ext.selection.CheckboxModel' ],
	xtype : 'customerNameSingleSelectList',
	selModel: { 
        selType: 'checkboxmodel',
        mode : 'SINGLE',
		allowDeselect : true
    },
	tbar : [ {
		xtype : 'form',
		border : 0,
		layout : 'hbox',
		defaults : {
			labelWidth : 50
		},
		items : [ {
			fieldLabel : '客户名',
			name : 'customerName',
			xtype : 'textfield'
		}, {
			xtype : 'button',
			text : '查询',
			style : 'margin-left:15px',
			handler : 'tbarSearch'
		} ]
	} ],
	columns : [ {
		dataIndex : 'id',
		hidden : true
	}, {
		text : '手机号码',
		dataIndex : 'mobile'
	},{
		text : '昵称',
		dataIndex : 'customerName'
	},{
		text : '状态',
		dataIndex : 'status',
		renderer : function(value) {
			return StatusEnum.getHtml(value);
		}
	} ],
	initComponent : function() {
		this.store = Ext.create('App.store.customer.CustomerStore', {
			storeId : 'customer.CustomerNameSingleSelectStore',
			autoDestroy : true,
			autoLoad : true,
			listeners : {
				load : function() {
					var win = Ext.getCmp('customerNameSingleSelectWindow'),list = win.down('customerNameSingleSelectList'),store = list.getStore(),count = store.getCount(),customerId = win.customerNameSingleSelect.down('[itemId=customerId]').getValue();
					for (var i = 0; i < count; i++) {
						var record = store.getAt(i);
						if(record.get('id')==customerId){
							list.getSelectionModel().select(record);
							return;
						}
					}
				}
			}
		});
		this.bbar = Ext.widget('pagingtoolbar',{
			store : this.store,
			scrollable : true,
			displayInfo : true
		});
		this.callParent(arguments);
	}
});
Ext.define('App.lib.CustomerNameSingleSelectWindow', {
	extend : 'App.config.BaseWindow',
	requires : [ 'App.controller.customer.CustomerNameSingleSelectController'],
	controller : 'customerNameSingleSelectController',
	xtype : 'customerNameSingleSelectWindow',
	id : 'customerNameSingleSelectWindow',
	title : '选择客户',
	buttons : [ {
		text : '确认',
		action : 'save'
	},{
		text : '清空',
		handler : function() {
			this.up('window').down('grid').getSelectionModel().deselectAll();
		}
	},{
		text : '关闭',
		handler : function() {
			this.up('window').close();
		}
	} ],
	items : [ {
		xtype : 'customerNameSingleSelectList'
	} ]
});
Ext.define('App.lib.CustomerNameSingleSelect', {
	extend : 'Ext.form.FieldSet',
	requires : ['App.controller.customer.CustomerNameSingleSelectController','App.lib.SelectField'],
	controller : 'customerNameSingleSelectController',
	alias : 'widget.customerNameSingleSelect',
	layout : 'fit',
	border : false,
	padding:0,
	margin:0,
	config:{
		margin : 0,
		beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
		allowBlank : false,
		submitName:'mobile',
		submitValue:'customerId',
		fieldLabel : '客户名'
	},
	updateMargin: function(value){
		this.margin = value;
	},
    updateAllowBlank: function(value) {
    	this.items[1].allowBlank = value;
    },
    updateBeforeLabelTextTpl: function(value) {
    	this.items[1].beforeLabelTextTpl = value;
    },
    updateSubmitName: function(value) {
    	this.items[1].name = value;
    },
    updateSubmitValue: function(value) {
    	this.items[0].name = value;
    },
    updateFieldLabel: function(value) {
    	this.items[1].fieldLabel = value;
    },
	defaults : {
		msgTarget : 'under',
		labelWidth : 100,
		labelAlign : 'right'
	},
	items : [ {
		xtype : 'hidden',
		itemId:'customerId'
	}, {
		xtype : 'selectfield',
		itemId:'customerName',
		onClick : function(me) {
			Ext.widget('customerNameSingleSelectWindow', {customerNameSingleSelect : me.up('customerNameSingleSelect')}).show();
		}
	} ]
});
