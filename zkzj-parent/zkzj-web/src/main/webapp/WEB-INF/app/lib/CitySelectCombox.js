Ext.define('App.lib.CitySelectCombox', {
	alias : 'widget.citySelectCombox',
	extend : 'Ext.form.field.ComboBox',
	fieldLabel : "所属市",
	layout : 'anchor',
	editable : false,
	name:'cityId',
	triggerAction : 'all',
	store : Ext.create('Ext.data.Store', {
		fields : [ "id", "areaName" ],
		autoLoad : true,
		proxy : {
			type : 'ajax',
			url : basePath + '/eb/area/getCityList',
			reader : {
				type : 'json',
				root : 'data'
			}
		},
	}),
	allowBlank : false,
	displayField : 'areaName',
	valueField : 'id'
});
