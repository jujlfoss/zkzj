Ext.define('App.lib.UploadSingleSelectWindow', {
	extend : 'App.config.BaseWindow',
	requires : [ 'App.controller.uploadFile.UploadSingleSelectController'],
	controller : 'uploadSingleSelectController',
	xtype : 'uploadSingleSelectWindow',
	id : 'uploadSingleSelectWindow',
	layout : 'anchor',
	title:"请选择图片",
	buttons:[
	         {
	        	 text:"上传",
	        	 action:"save"
	         },
	              {
	        	 text:"关闭", handler:function(btn){
	        			btn.up("window").close();
	        	 }
	        	 
	         },
	         
	         
	         ],
	items:[{
		xtype:"panel",
		itemId:"imagePanel",
		layout:"table",
		height:200
	},{
		xtype:"form",
		layout:"form",
		enctype : "multipart/form-data",
		items:[{
			xtype:"filefield",
			fieldLabel : "图片",
			name : "files",
			allowBlank : false,
		}]
	}
],
});
Ext.define('App.lib.UploadSingleSelect', {
	extend : 'Ext.form.FieldSet',
	requires : ['App.controller.uploadFile.UploadSingleSelectController','App.lib.SelectField'],
	controller : 'uploadSingleSelectController',
	alias : 'widget.uploadSingleSelect',
	layout : 'fit',
	border : false,
	padding:0,
	margin:0,
	config:{
		margin : 0,
		beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
		allowBlank : false,
		submitValue:'thumbnailUrl',
		fieldLabel : '缩略图',
		emptyText:""
	},
	updateMargin: function(value){
		this.margin = value;
	},
    updateAllowBlank: function(value) {
    	this.items[0].allowBlank = value;
    },
    updateBeforeLabelTextTpl: function(value) {
    	this.items[0].beforeLabelTextTpl = value;
    },
    updateSubmitValue: function(value) {
    	this.items[0].name = value;
    },
    updateFieldLabel: function(value) {
    	this.items[0].fieldLabel = value;
    },
    updateEmptyText: function(value) {
    	this.items[0].emptyText = value;
    },
	defaults : {
		msgTarget : 'under',
		labelWidth : 100,
		labelAlign : 'right'
	},
	items : [ {
		xtype : 'selectfield',
		itemId:'files',
		onClick : function(me) {
			Ext.widget('uploadSingleSelectWindow', {uploadSingleSelect : me.up('uploadSingleSelect')}).show();
		}
	} ]
});
