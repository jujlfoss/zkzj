Ext.define('App.lib.ProviceCombox', {
    alias : 'widget.proviceCombox',
    extend : 'Ext.form.field.ComboBox',
    width: 320,
    bodyPadding: 1,
    fieldLabel : "所属省",
    queryMode : 'local',
    lazyInit : true,
    editable : false,
    triggerAction : 'all',
    layout: 'anchor',
    store : Ext.create('Ext.data.Store', {
        fields : [ "pId", "areaName" ],
        autoLoad : true,
        proxy : {
            type : 'ajax',
            url : basePath+'/sys/city/getList?id=0',
            reader : {
                type : 'json',
                root : 'data'
            }
        },
    }),
    allowBlank : false,
    emptyText : '请选择省...',
    displayField : 'areaName',
    valueField : 'id',
    name:"pId"

});

