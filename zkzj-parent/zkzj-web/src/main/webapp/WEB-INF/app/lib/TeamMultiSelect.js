Ext.define('App.lib.TeamMultiSelectList', {
	extend : 'App.config.BaseTree',
	xtype : 'teamMultiSelectList',
	requires : [ 'App.model.team.TeamCheckedModel' ],
	mixins : [ 'App.lib.TreeFilter' ],
	tbar : [ {
		xtype : 'form',
		border : 0,
		layout : 'hbox',
		defaults : {
			labelWidth : 50
		},
		items : [ {
			fieldLabel : '名称',
			name : 'teamName',
			xtype : 'textfield'
		}, {
			xtype : 'button',
			text : '查询',
			style : 'margin-left:15px',
			handler : 'tbarSearch'
		} ]
	} ],
	columns : [ {
		dataIndex : 'id',
		hidden : true
	}, {
		text : '名称',
		xtype : 'treecolumn',
		dataIndex : 'teamName'
	}, {
		text : '状态',
		dataIndex : 'status',
		renderer : function(value) {
			return StatusEnum.getHtml(value);
		}
	} ],
	initComponent : function() {
		this.store = Ext.create('App.store.team.TeamStore', {
			model : 'App.model.team.TeamCheckedModel',
			storeId : 'team.TeamMultiSelectStore',
			autoDestroy : true,
			autoLoad : true,
			proxy : {
				type : 'ajax',
				url : basePath + '/hrm/team/listChecked'
			},
			listeners : {
				load : function() {
					var win = Ext.getCmp('teamMultiSelectWindow');
					var teamIdsValue = win.teamMultiSelect.down('[itemId=teamIds]').getValue();
					if(ValidUtil.isNotBlank(teamIdsValue)){
						var teamIds = teamIdsValue.split(',');
						var store = win.down('teamMultiSelectList').getStore();
						for (var i = 0; i < teamIds.length; i++) {
							store.getNodeById(teamIds[i]).set('checked', true);
						}
					}
				}
			}
		});
		this.callParent(arguments);
	}
});
Ext.define('App.lib.TeamMultiSelectWindow', {
	extend : 'App.config.BaseWindow',
	requires : [ 'App.controller.team.TeamMultiSelectController'],
	controller : 'teamMultiSelectController',
	xtype : 'teamMultiSelectWindow',
	id : 'teamMultiSelectWindow',
	title : '选择部门',
	buttons : [ {
		text : '确认',
		action : 'save'
	},{
		text : '清空',
		action : 'reset'
	}, {
		text : '关闭',
		handler : function() {
			this.up('window').close();
		}
	} ],
	items : [ {
		xtype : 'teamMultiSelectList'
	} ]
});
Ext.define('App.lib.TeamMultiSelect', {
	extend : 'Ext.form.FieldSet',
	requires : ['App.controller.team.TeamMultiSelectController','App.lib.SelectField'],
	controller : 'teamMultiSelectController',
	alias : 'widget.teamMultiSelect',
	layout : 'fit',
	border : false,
	padding:0,
	margin:0,
	config:{
		beforeLabelTextTpl : '',
		allowBlank : true,
		submitName:'teamNames',
		submitValue:'teamIds',
		fieldLabel : '部门',
		margin : 0
	},
	updateMargin: function(value){
		this.margin = value;
	},
    updateAllowBlank: function(value) {
    	this.items[1].allowBlank = value;
    },
    updateBeforeLabelTextTpl: function(value) {
    	this.items[1].beforeLabelTextTpl = value;
    },
    updateSubmitName: function(value) {
    	this.items[1].name = value;
    },
    updateSubmitValue: function(value) {
    	this.items[0].name = value;
    },
    updateFieldLabel: function(value) {
    	this.items[1].fieldLabel = value;
    },
	defaults : {
		msgTarget : 'under',
		labelWidth : 100,
		labelAlign : 'right'
	},
	items : [ {
		xtype : 'hidden',
		itemId : 'teamIds'
	}, {
		itemId : 'teamNames',
		xtype : 'selectfield',
		onClick : function(me) {
			Ext.widget('teamMultiSelectWindow', {teamMultiSelect : me.up('teamMultiSelect')}).show();
		}
	} ]
});
