require.async([ 'JqueryBase', 'UrlUtil','common','CartUtil' ], function() {
	BaseUtil.get('eb/goods/front/get', function(data) {
		var goods = '<h3 class="title">{goodsName}</h3> <ul class="desc_ul"> <li class="clearfix"><span class="name left">套餐介绍：</span><span class="detail">{description}</span></li></ul>  <div class="price mt20"> <span><em>积分：</em>{price}</span><span class="price02">市场参考价：{referPrice}元</span> </div> <div class="mt20 clearfix"> <a href="{atonce}" class="yue_btn left">立即兑换</a><a href="{atcart}" class="yue_btn left">加入购物车</a> </div>';
		var goodsHtml = goods.replaceAll('{goodsName}',data.goodsName)
		.replaceAll('{price}',data.price)
		.replaceAll('{referPrice}',NumberUtil.roundString(data.referPrice))
		.replaceAll('{description}',data.description)
		.replaceAll('{atonce}','javascript:CartUtil.submit('+data.id+')')
		.replaceAll('{atcart}','javascript:CartUtil.addToCart('+data.id+')');
		$("#goods").html(goodsHtml);
		var imgHtml = [],imgUrls = data.detailUrls.split(','),templete = '<img src="{basePath}/file/display/{imgUrl}" width="451" height="268" />';
		for (var i = 0; i < imgUrls.length; i++) {
			imgHtml.push(templete.replaceAll('{basePath}',basePath).replaceAll('{imgUrl}',imgUrls[i]));
		}
		$("#showbox").html(imgHtml.join(''));
		$("#imgbox").html('<img src="{basePath}/file/display/{infoUrl}" />'.replaceAll('{basePath}',basePath).replaceAll('{infoUrl}',data.infoUrl));
		$.ljsGlasses.pcGlasses({
			"boxid" : "showbox",
			"sumid" : "showsum",
			"boxw" : 465,// 宽度,该版本中请把宽高填写成一样
			"boxh" : 268,// 高度,该版本中请把宽高填写成一样
			"sumw" : 83,// 列表每个宽度,该版本中请把宽高填写成一样
			"sumh" : 52,// 列表每个高度,该版本中请把宽高填写成一样
			"sumi" : 10,// 列表间隔
			"sums" : 5,// 列表显示个数
			"sumsel" : "sel",
			"sumborder" : 1,// 列表边框，没有边框填写0，边框在css中修改
			"lastid" : "showlast",
			"nextid" : "shownext"
		});// 方法调用，务必在加载完后执行
	}, {
		id : UrlUtil.getUrlParam('id')
	});
});