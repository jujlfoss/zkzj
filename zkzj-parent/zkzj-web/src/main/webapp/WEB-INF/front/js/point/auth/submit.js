require.async([ 'JqueryBase','UrlUtil' ], function(window) {
	var goodsIds = UrlUtil.getUrlParam('goodsIds');
	if(!goodsIds){
		window.alert('无法提交空的订单');
		window.location.href=basePath+'/index.html';
	}
	BaseUtil.get("eb/goods/listGoods",function(data){
		var html = [],templete = '<li class="clearfix"> <div class="proimg left"> <img src="{img}"> </div> <div class="txt left"> <span class="name">{goodsName}</span> <span class="price">市场参考价：{referPrice}元</span> </div> <div class="score right"> {price}<em>积分</em> </div> </li>';
		$.each(data,function(idx,item){
			html.push(templete.replaceAll('{goodsName}',item.goodsName)
			.replaceAll('{price}',item.price)
			.replaceAll('{referPrice}',NumberUtil.roundString(item.referPrice))
			.replaceAll('{img}',basePath+'/file/display/'+item.thumbnailUrl));
		});
		$("#goods-ul").html(html.join(''));
	},{goodsIds:goodsIds});
	
	(function calc(){
		BaseUtil.get('eb/cart/calc', function(result) {
			var data = result.data;
			var totalPrice = data.totalPrice
			$('#totalPrice').text(totalPrice);
			if (totalPrice > data.totalPoint) {
				$("#sure_btn").css('background', 'lightgray');
				$("#sure_btn").attr('disabled', true);
				$("#sure_btn").val('积分不足');
				return;
			}
			$("#sure_btn").css('background','#F98322');
			$("#sure_btn").attr('disabled',false);
			$("#sure_btn").val('确认兑换');
		}, {
			goodsIds : goodsIds
		});
	})();
	
	BaseUtil.get("eb/area/front/pageArea",function(result){
		var areaData = result.data,cityCode = CookieUtil.get(Constant.CITY_CODE);
		$.each(areaData.items,function(idx,item){  
			$("#citySelect").append('<option cityId="'+item.cityId+'" value="'+item.id+'" '+(cityCode == item.id ? 'selected':'') +'>'+item.name+'</option>');
		});
		changeDistrict($("#citySelect option:selected").attr('cityId'));
	});
	
	function changeDistrict(cityId){
		BaseUtil.get("eb/area/front/pageArea",function(result){
			$("#countySelect option").remove();
			 var areaData = result.data;  
			 $.each(areaData.items,function(idx,item){
				 $("#countySelect").append('<option value="'+item.id+'" >'+item.areaName+'</option>');
			 });
			 initEstate($("#countySelect option:selected").val());
		 },{"id":0,"parentId":cityId}); 
	}
	
	$("#citySelect").change(function(){
		changeDistrict($(this).find('option:selected').attr('cityId'));
	});
	
	$("#countySelect").change(function(){
		initEstate($(this).val());
	});
	
	var estateTemplete = '<li class="clearfix estateLi estateLi{id}"><i class="radiocss {checkedcss}"><input type="radio" name="itemradio" hidden="true" class="itemradio" value="{id}" {checked} /></i> <span> <p class="position01">{estateName}</p> <p class="addr01">{address}</p> </span></li>';
	function initEstate(areaId){
		BaseUtil.get("im/estate/front/pageEstate",function(result){
			var items = result.data.items,estateHtml=[];
			if(items){
				$.each(items,function(idx,item){ 
					var html = estateTemplete.replaceAll('{id}',item.id).replaceAll('{estateName}',item.estateName).replaceAll('{address}',item.address)
					if(idx==0){
						html = html.replaceAll('{checkedcss}','checked').replaceAll('{checked}','checked="true"');
					}else{
						html = html.replaceAll('{checkedcss}','').replaceAll('{checked}','');
					}
					estateHtml.push(html);
				});
			}
			if(estateHtml.length==0){
				$('#area-ul').html("<li class='clearfix' style='text-align:center;'>该区域暂时无服务点,请选择其他区域</>");
				return;
			}
			$('#area-ul').html(estateHtml.join(''));
		},{'areaId':areaId});  
	}
	
	$(document).on('click','.radiocss',function(){
		var radio = $(this).find('.itemradio');
		if(!radio.is(':checked')){
			$(".radiocss").removeClass('checked');
			$(this).addClass('checked');
			radio.prop('checked',true);
		}
	});
	$("#sure_btn").click(function(){
		var btn = $(this);
		btn.attr('disabled',true);
		var estateId = ValidUtil.trim($("[name=itemradio]:checked").val());
		if(!estateId){
			layer.msg('请选择上门取货服务点');
			btn.attr('disabled',false);
			return;
		}
		BaseUtil.post('eb/order/saveOrder', function(result) {
			if(result.success){
				window.location.href = basePath + '/user/auth/myorder.html';
				return;
			}
			layer.msg(result.msg);
			btn.attr('disabled',false);
		}, {
			estateId : estateId,
			goodsIds : goodsIds,
			cartIds : UrlUtil.getUrlParam('cartIds')
		});
	});
	
});