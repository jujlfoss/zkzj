require.async(['JqueryBase','CartUtil'],function(window){
	var html = [],templete = '<li class="clearfix cartLi cartLi{cartId}" cartid="{cartId}" liid="{goodsId}" ><span class="itemcheckspan checkboxcss checked mt37" id="itemcheckspan{goodsId}" ><input type="checkbox" checked="checked" hidden="true" class="itemcheck" id="itemcheck{goodsId}" /></span> <div class="proimg left"> <img src="{basePath}/file/display/{thumbnailUrl}"> </div> <div class="txt left"> <span class="name">{goodsName}</span> <span class="price">市场参考价：{referPrice}元</span> </div> <a href="javascript:{func}" class="del_btn right">删除</a> <div class="score right"><i class="price">{goodsPrice}</i><em>积分</em> </div> </li>';
	var nullcart = '<li id="nullcaritem" style="text-align: center;font:large;">购物车空空如也，去商城看看吧~！</li>';
	var totaldiv = '<div class="sumscore"> <span>积分共计：</span><i id="totalPrice">{totalPrice}</i><em>积分</em> </div> <div class="mt20 right"> <input type="button" id="sure_btn" class="sure_btn" value="确认兑换"></input> </div>';
	BaseUtil.get('eb/cart/listCart',function(data){
		if(data){
			var totalPrice = 0;
			for ( var key in data) {
				var item = data[key];
				var part = templete.replaceAll('{basePath}',basePath).replaceAll('{cartId}',item.id).replaceAll('{func}','removeCartItem('+item.id+')').replaceAll('{goodsId}',item.goodsId).replaceAll('{thumbnailUrl}',item.thumbnailUrl).replaceAll('{goodsName}',item.goodsName).replaceAll('{goodsPrice}',item.price).replaceAll('{referPrice}',NumberUtil.roundString(item.referPrice));
				html.push(part);
				totalPrice = NumberUtil.add(totalPrice,item.price);
			}
			if (html.length == 0) {
				$("#cartlist").html(nullcart);
				return;
			}
			$("#cartlist").html(html.join(''));
			$("#totaldiv").html(totaldiv.replaceAll('{totalPrice}',0));
			init();
			calc();
		}
	});
	window.removeCartItem = function(id){
		layer.confirm('确认要删除吗?', {
			btn : [ '确认', '取消' ]
		}, function(index) {
			BaseUtil.post('eb/cart/removeCart',function(data){
				if(data.success){
					$(".cartLi"+id).remove()
					if($(".cartLi").length>0){
						calc();
						layer.close(index);
						return;
					}
					layer.close(index);
					location.reload();
				}
			},{
				id:id
			});
		});
	}
	$("#allcheckspan").click(function(){
		var checkbox = $(this).find("#allcheck");
		if (checkbox.is(':checked')) {
			$(this).removeClass('checked');
			checkbox.prop('checked',false);
			$(".itemcheckspan").removeClass('checked');
			$(".itemcheck").prop('checked',false);
		}else{
			$(this).addClass('checked');
			checkbox.prop('checked',true);
			$(".itemcheckspan").addClass('checked');
			$(".itemcheck").prop('checked',true);
		}
		calc();
	});
	function init(){
		$(".itemcheckspan").click(function(){
			var checkbox = $(this).find(".itemcheck");
			if (checkbox.is(':checked')) {
				$(this).removeClass('checked');
				checkbox.prop('checked',false);
				$("#allcheckspan").removeClass('checked');
				$("#allcheck").prop('checked',false);
			}else{
				$(this).addClass('checked');
				checkbox.prop('checked',true);
				var items = $('.itemcheck'),allcheck = true;
				for (var i = 0; i < items.length; i++) {
					if(!$(items[i]).is(':checked')){
						allcheck = false;
					}
				}
				if(allcheck){
					$("#allcheckspan").addClass('checked');
					$("#allcheck").prop('checked',true);
				}
			}
			calc();
		});
	}
	function calc(){
		var items = $(".itemcheck"),goodsIds = [];
		for (var i = 0; i < items.length; i++) {
			var item = $(items[i]);
			if(item.is(':checked')){
				goodsIds.push(item.parents('li').attr('liid'));
			}
		}
		if (goodsIds.length == 0) {
			$("#sure_btn").css('background', 'lightgray');
			$("#sure_btn").attr('disabled', true);
			$("#sure_btn").val('未选择商品');
			$('#totalPrice').text(0);
			return;
		}
		BaseUtil.get('eb/cart/calc', function(result) {
			if(result.success){
				var data = result.data;
				var totalPrice = data.totalPrice
				$('#totalPrice').text(totalPrice);
				if (totalPrice <= data.totalPoint) {
					$("#sure_btn").css('background','#F98322');
					$("#sure_btn").attr('disabled',false);
					$("#sure_btn").val('确认兑换');
					return;
				}
				$("#sure_btn").css('background', 'lightgray');
				$("#sure_btn").attr('disabled', true);
				$("#sure_btn").val('积分不足');
				return;
			}
			layer.msg(result.msg);
		}, {
			goodsIds : goodsIds.join(',')
		});
	}
	$(document).on('click',"#sure_btn",function(){
		var goodsIds = [];
		var items = $(".itemcheck"),goodsIds = [],cartIds = [];
		for (var i = 0; i < items.length; i++) {
			var item = $(items[i]);
			if(item.is(':checked')){
				var li =item.parents('li');
				goodsIds.push(li.attr('liid'));
				cartIds.push(li.attr('cartid'));
			}
		}
		if (cartIds.length == 0) {
			layer.msg('还未选择商品,请先选择')
			return;
		}
		CartUtil.submit(goodsIds.join(','),cartIds.join(','));
	});
});