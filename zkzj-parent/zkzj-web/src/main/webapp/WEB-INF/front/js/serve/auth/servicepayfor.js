require.async(['JqueryBase','UrlUtil','BespeakEnum'],function(window){
	
var ordno = UrlUtil.getUrlParam("ordno");
$("#orderno").html("订单号：" + ordno);
$("#paymethod").val('');
$("#code").val(''); 
window.order = function(valid){ 

	layer.load(2);//加载中 
	BaseUtil.get("eb/bespeak/getBespeakOrder",function(result){ 
		layer.closeAll('loading');//关闭加载中
		if(result.data){
			if(result.data.bespeakStatus == BespeakEnum.CANCEL.getValue()){
				$("#payfor").hide();
				layer.msg("订单已过期");
				return;
			}
			if(valid && result.data.bespeakStatus == BespeakEnum.PAYSUCCESS.getValue()){
				location.href = basePath + '/serve/servicepaymsg.html';
			}
			if(NumberUtil.roundString(result.data.totalPrice) <= 0 ){//支付金额为0时，仅限余额支付
				$("#li_wx").hide(); 
				$("#li_apay").hide();    
				$("#balance").click();
			} 
			$("#servicesbos").show();
			$("#totalPrice").html(NumberUtil.roundString(result.data.totalPrice));
		} else {
			layer.msg('订单['+ordno+']信息异常', {icon: 5}); 
		}
		
	},{"bespeakNo":ordno}); 
}
order(false);
window.payfor = function(){ 
	var method = $("#payul .current").find("input")[0].value;
	if(ValidUtil.isBlank(method)){
		layer.msg("请选择支付方式");
		return;
	}
	if(method == 0) {//余额支付  
		openAccountPay();
	} else if(method == 1) {//支付宝支付
		layer.msg("非常抱歉，支付宝支付尚未开通，请使用其他方式支付", {icon: 5});
	} else if(method == 2) {//微信支付 
		openWechatPay(); 
	}
} 

window.openAccountPay = function(){   
	BaseUtil.get("crm/cashAccount/getPointAccountBalance",function(result){ 
		
		if(parseFloat(result) >= parseFloat($("#totalPrice").html())){ 
			layer.prompt({title: '请输入支付密码', formType: 1,btn: ['支付', '取消']}, function(code, index){
				if(ValidUtil.isBlank(code)){   
					layer.msg("输入支付密码");
					return;
				}
				BaseUtil.post("eb/bespeak/payBespeak",function(result){ 
					if(result.success){ 
						layer.close(index); 
						location.href = basePath + '/serve/servicepaymsg.html';
					} else {  
						if(result.code == 'account_pay_pwd_null'){
							layer.confirm('抱歉，您未设置支付密码，现在去设置 ?', {
								  btn: ['去设置','算了'] //按钮
								}, function(){ 
									location.href = basePath + '/user/auth/countsafe.html';
								}, function(){ 
								}
							); 
						} else {
							layer.msg(result.msg, {icon: 5});
						}
					} 
				},{"orderno":ordno,"paypsw":code}); 
			}); 
		} else {  
			layer.confirm('抱歉，您的账户余额不足', {
				  btn: ['去充值','使用其他支付方式'] //按钮
				}, function(){ 
					location.href = basePath + '/search/searchlist.html?location=recharge';
				}, function(){
				}
			);
		}
	},{}); 
}
window.openWechatPay = function(){   
	BaseUtil.get("pay/wechat/getQrCode",function(result){ 
		if(result.success){
			layer.open({//页面层
				title:['微信扫码支付预约服务订单', 'text-align: center'],
				type: 1,
				skin: 'layui-layer-rim', //加上边框  
				area: ['420px', '340px'], //宽高   
				closeBtn: 0,//不显示右上角关闭
				content: '<div id="qrcode" style="width:100px; height:100px; margin-top:15px;;margin-left: 100px"></div>',
				btn: ['支付完成', '关闭'] //只是为了演示
			,yes: function(){
				order(true);
				layer.closeAll();
			}
			,btn2: function(){
				order(true);
				layer.closeAll();
			}
			});  
			var qrcode = new QRCode(document.getElementById("qrcode"), {
				width : 200,
				height : 200
			});  
			qrcode.makeCode(result.data); 
		} else { 
			layer.msg(result.msg);
		}
	},{
		"orderNo":ordno
	});
}

});   

