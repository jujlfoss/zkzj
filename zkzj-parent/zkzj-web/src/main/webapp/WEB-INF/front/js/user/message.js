require.async(['JqueryBase','PageUtil','DateUtil','jquery.lazyload','jquery.tabs.min'],function(){
	
	listMessage(1);
	function listMessage(pageindex){
		
		var limit = 3;
		BaseUtil.get("eb/message/webListMessage",function(result){
			$("#message-div").html(''); 
			var result = result.data;
			var li='<div class="messagelist mt20"><h3 class="title">{title}<span>{createTime}</span><i class="del" onclick="del({id})"></i></h3><div class="content mt10">{content}</div></div>';
			
			var html = []; 
			$.each(result.items,function(idx,item){
				html.push(li.replaceAll("{title}",item.title).replaceAll("{createTime}",DateUtil.timeToString(item.createTime,DateUtil.TIME)).replaceAll("{content}",item.content).replaceAll("{id}",item.id));
			});   
			$("#message-div").append(html.join(''));   
			//分页信息
			if(result.items.length>0){
				$("#pagediv").html(PageUtil.getHtml(result.total,pageindex,3,limit)); 
			}else{
				 $("#pagediv").html("<span color='red' style='align-content: center'>暂无数据</span>"); 
			}
		},{'start':(pageindex - 1) * limit,'limit':limit});     
	} 
	

	window.page=function (pageindex){  
		listMessage(pageindex);
	}
	window.del=function(id){
		layer.confirm("是否删除该消息",{},function(yes){
			BaseUtil.post("eb/message/removeMessage", function(data) {
				layer.msg(data.msg);
				listMessage(1);
			}, {
				"id" : id
			})
		})
	}
	 $("#li3").addClass("current");
});