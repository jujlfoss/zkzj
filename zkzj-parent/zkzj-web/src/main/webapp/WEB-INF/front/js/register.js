require.async([ 'Constant', 'ValidUtil','xieyi'], function() {
	$("#xieyiA").click(function(){
		zhucexieyi('800px','600px');
	});
	$("#falvA").click(function(){
		falvshengming('800px','600px');
	});
	// 注册
	$("#register-logindiv [type=submit]").click(function() {
		var mobile = ValidUtil.trim($("#register-signup-username").val());
		if (!ValidUtil.isMobile(mobile)) {
			layer.msg('手机号码格式不正确！')
			return;
		}
		var userPwd = ValidUtil.trim($("#register-signup-password").val());
		if (!userPwd) {
			layer.msg('密码不能为空！')
			return;
		}
		var checkUserPwd = ValidUtil.trim($("#register-signup-password-re").val());
		if (userPwd !== checkUserPwd) {
			layer.msg('确认密码不正确！')
			return;
		}
		var mobileCode = ValidUtil.trim($("#register-signup-sms").val());
		if (!mobileCode) {
			layer.msg('短信验证码不能为空！')
			return;
		}
		BaseUtil.post('register', function(data) {
			if (data.success) {
				layer.msg('注册成功',{},function(){
					location.href = basePath + '/index.html';
				});
				return;
			}
			layer.msg(data.msg);
		}, {
			mobile : mobile,
			userPwd : userPwd,
			mobileCode : mobileCode,
			referrerMobile : ValidUtil.trim($("#register-signup-referrerMobile").val()),
			checkUserPwd : checkUserPwd
		});

	});

	var wait = 60;
	document.getElementById("register-smsBtn").disabled = false;
	document.getElementById("register-smsBtn").onclick = function() {
		var mobile = ValidUtil.trim($("#register-signup-username").val());
		if (!ValidUtil.isMobile(mobile)) {
			layer.msg('手机号码格式不正确！')
			return;
		}
		BaseUtil.post('regisCode',function(data){
			if(data.success){
				layer.msg('验证码发送成功！')
				time(document.getElementById('register-smsBtn'));
				return;
			}
			layer.msg(data.msg)
			return;
		},{
			mobile:mobile
		});
	}
	function time(o) {
		if (wait == 0) {
			$(".btn_mfyzm2").css("background", "#F98322");
			$(".btn_mfyzm2").css("color", "#fff");
			o.removeAttribute("disabled");
			o.value = "免费获取验证码";
			wait = 60;
		} else {
			$(".btn_mfyzm2").css("background", "#CCCCCC");
			$(".btn_mfyzm2").css("color", "#999");
			o.setAttribute("disabled", true);
			o.value = "重新发送(" + wait + ")";
			wait--;
			setTimeout(function() {
				time(o)
			}, 1000)
		}
	}
});