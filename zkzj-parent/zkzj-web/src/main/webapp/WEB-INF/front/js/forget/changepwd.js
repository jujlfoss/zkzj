require.async([ 'Constant', 'ValidUtil' ], function() {
	// 注册
	$("#register-logindiv [type=submit]").click(function() {
		var self = $(this);
		var userPwd = ValidUtil.trim($("#register-signup-password").val());
		if (!userPwd) {
			layer.msg('新密码不能为空！')
			return;
		}
		var checkUserPwd = ValidUtil.trim($("#register-signup-password-re").val());
		if (userPwd !== checkUserPwd) {
			layer.msg('确认密码不正确！')
			return;
		}
		self.attr('disabled',true);
		BaseUtil.post('web/front/changePwd', function(data) {
			if (data.success) {
				layer.msg('修改成功，正在跳转至登录...', {}, function() {
					location.href = basePath + '/login.html';
				});
				return;
			}
			self.attr('disabled',false);
			layer.msg(data.msg);
		}, {
			userPwd : userPwd,
			checkUserPwd : checkUserPwd
		});

	});
});