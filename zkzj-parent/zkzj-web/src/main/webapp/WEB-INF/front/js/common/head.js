require.sync(['jquery.SuperSlide2','main','city-data','hzw-city-picker.min']);
/******************定位**********************/
// 初始化获取定位信息
(function init() {
	// BaseUtil.get("eb/area/front/pageArea",function(result){
	// 	var ccCode = Constant.CITY_CODE_DEFAULT,ccName = Constant.CITY_NAME_DEFAULT;//默认城市
	// 	var areaData = result.data;
	// 	getCityList(areaData.items);//加载合作城市
	//
	// 	var code = CookieUtil.get(Constant.CITY_CODE), name = CookieUtil
	// 			.get(Constant.CITY_NAME);
    //
	// 	if (code && name) {
	// 		changeCity(code, name, false);
	// 	} else {
	// 		new BMap.LocalCity().get(function(result) {
	// 			var map = new BMap.Map("allmap");
	// 			//定位城市中包含则选中，否则选中无锡
	// 			$.each(areaData.items,function(idx,item){
	// 				if(result.name == item.name && result.code == item.id){//定位城市存在合作城市列表中，选中
	// 					ccCode = result.code;
	// 					ccName = result.name;
	// 					CookieUtil.set(Constant.CITY_LON, result.center.lng); // 存储 cookie
	// 					CookieUtil.set(Constant.CITY_LAT, result.center.lat); // 存储 cookie
	// 				}
	// 			});
	// 			changeCity(ccCode, ccName, false);
	// 		});
	// 	}
	// });
	// 加载城市选择列表
	// function getCityList(hot) {
	// 	cityData.hot = hot;
	// 	new HzwCityPicker({
	// 		data : cityData,
	// 		target : 'cityChoice',
	// 		valType : 'k-v',
	// 		hideCityInput : {
	// 			name : 'cityHidden',
	// 			id : 'cityHidden'
	// 		},
	// 		hideProvinceInput : {
	// 			name : 'provinceHidden',
	// 			id : 'provinceHidden'
	// 		},
	// 		callback : function(a) {// /回调切换城市
	// 			var code = $("#cityHidden").val().split("-")[0];
	// 			var name = $("#cityHidden").val().split("-")[1];
	// 			changeCity(code, name, true);
	// 		}
	// 	}).init();
	// }
	// function changeCity(code, name, flag,lon,lat) {
	// 	CookieUtil.set(Constant.CITY_CODE, code); // 存储 cookie
	// 	CookieUtil.set(Constant.CITY_NAME, name); // 存储 cookie
	// 	$("#currCity a").html(name);
	// 	if (flag) {
	// 		location.reload();
	// 	}
	// }
})();
require.async([ 'Constant', 'ValidUtil', 'JSEncrypt' ], function() {
	/*****************登录注册*********************/
	var changeImageCode = function(mobile){
		$("#imageCodeImg").attr("src",basePath+"/imageCode?mobile="+mobile+"&dc="+Math.random());
	}
	var bindImageCodeClick = function(mobile){
        $("#imageCodeImg").unbind("click");
        $("#imageCodeImg").bind("click", function(){
        	changeImageCode(mobile);
        });
	}
	// 登录
	$("#cd-login [type=submit]").click(function() {
		var userName = ValidUtil.trim($("#signin-username").val());
		if (!userName) {
			layer.msg('手机号码不能为空！')
			return;
		}
		var userPwd = ValidUtil.trim($("#signin-password").val());
		if (!userPwd) {
			layer.msg('密码不能为空！')
			return;
		}
		var imageCode = null;
		if($("#imageCodeP:hidden").length<1){
			imageCode = ValidUtil.trim($("#imageCode").val());
			if (!imageCode) {
				layer.msg('验证码不能为空！')
				return;
			}
		}
		var encrypt = new JSEncrypt();
		encrypt.setPublicKey(BaseUtil.getTextData(Constant.URL_PUBLICKEY));
		userPwd = encrypt.encrypt(userPwd);
		if (!userPwd) {
			$("#signin-password").val('');
			layer.msg('您的操作太快了,请重试！');
			return;
		}
		$("#signin-password").val(userPwd);
		BaseUtil.post('login', function(data) {
			if (data.success) {
				location.reload();
				return;
			}
			$("#signin-password").val('');
			if(data.code=="pwd_error_too_many"){
				layer.msg("请输入图片验证码");
				bindImageCodeClick(userName);
				changeImageCode(userName);
				$("#imageCode").val('');
				$("#imageCodeP").show();
				return;
			}
			if($("#imageCodeP:hidden").length>0){
				bindImageCodeClick(userName);
				changeImageCode(userName);
			}
			layer.msg(data.msg);
		}, {
			userName : userName,
			userPwd : userPwd,
			remember : false,
			imageCode : imageCode
		});

	});

	// 注册
	$("#cd-signup [type=submit]").click(function() {
		var mobile = ValidUtil.trim($("#signup-username").val());
		if (!ValidUtil.isMobile(mobile)) {
			layer.msg('手机号码格式不正确！')
			return;
		}
		var userPwd = ValidUtil.trim($("#signup-password").val());
		if (!userPwd) {
			layer.msg('密码不能为空！')
			return;
		}
		var checkUserPwd = ValidUtil.trim($("#signup-password-re").val());
		if (userPwd !== checkUserPwd) {
			layer.msg('确认密码不正确！')
			return;
		}
		var mobileCode = ValidUtil.trim($("#signup-sms").val());
		if (!mobileCode) {
			layer.msg('短信验证码不能为空！')
			return;
		}
		BaseUtil.post('register', function(data) {
			if (data.success) {
				layer.msg('注册成功', {icon: 1},function(){
					location.href = basePath + '/index.html';
				});
				return;
			}
			layer.msg(data.msg);
		}, {
			mobile : mobile,
			userPwd : userPwd,
			mobileCode : mobileCode,
			referrerMobile : ValidUtil.trim($("#signup-referrerMobile").val()),
			checkUserPwd : checkUserPwd
		});

	});

	var wait = 60;
	document.getElementById("smsBtn").disabled = false;
	document.getElementById("smsBtn").onclick = function() {
		var mobile = ValidUtil.trim($("#signup-username").val());
		if (!ValidUtil.isMobile(mobile)) {
			layer.msg('手机号码格式不正确！')
			return;
		}
		BaseUtil.post('regisCode',function(data){
			if(data.success){
				layer.msg('验证码发送成功！')
				time(this);
				return;
			}
			layer.msg(data.msg)
			return;
		},{
			mobile:mobile
		});
	}
	function time(o) {
		if (wait == 0) {
			$(".btn_mfyzm").css("background", "#F98322");
			$(".btn_mfyzm").css("color", "#fff");
			o.removeAttribute("disabled");
			o.value = "免费获取验证码";
			wait = 60;
		} else {
			$(".btn_mfyzm").css("background", "#CCCCCC");
			$(".btn_mfyzm").css("color", "#999");
			o.setAttribute("disabled", true);
			o.value = "重新发送(" + wait + ")";
			wait--;
			setTimeout(function() {
				time(o)
			}, 1000)
		}
	}
});
