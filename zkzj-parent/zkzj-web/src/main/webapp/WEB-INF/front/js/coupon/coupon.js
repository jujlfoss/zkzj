require.async(['JqueryBase','PageUtil','jquery.lazyload','jquery.tabs.min','UserdEnum','DateUtil'],function(){
	
	listCoupon(1,0,"coupon-ul",null,"available");
	//优惠卷
	function listCoupon(pageindex,isUsed,cid,overdue,available){
		var limit = 12;
		BaseUtil.get("eb/coupon/webPageCoupon",function(result){
			var result = result.data;
			var data = result.items;
			var li="";
			for ( var attr in data) {
				 li+="<li><div class='yhq_img'><div class='title'>"+data[attr].couponTypeName+"</div>" +
						"<div class='price'>"+data[attr].sellPrice+" 积分</div></div><div class='yhq_txt'><p>• 使用说明："+data[attr].useExplain+"。</p>" +
								"<p>• 有效日期至 "+DateUtil.timeToString(data[attr].validityDate,DateUtil.DATE)+"</p>";
				 if(isUsed==0&&available=='available'&&data[attr].couponTypeId!=1){
					var url=basePath+"/serve/servicebespeak.html?id="+data[attr].serveId
					li+="<a class='btn_a' href='"+url+"'>立即使用</a></div></li>";
				 }else{
					li+="</div></li>" 
				 }
			}
//			var li="<li><div class='yhq_img'><div class='title'>{couponTypeName}</div><div class='price'>{sellPrice} 积分</div></div><div class='yhq_txt'><p>• 使用说明：{useExplain}。</p><p>• 有效日期至 {validityDate}</p><a class='btn_a' href=''>立即使用</a></div></li>";
//			var html = []; 
//			$.each(result.items,function(idx,item){
//				html.push(li.replaceAll("{couponTypeName}",item.couponTypeName).replaceAll("{sellPrice}",item.sellPrice).replaceAll("{useExplain}",item.useExplain).replaceAll("{validityDate}",DateUtil.timeToString(item.validityDate,DateUtil.DATE)));
//			});   
			$("#"+cid).html(li);   
			//分页信息
			if(result.items.length>0){
				$("#pagediv").html(PageUtil.getHtml(result.total,pageindex,12,limit)); 
			}else{
				 $("#pagediv").html("<span color='red' style='align-content: center'>暂无数据</span>"); 
			}
		},{'start':(pageindex - 1) * limit,'limit':limit,'isUsed':isUsed,'overdue':overdue,"available":available});     
	} 
	

	window.page=function (pageindex){ 
		var status = $("#status").val();
		var cid = $("#appendId").val();
		var available = $("#available").val();
		var overdue  = $("#overdue").val();
		listCoupon(pageindex,$("#status").val(),$("#appendId").val(),$("#overdue").val(),$("#available").val());
	}
	$("#nouser").click(function(){
		$("#status").val(0);
		$("#appendId").val("coupon-ul");
		$("#available").val("available");
		$("#overdue").val("");
		listCoupon(1,0,"coupon-ul",null,"available");
		
	})
		
	$("#user").click(function(){
		$("#status").val(1);
		$("#appendId").val("coupon-ul1");
		$("#available").val("");
		$("#overdue").val("");
		listCoupon(1,1,"coupon-ul1",null,null);
		
	})
		$("#pass").click(function(){
			$("#status").val(0);
			$("#appendId").val("coupon-ul2");
			$("#available").val("");
			$("#overdue").val("overdue");
		listCoupon(1,0,"coupon-ul2","overdue",null);
	})
		
		$("img[original]").lazyload({ placeholder:"images/color3.gif" });
		
		$('.demo1').Tabs();
		$('.demo2').Tabs({
			event:'click'
		});
		$('.demo3').Tabs({
			timeout:300
		});
		$('.demo4').Tabs({
			auto:3000
		});
		$('.demo5').Tabs({
			event:'click',
			callback:lazyloadForPart
		});
		//部分区域图片延迟加载
		function lazyloadForPart(container) {
			container.find('img').each(function () {
				var original = $(this).attr("original");
				if (original) {
					$(this).attr('src', original).removeAttr('original');
				}
			});
		}
		 $("#li5").addClass("current");
});