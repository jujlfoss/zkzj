<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/assets/config/jquery-config.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>重卡之家 - 购物车</title>
<style type="text/css">
.proimg img{
	width:100%;
	height:100%;
}
</style>
</head>
<body>
	<jsp:include page="/WEB-INF/front/views/common/head.jsp" />
	<div class="container wd clearfix ">
		<div class="location mt20">
			<a href="<%=basePath%>/index.html">首页</a> > <a href="<%=basePath%>/search/searchlist.html?location=point">积分商城</a> > <a href="javascript:location.reload()">购物车</a>
		</div>
		<div class="servicesbox mt30">
			<h3>
				<span id="allcheckspan" class="checkboxcss checked mt5"><input type="checkbox" checked="checked" hidden="true" id="allcheck" /></span>全部商品
			</h3>
			<div class="orderinfo p20 border clearfix">
				<ul id="cartlist">
				</ul>
				<div class="mt20 right" id="totaldiv" ></div>
			</div>
		</div>

		<div class="box05 clearfix mt30">
			<div class="wzcx_banner" onclick="javascript:location.href='http://www.dongfang789.com/productCenter.html'">
				<img src="<%=basePath %>/front/images/obd11.png">
			</div>
		</div>
		<div class="box05 clearfix mt30">
			<div class="wzcx_banner" onclick="javascript:location.href='http://www.dongfang789.com/productCenter.html'">
				<img src="<%=basePath %>/front/images/gps11.png">
			</div>
		</div>
	</div>
	<jsp:include page="/WEB-INF/front/views/common/foot.jsp" />
	<script src="<%=basePath%>/front/js/point/auth/cart.js"></script>
</body>
</html>
