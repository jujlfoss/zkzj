\<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/assets/config/jquery-config.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>APP下载</title>
<style>
* {
	margin: 0;
	padding: 0;
}

a, a:hover {
	text-decoration: none;
}

.downloadbox {
	width: 1000px;
	margin: 100px auto 0;
}

.phonebox {
	width: 430px;
	float: left;
}

.phonebox img {
	width: 100%;
}

.download {
	float: right;
	margin-top: 100px;
}

.download h3 {
	font-size: 48px;
	font-weight: normal;
	color: #f98322;
	margin-bottom: 20px;
}

.clear {
	clear: both;
}

.box01 {
	float: left;
	margin-right: 20px;
	width: 200px;
}

.imgbox img {
	width: 100%;
}

.imgbox {
	width: 200px;
	height: 200px;
}

.download_btn {
	display: block;
	margin-top: 20px;
	width: 200px;
	height: 54px;
	line-height: 54px;
	border-radius: 5px;
	color: #fff;
	text-align: center;
	font-size: 30px;
	background: #06a6f9;
}
</style>
</head>
<body>
	<jsp:include page="/WEB-INF/front/views/common/head.jsp" />
	<div class="downloadbox ">
		<div class="phonebox">
			<img src="<%=basePath%>/front/images/phone-new.png">
		</div>
		<div class="download">
			<h3>不用排队的上门洗车</h3>
			<div class="clear"></div>
			<div class="box01">
				<div class="imgbox">
					<img src="<%=basePath%>/front/images/ewm.png">
				</div>
				<a class="download_btn" href="#">iphone下载</a>
			</div>
			<div class="box01">
				<div class="imgbox">
					<img src="<%=basePath%>/front/images/ewm.png">
				</div>
				<a class="download_btn" href="#">android下载</a>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</body>
</html>
