<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/assets/config/jquery-config.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>重卡之家</title>
	</head>
	<style type="text/css">
  #coupon-ul li,#coupon-ul1 li,#coupon-ul2 li {
	margin-right: 3px;
}
</style>
	
	<body>
		<!--弹出登录-->
	<jsp:include page="/WEB-INF/front/views/common/head.jsp" />
	<jsp:include page="/WEB-INF/front/views/common/search.jsp" />	  
	
	<div class="container mtb20 clearfix wd">
		<jsp:include page="/WEB-INF/front/views/common/left.jsp" />	
		<div class="count_content right border p20">			 
			<div class="countbox mt20">
				<h3 class="title">优惠券</h3>
				<div class="box demo2 mt20">
				<ul class="tab_menu">
					<li class="current" id="nouser">未使用</li>
					<li id="user">已使用</li>
					<li id="pass">已过期</li> 
				</ul>
				<input type="hidden" id="status" value=""/>
				<input type="hidden" id="appendId"/>
				<input type="hidden" id="available" value="available"/>
				<input type="hidden" id="overdue"/>
				<div class="tab_box">
					<div class="yhq unused clearfix">
			 	 	 <ul class="u1" id="coupon-ul">
			 	 		<!--<li>
			 	 			<div class="yhq_img">
			 	 				<div class="title">30元打蜡抵扣券</div>
			 	 				<div class="price">3000 积分</div>
			 	 			</div>
			 	 			<div class="yhq_txt">
			 	 				<p>• 使用说明：该抵扣券仅供打蜡洗车服务使用，不抵用现金，不退不换。</p>					
								<p>• 有效期：2017/06/23—2017/07/23</p>
			 	 			 
			 	 				<a class="btn_a" href="">立即兑换</a>
			 	 			</div>
			 	 		</li> -->
			 	 	</ul>
 	                </div> 
					<div class="yhq hide used clearfix">
			 	 	<ul id="coupon-ul1">
			 	 		<!-- <li>
			 	 			<div class="yhq_img">
			 	 				<div class="title">30元打蜡抵扣券</div>
			 	 				<div class="price">3000 积分</div>
			 	 			</div>
			 	 			<div class="yhq_txt">
			 	 				<p>• 使用说明：该抵扣券仅供打蜡洗车服务使用，不抵用现金，不退不换。</p>					
								<p>• 有效期：2017/06/23—2017/07/23</p>
			 	 			 
			 	 				<a class="btn_a" href="">立即兑换</a>
			 	 			</div>
			 	 		</li>
			 	 		 -->
			 	 	</ul>
 	                </div> 
					<div class="yhq hide expired clearfix">
			 	 	<ul id="coupon-ul2">
			 	 		<!-- <li>
			 	 			<div class="yhq_img">
			 	 				<div class="title">30元打蜡抵扣券</div>
			 	 				<div class="price">3000 积分</div>
			 	 			</div>
			 	 			<div class="yhq_txt">
			 	 				<p>• 使用说明：该抵扣券仅供打蜡洗车服务使用，不抵用现金，不退不换。</p>					
								<p>• 有效期：2017/06/23—2017/07/23</p>
			 	 			 
			 	 				<a class="btn_a" href="">立即兑换</a>
			 	 			</div>
			 	 		</li> -->
			 	 	</ul>
 	                </div> 		
 	               	 
				</div>
			</div>
			</div>
			 <div id="pagediv"></div> 
		</div>
	</div>
 <jsp:include page="/WEB-INF/front/views/common/foot.jsp" />
 <script src="<%=basePath%>/front/js/coupon/coupon.js"></script>
	</body>
</html>
