<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/assets/config/jquery-config.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>重卡之家</title>
		<style type="text/css">
			#pay a{
				display:inline;
				color: #fff;
				background: #F98322;
				border-radius: 5px;
				padding:12px;
				margin-left:5px;
			}
			#pay{
				text-align:center;
			}
		</style>
	</head>
	<jsp:include page="/WEB-INF/front/views/common/head.jsp" />
	<jsp:include page="/WEB-INF/front/views/common/search.jsp" />	
	<div class="container mtb20 clearfix wd">
		<jsp:include page="/WEB-INF/front/views/common/left.jsp" />	
		<div class="count_content right border p20">
			<div class="countbox">
				<h3 class="title">订单详情</h3>
				<div class="schedule">
					<div class="step1" id="step1">
						<div class="step01" id="c1" >待支付</div>
						<div class="step01" id="c2">已支付</div>
						<div class="step01" id="c3">已接单</div>
						<div class="step01" id="c4">已完成</div>
					<!-- 	<div class="step01" id="c5">员工已删除</div> -->
					</div>
				</div>
			</div>
			<div class="countbox mt20"> 
				<div class="detail_list mt20">
					<h3>联系人信息</h3>
				    <div class="" id="customer">
				    	<!-- <p><span class="name">联系人:</span><span>李先生</span></p>
				    	<p><span class="name">手机号码:</span><span>李先生</span></p>
				    	<p><span class="name">车牌号:</span><span>李先生</span></p> -->
				    </div>
				</div>
				<div class="detail_list mt20">
					<h3>预约信息</h3>
				     <div class="" id="order">
				    	<!-- <p><span class="name">洗车项目:</span><span>精致洗车套餐</span></p>
				    	<p><span class="name">洗车服务点:</span><span>凯悦华庭小区服务点</span></p>
				    	<p><span class="name">服务点地址:</span><span>四川省成都市高新区天府大道中段天府</span></p> -->
				    </div>
				</div>
				<div class="detail_list mt20">
					<h3>消费信息</h3>
				    <div class="detail_list" id="price">
				    	<!-- <p><span class="name">服务总价:</span><span>¥380.00</span></p>
				    	<p><span class="name">优惠券:</span><span>¥7.00</span></p>
				    	<p><span class="name">订单总价:</span><span>¥373.00</span></p>
				    	<p><span class="name">实付款:</span><span>¥0.00</span></p> -->
				    </div>
				</div>
				 <div id="img" style="margin: 0 auto">
			    
			  
			  </div>
				<div class="detail_list mt20" id="pay">
					
				</div>
				<div class="detail_list mt20" id="back" style="float: left">
					
				</div>
			 
				
			</div>
		</div>
	</div>
  <jsp:include page="/WEB-INF/front/views/common/foot.jsp" />
   <script src="<%=basePath%>/front/js/user/myorderdetail.js"></script>
  
	</body>
</html>
