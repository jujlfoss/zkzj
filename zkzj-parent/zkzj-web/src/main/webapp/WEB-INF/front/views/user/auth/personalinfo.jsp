<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/assets/config/jquery-config.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>重卡之家</title>
	</head>
	
	<body>
    <jsp:include page="/WEB-INF/front/views/common/head.jsp" />
	<jsp:include page="/WEB-INF/front/views/common/search.jsp" />	  
	<div class="container mtb20 clearfix wd">
		<jsp:include page="/WEB-INF/front/views/common/left.jsp" />	
		<div class="count_content right border p20">
			<div class="countbox">
				<h3 class="title">个人资料</h3>
				<div class="schedule">
					<div class="step1">
						
					</div>
				</div>
			</div>
			<div class="countbox mt20"> 
				<div class="detail_list mt20">
					<h3>头像信息</h3>
				     <div class="">
				        <div class="con4">							
							<div class="btns">
								 <div id="preview"><img id="image" height="120px" width="120px" src=""/></div>  
								<span class="btn upload">上传头像<input type="file" onchange="preview(this)" name="files" class="upload_pic" id="upload" /></span>
								<a onclick="upload()" class="a_btn right">保存</a>
							</div>
							
						</div>
				     </div>
				</div>
				<div class="detail_list mt20">
					<h3>基本信息</h3>
				    <div class="">
				    	<p><span class="name">账号:</span><span><input type="text" id="userMobile" class="inputcss" readonly="readonly" style="height:32px;line-height:32px;"></span><span><a id="updateCustomer" class="modify_a">修改(点修改进行编辑)</a></span></p>
				        <p><span class="name">昵称:</span><span><input type="text" id="customerName" readonly="readonly" class="inputcss" style="height:32px;line-height:32px;"></span>
				        <span ><a  id="update" class="modify_a">修改(点修改进行编辑)</a></span>
				        <span   id="sp">  <a style="display: none" id="determine" class="modify_a">确定</a></span>
				        <span ><a style="display: none" id="cancel" class="modify_a" >取消</a></span></p>
				    </div>
				</div>
				<div class="detail_list mt20">
					<h3>常用联系人<span class="addmen right"><i></i>新增联系人</span></h3>
				    <table class="contact_table mt20" style="width: 100%" id="cartab">
				    	<tr><th>姓名</th><th>手机号</th><th>车牌号</th><th>操作</th></tr>
				    	<!-- <tr><td>张三</td><td>15825876958</td><td>沪 C45673</td><td><a class="edit_a">编辑</a><a class="del_a">删除</a> </td></tr>
				    	<tr><td>张三</td><td>15825876958</td><td>沪 C45673</td><td><a class="edit_a">编辑</a><a class="del_a">删除</a> </td></tr>
				    	<tr><td>张三</td><td>15825876958</td><td>沪 C45673</td><td><a class="edit_a">编辑</a><a class="del_a">删除</a> </td></tr>
				    	<tr><td>张三</td><td>15825876958</td><td>沪 C45673</td><td><a class="edit_a">编辑</a><a class="del_a">删除</a> </td></tr> -->
 
				    </table>
				     <div id="pagediv"></div> 	 
				</div>				 
			</div>
		</div>
	</div>
<jsp:include page="/WEB-INF/front/views/common/foot.jsp" />
 <script src="<%=basePath%>/front/js/user/personalinfo.js"></script>
 <script src="<%=basePath%>/front/js/user/ajaxfileupload.js"></script>
 <script>
//获取上传按钮

</script> 
	</body>
</html>
