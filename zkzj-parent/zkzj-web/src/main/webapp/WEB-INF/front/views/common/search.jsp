<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%String serachBasePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();%>
<div class="banner mt20">
	<div class="wd position-r">
		<div class="searchbox clearfix">
			<form id="searchform" onsubmit="return searchformsubmit()">
			<input type="text" id="search_input_keyword" class="search_input left" /> 
			<a href="javascript:searchRedirect()" class="search_btn left"></a>
			<input type="submit" style="display: none;" /> 
			</form>
		</div>
		<div onclick="javascript:location='<%=serachBasePath%>/point/auth/cart.html'" class="searchbox clearfix" style="position: absolute;right:15px;background:white;color:#F98322;line-height:32px;padding-right:5px;font-weight:bold;cursor: pointer;">
			<a href="javascript:;" class="gwc_btn left"></a>(<span id="cartCount">0</span>)
		</div>
	</div>
</div>
<script>
function searchRedirect(){
	$('#searchform').submit();
}
function searchformsubmit(){
	location.href = '<%=serachBasePath%>/search/searchlist.html?keyword='+$("#search_input_keyword").val();
	return false;
}
require.async('UrlUtil',function(){
	$("#search_input_keyword").val(decodeURI(UrlUtil.getUrlParam('keyword')));
});
if (hasCustomerRole) {
	BaseUtil.get('eb/cart/getCount', function(data) {
		if (data.success) {
			$('#cartCount').text(data.data);
		}
	});
}
</script>