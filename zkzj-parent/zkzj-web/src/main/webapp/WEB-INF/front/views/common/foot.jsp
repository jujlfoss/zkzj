<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%String footBasePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();%>
<div class="footbox mt30 clearfix">
 	 <div class="wd clearfix">
 	 	<dl class="linklist" style="margin-left:0">
 	 		<dt>售后服务</dt>
 	 		<dd><a href="<%=footBasePath %>/about/falv.html">法律声明</a></dd>
 	 	</dl>
 	 	<dl class="linklist">
 	 		<dt>关于我们</dt>
 	 		<dd><a href="<%=footBasePath %>/about/useragreement.html">用户协议</a></dd>
 	 	</dl>
		<dl class="linklist">
 	 		<dt>联系我们</dt>
 	 		<dd><a href="<%=footBasePath %>/about/me.html">联系我们</a></dd>
		</dl>
 	 	
 	 	<div class="ewm right">
 	 		<img src="<%=footBasePath%>/front/images/ewm.png">
 	 	</div>
 	 </div>
 </div>