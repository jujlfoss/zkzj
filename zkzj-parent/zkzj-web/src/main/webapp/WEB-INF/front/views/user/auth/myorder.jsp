<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/assets/config/jquery-config.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>重卡之家</title>
	</head>
	<style>
</style>
	<body>
		<!--弹出登录-->
	<jsp:include page="/WEB-INF/front/views/common/head.jsp" />
	<jsp:include page="/WEB-INF/front/views/common/search.jsp" />	  
	<div class="container mtb20 clearfix wd">
		<jsp:include page="/WEB-INF/front/views/common/left.jsp" />	
		<div class="count_content right border p20">
			<div class="countbox">
				<h3 class="title">全部订单</h3>
				<ul class="order_ul clearfix mt20">
					<li class="current" onclick="getList(1)">
						<i></i>洗车服务订单
					</li>
					<li onclick="getList(2)">
						<i></i>积分兑换订单
					</li> 
				</ul>
			</div>
			<div class="countbox mt20"> 
				<div class="box demo2 mt20">
				<ul class="tab_menu" >
					<li class="current" id="paywait">未完成</li>
					<li id="finsh" >已完成</li> 
					<li class="current" id="paywait1" style="display: none">未完成</li>
					<li id="finsh1" style="display: none" >已完成</li> 
				</ul>
				<input type="text" value="1"  hidden="true" id="selectType"/>
				<input type="text" value="0,1,2"  hidden="true" id="status"/>
					<input type="text" value="2"  hidden="true" id="deleteStatus"/>
				<div class="tab_box">
					<div>
						<table class="ordertable" width="100%"  id="orderTab">
							
						</table>
						
					</div>
					<!-- <div>
						<table class="ordertable" width="100%" id="cancelTabl">
							<tr><Th>订单</Th><Th>洗车服务小区</Th><Th>状态</Th><Th>操作</Th></tr>
						</table>
						
					</div> -->
				</div>
				  <div id="pagediv"></div> 	 
				<div class="detail_list mt20" id="msg">
							<h3>洗车服务预约须知</h3>
						    <div class="notice">
						    	<p>洗车预约须知：
								• 预约成功后订单不可更改，如需更改请取消后重新预约，取消可能会产生订金损失，具体规则如下：
								</p>
								<p>距洗车开始30分钟之前取消订单，不收取费用；
								</p>
								<p>距洗车开始不足30分钟取消订单，收取一定比例的违约金，使用的优惠券不予退还；
								</p>
								<p>洗车开始后，不可取消订单。</p>
								<p>
								• 只有所选服务点小区内的车主可以享受上门洗车，小区之外的车主需要开车前往选择的服务点享受洗车服务。
								</p>
								
								<p>
								• 如洗车服务人员联系不上车主或小区外车主未按预定时间洗车，客服会取消订单并扣除用户一定比例的违约金。
								</p>
								<p>
								• 我们服务人员洗完车后会提供验车照片。
								</p>
						    </div>
					</div>
					
						<div class="detail_list mt20" style="display: none" id="msgs">
							<h3>积分兑换须知</h3>
						    <div class="notice">
						    	<p>
								• 实体商品需要到指定的服务点地址领取，可以在我的订单中查看具体地址
						    	</p>
						    		<p>
								• 兑换的电子券仅限兑换用户使用，兑换后不退不换。
						    	</p>
						    		<p>
								• 如有疑问请联系客服热线400-055-2799
						    	</p>
						    </div>
					</div>
			</div>
			</div>
		</div>
	</div>
 	<jsp:include page="/WEB-INF/front/views/common/foot.jsp" />
    <script src="<%=basePath%>/front/js/user/order.js"></script>
    <script src="<%=basePath%>/assets/front/startScore.js"></script>
	</body>
</html>
