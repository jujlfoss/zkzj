<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/assets/config/jquery-config.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>重卡之家 - 支付</title>
</head>
<body>
	<jsp:include page="/WEB-INF/front/views/common/head.jsp" />
	<div class="container wd clearfix ">
		<div class="location mt20">
			<a href="<%=basePath%>/index.html">首页</a> > <a
				href="javascript:location.reload()">充值卡支付</a>
		</div>
		<div class="servicesbox mt30">
			<h3>支付信息</h3>
			<div class="pay mtb20 clearfix border p20">
				<p id="orderno"></p>
				<p>充值卡绑定成功，15分钟若未支付，将自动解绑。请尽快付款！</p>
				<span class="left"> </span> <span class="money right">应付金额:<em
					id="totalPrice"></em> 元
				</span>
			</div>
		</div>
		<div class="servicesbox mt30">
			<h3>支付方式</h3>
			<div class="paymethod border p20 clearfix">
				<input type="hidden" id="paymethod">
				<ul class="pay_ul clearfix" id="payul">
					<li class="current">
						<input type="hidden" value="2">
						<i class="wx"></i>微信<span class=""></span>
					</li>
					<li>
						<input type="hidden" value="1"><i class="zfb">
						</i>支付宝<span class=""></span>
					</li>
				</ul>
				<div class="code">
				<a id="payfor" class="pay_btn">立即支付</a>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/WEB-INF/front/views/common/foot.jsp" />
 	<script type="text/javascript" src="<%=basePath %>/assets/front/qrcode.js"></script>
	<script src="<%=basePath %>/front/js/rechage/auth/payfor.js"></script>
</body>
</html>

