<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String leftBasePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ request.getContextPath();
%>

<div class="count_menu left border">
	<ul class="">
		<li id="li1"><a href="<%=leftBasePath%>/user/auth/usercenter.html"><i></i>我的账户</a></li>
		<li id="li2"><a href="<%=leftBasePath%>/user/auth/myorder.html"><i></i>全部订单</a></li>
		<li id="li3"><a href="javascript:washclick();"><i></i>我的消息（<span id="msgCount"></span>）</a></li>
		<li id="li4"><a href="<%=leftBasePath%>/user/auth/washcard.html"><i></i>洗车卡</a></li>
		<li id="li5"><a href="<%=leftBasePath%>/coupon/auth/mycoupon.html"><i></i>优惠券</a></li>
		<li id="li6"><a href="<%=leftBasePath%>/user/auth/personalinfo.html"><i></i>个人资料</a></li>
		<li id="li7"><a href="<%=leftBasePath%>/user/auth/countsafe.html"><i></i>账户安全</a></li>
		<li id="li8"><a href="<%=leftBasePath%>/user/auth/suggest.html"><i></i>意见反馈</a></li>
	</ul>
</div>
<script type="text/javascript">
BaseUtil.get('eb/message/getUnreadCount',function(result){
	if(result.success){
		$("#msgCount").text(result.data||0);
		return;
	}
	$("#msgCount").text(0);
});
function washclick(){
	BaseUtil.post('eb/message/readAll',function(){
		location.href = '<%=leftBasePath%>/user/auth/message.html'
	});
}
</script>
