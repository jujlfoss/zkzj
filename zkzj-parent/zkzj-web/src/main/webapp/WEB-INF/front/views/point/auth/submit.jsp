<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/assets/config/jquery-config.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>重卡之家 - 提交订单</title>
<style type="text/css">
.position ul li{
	border:none !important;
}
.position{
	height:auto !important;
}
.proimg img{
	width:100%;
	height:100%;
}
</style>
</head>
<body>
	<jsp:include page="/WEB-INF/front/views/common/head.jsp" />
	<div class="container wd clearfix ">
		<div class="location mt20">
			<a href="<%=basePath%>/index.html">首页</a> > <a
				href="<%=basePath%>/point/scoremall.html">积分商城</a> > <a href="javascript:;">订单信息</a>
		</div>
		<div class="servicesbox mt30">
			<h3>选择上门取货服务点</h3>
			<div class="areabox border p20 clearfix">
				<div class="mb10 border-b pb10 ">
					<span>请选择有重卡之家服务点的地区:</span>
					<select class="selectcss" style="font-size: 14px" id="citySelect"></select> 
					<select class="selectcss" style="font-size: 14px" id="countySelect"></select>
				</div>
				<div class="position" style="width: 100%;">
					<ul id="area-ul">
					</ul>
				</div>
			</div>
		</div>
		<div class="servicesbox mt30">
			<h3>确认订单信息</h3>
			<div class="orderinfo p20 border clearfix">
				<ul id="goods-ul">
				</ul>
				<div class="mt20 right">
					<div class="sumscore">
						<span>积分共计：</span><i id="totalPrice">0</i><em>积分</em>
					</div>
					<div class="mt20 right">
						<input type="button" class="sure_btn" id="sure_btn" value="确认提交"></input>
					</div>

				</div>
			</div>
		</div>
		<div class="servicesbox mt30">
			<h3>积分兑换须知：</h3>
			<div class="notice border clearfix p20">
				<p>
				• 实体商品需要到指定的服务点地址领取，可以在我的订单中查看具体地址<br> 
				• 兑换的电子券仅限兑换用户使用，兑换后不退不换。<br> 
				• 如有疑问请联系客服热线400-055-2799<br> 
				</p>
			</div>
		</div>
	</div>
	<jsp:include page="/WEB-INF/front/views/common/foot.jsp" />
	<script src="<%=basePath%>/front/js/point/auth/submit.js"></script>
</body>
</html>
