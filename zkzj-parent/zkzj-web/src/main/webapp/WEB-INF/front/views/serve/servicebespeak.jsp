<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/assets/config/jquery-config.jsp" %> 
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>重卡之家</title>
		<style type="text/css">
		.servicesbox select {
		    font-size: 16px;
		    border: 1px solid #F98322;
		    color: #F98322;
		    width: 200px;
		    height: 42px;
		}
		</style>
	</head>
	<body>
		<jsp:include page="/WEB-INF/front/views/common/head.jsp" />
		<jsp:include page="/WEB-INF/front/views/common/search.jsp" /> 
	<div class="container wd clearfix ">
	    <div class="location mt20">
	    	<a href="<%=basePath%>/index.html">首页</a> > <a href="<%=basePath%>/search/searchlist.html?location=serve">洗车服务</a> > <a href="javascript:location.reload()">预约信息</a>
	    </div>
	    
	    <div class="orderbox border p20 mt20 clearfix">
	    	<div class="leftimg left">   
		    	<div id="showbox">
					  <img src="" name="showboximg" width="451" height="268" />
					  <img src="" name="showboximg" width="451" height="268" />
					  <img src="" name="showboximg" width="451" height="268" />
					  <img src="" name="showboximg" width="451" height="268" />
					  <img src="" name="showboximg" width="451" height="268" /> 
					</div><!--展示图片盒子-->
					<div id="showsum"></div><!--展示图片里边-->
					<!--<p class="showpage">
					  <a href="javascript:void(0);" id="showlast"> < </a>
					  <a href="javascript:void(0);" id="shownext"> > </a>
					</p>-->
			</div>
			<div class="rightcontent right">
				<h3 style="font-size: 30px;margin-bottom: 20px;" id="serve_title"></h3>
				<ul class="desc_ul">
					<li class="clearfix"><span class="name left">套餐介绍：</span><span id="servedesc" class="detail"></span></li>
				    <!-- <li class="clearfix"><span class="name left">建议周期：</span><span class="detail">半年或视车内实际情况而定</span></li> -->
				</ul> 
				<div class="parameter">
					<span class="border-r">已售出: <em id="serve_record"></em></span>
					<span class="border-r">评价数: <em id="serve_comment"></em></span>
					<span>好评率: <em id="serve_comment_rate"></em></span> 
				</div>
				<div class="car mt20" id="standard_list">
					<span class="label">选择规格:</span>
					<!-- <a class="car_a cur">轿车</a>
					<a class="car_a">SUV</a> -->
				</div>
				<div class="price mt20 clearfix"><span class="left"><em>￥</em><i id="serve_price">0</i></span><a href="#serve_query" class="yue_btn left">我要预约</a></div>
				 
			</div>
	    </div>
	    <div class="servicesbox mt30">
	    	<h3>服务流程</h3>
	    	<div class="flowbox border">
	    		<img src="<%=basePath%>/front/images/flow_bg.png">
	    	</div>
	    </div>
	    <div class="servicesbox mt30" id="serve_query">
	    	<h3>服务区域查询</h3>
	    	
	    	<div class="areabox border p20 clearfix">
	    	<div class="mb20">
   			    <!-- <select class="datainp" id="province">
   			    	<option value="">请选择省份</option>
   			    </select>  --> 
   			    <select class="selectcss" id="city" onchange="window.getArea(this.value);window.getEstate(null,true)">  
   			    	<option value="">请选择合作城市</option>
   			    </select>
   			    <select class="selectcss" id="district" onchange="window.getEstate(null,true)">
   			    	<option value="">请选择区/县</option> 
   			    </select>
   			    <!-- <a class="looktime_btn" href="javascript:void(0)">查看服务小区</a> -->
   			</div>
   			<div>
	    		<div class="map border left">
	    			<div id="l-map"></div>
					<div id="r-result" style="display: none;">
						<input type="button" value="批量反地址解析+商圈" onclick="bdGEO(0)" />
						<div id="result"></div>
					</div>
	    		</div>
	    		<div class="position right">
	    			<ul id="serve_area_ul">
	    				<!-- <li class="clearfix">
	    					<i>1</i>
	    					<span>
	    						<p class="position01">凯悦华庭小区服务点</p>
	    						<p class="addr01">四川省成都市高新区天府大道中段天府</p>
	    					</span>
	    					<a href="reservationInfoSure.html"  class="a_btn">立即预约</a>
	    				</li> -->
	    			</ul>
	    		</div>
	    		</div>
	    	</div>
	    </div>
	    <div class="servicesbox mt30">
	    	<h3>服务详情</h3>
	    	<div class="pro_detail border p20 clearfix">
	    		<table class="prodetail_table" style="width: 100%">
	    			<tr>
	    				<td width="60">产品名称:</td>
	    				<td ><i id="info_servename"></i></td>
	    			</tr>
	    			<tr>
	    				<td width="60">产品介绍:</td>
	    				<td ><i id="info_servedesc"></i></td>
	    			</tr>
	    			<tr>
	    				<td width="60">产品构成:</td>
	    				<td ><i id="info_productComposition"></i> </td>
	    			</tr>
	    			<tr>
	    				<td width="60">收费标准:</td>
	    				<td ><i id="info_charge"></i><!-- 轿车（300元/60分钟）、SUV（320/80分钟） --> </td>
	    			</tr>
	    		</table>
	    		<div class="imgbox">
	    			<img src="" id="info_serveinfoimg">
	    		</div>
	    	</div>
	    </div>
	    <div class="servicesbox mt30">
	    	<h3>用户评价</h3>
	    			<div  class="mapbox"  id="allmap"></div> 
	    	<div class="userValue border mt20 clearfix p20">
	    		<ul id="usercomment">
	    			<!-- <li class="clearfix mt20">
	    				<div class="userimg"></div>
	    				<div class="userinfo">
		    			<h3>用户名</h3>
		    			<p class="mb5"><span class="staricon"></span><span class="staricon"></span></p>
		    			<p class="content">服务服务态度非常好服务态度非常好服务态度非常好服务态度非常好服务态度非常好服务态度非常好服务态度非常好服务态度非常好服务态度非常好服务态度非常好服务态度非常好服务态度非常好服务态度非常好服务态度非常好服务态度非常好服务态度非常好服务态度非常好态度非常好</p>
		    			<p class="time">发表于2017-05-19  09:34:05</p>
		    		</div>
	    			</li> -->
	    		</ul>
	    		<div id="usercommentpage" style="margin-top: 20px"></div> 
	    	</div>
	    </div>
	    <div class="servicesbox mt30">
	    	<h3>洗车预约须知：</h3>
	    	<div class="notice border clearfix p20">
	    		<p>• 预约成功后订单不可更改，如需更改请取消后重新预约，取消可能会产生订金损失，具体规则如下：<br>
距洗车开始30分钟之前取消订单，不收取费用；<br>
距洗车开始不足30分钟取消订单，收取一定比例的违约金，使用的优惠券不予退还；<br>
洗车开始后，不可取消订单。<br>
• 只有所选服务点小区内的车主可以享受上门洗车，小区之外的车主需要开车前往选择的服务点享受洗车服务。<br> 
• 如洗车服务人员联系不上车主或小区外车主未按预定时间洗车，客服会取消订单并扣除用户一定比例的违约金。<br> 
• 我们服务人员洗完车后会提供验车照片。</p>
	    	</div>
	    </div>
	</div>
		<jsp:include page="/WEB-INF/front/views/common/foot.jsp" />
	</body>
	<script type="text/javascript" src="<%=basePath%>/front/js/serve/servicebasepeak.js"></script>
	<style type="text/css">
		body, html{width: 100%;height: 100%;margin:0;font-family:"微软雅黑";}
		#l-map{height:345px;width:100%;} 
		#r-result{width:100%; font-size:14px;line-height:20px;}
	</style>
</html>
