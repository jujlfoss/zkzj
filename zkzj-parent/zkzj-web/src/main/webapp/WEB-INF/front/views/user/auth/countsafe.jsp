<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/assets/config/jquery-config.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>重卡之家</title>
	</head>
	<body>
		<!--弹出登录-->
		  
	<jsp:include page="/WEB-INF/front/views/common/head.jsp" />
	<jsp:include page="/WEB-INF/front/views/common/search.jsp" />	  
	<div class="container mtb20 clearfix wd">
		<jsp:include page="/WEB-INF/front/views/common/left.jsp" />	
		<div class="count_content right border p20">
			<div class="countbox mt20"> 
				<div class="detail_list mt20">
					<h3>修改登录密码</h3>
				    <div class="modify_login">
				    	<p><span class="name">原密码:</span><span><input type="password" id="psw" class="inputcss"  style="height:32px;line-height:32px;"></span></p>
				    	<p><span class="name">新密码:</span><span><input type="password" class="inputcss" id="newPsw" style="height:32px;line-height:32px;"></span></p>
				    	<p><span class="name">再次输入新密码:</span><span><input type="password" id="newPswRe" class="inputcss" style="height:32px;line-height:32px;"></span></p>
				         <p><span class="name">图片验证码:</span>
				         <span>
				         <input type="text" maxlength="4" class="inputcss" id="imgcode" style="width:100px;height:32px;line-height:32px;"/>
				         <img src="data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" id="login-imageCodeImg"  class="inputcss" style="width:82px;height:32px;line-height:32px;"/></span></p>
				        <a class="a_btn" onclick="changPsw()">确认修改</a>
				    </div>
				</div>
				<div class="detail_list mt20">
					<h3>设置/修改支付密码</h3>
				     <div class="modify_login"> 
				     	<p><span class="name">新密码:</span><span><input type="password" id="newPayPsw" class="inputcss" style="height:32px;line-height:32px;"></span></p>
				    	<p><span class="name">再次输入新密码:</span><span><input type="password" id="payPswRn" class="inputcss" style="height:32px;line-height:32px;"></span></p>
				        <p><span class="name">短信验证码:</span><span><input type="text" id="code" class="inputcss" style="width:100px;height:32px;line-height:32px;"><input type="button" id="btn2"  class="btn_mfyzm" value="免费获取验证码" /></span></p>
				         <a class="a_btn" onclick="changePayPsw()">确认修改</a>
				    </div>
				</div>				 
			</div>
		</div>
	</div>
    <jsp:include page="/WEB-INF/front/views/common/foot.jsp" />
     <script src="<%=basePath%>/front/js/user/countsafe.js"></script>
	</body>
</html>
