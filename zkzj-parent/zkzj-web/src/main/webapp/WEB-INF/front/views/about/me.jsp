<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/assets/config/jquery-config.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<link rel="stylesheet" href="<%=basePath%>/front/css/reset.css">
<link rel="stylesheet" href="<%=basePath%>/front/css/slide.css">
<style>
.bg01 {
	width: 454px;
	height: 148px;
	background: url(<%=basePath%>/front/images/bg01.png) no-repeat left center;
	margin-top: 100px;
	margin-bottom: 40px;;
}

.bg02 {
	width: 634px;
	height: 151px;
	background: url(<%=basePath%>/front/images/bg02.png) no-repeat left center;
	margin-top: 100px;
	margin-bottom: 40px;
}

.bg03 {
	width: 658px;
	height: 150px;
	background: url(<%=basePath%>/front/images/bg03.png) no-repeat left center;
	margin-top: 100px;
	margin-bottom: 40px;
}
.bg04 {
	width: 658px;
	height: 150px;
	background: url(<%=basePath%>/front/images/bg04.png) no-repeat left center;
	margin-top: 100px;
	margin-bottom: 40px;
}

.ck-slide {
	width: 671px;
	height: 383px;
	margin: 0 auto;
}

.ck-slide ul.ck-slide-wrapper {
	height: 383px;
}

.textbox {
	width: 309px;
	height: 383px;
	background: #f98322;
	padding: 15px;
	position: relative;
}

.textbox i {
	display: block;
	width: 8px;
	height: 18px;
	background: url(<%=basePath%>/front/images/jticon.png) no-repeat;
	position: absolute;
	left: -6px;
	top: 183px;
	z-index: 999;
}

.textbox p {
	color: #fff;
	line-height: 20px;
	font-size: 14px;
}

.aboutuslist ul li {
	width: 310px;
	background: #fff;
	border: 1px solid #e5e5e5;
	display: inline-block;
	float: left;
	margin-right: 20px;
}

.advantages ul li:last-child, .aboutuslist ul li:last-child {
	margin-right: 0 !important;
}

.advantages ul li:last-child {
	border-right: none !important;
}

.aboutuslist ul li img {
	width: 100%;
	height: 160px;
}

.aboutuslist ul li p {
	padding: 5px 10px;
	font-size: 14px;
	max-height: 30px;
	overflow: hidden;
	text-align: center;
	word-wrap: break-word;
	word-break: break-all;
}

.advantages {
	background: #fbede1;
	padding: 50px 0;
}

.advantages ul li {
	width: 300px;
	float: left;
	height: 100px;
	padding: 10px;
	border-right: 1px solid #e5e5e5;
}

.advantages ul li h3 {
	font-size: 20px;
	color: #f98322;
}

.advantages ul li p {
	color: #999;
	line-height: 20px;
	text-align: left;
}

.specilpoint img {
	width: 100%;
}
.advantages ul li {
    width: 228px;
}


.about .about_logo {
    float:left;
	width: 188px;height:79px;
	background: url(<%=basePath%>/front/images/bg_00.png)  no-repeat; 
}
.about .about_text {
	display: block;
    padding-top: 15px;
    line-height: 25px;
	height:79px;
	
}

.bg_font{
   font-size:16px;
   color:#000;
   margin:8px 0;
   font-weight:600;
}
</style>

</head>
<body>
<jsp:include page="/WEB-INF/front/views/common/head.jsp" />
<jsp:include page="/WEB-INF/front/views/common/search.jsp" />
	<div class="wd">
		<h3 class="bg01"></h3>
		<div class="about clearfix">
			<span class="about_logo"><!-- <img src="./../../images/bg_00.png"/> --></span>
			<span class="about_text">重卡之家（江阴市靓爽汽车服务有限公司）是2017年成立，主营上门洗车业务，以物联网大时代为背景，结合线下线上模式的互联网汽车服务公司。公司让更多车主用更少的钱安心在家洗车而存在。</span>
		</div>
		<div class="jieshao clearfix">
			<div class="left">
				<div class="ck-slide">
					<ul class="ck-slide-wrapper">
						<li><a href="javascript:"><img
								src="<%=basePath%>/front/images/aboutus01.png" alt=""></a></li>
						<%-- <li style="display: none"><a href="javascript:"><img
								src="images/aboutus01.png" alt=""></a></li>
						<li style="display: none"><a href="javascript:"><img
								src="<%=basePath%>/front/images/aboutus01.png" alt=""></a></li>
						<li style="display: none"><a href="javascript:"><img
								src="<%=basePath%>/front/images/aboutus01.png" alt=""></a></li>
						<li style="display: none"><a href="javascript:"><img
								src="<%=basePath%>/front/images/aboutus01.png" alt=""></a></li> --%>
					</ul>
					<!-- <!-- <a href="javascript:;" class="ctrl-slide ck-prev">上一张</a> <a
						href="javascript:;" class="ctrl-slide ck-next">下一张</a> --> -->

				</div>
			</div>
			<div class="textbox right">
				<i></i>
				<p>
					重卡之家社区洗车隶属于江阴市靓爽汽车服务有限公司，成立于2017年，主营上门洗车业务。为顺应“互联网+”大时代，重卡之家推出了线上线下整合解决方案，方案包括微信公众号、APP客户端，PC端、员工端、管理端的互联网汽车服务公司。以当代最便捷的网约方式，为您提供最高效省心的优质上门洗车服务。重卡之家专利化的第二代陶瓷移动蒸汽洗车机，与哈尔滨工业大学联合开发，采用国内最先进的陶瓷发热技术，是国内首例以电池为动力源，零安全隐患的移动蒸汽设备。
					本公司以节能环保，节约用水，节省客户时间为前提，致力于专业的管家式汽车服务。同时响应国家40、50工程，为下岗人员创造更适合的就业机会。并依托地方政策扶持，推动学生自主创业，承担社会责任。
					<br>您的时间，我帮您节约 <br>您的爱车，我替您疼惜
				</p>
			</div>
		</div>
		<div class="aboutuslist mt20 clearfix">
			<ul>
				<li><a href="#"> <img
						src="<%=basePath%>/front/images/1.jpg" width=" "
						height=" ">
						<p>宽敞的会议室</p>
				</a></li>
				<li><a href="#"> <img
						src="<%=basePath%>/front/images/2.jpg" width=" "
						height=" ">
						<p>舒适的办公环境</p>
				</a></li>
				<li><a href="#"> <img
						src="<%=basePath%>/front/images/3.jpg" width=" "
						height=" ">
						<p>洗车现场</p>
				</a></li>
			</ul>
		</div>
		<h3 class="bg02"></h3>
		<div class="advantages mt20 clearfix">
			<ul>
				<li>
					<h3>绿色环保、节约用水</h3>
					<p>雾化无菌蒸馏汽，无化学清洗剂，不伤车漆，不污染环境。</p>
				</li>
				<li>
					<h3>高效便捷、节省时间</h3>
					<p>您手机预约，我上门洗车，30分钟，您的爱车焕然一新。</p>
				</li>
				<li>
					<h3>诚信可靠、专人服务</h3>
					<p>与物业深度合作入住社区，随时为您贴心服务。</p>
					<p>每一位技师经过专业的培训考核，您爱车的贴身管家。</p>
				</li>
				<li>
					<h3>智能安全</h3>
					<p>缺水自动报警、过热自动断电、超压自动泄压，安全可靠。</p>
				</li>
			</ul>
		</div>
		<h3 class="bg03"></h3>
		<div class="specilpoint">
			<img src="<%=basePath%>/front/images/point.png">
		</div>
		
		<h3 class="bg04"></h3>
		<div class="specilpoint">
			<img src="<%=basePath%>/front/images/ditu.png">
			<p class="bg_font" style="margin-top:40px">公司地址：江阴市长山大道55号天安数码城61单元608B室</p>
			<p class="bg_font">公众邮箱：public@xin7jia.com</p>
			<p class="bg_font">公司电话：0510-86992525</p>
		</div>
	</div>
	<jsp:include page="/WEB-INF/front/views/common/foot.jsp" />
	<script src="<%=basePath%>/assets/front/jquery-1.11.0.min.js"></script>
	<script src="<%=basePath%>/assets/front/slide.min.js"></script>
	<script>
		$('.ck-slide').ckSlide({
			autoPlay : true,//默认为不自动播放，需要时请以此设置
			dir : 'x',//默认效果淡隐淡出，x为水平移动，y 为垂直滚动
			interval : 3000
		//默认间隔2000毫秒

		});
	</script>
</body>
</html>
