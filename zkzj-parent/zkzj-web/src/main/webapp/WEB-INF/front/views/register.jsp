<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/assets/config/jquery-config.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>重卡之家 - 注册</title>
</head>
<body>
	<jsp:include page="/WEB-INF/front/views/common/head.jsp" />
	<div class="container mtb20 border clearfix wd">
		<h3 class="login_h3">注册</h3>
		<div class="logindiv" id="register-logindiv">
			<!-- 注册表单 -->
			<form class="cd-form" onsubmit="return false;">
				<p class="fieldset">
					<label class="image-replace cd-username" for="signup-username">手机号码</label>
					<input class="full-width has-padding has-border"
						id="register-signup-username" type="text" maxlength="11"
						placeholder="请输入手机号码">
				</p>

				<p class="fieldset">
					<label class="image-replace cd-password" for="signup-password">密码</label>
					<input class="full-width has-padding has-border"
						id="register-signup-password" type="password" maxlength="20"
						placeholder="输入密码，密码由6-20位字符组成">
				</p>

				<p class="fieldset">
					<label class="image-replace cd-password" for="signup-password">密码</label>
					<input class="full-width has-padding has-border"
						id="register-signup-password-re" type="password" maxlength="20"
						placeholder="确认密码">
				</p>
				<p class="fieldset position-r">
					<label class="image-replace cd-yzm" for="signup-password">短信验证码</label>
					<input class="full-width has-padding has-border" id="register-signup-sms"
						type="text" placeholder="输入短信验证码" maxlength="6"> <input
						type="button" id="register-smsBtn" class="btn_mfyzm btn_mfyzm2" value="免费获取验证码" />
				</p>
				<p class="fieldset">
					<label class="image-replace cd-username" for="signup-username">手机号码</label>
					<input class="full-width has-padding has-border"
						id="register-signup-referrerMobile" type="text" maxlength="11"
						placeholder="输入推荐人手机号码，送ta积分 (非必填)">
				</p>

				<p class="fieldset">
					<input class="full-width2" type="submit" value="立即注册"> <label
						for="accept-terms"
						style="display: block; line-height: 24px; text-align: center">点击立即注册表示您已阅读并同意
						<a href="javascript:;" style="text-decoration: underline;" id="xieyiA">《用户协议》</a>和<a style="text-decoration: underline;" id="falvA" href="javascript:;">《法律声明》</a>
					</label>
				</p>
			</form>
		</div>
	</div>
	<jsp:include page="/WEB-INF/front/views/common/foot.jsp" />
	<script type="text/javascript" src="<%=basePath%>/front/js/register.js"></script>
</body>
</html>
