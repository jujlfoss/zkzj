<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/assets/config/jquery-config.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>重卡之家</title>
</head>
<body>
	<jsp:include page="/WEB-INF/front/views/common/head.jsp" />
	<div class="container wd clearfix ">
		<div class="location mt20">
			<a href="<%=basePath%>/index.html">首页</a> > <a
				href="javascript:location.reload()">支付结果</a>
		</div>
		<div class="servicesbox mt30">
			<h3>支付结果</h3>
			<div class="pay mtb20 clearfix border p20">
				<p id="msg" style="font-size: 20">
					购买成功！去<a href="<%=basePath%>/user/auth/usercenter.html"><font
						color="red">洗车卡记录查看</font></a>
				</p>
				<p>
					<a href="<%=basePath%>/index.html"><font color="red">返回首页</font></a>
				</p>
				<span class="left"> </span>
			</div>
		</div>
	</div>

	<jsp:include page="/WEB-INF/front/views/common/foot.jsp" />
</body>
</html>