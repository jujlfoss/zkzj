<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/assets/config/jquery-config.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>重卡之家 - 修改密码</title>
</head>
<body>
	<jsp:include page="/WEB-INF/front/views/common/head.jsp" />
	<div class="container mtb20 border clearfix wd">
		<h3 class="login_h3">修改密码</h3>
		<div class="logindiv" id="register-logindiv">
			<form class="cd-form" onsubmit="return false;">
 				<p class="fieldset">
					<label class="image-replace cd-password" for="signup-password">新密码</label>
					<input class="full-width has-padding has-border"
						id="register-signup-password" type="password" maxlength="20"
						placeholder="输入新密码，密码由6-20位字符组成">
				</p>
				<p class="fieldset">
					<label class="image-replace cd-password" for="signup-password">确认密码</label>
					<input class="full-width has-padding has-border"
						id="register-signup-password-re" type="password" maxlength="20"
						placeholder="确认密码">
				</p>
				<p class="fieldset">
					<input class="full-width2" type="submit" value="确认修改">
				</p>
			</form>
		</div>
	</div>
	<jsp:include page="/WEB-INF/front/views/common/foot.jsp" />
	<script type="text/javascript"
		src="<%=basePath%>/front/js/forget/changepwd.js"></script>
</body>
</html>
