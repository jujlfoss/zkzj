<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/assets/config/jquery-config.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<link rel="stylesheet" href="<%=basePath%>/front/css/reset.css">
<link rel="stylesheet" href="<%=basePath%>/front/css/slide.css">
<style>
.wrapperbox {
	width: 100%;
	height: 100%;
	display: flex;
	display: -moz-box;
	display: -webkit-box;
	text-align: center;
	-moz-box-pack: center;
	-webkit-box-pack: center;
	-moz-box-align: center;
	-webkit-box-align: center;
}

.wrapperbox p {
	font-size: 18px;
}
</style>

</head>
<body>
	<jsp:include page="/WEB-INF/front/views/common/head.jsp" />
	<div class="wrapperbox">
		<div>
			<img src="<%=basePath%>/front/images/jsz.png">
			<p>页面正在建设中...</p>
		</div>
	</div>
</body>
</html>
