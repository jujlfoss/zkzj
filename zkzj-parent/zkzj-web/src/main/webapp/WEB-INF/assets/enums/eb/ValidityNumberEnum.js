/**
 * ValidityNumberEnum
 */
(function(window) {
	var ValidityNumberEnum = {
			ONE : {
			getText : function() {
				return '一个月';
			},
			getValue : function() {
				return 0;
			}
		},
		THREE : {
			getText : function() {
				return '三个月';
			},
			getValue : function() {
				return 1;
			}
		},
		HALFAYEAR : {
			getText : function() {
				return '半年';
			},
			getValue : function() {
				return 2;
			}
		},
		AYEAR : {
			getText : function() {
				return '一年';
			},
			getValue : function() {
				return 3;
			}
		},
		getHtml : function(value) {
			switch (value) {
			case 0:
				return '一个月';
			case 1:
				return '三个月';
			case 2:
				return '半年';
			case 3:
				return '一年';
			}
			
		}
	};
	window.ValidityNumberEnum = ValidityNumberEnum;
})(window);
