(function(window) {
	var CarTypeEnum = { 
			SUV : {
			getText : function() {
				return 'SUV/MPV';
			},
			getValue : function() {
				return 1;
			}
		},
		CAR : {
			getText : function() {
				return '轿车';
			},
			getValue : function() {
				return 2;
			}
		},
//		ALL : {
//			getText : function() {
//				return 'MPV';
//			},
//			getValue : function() {
//				return 3;
//			}
//		},
		getHtml : function(value) {
			switch (value) {
			case '1':
				return 'SUV/MPV';
			case '2':
				return '轿车';
//			case 3:
//				return 'MPV';
			
			}
		}
	};
	window.CarTypeEnum = CarTypeEnum;
})(window);
