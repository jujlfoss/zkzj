(function(window) {
	var CartUtil = {
		addToCart : function(goodsId) {
			if (BaseUtil.isLogin()) {
				BaseUtil.post('eb/cart/saveCart', function(data) {
					if (data.success) {
						layer.msg('加入成功');
						$('#cartCount').text(parseInt($('#cartCount').text())+1);
					}
				}, {
					goodsId : goodsId
				});
			}
		},
		submit : function(goodsIds, cartIds) {
			if (BaseUtil.isLogin()) {
				var url = basePath + '/point/auth/submit.html?goodsIds=' + goodsIds;
				if (cartIds) {
					url += '&cartIds=' + cartIds;
				}
				window.location.href = url;
			}
		},
		exchange : function(couponTypeId) {
			if (BaseUtil.isLogin()) {
				layer.confirm('确认要兑换吗?', {
					btn : [ '确认', '取消' ]
				}, function(index) {
					BaseUtil.post('eb/coupon/exchange', function(data) {
						layer.close(index);
						if (data.success) {
							layer.msg("兑换成功", {
							    time: 500
							},function(){
								window.location.href = basePath + '/coupon/auth/mycoupon.html';
							});
							return;
						}
						layer.msg(data.msg);
					}, {
						couponTypeId : couponTypeId
					});
				});
			}
		},
		recharge : function(rechargeCardTypeId,couponTypeName,sellPrice) {
			if (BaseUtil.isLogin()) {
				layer.confirm(couponTypeName+'，实付金额：'+sellPrice+'元', {
					btn : [ '确认', '取消' ],title:'确认订单信息!'
				}, function(index) {
					BaseUtil.post('eb/rechargeCard/bind',function(data){
						layer.close(index);
						if (data.success) {
							window.location.href = basePath + '/rechage/auth/payfor.html?rechargeCardNo=' + data.data;
							return;
						}
						layer.msg(data.msg);
					},{
						rechargeCardTypeId : rechargeCardTypeId
					});
				});
			}
		},
		wash : function(washCardTypeId,washCardName,sellPrice,estateName) {
			if (BaseUtil.isLogin()) {
				layer.confirm(washCardName+'，适用小区：'+$("#estateSelect option:selected").text()+'，实付金额：'+sellPrice+'元', {
					btn : [ '确认', '取消' ],title:'确认订单信息!'
				}, function(index) {
					BaseUtil.post('eb/washCard/bind',function(data){
						layer.close(index);
						if (data.success) {
							window.location.href = basePath + '/wash/auth/payfor.html?washCardNo=' + data.data;
							return;
						}
						layer.msg(data.msg);
					},{
						washCardTypeId : washCardTypeId
					});
				});
			}
		}
	};
	window.CartUtil = CartUtil;
})(window);