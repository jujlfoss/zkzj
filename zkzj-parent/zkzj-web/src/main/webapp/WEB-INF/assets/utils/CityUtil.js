(function(window) {
	var CityUtil = {
		provinceSelect : function(provinceId,cityId,districtId){ 
			document.getElementById(provinceId).length = 1;   
			$.each(cityData.province,function(idx,item){ 
			$("#"+provinceId).append('<option value="">'+item.name+'</option>');
			});
		},
		citySelect : function (provinceId,cityId,districtId){ 
			document.getElementById(cityId).length = 1;  
			document.getElementById(districtId).length = 1;  
			var pindex = document.getElementById(provinceId).selectedIndex-1;
			if(pindex < 0){
				return;
			}
			$.each(cityData.province[document.getElementById(provinceId).selectedIndex-1]["city"],function(idx,item){ 
				$("#"+cityId).append('<option value="" onchange="cityselect.districtSelect()">'+item.name+'</option>'); 
			});   
		},
		districtSelect : function (provinceId,cityId,districtId){ 
			document.getElementById(districtId).length = 1;  
			var pindex = document.getElementById(provinceId).selectedIndex-1;
			var cindex = document.getElementById(cityId).selectedIndex-1;
			if(pindex < 0 || cindex < 0){
				return;
			}
			$.each(cityData.province[document.getElementById(provinceId).selectedIndex-1]["city"][document.getElementById(cityId).selectedIndex-1]["district"],function(idx,item){ 
				$("#"+districtId).append('<option value="" onchange="cityselect.districtSelect()">'+item.name+'</option>'); 
			});  
		}
	};
	window.CityUtil = CityUtil;
})(window);