package com.zkzj.web.controller.main;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zkzj.dao.redis.RedisRepository;
import com.zkzj.entity.sys.User;
import com.zkzj.pojo.constants.Constant;
import com.zkzj.pojo.exceptions.ControllerException;
import com.zkzj.pojo.exceptions.ExceptionEnum;
import com.zkzj.pojo.exceptions.ServiceException;
import com.zkzj.pojo.vos.common.ResultVo;
import com.zkzj.service.sys.UserService;
import com.zkzj.utils.CodeUtil;
import com.zkzj.utils.DateUtil;
import com.zkzj.utils.EncodeUtil;
import com.zkzj.utils.IPUtil;
import com.zkzj.utils.MD5Util;
import com.zkzj.utils.PropertiesUtil;
import com.zkzj.utils.RSAUtil;
import com.zkzj.utils.SessionUtil;
import com.zkzj.utils.ValidUtil;
import com.zkzj.utils.VerifyCodeUtil;
import com.zkzj.web.controller.base.BaseController;
import com.zkzj.web.utils.ResultUtil;

/**
 * 
 * @author liu
 *
 */
@Controller
public class MainController extends BaseController {

	@Autowired
	private UserService userService;

	@Autowired
	private RedisRepository redisRepository;
	@Value("${versionFilePath}")
	private String versionFilePath;


	@RequestMapping("index.html")
	public String indexHtml() {
		return "front/views/index";
	}

	/**
	 * 通用controller
	 */
	@RequestMapping("/**/*.html")
	public String fontView(HttpServletRequest request) {
		return "front/views" + request.getServletPath().replaceAll(".html", "");
	}

	/**
	 * 获取getSubmitToken
	 * 
	 * @return
	 */
	@RequestMapping("getSubmitToken")
	@ResponseBody
	public String getSubmitToken() {
		final String token = UUID.randomUUID().toString();
		SessionUtil.setSessionAttribute(Constant.SUBMIT_TOKEN, token);
		return token;
	}



	/**
	 * 刷新refreshToken
	 * 
	 * @return
	 */
	@RequestMapping("m/refreshToken")
	@ResponseBody
	public ResultVo mRefreshToken(HttpServletRequest request) {
		try {
			String refreshToken = request.getHeader(Constant.REFRESH_TOKEN);
			if (ValidUtil.isBlank(refreshToken)) {
				throw new ControllerException(ExceptionEnum.REFRESH_TOKEN_NOT_EXISTS);
			}
			return ResultUtil.success(this.userService.refreshToken(refreshToken));
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 获取公有密钥
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("getPublicKey")
	@ResponseBody
	public String getPublicKey() {
		Map<String, Object> keyMap = RSAUtil.genKeyPair();
		SessionUtil.setSessionAttribute(Constant.PRIVATE_KEY, RSAUtil.getPrivateKey(keyMap));
		return RSAUtil.getPublicKey(keyMap);
	}

	/**
	 * 验证权限
	 * 
	 * @param permittedUrl
	 * @return
	 */
	@RequestMapping("isPermitted")
	@ResponseBody
	public boolean isPermitted(String permittedUrl) {
		Subject subject = SecurityUtils.getSubject();
		return subject.hasRole(Constant.ADMIN) ? true : subject.isPermitted(permittedUrl);
	}

	/**
	 * 验证登录
	 * 
	 * @param isLogin
	 * @return
	 */
	@RequestMapping("isLogin")
	@ResponseBody
	public ResultVo isLogin() {
		try {
			return ResultUtil.success(super.getCurrentCustomer() != null);
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	@RequestMapping({ "imageCode", "m/imageCode" })
	public void imageCode(String mobile, HttpServletResponse response) {
		try {
			if (ValidUtil.isBlank(mobile)) {
				throw new ControllerException(ExceptionEnum.MOBILE_NOT_NULL);
			}
			response.setHeader("Pragma", "No-cache");
			response.setHeader("Cache-Control", "no-cache");
			response.setDateHeader("Expires", 0);
			response.setContentType("image/jpeg");
			// 生成随机字串
			String verifyCode = VerifyCodeUtil.generateVerifyCode(4);
			// 存入
			redisRepository.set(Constant.VALIDCODE + mobile, verifyCode, 1800L);
			// 生成图片
			int w = 200, h = 80;
			VerifyCodeUtil.outputImage(w, h, response.getOutputStream(), verifyCode);
		} catch (IOException e) {
			throw new ControllerException(ExceptionEnum.VALID_CODE_GET_ERROR);
		}
	}

	@RequestMapping(value = "m/regisCode", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo mRegisCode(String appId, String mobile, HttpServletRequest request) {
//		try {
//			if (ValidUtil.isBlank(appId)) {
//				throw new ControllerException(ExceptionEnum.APPID_INFO_ERROR);
//			}
//			if (!ValidUtil.isMobile(mobile)) {
//				throw new ControllerException(ExceptionEnum.MOBILE_FORMAT_ERROR);
//			}
//			this.smsService.sendRegisterSms(mobile, appId, IPUtil.getIP(request));
//			return ResultUtil.success();
//		} catch (Exception e) {
//			return ResultUtil.error(log, e);
//		}
		return null;
	}

	@RequestMapping(value = "regisCode", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo regisCode(String mobile, HttpServletRequest request) {
		try {
			Serializable sessionId = SessionUtil.getSessionId();
			if (sessionId == null) {
				throw new ControllerException("设备信息错误");
			}
			if (!ValidUtil.isMobile(mobile)) {
				throw new ControllerException(ExceptionEnum.MOBILE_FORMAT_ERROR);
			}
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	@RequestMapping(value = "m/smsCode", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo mSmsCode(String appId, HttpServletRequest request) {
		try {
			if (ValidUtil.isBlank(appId)) {
				throw new ControllerException(ExceptionEnum.APPID_INFO_ERROR);
			}
			String mobile = super.getCurrentUser().getMobile();
			if (!ValidUtil.isMobile(mobile)) {
				throw new ControllerException(ExceptionEnum.MOBILE_FORMAT_ERROR);
			}
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	@RequestMapping(value = "smsCode", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo smsCode(HttpServletRequest request) {
		try {
			Serializable sessionId = SessionUtil.getSessionId();
			if (sessionId == null) {
				throw new ControllerException("设备信息错误");
			}
			String mobile = super.getCurrentUser().getMobile();
			if (!ValidUtil.isMobile(mobile)) {
				throw new ControllerException(ExceptionEnum.MOBILE_FORMAT_ERROR);
			}
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	private User checkExistsUser(String mobile) {
		User search = new User();
		search.setMobile(mobile);
		User user = this.userService.get(search);
		if (user == null) {
			throw new ControllerException(ExceptionEnum.USER_NOT_EXISTS);
		}
		return user;
	}

	@RequestMapping(value = "m/front/forgetCode", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo mForgetCode(String appId, String mobile, HttpServletRequest request) {
		try {
			this.checkExistsUser(mobile);
			if (ValidUtil.isBlank(appId)) {
				throw new ControllerException(ExceptionEnum.APPID_INFO_ERROR);
			}
			if (!ValidUtil.isMobile(mobile)) {
				throw new ControllerException(ExceptionEnum.MOBILE_FORMAT_ERROR);
			}
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	@RequestMapping(value = "web/front/forgetCode", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo forgetCode(String mobile, HttpServletRequest request) {
		try {
			this.checkExistsUser(mobile);
			Serializable sessionId = SessionUtil.getSessionId();
			if (sessionId == null) {
				throw new ControllerException("设备信息错误");
			}
			if (!ValidUtil.isMobile(mobile)) {
				throw new ControllerException(ExceptionEnum.MOBILE_FORMAT_ERROR);
			}
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	@RequestMapping(value = { "web/front/validSms" }, method = RequestMethod.POST)
	@ResponseBody
	public ResultVo validSms(String mobile, String mobileCode) {
		try {
			this.checkExistsUser(mobile);
			String mobileCodeKey = Constant.VALIDCODE + mobile;
			if (!this.redisRepository.exist(mobileCodeKey)) {
				throw new ControllerException(ExceptionEnum.MOBILE_VALID_NOT_EXISTS);
			}
			if (!((String) this.redisRepository.get(mobileCodeKey)).equalsIgnoreCase(mobileCode)) {
				throw new ControllerException(ExceptionEnum.MOBILE_VALID_CODE_ERROR);
			}
			this.redisRepository.delete(mobileCodeKey);
			SessionUtil.setSessionAttribute(Constant.CHANGE_PWD_MOBILE, mobile);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	@RequestMapping(value = { "m/front/validSms" }, method = RequestMethod.POST)
	@ResponseBody
	public ResultVo mValidSms(String mobile, String mobileCode) {
		try {
			this.checkExistsUser(mobile);
			String mobileCodeKey = Constant.VALIDCODE + mobile;
			if (!this.redisRepository.exist(mobileCodeKey)) {
				throw new ControllerException(ExceptionEnum.MOBILE_VALID_NOT_EXISTS);
			}
			if (!((String) this.redisRepository.get(mobileCodeKey)).equalsIgnoreCase(mobileCode)) {
				throw new ControllerException(ExceptionEnum.MOBILE_VALID_CODE_ERROR);
			}
			this.redisRepository.delete(mobileCodeKey);
			this.redisRepository.set(Constant.CHANGE_PWD_MOBILE + ":" + mobile, mobile, 1800L);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	@RequestMapping(value = { "web/front/changePwd" }, method = RequestMethod.POST)
	@ResponseBody
	public ResultVo changePwd(String userPwd, String checkUserPwd) {
		try {
			String mobile = (String) SessionUtil.getSessionAttribute(Constant.CHANGE_PWD_MOBILE);
			if (mobile == null) {
				throw new ControllerException(ExceptionEnum.MOBILE_NOT_NULL);
			}
			if (!userPwd.equals(checkUserPwd)) {
				throw new ServiceException(ExceptionEnum.USERPWD_EQUALS_ERROR);
			}
			User user = this.checkExistsUser(mobile);
			String salt = CodeUtil.getUniqueCode();
			user.setSalt(salt);
			user.setUserPwd(MD5Util.getMD5Pwd(salt, userPwd));
			user.setModifyTime(DateUtil.getTime());
			this.userService.updateByPrimaryKey(user);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	@RequestMapping(value = { "m/front/changePwd" }, method = RequestMethod.POST)
	@ResponseBody
	public ResultVo changePwd(String mobile, String userPwd, String checkUserPwd) {
		try {
			if (!this.redisRepository.exist(Constant.CHANGE_PWD_MOBILE + ":" + mobile)) {
				throw new ControllerException(ExceptionEnum.MOBILE_NOT_NULL);
			}
			if (!userPwd.equals(checkUserPwd)) {
				throw new ServiceException(ExceptionEnum.USERPWD_EQUALS_ERROR);
			}
			User user = this.checkExistsUser(mobile);
			String salt = CodeUtil.getUniqueCode();
			user.setSalt(salt);
			user.setUserPwd(MD5Util.getMD5Pwd(salt, userPwd));
			user.setModifyTime(DateUtil.getTime());
			this.userService.updateByPrimaryKey(user);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	@RequestMapping(value = { "getInviteLink", "m/getInviteLink" }, method = RequestMethod.GET)
	@ResponseBody
	public ResultVo getInvitelink(HttpServletRequest request) {
		try {
			String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
					+ request.getContextPath();
			return ResultUtil.success(basePath + "/invite/link.html?invite="
					+ EncodeUtil.base64UrlSafeEncode(super.getCurrentUser().getMobile().getBytes()));
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	@RequestMapping(value = { "m/front/getUpdateInfo" }, method = RequestMethod.GET)
	@ResponseBody
	public ResultVo getUpdateInfo(Integer versionCode, String type) {
		 return null;
	}

	@RequestMapping(value = { "m/front/versionUpdate" }, method = RequestMethod.GET)
	public void versionUpdate(HttpServletResponse response, String type, String versionName) {
		InputStream in = null;
		OutputStream out = null;
		try {
			String fileName = null;
			if (type.equals("customer")) {
				fileName = "customer_v" + versionName + ".apk";
			} else {
				fileName = "employee_v" + versionName + ".apk";
			}
			in = FileUtils.openInputStream(new File(versionFilePath + fileName));
			response.setContentType("application/vnd.android.package-archive;charset=utf-8");
			int length = in.available();
			response.setContentLength(length);
			response.setHeader("content-disposition",
					"attachment;filename=" + new String(fileName.getBytes(), "iso-8859-1"));
			out = response.getOutputStream();
			byte[] bytes = new byte[length];
			in.read(bytes);
			out.write(bytes);
		} catch (Exception e) {
			throw new ControllerException("获取下载出错");
		} finally {
			IOUtils.closeQuietly(out);
		}
	}

	@RequestMapping(value = { "m/front/downloadApp" }, method = RequestMethod.GET)
	public void downloadApp(HttpServletResponse response, String type) {
		InputStream in = null;
		OutputStream out = null;
		try {
			String fileName = null;
			String properties = versionFilePath + "version.properties";
			if (type.equals("customer")) {
				fileName = "customer_v" + PropertiesUtil.getProperty(properties, "versionCustomerName") + ".apk";
			} else {
				fileName = "employee_v" + PropertiesUtil.getProperty(properties, "versionEmployeeName") + ".apk";
			}
			in = FileUtils.openInputStream(new File(versionFilePath + fileName));
			response.setContentType("application/vnd.android.package-archive;charset=utf-8");
			int length = in.available();
			response.setContentLength(length);
			response.setHeader("content-disposition",
					"attachment;filename=" + new String(fileName.getBytes(), "iso-8859-1"));
			out = response.getOutputStream();
			byte[] bytes = new byte[length];
			in.read(bytes);
			out.write(bytes);
		} catch (Exception e) {
			throw new ControllerException("获取下载出错");
		} finally {
			IOUtils.closeQuietly(out);
		}
	}
}