package com.zkzj.web.controller.sys;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zkzj.entity.sys.Merchants;
import com.zkzj.pojo.query.sys.MerchantsQuery;
import com.zkzj.pojo.vos.common.PageVo;
import com.zkzj.pojo.vos.common.ResultVo;
import com.zkzj.service.sys.MerchantsService;
import com.zkzj.web.controller.base.BaseController;
import com.zkzj.web.utils.ResultUtil;

@RequestMapping("sys/merchants")
@Controller
public class MerchantsController extends BaseController {

	@Autowired
	private MerchantsService merchantsService;

	/**
	 * 分页
 	 * 
 	 * @param merchantsQuery
	 * @param start
	 * @param limit
	 * @return
	 */
	@RequestMapping("pageMerchants")
	@ResponseBody
	public PageVo pageMerchants(MerchantsQuery merchantsQuery, Integer start, Integer limit) {
		return this.merchantsService.pageMerchants(merchantsQuery, start, limit);
	}

	/**
	 * 新增
	 * 
	 * @param merchants
	 * @return
	 */
	@RequestMapping(value = "saveMerchants", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo saveMerchants(Merchants merchants) {
		try {
			this.merchantsService.saveMerchants(merchants);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}
	
	/**
	 * 更新
	 * 
	 * @param merchants
	 * @return
	 */
	@RequestMapping(value = "updateMerchants", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo updateMerchants(Merchants merchants) {
		try {
			this.merchantsService.updateMerchants(merchants);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 删除
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "removeMerchants", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo removeMerchants(long id) {
		try {
			this.merchantsService.removeMerchants(id);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 恢复
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "recover", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo recover(long id) {
		try {
			this.merchantsService.recover(id);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

}