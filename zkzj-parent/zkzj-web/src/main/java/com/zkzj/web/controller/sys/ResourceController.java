package com.zkzj.web.controller.sys;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zkzj.entity.sys.Resource;
import com.zkzj.pojo.constants.Constant;
import com.zkzj.pojo.exceptions.ControllerException;
import com.zkzj.pojo.exceptions.ExceptionEnum;
import com.zkzj.pojo.vos.common.PageVo;
import com.zkzj.pojo.vos.common.ResultVo;
import com.zkzj.pojo.vos.sys.IconVo;
import com.zkzj.pojo.vos.sys.PermitVo;
import com.zkzj.service.sys.ResourceService;
import com.zkzj.utils.PropertiesUtil;
import com.zkzj.utils.ShiroUtil;
import com.zkzj.utils.ValidUtil;
import com.zkzj.web.controller.base.BaseController;
import com.zkzj.web.utils.ResultUtil;

/**
 * ResourceController
 * 
 * @author liu
 * @date 2016年8月21日
 */
@RequestMapping("sys/resource")
@Controller
public class ResourceController extends BaseController {

	@Autowired
	private ResourceService resourceService;

	@RequestMapping("listMenu")
	@ResponseBody
	public Resource listMenu() {
		return this.resourceService.listMenu();
	}

	@RequestMapping("list")
	@ResponseBody
	public Resource list() {
		return this.resourceService.listResource();
	}

	@RequestMapping("listGrant")
	@ResponseBody
	public Resource listGrant(long roleId) {
		return this.resourceService.listGrant(roleId);
	}

	/**
	 * 获取权限集
	 * 
	 * @return
	 */
	@RequestMapping("listPermit")
	@ResponseBody
	public List<PermitVo> listPermit() {
		if (SecurityUtils.getSubject().hasRole(Constant.ADMIN)) {
			return this.resourceService.listPermit(null);
		}
		return this.resourceService.listPermit(ShiroUtil.getPermissions());
	}

	@RequestMapping("pageIcon")
	@ResponseBody
	public PageVo pageIcon(HttpServletRequest request, int start, int limit) {
		File[] files = new File(request.getServletContext()
				.getRealPath(PropertiesUtil.getConfigProperty("resource.path") + "/assets/icon/icons")).listFiles();
		int filesLength = files.length;
		int st = start, li = start + limit;
		if (li > filesLength) {
			li = filesLength;
		}
		List<IconVo> fileNames = new ArrayList<IconVo>();
		for (int i = st; i < li; i++) {
			String name = files[i].getName();
			fileNames.add(new IconVo("/assets/icon/icons/" + name, "icon_" + name.substring(0, name.indexOf("."))));
		}
		return new PageVo(start, limit, Long.valueOf(filesLength), fileNames);
	}

	@RequestMapping(value = "updateResource", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo updateResource(Resource resource) {
		try {
			this.checkSave(resource);
			if (resource.getParentId() == null) {
				throw new ControllerException(ExceptionEnum.PARAMS_NOT_NULL);
			}
			this.resourceService.updateResource(resource.getId(), resource);
			return ResultUtil.success(resource);
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	@RequestMapping(value = "saveFrist", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo saveFrist(Resource resource) {
		try {
			this.checkSave(resource);
			resource.setParentId(1L);
			this.resourceService.saveResource(resource);
			return ResultUtil.success(resource);
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	@RequestMapping(value = "saveChild", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo saveChild(Resource resource) {
		try {
			this.checkSave(resource);
			if (resource.getParentId() == null) {
				throw new ControllerException(ExceptionEnum.PARAMS_NOT_NULL);
			}
			this.resourceService.saveResource(resource);
			return ResultUtil.success(resource);
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 检查 resource
	 * 
	 * @param resource
	 */
	private void checkSave(Resource resource) {
		if (resource == null) {
			throw new ControllerException(ExceptionEnum.PARAMS_NOT_NULL);
		}
		if (ValidUtil.isBlank(resource.getResourceName())) {
			throw new ControllerException(ExceptionEnum.TEXT_NOT_NULL);
		}
		if (ValidUtil.isBlank(resource.getUrl())) {
			throw new ControllerException(ExceptionEnum.URL_NOT_NULL);
		}
	}

	@RequestMapping(value = "freeze", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo freeze(long id) {
		try {
			this.resourceService.freeze(id);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	@RequestMapping(value = "unfreeze", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo unfreeze(long id) {
		try {
			this.resourceService.unfreeze(id);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	@RequestMapping(value = "removeResource", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo removeResource(long id) {
		try {
			return ResultUtil.success(this.resourceService.removeResource(id));
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	@RequestMapping(value = "recover", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo recover(long id) {
		try {
			this.resourceService.recover(id);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	@RequestMapping(value = "move", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo move(long id, long parentId) {
		try {
			if (id == parentId) {
				throw new ControllerException(ExceptionEnum.PARENT_NOT_EQUAL_ID);
			}
			return ResultUtil.success(this.resourceService.move(id, parentId));
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

}
