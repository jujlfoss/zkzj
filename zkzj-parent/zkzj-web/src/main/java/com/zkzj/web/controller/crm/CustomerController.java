package com.zkzj.web.controller.crm;

import java.io.IOException;
import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.zkzj.entity.crm.Customer;
import com.zkzj.entity.sys.User;
import com.zkzj.pojo.query.crm.CustomerQuery;
import com.zkzj.pojo.vos.common.PageVo;
import com.zkzj.pojo.vos.common.ResultVo;
import com.zkzj.service.crm.CustomerService;
import com.zkzj.service.fileutil.FileUploadService;
import com.zkzj.service.sys.UserService;
import com.zkzj.web.controller.base.BaseController;
import com.zkzj.web.utils.ResultUtil;

@RequestMapping({ "crm/customer", "m/crm/customer" })
@Controller
public class CustomerController extends BaseController {

	@Autowired
	private CustomerService customerService;
	@Autowired
	private UserService userService;
	@Autowired
	private FileUploadService fileUploadService;

	/**
	 * 分页
	 * 
	 * @param customerQuery
	 * @param start
	 * @param limit
	 * @return
	 */
	@RequestMapping("pageCustomer")
	@ResponseBody
	public PageVo pageCustomer(CustomerQuery customerQuery, int start, int limit) {
		return this.customerService.pageCustomer(customerQuery, start, limit);
	}

	/**
	 * 新增
	 * 
	 * @param customer
	 * @return
	 */
	@RequestMapping(value = "saveCustomer", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo saveCustomer(Customer customer) {
		try {
			this.customerService.saveCustomer(customer);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 更新
	 * 
	 * @param employee
	 * @return
	 */
	@RequestMapping(value = "updateCustomer", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo updateCustomer(Customer customer) {
		try {
			this.customerService.updateCustomer(customer);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 删除
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "removeCustomer", method = RequestMethod.GET)
	@ResponseBody
	public ResultVo removeCustomer(long id) {
		try {
			this.customerService.removeCustomer(id);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 恢复
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "recover", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo recover(long id) {
		try {
			this.customerService.recover(id);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	@RequestMapping(value = "getCustomerName")
	@ResponseBody
	public ResultVo getCustomerName(long id) {
		try {
			return ResultUtil.success(this.customerService.getCustomerName(id));
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	@RequestMapping(value = "updatePayPsw")
	@ResponseBody
	public ResultVo updatePayPsw(String newPsw, String pswRe, String mobileCode) {
		try {
			this.customerService.updatePayPsw(getCurrentCustomer().getId(), newPsw, pswRe, mobileCode);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	@RequestMapping(value = "webUpdateCustomerName")
	@ResponseBody
	public ResultVo webUpdateCustomerName(String customerName) {
		try {
			Customer c = getCurrentCustomer();
			c.setCustomerName(customerName);
			this.customerService.updateCustomer(c);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	@RequestMapping(value = "whetherSetPayPsw")
	@ResponseBody
	public ResultVo whetherSetPayPsw() {
		try {
			return ResultUtil.success(customerService.whetherSetPayPsw(getCurrentCustomer().getId()));
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	@RequestMapping(value = "isCheckPass")
	@ResponseBody
	public ResultVo isCheckPass(String mobile, String payPsw, String code) {
		try {
			return ResultUtil.success(customerService.isCheckPass(getCurrentCustomer().getId(), mobile, payPsw, code));
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	@RequestMapping(value = "webUpdateUser")
	@ResponseBody
	public ResultVo webUpdateUser(String mobile) {
		try {
			User user = getCurrentUser();
			user.setMobile(mobile);
			user.setUserName(mobile);
			userService.saveOrUpdateSelective(user);
			// Customer c = getCurrentCustomer();
			// c.setCustomerName(customerName);
			// customerService.saveOrUpdateSelective(c);
			SecurityUtils.getSubject().logout();
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	@RequestMapping(value = "webUpload")
	@ResponseBody
	public ResultVo upload(@RequestParam("files") List<MultipartFile> files) {
		try {
			String img = fileUploadService.multipleUpload(files, "fileUploadPath");
			Customer c = getCurrentCustomer();
			c.setAvatarUrl(img);
			customerService.updateByPrimaryKeySelective(c);
			return ResultUtil.success();
		} catch (IOException e) {
			return ResultUtil.error(log, e);
		}

	}

	@RequestMapping(value = "webGetCustomer")
	@ResponseBody
	public ResultVo webGetCustomer() {
		try {
			return ResultUtil.success(customerService.getCurrentCustomerInfo(getCurrentCustomer().getId()));
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

}