package com.zkzj.web.utils;

import java.math.BigDecimal;
import java.util.Map;
import java.util.TreeMap;

import com.zkzj.pojo.constants.Constant;
import com.zkzj.pojo.vos.pay.WeChatRefund;
import com.zkzj.pojo.vos.pay.WeChatUnifiedOrder;
import com.zkzj.utils.HttpUtil;
import com.zkzj.utils.MD5Util;
import com.zkzj.utils.NumberUtil;
import com.zkzj.utils.PublicUtil;
import com.zkzj.utils.ValidUtil;

/**
 * 第三方支付工具类
 * @author Heguoyong
 *
 */
public class PayUtil {

	private PayUtil() {
	}

	/**
	 * 微信支付二维码：code_url
	 */ 
	private static String CODE_URL = "code_url";
	
	private static String SIGN;

	public static String fromatWechatCodeAndMsg(String return_code,String return_msg) {
		StringBuffer sb = new StringBuffer();
		sb.append("<xml><return_code><![CDATA[").append(return_code).append("]]></return_code>");
		sb.append("<return_msg><![CDATA[").append(return_msg).append("]]></return_msg></xml>");
		return sb.toString();
	}
	
	public static int fromatWechatTotalToInt(BigDecimal totalFee) {
		
		return NumberUtil.mul(totalFee, BigDecimal.valueOf(100)).intValue();
	}

	public static BigDecimal fromatWechatTotalToBigDecimal(String totalFee) {
		
		return NumberUtil.div(BigDecimal.valueOf(Double.parseDouble(totalFee)), BigDecimal.valueOf(100));
	}
	
	/**
	 * 微信扫码支付：获取支付二维码code_url
	 * @return
	 */ 
	public static String getWechatQRCodeUrl(WeChatUnifiedOrder uord) {
		
		TreeMap<String, String> treeMap = new TreeMap<String, String>();
		/**必要字段*/
		treeMap.put("appid", uord.getAppid());
		treeMap.put("mch_id", uord.getMchId());
		treeMap.put("nonce_str", uord.getNonceStr());
		treeMap.put("body", uord.getBody());
		treeMap.put("out_trade_no", uord.getOutTradeNo());
		treeMap.put("total_fee", String.valueOf(uord.getTotalFee()));
		treeMap.put("spbill_create_ip", uord.getSpbillCreateIp());
		treeMap.put("trade_type", uord.getTradeType());
		treeMap.put("notify_url", uord.getNonceStr());
		/**非必要字段*/
		treeMap.put("attach", uord.getAttach());
		
		String result = HttpUtil.post(WeChatUnifiedOrder.URL, wechatXmlParams(treeMap,uord.getKey()));
		System.out.println("微信返回结果：\n" + result);
		return PublicUtil.xmlStrToMap(result).get(PayUtil.CODE_URL);
	}
	
	public static String wechatXmlParams(TreeMap<String, String> treeMap,String secretkey) {
		
		StringBuffer param = new StringBuffer();
		for (String key : treeMap.keySet()) {
			param.append(key).append("=").append(treeMap.get(key)).append("&");
		}
		param.append("key=" + secretkey);
		SIGN = MD5Util.MD5Encode(param.toString(),"utf-8").toUpperCase();
		treeMap.put("sign", SIGN);
		
		StringBuilder xml = new StringBuilder();
		xml.append("<xml>\n");
		for (Map.Entry<String, String> entry : treeMap.entrySet()) {
			if ("body".equals(entry.getKey()) || "sign".equals(entry.getKey())) {
				xml.append("<" + entry.getKey() + "><![CDATA[").append(entry.getValue()).append("]]></" + entry.getKey() + ">\n");
			} else {
				xml.append("<" + entry.getKey() + ">").append(entry.getValue()).append("</" + entry.getKey() + ">\n");
			}
		}
		xml.append("</xml>");
		return xml.toString();
	}
	/**
	 * 微信用户申请退款
	 * @return
	 */ 
	public static boolean refund(WeChatRefund refund) {
		
		TreeMap<String, String> treeMap = new TreeMap<String, String>();
		/**必要字段*/
		treeMap.put("appid", refund.getAppid());
		treeMap.put("mch_id", refund.getMchId());
		treeMap.put("nonce_str", refund.getNonceStr());
		if(ValidUtil.isNotBlank(refund.getTransactionId())) {
			treeMap.put("transaction_id", refund.getTransactionId());
		} else {
			treeMap.put("out_trade_no", refund.getOutTradeNo());
		}
		treeMap.put("out_refund_no", refund.getOutTradeNo());
		treeMap.put("total_fee", String.valueOf(refund.getTotalFee()));
		treeMap.put("refund_fee", String.valueOf(refund.getTotalFee()));
		treeMap.put("refund_desc", refund.getRefundDesc());
		
		String result = HttpUtil.post(WeChatRefund.URL, wechatXmlParams(treeMap, refund.getKey()));
		System.out.println("微信返回结果：\n" + result);
		
		return Constant.SUCCESS.equals(PublicUtil.xmlStrToMap(result).get("return_code"));
	}
}
