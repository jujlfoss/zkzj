package com.zkzj.web.controller.sys;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zkzj.entity.sys.MerchantsType;
import com.zkzj.pojo.query.sys.MerchantsTypeQuery;
import com.zkzj.pojo.vos.common.PageVo;
import com.zkzj.pojo.vos.common.ResultVo;
import com.zkzj.service.sys.MerchantsTypeService;
import com.zkzj.web.controller.base.BaseController;
import com.zkzj.web.utils.ResultUtil;

@RequestMapping("sys/merchantsType")
@Controller
public class MerchantsTypeController extends BaseController {

	@Autowired
	private MerchantsTypeService merchantsTypeService;

	/**
	 * 分页
 	 * 
 	 * @param merchantsTypeQuery
	 * @param start
	 * @param limit
	 * @return
	 */
	@RequestMapping("pageMerchantsType")
	@ResponseBody
	public PageVo pageMerchantsType(MerchantsTypeQuery merchantsTypeQuery, Integer start, Integer limit) {
		return this.merchantsTypeService.pageMerchantsType(merchantsTypeQuery, start, limit);
	}

	/**
	 * 新增
	 * 
	 * @param merchantsType
	 * @return
	 */
	@RequestMapping(value = "saveMerchantsType", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo saveMerchantsType(MerchantsType merchantsType) {
		try {
			this.merchantsTypeService.saveMerchantsType(merchantsType);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}
	
	/**
	 * 更新
	 * 
	 * @param merchantsType
	 * @return
	 */
	@RequestMapping(value = "updateMerchantsType", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo updateMerchantsType(MerchantsType merchantsType) {
		try {
			this.merchantsTypeService.updateMerchantsType(merchantsType);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 删除
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "removeMerchantsType", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo removeMerchantsType(long id) {
		try {
			this.merchantsTypeService.removeMerchantsType(id);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 恢复
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "recover", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo recover(long id) {
		try {
			this.merchantsTypeService.recover(id);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

}