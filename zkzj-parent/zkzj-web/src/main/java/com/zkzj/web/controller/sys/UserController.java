package com.zkzj.web.controller.sys;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zkzj.dao.redis.RedisRepository;
import com.zkzj.entity.sys.User;
import com.zkzj.pojo.constants.Constant;
import com.zkzj.pojo.exceptions.ControllerException;
import com.zkzj.pojo.exceptions.ExceptionEnum;
import com.zkzj.pojo.query.sys.UserQuery;
import com.zkzj.pojo.vos.common.PageVo;
import com.zkzj.pojo.vos.common.ResultVo;
import com.zkzj.service.hrm.EmployeeService;
import com.zkzj.service.sys.UserService;
import com.zkzj.utils.CodeUtil;
import com.zkzj.utils.DateUtil;
import com.zkzj.utils.MD5Util;
import com.zkzj.utils.ValidUtil;
import com.zkzj.web.controller.base.BaseController;
import com.zkzj.web.utils.ResultUtil;

/**
 * UserController
 * 
 * @author liu
 * @date 2016年8月21日
 */
@RequestMapping({ "sys/user", "m/sys/user" })
@Controller
public class UserController extends BaseController {

	@Autowired
	private UserService userService;
	@Autowired
	private EmployeeService employeeService;
	@Autowired
	private RedisRepository redisRepository;
	@Value("${user.resetPwd}")
	private String resetPwd;

	@RequestMapping("page")
	@ResponseBody
	public PageVo page(UserQuery userQuery, int start, int limit) {
		return this.userService.page(userQuery, start, limit);
	}

	/**
	 * 添加用户
	 * 
	 * @param user
	 * @param userPwdRe
	 * @return
	 */
	@RequestMapping(value = "saveUser", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo saveUser(User user/* , String userPwdRe */) {
		try {
			// 验证
			if (user == null) {
				throw new ControllerException(ExceptionEnum.PARAMS_NOT_NULL);
			}
			// 用户名
			String userName = user.getUserName();
			if (ValidUtil.isBlank(userName)) {
				throw new ControllerException(ExceptionEnum.USERNAME_NOT_NULL);
			}
			if (!ValidUtil.isUserName(userName)) {
				throw new ControllerException(ExceptionEnum.USERNAME_FORMAT_ERROR);
			}
			// // 密码
			// String userPwd = user.getUserPwd();
			// if (ValidUtil.isBlank(userPwd)) {
			// throw new ControllerException(ExceptionEnum.USERPWD_NOT_NULL);
			// }
			// if (!ValidUtil.isPwd(userPwd)) {
			// throw new
			// ControllerException(ExceptionEnum.USERPWD_FORMAT_ERROR);
			// }
			// if (ValidUtil.isBlank(userPwdRe)) {
			// throw new ControllerException(ExceptionEnum.USERPWD_CHECK_NULL);
			// }
			// if (!userPwd.trim().equals(userPwdRe.trim())) {
			// throw new
			// ControllerException(ExceptionEnum.USERPWD_EQUALS_ERROR);
			// }
			// 手机
			String mobile = user.getMobile();
			if (ValidUtil.isNotBlank(mobile)) {
				if (!ValidUtil.isMobile(mobile)) {
					throw new ControllerException(ExceptionEnum.MOBILE_FORMAT_ERROR);
				}
			} else {
				user.setMobile(null);
			}
			// 邮箱
			String email = user.getEmail();
			if (ValidUtil.isNotBlank(email)) {
				if (!ValidUtil.isEmail(email)) {
					throw new ControllerException(ExceptionEnum.EMAIL_FORMAT_ERROR);
				}
			} else {
				user.setEmail(null);
			}
			user.setUserPwd(resetPwd);
			this.employeeService.saveEmployeeUser(user);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 编辑用户
	 * 
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "updateUser", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo updateUser(User user) {
		try {
			// 验证
			if (user == null) {
				throw new ControllerException(ExceptionEnum.PARAMS_NOT_NULL);
			}
			// 用户名
			String userName = user.getUserName();
			if (ValidUtil.isBlank(userName)) {
				throw new ControllerException(ExceptionEnum.USERNAME_NOT_NULL);
			}
			if (!ValidUtil.isUserName(userName)) {
				throw new ControllerException(ExceptionEnum.USERNAME_FORMAT_ERROR);
			}
			// 手机
			String mobile = user.getMobile();
			if (ValidUtil.isNotBlank(mobile)) {
				if (!ValidUtil.isMobile(mobile)) {
					throw new ControllerException(ExceptionEnum.MOBILE_FORMAT_ERROR);
				}
			} else {
				user.setMobile(null);
			}
			// 邮箱
			String email = user.getEmail();
			if (ValidUtil.isNotBlank(email)) {
				if (!ValidUtil.isEmail(email)) {
					throw new ControllerException(ExceptionEnum.EMAIL_FORMAT_ERROR);
				}
			} else {
				user.setEmail(null);
			}
			this.userService.updateUser(user);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 重置用户密码
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "resetPwd", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo resetPwd(long id) {
		try {
			this.userService.resetPwd(id);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 删除用户
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "removeUser", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo removeUser(long id) {
		try {
			this.userService.removeUser(id);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 删除用户
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "recover", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo recover(long id) {
		try {
			this.userService.recover(id);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 冻结用户
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "freeze", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo freeze(long id) {
		try {
			this.userService.freeze(id);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 解冻用户
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "unfreeze", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo unfreeze(long id) {
		try {
			this.userService.unfreeze(id);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 授予角色
	 * 
	 * @param id
	 * @param roleIds
	 * @return
	 */
	@RequestMapping(value = "grant", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo grant(long userId, String roleIds) {
		try {
			Set<Long> roleSet = null;
			if (ValidUtil.isNotBlank(roleIds)) {
				roleSet = new HashSet<Long>();
				String[] roleArr = roleIds.split(",");
				for (String roleId : roleArr) {
					roleSet.add(Long.valueOf(roleId));
				}
			}
			this.userService.grant(userId, roleSet);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	@RequestMapping(value = "getUserName", method = RequestMethod.GET)
	@ResponseBody
	public ResultVo getUserName(long id) {
		try {
			return ResultUtil.success(userService.getByPrimaryKey(id).getUserName());
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}

	}

	@RequestMapping(value = "getCurUserId", method = RequestMethod.GET)
	@ResponseBody
	public ResultVo getCurUserId() {
		try {
			return ResultUtil.success(getCurrentUser().getId());
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}

	}

	@RequestMapping(value = { "changePwd", "front/changePwd" }, method = RequestMethod.POST)
	@ResponseBody
	public ResultVo changePwd(String oldPwd, String newPwd, String newPwdRe) {
		try {
			User user = super.getCurrentUser();
			String salt = user.getSalt();
			if (!MD5Util.getMD5Pwd(salt, oldPwd).equals(user.getUserPwd())) {
				throw new ControllerException(ExceptionEnum.OLD_PWD_ERROR);
			}
			if (!newPwd.equals(newPwdRe)) {
				throw new ControllerException(ExceptionEnum.USERPWD_EQUALS_ERROR);
			}
			String newSalt = CodeUtil.getUniqueCode();
			user.setSalt(newSalt);
			user.setUserPwd(MD5Util.getMD5Pwd(newSalt, newPwd));
			user.setModifyTime(DateUtil.getTime());
			this.userService.updateByPrimaryKey(user);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}

	}

	@RequestMapping(value = "getCurUserInfo")
	@ResponseBody
	public ResultVo getCurUserInfo() {
		try {
			return ResultUtil.success(getCurrentUser());
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}

	}

	@RequestMapping(value = "updatePwd", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo updatePwd(String oldPwd, String newPwd, String newPwdRe, String code) {
		try {
			String mobileCodeKey = Constant.VALIDCODE + getCurrentUser().getMobile();
			if (!((String) this.redisRepository.get(mobileCodeKey)).equalsIgnoreCase(code)) {
				throw new ControllerException("验证码错误");
			}
			this.userService.changePwd(getCurrentUser().getId(), oldPwd, newPwd, newPwdRe);
			redisRepository.delete(mobileCodeKey);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}

	}

}