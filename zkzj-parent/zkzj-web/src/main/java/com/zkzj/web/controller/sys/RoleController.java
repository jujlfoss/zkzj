package com.zkzj.web.controller.sys;

import java.util.List;
import java.util.Set;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zkzj.entity.enums.StatusEnum;
import com.zkzj.entity.sys.Role;
import com.zkzj.pojo.constants.Constant;
import com.zkzj.pojo.vos.common.PageVo;
import com.zkzj.pojo.vos.common.ResultVo;
import com.zkzj.service.sys.RoleService;
import com.zkzj.utils.ValidUtil;
import com.zkzj.web.controller.base.BaseController;
import com.zkzj.web.utils.ResultUtil;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * RoleController
 * 
 * @author liu
 * @date 2016年8月21日
 */
@RequestMapping("/sys/role")
@Controller
public class RoleController extends BaseController {

	@Autowired
	private RoleService roleService;

	/**
	 * 分页查询
	 * 
	 * @param role
	 * @param start
	 * @param limit
	 * @return
	 */
	@RequestMapping("page")
	@ResponseBody
	public PageVo page(Role role, int start, int limit) {
		Example example = new Example(Role.class);
		Criteria criteria = example.createCriteria();
		String roleName = role.getRoleName();
		if (ValidUtil.isNotBlank(roleName)) {
			criteria.andLike("roleName", "%" + roleName + "%");
		}
		return this.roleService.pageByExample(example, start, limit);
	}

	/**
	 * 获取角色list
	 * 
	 * @return
	 */
	@RequestMapping("list")
	@ResponseBody
	public List<Role> list() {
		Example example = new Example(Role.class);
		Criteria c = example.createCriteria();
		if (SecurityUtils.getSubject().hasRole(Constant.FRANCHISEE)) {
			c.andEqualTo("roleName", Constant.WAITER);
		}
		c.andEqualTo("status", StatusEnum.NORMAL.getValue());
		return this.roleService.listByExample(example);
	}

	/**
	 * 获取角色list
	 * 
	 * @return
	 */
	@RequestMapping("listIdByUserId")
	@ResponseBody
	public List<Long> listIdByUserId(long userId) {
		return this.roleService.listIdByUserId(userId);
	}

	/**
	 * 授权
	 * 
	 * @param roleId
	 * @param resourceIds
	 * @return
	 */
	@RequestMapping("grant")
	@ResponseBody
	public ResultVo grant(long id, @RequestParam(name = "resourceIds", required = false) Set<Long> resourceIds) {
		try {
			this.roleService.grant(id, resourceIds);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 添加角色
	 * 
	 * @param role
	 * @return
	 */
	@RequestMapping("save")
	@ResponseBody
	public ResultVo save(Role role) {
		try {
			this.roleService.saveRole(role);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 编辑角色
	 * 
	 * @param role
	 * @return
	 */
	@RequestMapping("updateRole")
	@ResponseBody
	public ResultVo updateRole(Role role) {
		try {
			this.roleService.updateRole(role.getId(), role);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 删除角色
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "removeRole", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo removeRole(long id) {
		try {
			this.roleService.removeRole(id);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 恢复角色
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "recover", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo recover(long id) {
		try {
			this.roleService.recover(id);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 冻结用户
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "freeze", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo freeze(long id) {
		try {
			this.roleService.freeze(id);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 解冻用户
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "unfreeze", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo unfreeze(long id) {
		try {
			this.roleService.unfreeze(id);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

}