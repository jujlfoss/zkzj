package com.zkzj.web.controller.base;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.zkzj.entity.crm.Customer;
import com.zkzj.entity.hrm.Employee;
import com.zkzj.entity.sys.User;
import com.zkzj.pojo.constants.Constant;
import com.zkzj.service.crm.CustomerService;
import com.zkzj.service.hrm.EmployeeService;
import com.zkzj.utils.SessionUtil;

/**
 * BaseController
 * 
 * @author liu
 * @date 2016年8月21日
 */
public abstract class BaseController {

	protected Logger log = LogManager.getLogger(this.getClass());

	@Autowired
	private CustomerService customerService;

	@Autowired
	private EmployeeService employeeService;

	/**
	 * 得到当前登录用户信息
	 * 
	 * @return
	 */
	protected User getCurrentUser() {
		return (User) SessionUtil.getSessionAttribute(Constant.CURRENT_USER);
	}

	/**
	 * 得到当前登录的客户信息
	 * 
	 * @return
	 */
	protected Customer getCurrentCustomer() {
		User user = this.getCurrentUser();
		return user == null ? null : this.customerService.getCustomerByUserId(user.getId());
	}

	/**
	 * 得到当前登录的员工信息
	 * 
	 * @return
	 */
	protected Employee getCurrentEmployee() {
		User user = this.getCurrentUser();
		return user == null ? null : this.employeeService.getEmployeeByUserId(user.getId());
	}

	/**
	 * 得到当前登录的员工类型
	 * 
	 * @return
	 */
	protected Integer getUserType() {
		return this.getCurrentUser().getUserType();
	}

}