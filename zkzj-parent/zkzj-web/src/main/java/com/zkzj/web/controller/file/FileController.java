package com.zkzj.web.controller.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.zkzj.pojo.vos.common.ResultVo;
import com.zkzj.service.fileutil.FileUploadService;
import com.zkzj.web.controller.base.BaseController;
import com.zkzj.web.utils.ResultUtil;

/**
 * 图片上传 controller
 * 
 * @author liu
 *
 */
@RequestMapping({"file","m/file"})
@Controller
public class FileController extends BaseController {

	private static final String CONTENTTYPE = "image/jpeg";

	@Autowired
	private FileUploadService fileUploadService;

	@Value("${fileUploadPath}")
	private String fileUploadPath;

	/**
	 * 图片显示
	 * 
	 * @param img
	 * @param request
	 * @param response
	 */
	@RequestMapping("display/{img}")
	public void display(@PathVariable String img, HttpServletRequest request, HttpServletResponse response) {
		FileInputStream fis = null;
		OutputStream out = null;
		try {
			String servletPath = request.getServletPath();
			String ext = servletPath.substring(servletPath.indexOf("."));
			out = response.getOutputStream();
			response.setContentType(CONTENTTYPE);
			fis = new FileInputStream(new File(fileUploadPath + img + ext));
			byte[] b = new byte[fis.available()];
			fis.read(b);
			out.write(b);
			out.flush();
		} catch (Exception e) {

		} finally {
			IOUtils.closeQuietly(fis);
			IOUtils.closeQuietly(out);
		}
	}

	/**
	 * 上传差切图
	 * 
	 * @param files
	 * @param height
	 * @param width
	 * @param req
	 * @return
	 */
	@RequestMapping("uploadWithThumbnails")
	@ResponseBody
	public ResultVo uploadWithThumbnails(@RequestParam("file") List<MultipartFile> files,
			@RequestParam("height") Integer height, @RequestParam("width") Integer width, HttpServletRequest req) {
		try {
			return ResultUtil.success(fileUploadService.uploadWithThumbnails(files, height, width, "fileUploadPath"));
		} catch (IOException e) {
			log.debug(e.getMessage() + "上传图片失败 ", e);
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 上传
	 * 
	 * @param files
	 * @return
	 */
	@RequestMapping(value = "upload")
	@ResponseBody
	public ResultVo upload(@RequestParam("files") List<MultipartFile> files) {
		try {
			return ResultUtil.success(fileUploadService.multipleUpload(files, "fileUploadPath"));
		} catch (IOException e) {
			log.debug(e.getMessage() + "上传图片失败 ", e);
			return ResultUtil.error(log, e);
		}

	}

	/**
	 * 删除图片
	 * 
	 * @param img
	 * @return
	 */
	@RequestMapping(value = "{img}/deleteImage")
	@ResponseBody
	public ResultVo deleteImage(@PathVariable String img) {
		try {
			fileUploadService.deleteImage(img);
			return ResultUtil.success();
		} catch (Exception e) {
			log.debug(e.getMessage() + "上传图片失败 ", e);
			return ResultUtil.error(log, e);
		}

	}

}
