package com.zkzj.web.controller.sys;

import com.zkzj.utils.tree.BaseTree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zkzj.entity.sys.Area;
import com.zkzj.pojo.query.sys.AreaQuery;
import com.zkzj.pojo.vos.common.PageVo;
import com.zkzj.pojo.vos.common.ResultVo;
import com.zkzj.service.sys.AreaService;
import com.zkzj.web.controller.base.BaseController;
import com.zkzj.web.utils.ResultUtil;

@RequestMapping("sys/area")
@Controller
public class AreaController extends BaseController {

	@Autowired
	private AreaService areaService;

	/**
	 * 分页
 	 * 
 	 * @param areaQuery
	 * @param start
	 * @param limit
	 * @return
	 */
	@RequestMapping("pageArea")
	@ResponseBody
	public PageVo pageArea(AreaQuery areaQuery, Integer start, Integer limit) {
		return this.areaService.pageArea(areaQuery, start, limit);
	}

	/**
	 * 新增
	 * 
	 * @param area
	 * @return
	 */
	@RequestMapping(value = "saveArea", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo saveArea(Area area) {
		try {
			this.areaService.saveArea(area);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}
	
	/**
	 * 更新
	 * 
	 * @param area
	 * @return
	 */
	@RequestMapping(value = "updateArea", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo updateArea(Area area) {
		try {
			this.areaService.updateArea(area);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 删除
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "removeArea", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo removeArea(long id) {
		try {
			this.areaService.removeArea(id);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 恢复
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "recover", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo recover(long id) {
		try {
			this.areaService.recover(id);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	@RequestMapping(value = "areaTree")
	@ResponseBody
	public BaseTree recover() {
		return areaService.areaTree();
	}

}