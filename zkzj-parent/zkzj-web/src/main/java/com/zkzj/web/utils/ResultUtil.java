package com.zkzj.web.utils;

import java.io.Serializable;

import org.apache.logging.log4j.Logger;

import com.zkzj.pojo.exceptions.ControllerException;
import com.zkzj.pojo.exceptions.ExceptionEnum;
import com.zkzj.pojo.exceptions.ServiceException;
import com.zkzj.pojo.vos.common.ResultVo;

/**
 * 
 * @author liu
 *
 */
public class ResultUtil {

	public final static String SUCCESS_MSG = "操作成功";

	private ResultUtil() {
	}

	public static ResultVo success() {
		return new ResultVo(true, null, SUCCESS_MSG);
	}

	public static ResultVo success(final Serializable data) {
		return new ResultVo(true, data, null, SUCCESS_MSG);
	}

	public static ResultVo success(final Serializable data, final String msg) {
		return new ResultVo(true, data, null, msg);
	}

	public static ResultVo error(final Logger log, final Exception e) {
		log.info(e.getMessage(), e);
		if (e instanceof ControllerException) {
			return new ResultVo(false, ((ControllerException) e).getCode(), e.getMessage());
		}
		if (e instanceof ServiceException) {
			return new ResultVo(false, ((ServiceException) e).getCode(), e.getMessage());
		}
		return new ResultVo(false, ExceptionEnum.UNKNOWN.getCode(), ExceptionEnum.UNKNOWN.getMsg());
	}

}
