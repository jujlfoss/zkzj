package com.zkzj.web.controller.hrm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zkzj.entity.hrm.Employee;
import com.zkzj.pojo.query.hrm.EmployeeQuery;
import com.zkzj.pojo.vos.common.PageVo;
import com.zkzj.pojo.vos.common.ResultVo;
import com.zkzj.service.hrm.EmployeeService;
import com.zkzj.web.controller.base.BaseController;
import com.zkzj.web.utils.ResultUtil;

@RequestMapping({ "hrm/employee", "m/hrm/employee" })
@Controller
public class EmployeeController extends BaseController {

	@Autowired
	private EmployeeService employeeService;

	/**
	 * 分页
	 * 
	 * @param employeeQuery
	 * @param start
	 * @param limit
	 * @return
	 */
	@RequestMapping("pageEmployee")
	@ResponseBody
	public PageVo pageEmployee(EmployeeQuery employeeQuery, int start, int limit) {
		return this.employeeService.pageEmployee(employeeQuery, start, limit);
	}

	/**
	 * 新增
	 * 
	 * @param employee
	 * @return
	 */
	@RequestMapping(value = "saveEmployee", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo saveEmployee(Employee employee) {
		try {
			this.employeeService.saveEmployee(employee);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 更新
	 * 
	 * @param employee
	 * @return
	 */
	@RequestMapping(value = "updateEmployee", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo updateEmployee(Employee employee) {
		try {
			this.employeeService.updateEmployee(employee);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 删除
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "removeEmployee", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo removeEmployee(long id) {
		try {
			this.employeeService.removeEmployee(id);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 恢复
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "recover", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo recover(long id) {
		try {
			this.employeeService.recover(id);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 冻结用户
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "freeze", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo freeze(long id) {
		try {
			this.employeeService.freeze(id);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 解冻用户
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "unfreeze", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo unfreeze(long id) {
		try {
			this.employeeService.unfreeze(id);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}


	@RequestMapping("get")
	@ResponseBody
	public Employee get() {
		Employee employee = super.getCurrentEmployee();
		employee.setId(null);
		return employee;
	}

}