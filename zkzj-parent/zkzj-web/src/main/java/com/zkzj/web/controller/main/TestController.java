package com.zkzj.web.controller.main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.zkzj.entity.sys.User;
import com.zkzj.pojo.vos.common.ResultVo;
import com.zkzj.service.sys.UserService;
import com.zkzj.utils.CodeUtil;
import com.zkzj.utils.LuceneUtil;
import com.zkzj.web.controller.base.BaseController;
import com.zkzj.web.utils.ResultUtil;

import tk.mybatis.mapper.entity.Example;

/**
 * 
 * @author liu
 *
 */
@RequestMapping("test")
@Controller
public class TestController extends BaseController {
	@Autowired
	private UserService userService;

	@RequestMapping("upload")
	@ResponseBody
	public ResultVo upload(HttpServletRequest request, @RequestParam(value = "file") MultipartFile file)
			throws IOException {
		// 上传
		String fileName = CodeUtil.getUniqueCode() + "." + FilenameUtils.getExtension(file.getOriginalFilename());
		String dirPath = "e://file//";
		File dirFile = new File(dirPath);
		if (!dirFile.exists()) {
			dirFile.mkdirs();
		}
		FileUtils.writeByteArrayToFile(new File(dirPath + fileName), file.getBytes());
		return ResultUtil.success();
	}

	@RequestMapping("getByExample")
	@ResponseBody
	public User getByExample() {
		Example ex = new Example(User.class);
		ex.createCriteria().andNotEqualTo("id", 1);
		return this.userService.getByExample(ex);
	}

	@RequestMapping("/index")
	public String index() { 
		return "front/index";
	}

	@RequestMapping("/create")
	public void create() {
		LuceneUtil.create(userService.listAll());
	} 

	@RequestMapping("search/{keyword}")
	@ResponseBody
	public List<Object> search(@PathVariable String keyword) {
		return LuceneUtil.search(keyword, new ArrayList<String>() {
			private static final long serialVersionUID = 1L;
			{
				add("userName");
			}
		}, User.class);
	}

}