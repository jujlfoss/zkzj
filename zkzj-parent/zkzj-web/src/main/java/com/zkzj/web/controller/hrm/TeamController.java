package com.zkzj.web.controller.hrm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zkzj.entity.hrm.Team;
import com.zkzj.pojo.exceptions.ControllerException;
import com.zkzj.pojo.exceptions.ExceptionEnum;
import com.zkzj.pojo.vos.common.PageVo;
import com.zkzj.pojo.vos.common.ResultVo;
import com.zkzj.service.hrm.TeamService;
import com.zkzj.web.controller.base.BaseController;
import com.zkzj.web.utils.ResultUtil;

@RequestMapping("hrm/team")
@Controller
public class TeamController extends BaseController {

	@Autowired
	private TeamService teamService;

	@RequestMapping("page")
	@ResponseBody
	public PageVo page(Team team, int start, int limit) {
		return this.teamService.pageTeam(team, start, limit);
	}

	@RequestMapping("list")
	@ResponseBody
	public Team list(String teamName) {
		return this.teamService.listTeam();
	}

	@RequestMapping("listChecked")
	@ResponseBody
	public Team listChecked() {
		return this.teamService.listChecked();
	}

	/**
	 * 添加
	 * 
	 * @param team
	 * @return
	 */
	@RequestMapping(value = "saveFrist", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo saveFrist(Team team) {
		try {
			team.setParentId(1L);
			this.teamService.saveTeam(team);
			return ResultUtil.success(team);
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	@RequestMapping(value = "saveChild", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo saveChild(Team team) {
		try {
			if (team.getParentId() == null) {
				throw new ControllerException(ExceptionEnum.PARAMS_NOT_NULL);
			}
			this.teamService.saveTeam(team);
			return ResultUtil.success(team);
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 编辑
	 * 
	 * @param employee
	 * @return
	 */
	@RequestMapping(value = "updateTeam", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo updateTeam(Team team) {
		try {
			this.teamService.updateTeam(team);
			return ResultUtil.success(team);
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 移动
	 * 
	 * @param id
	 * @param parentId
	 * @return
	 */
	@RequestMapping(value = "move", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo move(long id, long parentId) {
		try {
			if (id == parentId) {
				throw new ControllerException(ExceptionEnum.PARENT_NOT_EQUAL_ID);
			}
			return ResultUtil.success(this.teamService.move(id, parentId));
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 删除
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "removeTeam", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo removeTeam(long id) {
		try {
			return ResultUtil.success(this.teamService.removeTeam(id));
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 删除用户
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "recover", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo recover(long id) {
		try {
			this.teamService.recover(id);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}
}