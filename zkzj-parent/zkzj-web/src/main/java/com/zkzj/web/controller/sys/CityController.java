package com.zkzj.web.controller.sys;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zkzj.entity.sys.City;
import com.zkzj.pojo.query.sys.CityQuery;
import com.zkzj.pojo.vos.common.PageVo;
import com.zkzj.pojo.vos.common.ResultVo;
import com.zkzj.service.sys.CityService;
import com.zkzj.web.controller.base.BaseController;
import com.zkzj.web.utils.ResultUtil;

import java.util.List;

@RequestMapping("sys/city")
@Controller
public class CityController extends BaseController {

	@Autowired
	private CityService cityService;

	/**
	 * 分页
 	 * 
 	 * @param cityQuery
	 * @param start
	 * @param limit
	 * @return
	 */
	@RequestMapping("pageCity")
	@ResponseBody
	public PageVo pageCity(CityQuery cityQuery, Integer start, Integer limit) {
		return this.cityService.pageCity(cityQuery, start, limit);
	}

	/**
	 * 新增
	 * 
	 * @param city
	 * @return
	 */
	@RequestMapping(value = "saveCity", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo saveCity(City city) {
		try {
			this.cityService.saveCity(city);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}
	
	/**
	 * 更新
	 * 
	 * @param city
	 * @return
	 */
	@RequestMapping(value = "updateCity", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo updateCity(City city) {
		try {
			this.cityService.updateCity(city);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 删除
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "removeCity", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo removeCity(long id) {
		try {
			this.cityService.removeCity(id);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	/**
	 * 恢复
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "recover", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo recover(long id) {
		try {
			this.cityService.recover(id);
			return ResultUtil.success();
		} catch (Exception e) {
			return ResultUtil.error(log, e);
		}
	}

	@RequestMapping("getList")
	@ResponseBody
	public List<City> getList(Long id) {
		return cityService.getList(id);
	}

}