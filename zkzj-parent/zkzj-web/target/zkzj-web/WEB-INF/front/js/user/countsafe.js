require.async([ 'JqueryBase','JSEncrypt' ], function() {
	window.changPsw = function() {
		var psw = $("#psw").val();
		var newPsw = $("#newPsw").val();
		var newPswRe = $("#newPswRe").val();
		var imgcode = $("#imgcode").val();
		if (psw == "") {
			layer.msg("请输入原密码");
			return;
		}
		if(psw.length<6||psw.length>12){
			layer.msg("密码最少6位最大11位");
			return;
		}
		if (newPsw == "") {
			layer.msg("请输入新密码");
			return;
		}
		if (newPswRe == "") {
			layer.msg("请再次输入密码");
			return;
		}
		if (newPsw != newPswRe) {
			layer.msg("两次密码输入不一至,请重新输入");
			return;
		}
		if(imgcode ==""){
			layer.msg("请输入图片验证码");
			return;
		}
		BaseUtil.post("sys/user/updatePwd", function(data) {
			if(data.success){
				layer.msg(data.msg);
				$("#psw").val("");
				$("#newPsw").val("");
				 $("#newPswRe").val("");
				 $("#imgcode").val("");
				$("#login-imageCodeImg").attr("src",basePath+"/imageCode?mobile="+result.data.mobile+"&dc="+Math.random());
			}else{
				layer.msg(data.msg);
				 $("#imgcode").val("");
				$("#login-imageCodeImg").attr("src",basePath+"/imageCode?mobile="+result.data.mobile+"&dc="+Math.random());
			}
			
		}, {
			"oldPwd" : psw,
			"newPwd" : newPsw,
			"newPwdRe" : newPswRe,
			"code":imgcode
		})
	}
	window.changePayPsw = function() {
		var newPayPsw = $("#newPayPsw").val();
		var payPswRn = $("#payPswRn").val();
		var code = $("#code").val();
	   if(!ValidUtil.isNumber(newPayPsw)){
		   layer.msg("支付密码只能是数字");
			return;
	   }
		if (newPayPsw == "") {
			layer.msg("请输入支付密码");
			return;
		}
		if(newPayPsw.length<6||newPayPsw.length>12){
			layer.msg("支付密码最少6位最大11位");
			return;
		}
		if (payPswRn == "") {
			layer.msg("请确认支付密码");
			return;
		}
		if (code == "") {
			layer.msg("请输入手机验证码");
			return;
		}
		if (newPayPsw != payPswRn) {
			layer.msg("两次密码输入不一至,请重新输入");
			return;
		}
		BaseUtil.post("crm/customer/updatePayPsw", function(data) {
			layer.msg(data.msg);
			 $("#newPayPsw").val("");
			 $("#payPswRn").val("");
			 $("#code").val("");
			 var o = document.getElementById("btn2");
				$(".btn_mfyzm").css("background", "#F98322");
				$(".btn_mfyzm").css("color", "#fff");
				o.removeAttribute("disabled");
				o.value = "免费获取验证码";
				wait = 0;
		}, {
			"newPsw" : newPayPsw,
			"pswRe" : payPswRn,
			"mobileCode" : code
		})
	}
	var wait = 60;
	document.getElementById("btn2").disabled = false;
	document.getElementById("btn2").onclick = function() {
		var self = this;
		BaseUtil.post('smsCode', function(data) {
			if (data.success) {
				layer.msg('验证码发送成功！');
				time(self);
				return;
			}
			layer.msg(data.msg)
			return;
		});
	}

	function time(o) {
		if (wait == 0) {
			$(".btn_mfyzm").css("background", "#F98322");
			$(".btn_mfyzm").css("color", "#fff");
			o.removeAttribute("disabled");
			o.value = "免费获取验证码";
			wait = 60;
		} else {
			$(".btn_mfyzm").css("background", "#CCCCCC");
			$(".btn_mfyzm").css("color", "#999");
			o.setAttribute("disabled", true);
			o.value = "重新发送(" + wait + ")";
			wait--;
			setTimeout(function() {
				time(o)
			}, 1000)
		}
	}
	var result = BaseUtil.getData("crm/customer/webGetCustomer");
	$("#login-imageCodeImg").attr("src",basePath+"/imageCode?mobile="+result.data.mobile+"&dc="+Math.random());
	$("#login-imageCodeImg").click(function(){
		$("#login-imageCodeImg").attr("src",basePath+"/imageCode?mobile="+result.data.mobile+"&dc="+Math.random());
	})
	 $("#li7").addClass("current");

});
