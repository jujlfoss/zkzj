require.async([ 'JqueryBase','PageUtil'], function() {
	jQuery.extend({
		alertWindow : function(id,type,e, t, n) {
			var e = e, t = t, r;
			n === undefined ? r = "#FF7C00" : r = n;
			$(".alertWindow1").remove();
//			if ($("body").find(".alertWindow1").length === 0) {
				var i = '<div  class="alertWindow1" style="width: 100%;height: 100%; background:rgba(0,0,0,0.5);position: fixed; left:0px; top: 0px; z-index: 9999;"><div  style="width: 600px;padding-bottom:20px;background: #FFF;margin: 180px auto;border: 2px solid #CFCFCF;">'
						+ '<div class="alertWindowCloseButton1" style="float: right; width: 24px; height: 24px;margin-right:5px;cursor: pointer;background:url('+basePath+'/front/images/images/close.png) no-repeat;text-indent:-9999px;">关闭</div><h1 class="alertWindowTitle" style="font-size:16px;font-weight:bold;height:40px;line-height:40px;background:#F98322;color:#fff;text-align:center;">'
						+ e
						+ '</h1>'
						+ '<div class="alertWindowContent" style="padding:20px;text-align:center;font-size: 15px;color: #7F7F7F;"></div>'
						+ '<p style="text-align:center"><input class="alertWindowCloseSure1" onclick="sub('+type+','+id+')"  type="button" value="确定" style="width: 100px;height: 30px;background:'
						+ r
						+ ';border:none;font-size:16px;color:#FFFFFF;cursor: pointer;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;"></p>'
						+ "</div>" + "</div>";
				$("body").append(i);
				$(".alertWindowTitle").html(e), $(".alertWindowContent").html(t)
				var s = $(".alertWindow1");
				$(".alertWindowCloseButton1").click(function() {
					s.hide()
				});
//			} else
//				$(".alertWindowTitle").html(e), $(".alertWindowContent")
//						.html(t), $(".alertWindow1").show()
		}
	})
	
	
	listCar(1);
	//优惠卷
	function listCar(pageindex){
		
		var limit = 5;
		BaseUtil.get("crm/car/webListCar",function(result){
			var result = result.data;
			var tb = document.getElementById('cartab');
		     var rowNum=tb.rows.length;
		     for (i=1;i<rowNum;i++)
		     {  
		         tb.deleteRow(i);
		         rowNum=rowNum-1;
		         i=i-1;
		     }
		     var li = "";
			$.each(result.items,function(idx,item){
				 li+='<tr><td>'+item.ownerName+'</td><td>'+item.ownerMobile+'</td><td>'+item.carNo+'</td>'
					+'<td><a class="edit_a" onclick="updateCar('+item.id+')">编辑</a>'
					+'<a class="del_a"   onclick="delCar('+item.id+')">删除</a>';
				 if(item.defChose!=true){
					li+=' <a class="del_a"   onclick="defChose('+item.id+')">设置默认</a> </td></tr>';
				 }else{
					li+=' </td></tr>';
				 }
			});   
			$("#cartab").append(li);   
			//分页信息
			if(result.items.length>0){
				$("#pagediv").html(PageUtil.getHtml(result.total,pageindex,5,limit)); 
			}else{
				 $("#pagediv").html("<span color='red' style='align-content: center'>暂无数据</span>"); 
			}
		},{'start':(pageindex - 1) * limit,'limit':limit});     
	} 
	
    window.defChose = function(id){
    	BaseUtil.post('crm/car/updateDefChose', function(data) {
		     layer.msg(data.msg);
			listCar(1);
		
	},{
		"id":id
	});
    }
	window.page=function (pageindex){  
		listCar(pageindex);
	}
	
	var result = BaseUtil.getData("crm/customer/webGetCustomer");
	var name = result.data.customerName;
	$("#customerName").val(name);
	$("#userMobile").val(result.data.mobile);
	$("#image").attr("src",basePath+'/file/display/'+result.data.avatarUrl);
	window.delCar = function(id){
		BaseUtil.post('crm/car/removeCar', function(data) {
			     layer.msg(data.msg);
				listCar(1);
			
		},{
			"id":id
		});
	}
	
	$("#determine").click(function(){
		var customerName = $("#customerName").val();
		if(name == customerName){
			layer.msg("呢称未发生改变");
			return;
		}
		if(customerName == ""){
			layer.msg("请输入呢称");
			return;
		}
		BaseUtil.post('crm/customer/webUpdateCustomerName', function(data) {
			$("#customerName").attr("readOnly",true);
			$("#determine").css("display",'none');
			$("#cancel").css("display",'none');
			$("#update").css("display",'inline');
			 layer.msg(data.msg);
			
		},{
			"customerName":customerName
		});
	})
	$("#update").click(function(){
		$("#customerName").attr("readOnly",false);
		$("#determine").css("display",'inline');
		$("#cancel").css("display",'inline');
		$("#update").css("display",'none');
	})
	$("#cancel").click(function(){
		$("#customerName").attr("readOnly",true);
		$("#determine").css("display",'none');
		$("#cancel").css("display",'none');
		$("#update").css("display",'inline');
	})
	
	window.sub=function(type,id){
		if(type==1){
			var mobile  =$("#mobile").val();
			var payPsw = $("#payPsw").val();
			if(payPsw == ""){
				layer.msg('请输入支付密码！');
				return;
			}
			var code = $("#code").val();
			if(code == ""){
				layer.msg('请输入短信难码！');
				return;
			}
			if(!ValidUtil.isMobile(mobile)){
		    	   layer.msg('请输入正确手机格式！');
		    	   return;
		    	}
			
			var result = BaseUtil.getData("crm/customer/isCheckPass?mobile="+mobile+"&payPsw="+payPsw+"&code="+code);
			if(result.data==true){
				BaseUtil.post('crm/customer/webUpdateUser', function(data) {
				if (data.success) {
					window.location.href = basePath+"/login.html";
				}
				
			},{
				"mobile":mobile
			});
			}else{
				layer.msg(result.msg);
				$(".btn_mfyzm").css("background", "#F98322");
				$(".btn_mfyzm").css("color", "#fff");
				var o=document.getElementById("btn2");
				o.removeAttribute("disabled");
				o.value = "免费获取验证码";
				wait = 0;
			}
		}else {
			var ownerName = $("#userName").val();
			var ownerMobile = $("#ownMobile").val();
			var carNo = $("#carNo").val();
			if(ownerName == ""){
				layer.msg("请输入姓名");
				return;
			}
			if(!ValidUtil.isMobile(ownerMobile)){
		    	   layer.msg('请输入正确手机格式！');
		    	   return;
		    	}
			if(!ValidUtil.isVehicleNumber(carNo)){
				 layer.msg('请输入正确的车牌格式！');
		    	   return;
			}
			if(carNo == ""){
				layer.msg("请输入车牌号");
				return;
			}
			if(type == 2){
				BaseUtil.post('crm/car/saveCar', function(data) {
					if (data.success) {
						layer.msg(data.msg);
						$(".alertWindow1").hide();
						listCar(1);
					}else{
						layer.msg(data.msg);	
					}
					
				},{
					"carNo":carNo,
					"ownerName":ownerName,
					"ownerMobile":ownerMobile
				});
			}else{
				BaseUtil.post('crm/car/updateCar', function(data) {
					if (data.success) {
						layer.msg(data.msg);
						$(".alertWindow1").hide();
						listCar(1);
					}else{
						layer.msg(data.msg);	
					}
					
				},{ 
					"carNo":carNo,
					"ownerName":ownerName,
					"ownerMobile":ownerMobile,
					"id":id
				});
			}
			
		}
	}
	window.upload = function(){
		ajaxFileUpload();
	}
	  function ajaxFileUpload() {
		if($("#upload").val()==""){
			 layer.msg("请选择一张图片");
			return;
		}
          $.ajaxFileUpload
          (
              {
                  url: basePath+'/crm/customer/webUpload', //用于文件上传的服务器端请求地址
                  secureuri: false, //是否需要安全协议，一般设置为false
                  fileElementId: 'upload', //文件上传域的ID
                  dataType: 'json', //返回值类型 一般设置为json
                  success: function (data, status)  //服务器成功响应处理函数
                  {
                	  layer.msg(data.msg);
                	  BaseUtil.getSubmitToken();

                  },
                  error: function (data, status, e)//服务器响应失败处理函数
                  {
                     layer.msg(data.msg);
                     BaseUtil.getSubmitToken();
                  }
              }
          )
          return false;
      }
	
//    $(".edit_a").click(function(){ 
//        jQuery.alertWindow("编辑联系人",i);
//    });
    $(".addmen").click(function(){
    	var i='<p style="margin:10px 0"><span class="name" style="display: inline-block;width: 100px; text-align: right; margin-right: 5px; ">'
    		+'姓名:</span><span><input type="text" id="userName" class="inputcss" style="height:32px;line-height:32px;"></span></p><p style="margin:10px 0"><span class="name" style="display: inline-block;width: 100px; text-align: right; margin-right: 5px; ">'
    		+'手机号码:</span><span><input type="text" id="ownMobile" class="inputcss" style="height:32px;line-height:32px;"></span></p><p style="margin:10px 0"><span class="name" style="display: inline-block;width: 100px; text-align: right; margin-right: 5px; ">'
    		+'车牌号:</span><span><input type="text" id="carNo"  class="inputcss" style="height:32px;line-height:32px;"></span></p>';
        jQuery.alertWindow(null,'2',"新增联系人",i);
    });
	window.updateCar = function(id){
		var is='<p style="margin:10px 0"><span class="name" style="display: inline-block;width: 100px; text-align: right; margin-right: 5px; ">'
			+'姓名:</span><span><input type="text" id="userName" class="inputcss" style="height:32px;line-height:32px;"></span></p><p style="margin:10px 0"><span class="name" style="display: inline-block;width: 100px; text-align: right; margin-right: 5px; ">'
			+'手机号码:</span><span><input type="text" id="ownMobile" class="inputcss" style="height:32px;line-height:32px;"></span></p><p style="margin:10px 0"><span class="name" style="display: inline-block;width: 100px; text-align: right; margin-right: 5px; ">'
			+'车牌号:</span><span><input type="text" id="carNo"  class="inputcss" style="height:32px;line-height:32px;"></span></p>';
		 jQuery.alertWindow(id,'3',"编辑联系人",is);
		 var result = BaseUtil.getData("crm/car/getCarInfo?id="+id);
		 $("#userName").val(result.data.ownerName);
		 $("#ownMobile").val(result.data.ownerMobile);
		 $("#carNo").val(result.data.carNo);
	}
   
	
    var wait = 60;
    $("#updateCustomer").click(function(){
		var checkResult = BaseUtil.getData("crm/customer/whetherSetPayPsw");
		if(checkResult.data){
			 var psw='<div class="detail_list mt20"><h3>修改账号</h3> <div class="modify_login">' 
			     	+'<p><span class="name">手机号:</span><span><input type="text" id="mobile" class="inputcss" style="height:32px;line-height:32px;"></span></p>'
			     	+'<p><span class="name">支付密码:</span><span><input type="password" id="payPsw" class="inputcss" style="height:32px;line-height:32px;"></span></p>'
			     	+'<p><span class="name">短信验证码:</span><span><input type="text" id="code" class="inputcss" style="width:100px;height:32px;line-height:32px;"><input type="button" id="btn2"  class="btn_mfyzm" value="免费获取验证码" /></span></p>'
			     	+' </div>';
			        jQuery.alertWindow(null,'1',"修改账号",psw);
			    	document.getElementById("btn2").disabled = false;
			    	document.getElementById("btn2").onclick = function() {
			    		var self = this;
			    		var mobile = $("#mobile").val();
			    		if(!ValidUtil.isMobile(mobile)){
					    	   layer.msg('请输入正确手机格式！');
					    	   return;
					    	}
//			    	
			    		BaseUtil.post('regisCode', function(data) {
			    			if (data.success) {
			    				layer.msg('验证码发送成功！');
			    				time(self);
			    				return;
			    			}
			    			layer.msg(data.msg)
			    			return;
			    		},{
			    			"mobile":mobile
			    		});
			    	}
				function time(o) {
					if (wait == 0) {
						$(".btn_mfyzm").css("background", "#F98322");
						$(".btn_mfyzm").css("color", "#fff");
						o.removeAttribute("disabled");
						o.value = "免费获取验证码";
						wait = 60;
					} else {
						$(".btn_mfyzm").css("background", "#CCCCCC");
						$(".btn_mfyzm").css("color", "#999");
						o.setAttribute("disabled", true);
						o.value = "重新发送(" + wait + ")";
						wait--;
						setTimeout(function() {
							time(o)
						}, 1000)
					}
				}
		}else{
			layer.msg('请先设置支付密码！');
			return;
		}
	})
					window.preview = function(file) {
						var prevDiv = document.getElementById('preview');
						if (file.files && file.files[0]) {
							var reader = new FileReader();
							reader.onload = function(evt) {
								prevDiv.innerHTML = '<img height="120px" width="120px" src="'
										+ evt.target.result + '" />';
							}
							reader.readAsDataURL(file.files[0]);
						} else {
							$("#image").attr("src", file.value);
						}
					}  
    $("#li6").addClass("current");
});
