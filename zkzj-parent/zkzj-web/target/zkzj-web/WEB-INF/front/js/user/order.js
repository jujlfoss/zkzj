require.async([ 'JqueryBase','PageUtil','base64','DateUtil','jquery.lazyload','jquery.tabs','BespeakEnum','OrderStatusEnum' ], function() {
	jQuery.extend({
		alertWindow : function(id,e, t, n) {
			var e = e, t = t, r;
			n === undefined ? r = "#FF7C00" : r = n;
			$(".alertWindow1").remove();
				var i = '<div  class="alertWindow1" style="width: 100%;height: 100%; background:rgba(0,0,0,0.5);position: fixed; left:0px; top: 0px; z-index: 9999;"><div  style="width: 600px;padding-bottom:20px;background: #FFF;margin: 180px auto;border: 2px solid #CFCFCF;">'
						+ '<div class="alertWindowCloseButton1" style="float: right; width: 24px; height: 24px;margin-right:5px;cursor: pointer;background:url('+basePath+'/front/images/images/close.png) no-repeat;text-indent:-9999px;">关闭</div><h1 class="alertWindowTitle" style="font-size:16px;font-weight:bold;height:40px;line-height:40px;background:#F98322;color:#fff;text-align:center;">'
						+ e
						+ '</h1>'
						+ '<div class="alertWindowContent" style="padding:20px;text-align:center;font-size: 15px;color: #7F7F7F;"></div>'
						+ ((e.indexOf('信息') == -1) ? ('<p style="text-align:center"><input class="alertWindowCloseSure1" onclick="window.commentServe('+id+')"  type="button" value="确定" style="width: 100px;height: 30px;background:'
						+ r 
						+ ';border:none;font-size:16px;color:#FFFFFF;cursor: pointer;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;"></p>'):'')
						+ "</div>" + "</div>";
				$("body").append(i);
				$(".alertWindowTitle").html(e), $(".alertWindowContent").html(t)
				var s = $(".alertWindow1");
				$(".alertWindowCloseButton1").click(function() {
					s.hide()
				});
		}
	})
	listBespeak(1, BespeakEnum.PAYWAIT.getValue()+","+BespeakEnum.PAYSUCCESS.getValue()+","+BespeakEnum.ORDERS.getValue(),1);
	function listBespeak(pageindex,status,type,deleteStatus){
		var limit = 3;
		if(type == 1){
			var url = 'eb/bespeak/webBespeakList';
		BaseUtil.get(url,function(result){
			var result = result.data;
			var li="";
			var tb = document.getElementById("orderTab");
		     var rowNum=tb.rows.length;
		     for (i=0;i<rowNum;i++)
		     {  
		         tb.deleteRow(i);
		         rowNum=rowNum-1;
		         i=i-1;
		     }
			var html = []; 
			var li='<tr><Th>订单</Th><Th>洗车服务小区</Th><Th>状态</Th><Th>操作</Th></tr>';
			$.each(result.items,function(idx,item){
				var bespeakStatus = item.bespeakStatus;
				var name =item.estateName==null?'无':item.estateName;
				li+='<tr><Td><p>'+item.serveName+'</p><p class="order2">下单时间：'+DateUtil.timeToString(item.createTime,DateUtil.SECOND)+'</p>'
				+'  </Td><Td><p>'+name+'</p><p class="order2">预约时间：'+DateUtil.timeToString(item.bespeakTime,DateUtil.SECOND)+'</p></Td><td><p>';
				if(bespeakStatus == BespeakEnum.PAYWAIT.getValue()){
					li+='待支付</p><p><a class="order3" onclick="detail('+item.id+','+type+')">查看详情</a></p></Td><Td><a onclick="pay(\''+item.bespeakNo+'\')" class="order4" >去支付</a></Td></tr>'
				}
				if(bespeakStatus == BespeakEnum.PAYSUCCESS.getValue()){
					li+='已支付</p><p><a class="order3" onclick="detail('+item.id+','+type+')">查看详情</a></p></Td><Td><a onclick ="cancel(\''+item.bespeakNo+'\')" class="order4">取消</a></Td></tr>'
				}
                if(bespeakStatus == BespeakEnum.ORDERS.getValue()){
                	li+='已接单</p><p><a class="order3" onclick="detail('+item.id+','+type+')">查看详情</a></p></Td><Td><a onclick ="cancel(\''+item.bespeakNo+'\')" class="order4">取消</a></Td></tr>'
				}
                if(bespeakStatus == BespeakEnum.FINISH.getValue()){
                	li+='已完成</p><p><a class="order3" onclick="detail('+item.id+','+type+')">查看详情</a></p></td><td>';
				    if(item.isEvaluate){
				    	li+='<a class="order4" onclick="openCommentServeInfo('+item.id+')">查看评价</a>';
				    }else{
				    	li+='<a class="order4" onclick="openCommentServe('+item.id+')" >待评价</a>'
				    }
				    li+='<a class="order4" onclick="toServeOrderConfirm('+item.id+','+item.estateId+')">再次预约</a><a class="order4" onclick="del('+item.id+','+type+')">删除</a></Td></tr>';
                }
                if(bespeakStatus == BespeakEnum.CANCEL.getValue()){
                	li+='已取消</p><td><a class="order4" onclick="toServeOrderConfirm('+item.id+','+item.estateId+')">再次预约</a><a class="order4" onclick="del('+item.id+','+type+')">删除</a></Td></tr>'
				}
//                if(bespeakStatus == BespeakEnum.EMP_DELETED.getValue()){
//                	li+='员工已删除</p><p><a class="order3" onclick="detail('+item.id+','+type+')">查看详情</a></p></td><td>';
//				}
			});   
			$("#orderTab").append(li);   
			if(result.items.length>0){
				$("#pagediv").html(PageUtil.getHtml(result.total,pageindex,3,limit)); 
			}else{
				 $("#pagediv").html("<span color='red' style='align-content: center'>暂无数据</span>"); 
			}
		},{'start':(pageindex - 1) * limit,'limit':limit,"statusValues":status,"deleteStatus":deleteStatus});     
		}else{
			var li="";
			var url="eb/order/webOrderList";
		    BaseUtil.get(url,function(result){
			var result = result.data;
			var tb = document.getElementById("orderTab");
		     var rowNum=tb.rows.length;
		     for (i=0;i<rowNum;i++)
		     {  
		         tb.deleteRow(i);
		         rowNum=rowNum-1;
		         i=i-1;
		     }
			var html = []; 
			var li='<tr><Th>订单</Th><Th>洗车服务小区</Th><Th>状态</Th><Th>操作</Th></tr>';
			$.each(result.items,function(idx,item){
				var orderStatus = item.orderStatus;
				li+='<tr><Td><p>'+item.orderNo+'</p><p class="order2">下单时间：'+DateUtil.timeToString(item.createTime,DateUtil.SECOND)+'</p></Td><Td><p>'+item.estateName+'</p></td><td><p>';
				if(orderStatus == OrderStatusEnum.WAIT.getValue()){
					li+='待领取</p><p><a class="order3" onclick="detail('+item.id+','+type+')">查看详情</a></p></Td></tr>'
				}
				if(orderStatus == OrderStatusEnum.FINISH.getValue()){
					li+='已完成</p><p><a class="order3" onclick="detail('+item.id+','+type+')">查看详情</a></p></td><td><a class="order4" onclick="del('+item.id+','+type+')">删除</a></Td></tr>'
				}
                if(orderStatus == OrderStatusEnum.CANCEL.getValue()){
                	//<p><a class="order3" onclick="detail('+item.id+','+type+')">查看详情</a></p>
                	li+='已取消</p></td></tr>';
                }
                if(orderStatus == OrderStatusEnum.EMPLOYEE_DELETE.getValue()){
                	li+='已完成</p><p><a class="order3" onclick="detail('+item.id+','+type+')">查看详情</a></p></td><td><a class="order4" onclick="del('+item.id+','+type+')">删除</a></Td></tr>';
                }
			});   
			$("#orderTab").html(li);  
			//分页信息
             if(result.items.length>0){
            	 $("#pagediv").html(PageUtil.getHtml(result.total,pageindex,3,limit)); 
			}else{
				 $("#pagediv").html("<span color='red' style='align-content: center'>暂无数据</span>"); 
			}
		},{'start':(pageindex - 1) * limit,'limit':limit,"oStatus":status});     
		
		}
	} 
	window.pay=function(o){
		location.href = basePath + '/serve/auth/servicepayfor.html?ordno='+o;
	}
	window.cancel = function(o){
		//询问框
		layer.confirm('距离服务开始前半小时取消，将扣除20%的违约金，确定取消吗?', {
			btn: ['确定','算了'] //按钮
		}, function(){
			BaseUtil.post('eb/bespeak/cancelBespeakOrder',function(result){
				if(result.success){
					layer.msg('成功取消订单', {icon: 1});
					var str = BespeakEnum.PAYWAIT.getValue()+","+BespeakEnum.PAYSUCCESS.getValue()+","+BespeakEnum.ORDERS.getValue();
					getOrder(str,'orderTab')
				} else {
					layer.alert(result.msg);
				}
			},{"orderno":o});
		}, function(){  
		});
	}
	window.detail = function(id,type){
		if(type == 1){
			location.href=basePath+'/user/auth/myorderdetail.html?id='+id;
		}else{
			location.href=basePath+'/user/auth/myorderdetails.html?id='+id;
		}
	}
	
	$("#paywait").click(function(){
		var str = BespeakEnum.PAYWAIT.getValue()+","+BespeakEnum.PAYSUCCESS.getValue()+","+BespeakEnum.ORDERS.getValue();
		$("#deleteStatus").val("");
		getOrder(str,'orderTab',"")
	})
	$("#finsh").click(function(){
		var type = $("#selectType").val();
		var str  =  BespeakEnum.FINISH.getValue()+","+BespeakEnum.CANCEL.getValue();
		$("#deleteStatus").val(BespeakEnum.EMP_DELETED.getValue());
		getOrder(str,'orderTab',BespeakEnum.EMP_DELETED.getValue())
	})
	
	$("#paywait1").click(function(){
		var str= OrderStatusEnum.WAIT.getValue();
		$("#deleteStatus").val("");
		getOrder(str,'orderTab',"")
	})
	$("#finsh1").click(function(){
		var str  =  OrderStatusEnum.FINISH.getValue()+","+OrderStatusEnum.EMPLOYEE_DELETE.getValue()+","+OrderStatusEnum.CANCEL.getValue();
		$("#deleteStatus").val("");
		getOrder(str,'orderTab',"")
	})
   function getOrder(status,apendId,deleteStatus){
	   var type = $("#selectType").val();
	   $("#status").val(status);
	   listBespeak(1,status,type,deleteStatus);
   }
	window.page=function (pageindex){  
		  var type = $("#selectType").val();
		   var status = $("#status").val();
		listBespeak(pageindex,status,type, $("#deleteStatus").val());
	}
	window.getList = function(type){
		$("#selectType").val(type);
		var status ="";
		if(type == 1){
			$("#finsh").css("display",'block')
			$("#paywait").css("display",'block')
			$("#finsh1").css("display",'none')
			$("#paywait1").css("display",'none')
			$("#msg").css("display",'block');
			$("#msgs").css("display",'none');
			status = BespeakEnum.PAYWAIT.getValue()+","+BespeakEnum.PAYSUCCESS.getValue()+","+BespeakEnum.ORDERS.getValue();
		}else{
			$("#finsh").css("display",'none')
			$("#paywait").css("display",'none')
			$("#finsh1").css("display",'block')
			$("#paywait1").css("display",'block')
				$("#msg").css("display",'none');
			$("#msgs").css("display",'block');
			status= OrderStatusEnum.WAIT.getValue()
		}
		 $("#status").val(status);
		 $("#deleteStatus").val("");
		 listBespeak(1,status,type,"");
	}
	  window.del = function(id,type){
		  
		//询问框
		layer.confirm('确定删除吗？', {
			btn: ['确定','算了'] //按钮
		}, function(){
			 var url = "";
			  var status="";
			  if(type == 1){
				  url = "eb/bespeak/deleteByCustomer";
				  status = BespeakEnum.FINISH.getValue()+","+BespeakEnum.CANCEL.getValue();
			  }else{
				  status = OrderStatusEnum.FINISH.getValue()+","+OrderStatusEnum.EMPLOYEE_DELETE.getValue()+","+OrderStatusEnum.CANCEL.getValue();
				  url = "eb/order/del";
			  }
			   BaseUtil.post(url, function(data) {
						layer.msg(data.msg);
						getOrder(status,"orderTab", $("#deleteStatus").val())	
						
					
				},{ 
					"id":id
				});
		}, function(){  
		});
		  
		 
	   }
	
	/** 评价服务 **/
	window.commentServe = function(bespeakId){ 
		var content = $("#evaluateContent").val();
		var level = $("#evaluateFraction").html();
		if(ValidUtil.isBlank(level)){
			layer.msg("请选择评价星级"); 
			return;
		}
		if(ValidUtil.isBlank(content)){ 
			layer.msg("请输入评价内容");
			return;
		}
		BaseUtil.post('eb/comments/saveComments',function(result){
			if(result.success){
				 var status = BespeakEnum.FINISH.getValue()+","+BespeakEnum.CANCEL.getValue();;
				getOrder(status,'orderTab',BespeakEnum.EMP_DELETED.getValue());
				$(".alertWindow1").hide();  
				layer.msg("评价成功"); 
			} else {
				layer.msg(result.msg); 
			} 
    	},{"content":content,"bespeakId":bespeakId,"level":level}); 
	}
	/** 评价窗体 **/
	window.openCommentServe = function(id){ 
		var i='<div id="starttwo" class="block clearfix"><table class="evalute_table"><TR><Td>评分:</td><TD><div class="star_score"></div><p style="display:none;">您的评分：<span class="fenshu" id="evaluateFraction"></span> 分</p></td></tr><TR><TD>内容:</tD><TD><textarea class="areacss" id="evaluateContent"></textarea></td></tr></table></div>';
		jQuery.alertWindow(id,"用户评价",i)
		scoreFun($("#starttwo"),{
           fen_d:22,//每一个a的宽度
           ScoreGrade:5//a的个数 10或者
        });
	}
	/** 评价窗体 **/
	window.openCommentServeInfo = function(id){ 
		
		var i='<div id="starttwo" class="block clearfix"><table class="evalute_table"><TR><Td>评分:</td><TD><div id="">{levelxx}</div><p style="display:none;">您的评分：<span class="fenshu" id="evaluateFraction"></span> 分</p></td></tr><TR><TD>内容:</tD><TD><textarea class="areacss" readonly="readonly">{evaluateContent}</textarea></td></tr></table></div>';
		var arr = [];
		BaseUtil.get('eb/comments/getComments',function(result){  
			var imgs = '';
			for(var j = 0; j < result.data.level; j++){
				imgs += '<img src="'+basePath+'/front/images/starsy.png" />';
			}    
			arr.push(i.replaceAll("{evaluateContent}",result.data.content) 
					.replaceAll("{levelxx}",imgs));   
			jQuery.alertWindow(id,"用户评价信息",arr.join('')) 
			scoreFun($("#starttwo"),{
				fen_d:22,//每一个a的宽度
				ScoreGrade:5//a的个数 10或者
			});
		},{"bespeakId":id});   
	}
	/** 再次预约 **/
	window.toServeOrderConfirm = function(id,estateId){ 
		BaseUtil.get('eb/bespeak/getBespeakInfo',function(result){
			var data = result.data; 
			if(data.serveStandardId){ 
				var param = "estateId="+estateId+"&serveStandardId="+data.serveStandardId;
				param = BASE64.encoder(param);   
				BaseUtil.loginRedirect("serve/auth/servicesure.html?"+param);
			} else {
				layer.msg("该服务已下架");  
			} 
    	},{"id":id});
	}
	
	
		$(".order_ul").find("li").click(function(){
			$(".order_ul").find("li").removeClass("current");
			$(this).addClass("current");
		})
			$("img[original]").lazyload({ placeholder:"images/color3.gif" });
			
			$('.demo1').Tabs();
			$('.demo2').Tabs({
				event:'click'
			});
			$('.demo3').Tabs({
				timeout:300
			});
			$('.demo4').Tabs({
				auto:3000
			});
			$('.demo5').Tabs({
				event:'click',
				callback:lazyloadForPart
			});
			//部分区域图片延迟加载
			function lazyloadForPart(container) {
				container.find('img').each(function () {
					var original = $(this).attr("original");
					if (original) {
						$(this).attr('src', original).removeAttr('original');
					}
				});
			}
			$("#li2").addClass("current");
		
});
