require.async(['JqueryBase','PageUtil','jquery.lazyload','jquery.tabs.min','ValidityNumberEnum','DateUtil','UserdEnum'],function(){
	jQuery.extend({
		alertWindow : function(id,type,e, t, n) {
			var e = e, t = t, r;
			n === undefined ? r = "#FF7C00" : r = n;
			$(".alertWindow1").remove();
//			if ($("body").find(".alertWindow1").length === 0) {
				var i = '<div  class="alertWindow1" style="width: 100%;height: 100%; background:rgba(0,0,0,0.5);position: fixed; left:0px; top: 0px; z-index: 9999;"><div  style="width: 600px;padding-bottom:20px;background: #FFF;margin: 180px auto;border: 2px solid #CFCFCF;">'
						+ '<div class="alertWindowCloseButton1" style="float: right; width: 24px; height: 24px;margin-right:5px;cursor: pointer;background:url('+basePath+'/front/images/images/close.png) no-repeat;text-indent:-9999px;">关闭</div><h1 class="alertWindowTitle" style="font-size:16px;font-weight:bold;height:40px;line-height:40px;background:#F98322;color:#fff;text-align:center;">'
						+ e
						+ '</h1>'
						+ '<div class="alertWindowContent" style="padding:20px;text-align:center;font-size: 15px;color: #7F7F7F;"></div>'
						+ '<p style="text-align:center"><input class="alertWindowCloseSure1"   type="button" value="关闭" style="width: 100px;height: 30px;background:'
						+ r
						+ ';border:none;font-size:16px;color:#FFFFFF;cursor: pointer;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;"></p>'
						+ "</div>" + "</div>";
				$("body").append(i);
				$(".alertWindowTitle").html(e), $(".alertWindowContent").html(t)
				var s = $(".alertWindow1");
				$(".alertWindowCloseButton1").click(function() {
					s.hide()
				});
				$(".alertWindowCloseSure1").click(function() {
					s.hide()
				});
		}
	})
	listCoupon(1,0);
	//优惠卷
	function listCoupon(pageindex,isOverdue){
		var limit = 12;
		BaseUtil.get("eb/washCard/webCardList",function(result){
			var result = result.data;
			var data = result.items;
			$(".ul1").html("");
			var cardServe = "";
			var li="";
			for (var i = 0; i < data.length; i++) {
				cardServe = data[i].cardServeList;
				var tab="";
				for ( var attr in cardServe) {
					tab+="<tr><td>"+cardServe[attr].serveName+"</td>" +
						"<td>"+cardServe[attr].residueNum+"</td></tr>";
				}
				li+="<li ><div class='yhq_img'><div class='title'>"+data[i].cardTypeName+"</div><div class='price'>"+data[i].sellPrice+" 积分</div>" +
						"</div><div class='yhq_txt'><p>服务小区:"+data[i].estateName+"</p><p>有效天数:"+ValidityNumberEnum.getHtml(data[i].validityNumber)+"</p>" +
								"<p>适用车型:"+data[i].serveStandardName+"</p><p>有效日期至:"+DateUtil.timeToString(data[i].validityDate,DateUtil.DATE)+"</p>"
				li+="<a class='btn_a' onClick='showCard(\""+tab+"\")'>查看服务次数</a></div></li>";
			}
		$("#coupon-ul").append(li);   
//			//分页信息
			if(result.items.length>0){
				$("#pagediv").html(PageUtil.getHtml(result.total,pageindex,12,limit)); 
			}else{
				$(".ul1").html("");
				 $("#pagediv").html("<span color='red' style='align-content: center'>暂无数据</span>"); 
			}
		},{'start':(pageindex - 1) * limit,'limit':limit,"isOverdue":isOverdue});     
	} 
	window.showCard=function(cardServe){
		var tab = '<table style="display: inline"><tr><td width="150px" style="color: red;">服务名</td ><td width="150px" style="color: red;">服务剩余次数</td></tr>';
		tab+=cardServe+"</table>";
		var i=tab;
		 jQuery.alertWindow(null,'2',"服务剩余次数",i);
	}

	window.page=function (pageindex){  
		listCoupon(pageindex,$("#isOverdue").val());
	}
	$("#nouser").click(function(){
		$("#isOverdue").val(0);
		listCoupon(1,0);
		
	})
		
	$("#pass").click(function(){
		$("#isOverdue").val(1);
		listCoupon(1,1);
		
	})
	
		
		$("img[original]").lazyload({ placeholder:"images/color3.gif" });
		
		$('.demo1').Tabs();
		$('.demo2').Tabs({
			event:'click'
		});
		$('.demo3').Tabs({
			timeout:300
		});
		$('.demo4').Tabs({
			auto:3000
		});
		$('.demo5').Tabs({
			event:'click',
			callback:lazyloadForPart
		});
		//部分区域图片延迟加载
		function lazyloadForPart(container) {
			container.find('img').each(function () {
				var original = $(this).attr("original");
				if (original) {
					$(this).attr('src', original).removeAttr('original');
				}
			});
		}
		 $("#li4").addClass("current");
});