require.async([ 'JqueryBase','PageUtil','DateUtil','jquery.lazyload','jquery.tabs' ], function() {
	BaseUtil.get('crm/cashAccount/getCashAccountInfo',function(data){
		var data =data.data;
		if (data) {
			var html = [],templete = '<li><span class="price">{consumptionMoney}<em>元</em></span><span class="txt">消费金额</span></li><li><span class="price">{integral}<em>分</em></span><span class="txt">我的积分</span></li><li><span class="price">{balance}<em>元</em></span><span class="txt">账户余额</span></li>';
				var part = templete.replaceAll('{consumptionMoney}',Math.abs(data.consumptionMoney))
				.replaceAll('{integral}',data.integral)
				.replaceAll('{balance}',data.balance)
				html.push(part);
			$("#cashAccount").append(html.join(''));
		}
	});
	listHistory(1,1);
	//消费列表
	function listHistory(pageindex,type){
		var limit = 5;
		var url ="";
		var id= "";
		if(type == 1){
			url = "crm/cashAccountHistory/webcashAccountHistoryList";
			id="cashHistory";
		}else if(type == 2 ){
			url = "crm/pointAccountHistory/webListPointAccountHistory";
			id="pointHistory";
		}else{
			url="eb/rechargeCard/webListRechargeCard"
			id="rechargeCard"
		} 
		BaseUtil.get(url,function(result){
			var result = result.data;
			var li='<li class="clearfix"><div class="left">';
			if(type ==1){
				li+='<P class="name">{description}</P><p class="date">{createTime}</p></div><div class="right mt10"><div class="price">{fee}</div></div></li>';
			}else if(type == 2){
				li+='<P class="name">{description}</P><p class="date">{createTime}</p></div><div class="right mt10"><div class="price">{fee}</div></div></li>';
			}else{
				li+='<P class="name">{rechargeCardTypeName}</P><p class="date">{bindTime}</p></div><div class="right mt10"><div class="price">{price}</div></div></li>';
			}
			var html = []; 
			$.each(result.items,function(idx,item){
				html.push(li.replaceAll("{description}",item.description).
						replaceAll("{createTime}",DateUtil.timeToString(item.createTime,DateUtil.DATE)).
						replaceAll("{fee}",item.fee).
						replaceAll("{balance}",item.balance).
						replaceAll("{rechargeCardTypeName}",item.rechargeCardTypeName).
						replaceAll("{price}",item.price).
						replaceAll("{bindTime}",DateUtil.timeToString(item.bindTime,DateUtil.DATE)));
			});   
			$("#"+id).html(html.join(''));   
			//分页信息
			if(result.items.length>0){
				$("#pagediv").html(PageUtil.getHtml(result.total,pageindex,5,limit)); 
			}else{
				 $("#pagediv").html("<span color='red' style='align-content: center'>暂无数据</span>"); 
			}
		},{'start':(pageindex - 1) * limit,'limit':limit}); 
	} 
	window.page=function (pageindex){  
		listHistory(pageindex,$("#type").val());
	}
	window.webcashAccountHistoryList=function(type){
		$("#type").val(type);
		listHistory(1,type);
	}
	$("img[original]").lazyload({ placeholder:"images/color3.gif" });
	
	$('.demo1').Tabs();
	$('.demo2').Tabs({
		event:'click'
	});
	$('.demo3').Tabs({
		timeout:300
	});
	$('.demo4').Tabs({
		auto:3000
	});
	$('.demo5').Tabs({
		event:'click',
		callback:lazyloadForPart
	});
	//部分区域图片延迟加载
	function lazyloadForPart(container) {
		container.find('img').each(function () {
			var original = $(this).attr("original");
			if (original) {
				$(this).attr('src', original).removeAttr('original');
			}
		});
	}
	$("#li1").addClass("current");

});
