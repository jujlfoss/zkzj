require.async([ 'JqueryBase','PageUtil','NumberUtil','BespeakEnum' ], function() {
	var id = UrlUtil.getUrlParam('id');
    var type = UrlUtil.getUrlParam('type');
    	BaseUtil.get('eb/bespeak/getBespeakInfo',function(result){
    		var data = result.data;
    		var str="";
    		var coustomerHTML="";
    		if (data) {
    			var bespeakStatus = data.bespeakStatus;
    			if(bespeakStatus == BespeakEnum.PAYWAIT.getValue()){
    				str="c1";
				}
				if(bespeakStatus == BespeakEnum.PAYSUCCESS.getValue()){
					str="c2";
				}
                if(bespeakStatus == BespeakEnum.ORDERS.getValue()){
                	str="c3";
                	coustomerHTML+='<p><span class="name">接单人:</span><span>'+data.empName+'</span></p><p><span class="name">接单人电话:</span><span>'+data.empPhone+'</span></p>';
				}
                if(bespeakStatus == BespeakEnum.FINISH.getValue()){
                	str="c4";
				}
//                if(bespeakStatus == BespeakEnum.EMP_DELETED.getValue()){
//                	str="c5";
//				}
                $("#"+str).attr("style","background:#F98322!important;color:#fff!important")
    			 coustomerHTML+='<p><span class="name">联系人:</span><span>{ownerName}</span></p><p><span class="name">手机号码:</span><span>{ownerMobile}</span></p><p><span class="name">车牌号:</span><span>{carNo}</span></p>';
    			var cHtml = coustomerHTML.replaceAll('{ownerName}',data.ownerName).replaceAll('{ownerMobile}',data.ownerMobile).replaceAll('{carNo}',data.carNo);
    			$("#customer").html(cHtml);
    			var orderHTML='<p><span class="name">服务规格:</span><span>{serveStandardName}</span></p><p><span class="name">洗车项目:</span><span>{serveName}</span></p><p><span class="name">预约时间:</span><span>{bespeakTime}</span></p><p><span class="name">洗车服务点:</span><span>{estateName}</span></p><p><span class="name">服务点地址:</span><span>{provinceName}{cityName}{countyName}{address}</span></p><p><span class="name">停车地点:</span><span>{stopPlace}</span></p>';
    			var oHtml = orderHTML.replaceAll('{serveName}',data.serveName).
			    			replaceAll('{bespeakTime}',DateUtil.timeToString(data.bespeakTime,DateUtil.SECOND)).
			    			replaceAll('{estateName}',data.estateName).
			    			replaceAll('{provinceName}',data.provinceName).
			    			replaceAll('{cityName}',data.cityName)
			    			.replaceAll('{countyName}',data.countyName).
			    			replaceAll('{address}',data.address).replaceAll('{stopPlace}',data.stopPlace).replaceAll('{serveStandardName}',data.serveStandardName);
    			$("#order").html(oHtml);
    			if(null!=data.beforePictureUrls){
    				var urls = data.beforePictureUrls.split(",");
    				var html = [];
    				for (var i = 0; i < urls.length; i++) {
    					var beforeimg = basePath+'/file/display/'+urls[i];
    					html.push("<p style='text-align:center;'><img onclick='openImg(\""+beforeimg+"\")' style='cursor:pointer;margin:2px;border:3px solid lightgray;' width='500px' height='300px' src='"+beforeimg+"'/>");
					}
    				$("#img").append("洗车前图片:<br/>"+html.join('')+"</p>"); 
    			}
    			if(null!=data.afterPictureUrls){
    				var urls = data.afterPictureUrls.split(",");
    				var html = [];
    				for (var i = 0; i < urls.length; i++) {
    					var afterimg = basePath+'/file/display/'+urls[i];
    					html.push("<p style='text-align:center;'><img onclick='openImg(\""+afterimg+"\")' style='cursor:pointer;margin:2px;border:3px solid lightgray;' width='500px' height='300px' src='"+afterimg+"'/>");
					}
    				$("#img").append("<br/>洗车后图片:<br/>"+html.join('')+"</p>"); 
    			}
    			
    			
    		
    					//"$("#img").append("洗车前图片:<img src='"+basePath+"/file/display/ "+data.beforePictureUrls+"'/>)
    			var orderPriceHTML='<p><span class="name">订单编号:</span><span>{bespeakNo}</span></p><p><span class="name">洗车卡名:</span><span>{washCardName}</span></p><p><span class="name">服务总价:</span><span>{serviceTotalPrice}</span></p><p><span class="name">优惠券:</span><span>{price}</span></p><p><span class="name">订单总价:</span><span>{totalPrice}</span></p><p><span class="name">实付款:</span><span>¥{totalPrice}</span>';
	    			var opHtml = orderPriceHTML.replaceAll('{serviceTotalPrice}',NumberUtil.roundString(data.serviceTotalPrice)).
	    			replaceAll('{price}',data.price==null?NumberUtil.roundString(0):NumberUtil.roundString(data.price)).
	    			replaceAll('{provinceName}',data.provinceName).
	    			replaceAll('{bespeakNo}',data.bespeakNo).
	    			replaceAll('{totalPrice}',NumberUtil.roundString(data.totalPrice)).
	    			replaceAll('{washCardName}',data.washCardName==null?"":data.washCardName);
	    			$("#price").html(opHtml);
	    			if(data.bespeakStatus==BespeakEnum.PAYWAIT.getValue()){
	    				$("#pay").append('<a onclick="pay(\''+data.bespeakNo+'\')" class="btn_a">支付</a>');
	    			}
	    			$("#pay").append('<a onclick="javascript:history.back(-1)" class="btn_a">返回</a>');
    		}
    	},{
    		"id":id
    	});
    	window.pay=function(o){
    		location.href = basePath + '/serve/auth/servicepayfor.html?ordno='+o;
    	}
    	 $("#li2").addClass("current");
});
//页面层
function openImg(src){
	layer.open({
		title:['图片', 'text-align: center'],
		type:1,
		area: ['40%', '60%'], //宽高
		maxmin:true,
		content: '<img style="width:100%; height:100%;" src="'+src+'"/>'
	});
}