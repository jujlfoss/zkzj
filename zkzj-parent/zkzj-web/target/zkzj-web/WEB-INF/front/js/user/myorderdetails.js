require.async([ 'JqueryBase','PageUtil','NumberUtil','BespeakEnum' ], function() {
	var id = UrlUtil.getUrlParam('id');
    	BaseUtil.get('eb/order/getOrderInfo',function(result){
    		var data = result.data;
    		if (data) {
    			var orderHTML='<p><span class="name">订单号:</span><span>{orderNo}</span></p><p><span class="name">订单金额:</span><span>{totalPrice}</span></p><p><span class="name">洗车服务点:</span><span>{estateName}</span></p><p><span class="name">服务点地址:</span><span>{provinceName}{cityName}{countyName}{address}</span></p>';
    			var oHtml = orderHTML.
			    			replaceAll('{estateName}',data.estateName).
			    			replaceAll('{provinceName}',data.provinceName).
			    			replaceAll('{cityName}',data.cityName)
			    			.replaceAll('{countyName}',data.countyName).
			    			replaceAll('{address}',data.address).replaceAll('{orderNo}',data.orderNo).replaceAll('{totalPrice}',NumberUtil.roundString(data.totalPrice));
    			$("#order").html(oHtml);
//	    			if(data.orderStatus==BespeakEnum.PAYWAIT.getValue()){
//	    				$("#pay").append('<a href="#" class="btn_a">去支付</a>');
//	    			}
	    			getOrderDetailById(data.id);
	    			$("#pay").append('<a onclick="javascript:history.back(-1)" class="btn_a">返回</a>');
    		}
    	},{
    		"id":id
    	});
    	
    	function getOrderDetailById(orderId){
    		var tb = document.getElementById("cartab");
		     var rowNum=tb.rows.length;
		     for (i=1;i<rowNum;i++)
		     {  
		         tb.deleteRow(i);
		         rowNum=rowNum-1;
		         i=i-1;
		     }
    		BaseUtil.get("eb/orderDetail/getOrderDetailByOrderId",function(result){
    			var result = result.data;
    			var li='<tr><td>{goodsTypeName}</td><td>{goodsName}</td><td>{price}</td><td>{totalPrice}</td></tr>';
    			var html = []; 
    			$.each(result.items,function(idx,item){
    				html.push(li.replaceAll("{goodsTypeName}",item.goodsTypeName).
    						replaceAll("{price}",NumberUtil.roundString(item.price)).
    						replaceAll("{goodsName}",item.goodsName).
    						replaceAll("{totalPrice}",NumberUtil.roundString(item.totalPrice)));
    			});   
    			$("#cartab").append(html.join(''));   
    			//分页信息
    		},{'orderId':orderId});     
    	}
    	 $("#li2").addClass("current");
});
