require.async(['JqueryBase','PageUtil'],function(){
	//服务列表
	(function listServe(pageindex){
		
		var cityCode = CookieUtil.get(Constant.CITY_CODE);
		if(!cityCode) {
			layer.alert("定位城市失败，请刷新后重试！");
			return;
		} 
		var limit = 5;
		BaseUtil.get("eb/serve/front/pageFrontServe",function(result){ 
			var result = result.data; 
			var li = '<li class="clearfix"><div class="proimg left"><img style="width:175px;height:100px;" onclick="{onck}" src="{imgPath}" /></div><div class="txt left"><span class="name">{servName}</span><span class="price">价格：{price}元</span><span class="name">评价：<img style="" src="{imgPath}" /></span></div><div class=" right"><a href="{url}"class="a_btn">我要预约</a></div></li>';
			var html = []; 
			$.each(result.items,function(idx,item){
				html.push(li.replaceAll("{basePath}",basePath)
						.replaceAll("{servName}",item.serveName)
						.replaceAll("{price}",item.servicePrice)
						.replaceAll("{imgPath}",basePath+"/file/display/"+item.thumbnailUrl)
						.replaceAll("{url}",basePath+"/serve/servicebespeak.html?id="+item.id+"&_=")
						.replaceAll("{onck}","location.href='"+basePath+"/serve/servicebespeak.html?id="+item.id+"&_='") 
				); 
			});   
			$("#serviceUl").html(html.join(''));   
			//分页信息
			$("#pagediv").html(PageUtil.getHtml(result.total,pageindex,5,limit)); 
		},{'baiduCode':cityCode,'start':(pageindex - 1) * limit,'limit':limit});     
	})(1);/** 加载第一页*/
	function page(pageindex){  
		listServe(pageindex);
	}
});