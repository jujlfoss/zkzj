require.async([ 'Constant', 'ValidUtil' ], function() {
	// 注册
	$("#register-logindiv [type=submit]").click(function() {
		var self = $(this);
		var mobile = ValidUtil.trim($("#register-signup-username").val());
		if (!mobile) {
			layer.msg('手机号码不能为空！')
			return;
		}
		var mobileCode = ValidUtil.trim($("#register-signup-sms").val());
		if (!mobileCode) {
			layer.msg('短信验证码不能为空！')
			return;
		}
		self.attr('disabled',true);
		BaseUtil.post('web/front/validSms', function(data) {
			if (data.success) {
				layer.msg('验证成功，正在跳转至下一步...', {}, function() {
					location.href = basePath + '/forget/changepwd.html';
				});
				return;
			}
			self.attr('disabled',false);
			layer.msg(data.msg);
		}, {
			mobile : mobile,
			mobileCode : mobileCode
		});

	});

	var wait = 60;
	document.getElementById("register-smsBtn").disabled = false;
	document.getElementById("register-smsBtn").onclick = function() {
		var mobile = ValidUtil.trim($("#register-signup-username").val());
		if (!mobile) {
			layer.msg('手机号码不能为空！')
			return;
		}
		BaseUtil.post('web/front/forgetCode', function(data) {
			if (data.success) {
				layer.msg('验证码发送成功！')
				time(document.getElementById('register-smsBtn'));
				return;
			}
			layer.msg(data.msg)
			return;
		}, {
			mobile : mobile
		});
	}
	function time(o) {
		if (wait == 0) {
			$(".btn_mfyzm2").css("background", "#F98322");
			$(".btn_mfyzm2").css("color", "#fff");
			o.removeAttribute("disabled");
			o.value = "免费获取验证码";
			wait = 60;
		} else {
			$(".btn_mfyzm2").css("background", "#CCCCCC");
			$(".btn_mfyzm2").css("color", "#999");
			o.setAttribute("disabled", true);
			o.value = "重新发送(" + wait + ")";
			wait--;
			setTimeout(function() {
				time(o)
			}, 1000)
		}
	}
});