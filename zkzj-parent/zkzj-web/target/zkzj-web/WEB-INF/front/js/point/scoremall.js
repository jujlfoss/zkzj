require.async([ 'JqueryBase','CartUtil' ], function() {
	BaseUtil.get('eb/couponType/front/getRamdomCouponType',function(data){
		if (data) {
			var html = [],templete = '<li> <div class="yhq_img"> <div class="title">{couponTypeName}</div> <div class="">{sellPrice} 积分</div> </div> <div class="yhq_txt"> <p>使用说明：{useExplain}</p> <p>有效期：{validityNumber}(天)</p><p>可抵扣：{price} 元</p>   <a class="btn_a" href="{func}">立即兑换</a> </div> </li>';
			for (var key in data) {
				var item = data[key];
				var part = templete.replaceAll('{couponTypeName}',item.couponTypeName)
				.replaceAll('{sellPrice}',item.sellPrice)
				.replaceAll('{price}',NumberUtil.roundString(item.price))
				.replaceAll('{useExplain}',item.useExplain)
				.replaceAll('{validityNumber}',item.validityNumber)
				.replaceAll('{func}','javascript:CartUtil.exchange('+item.id+')');
				html.push(part);
			}
			$("#coupon-ul").append(html.join(''));
		}
	});
	window.goodsclick = function(id){
		location.href=basePath+'/point/scoredetail.html?id='+id;
	}
	BaseUtil.get('eb/goods/front/getRandomFourGoods',function(data){
		if (data) {
			var html = [],templete = '<li> <div class="imgbox" onclick="{liclick}"> <img src="{basePath}/file/display/{thumbnailUrl}"> </div> <div class="desc">{goodsName}</div> <div class="scores">{price} 积分</div> <div class="btns mt10 clearfix"> <a class="buy left" href="{atonce}">立即兑换</a> <a class="cart right" href="{addtocart}">加入购物车</a> </div> </li>';
			for (var key in data) {
				var item = data[key];
				var part = templete.replaceAll('{thumbnailUrl}',item.thumbnailUrl)
				.replaceAll('{goodsName}',item.goodsName)
				.replaceAll('{price}',item.price)
				.replaceAll('{basePath}',basePath)
				.replaceAll('{addtocart}','javascript:CartUtil.addToCart('+item.id+');')
				.replaceAll('{atonce}','javascript:CartUtil.submit('+item.id+')')
				.replaceAll('{liclick}','goodsclick('+item.id+')');
				html.push(part);
			}
			$("#goods-ul").append(html.join(''));
		}
	});
});
