require.async([ 'JqueryBase', 'UrlUtil','PayMethodEnum'], function() {
	var washCardNo = UrlUtil.getUrlParam('washCardNo');
	if (!washCardNo) {
		layer.alert("充值卡编号错误，请重新下单！", function(index) {
			layer.close(index);
			location.href = basePath + '/search/searchlist.html?location=wash';
		});
		return;
	}
	BaseUtil.get('eb/washCard/getTotalPrice', function(result) {
		if (result.success) {
			$("#totalPrice").text(NumberUtil.roundString(result.data));
			return;
		}
		layer.msg(result.msg);
	}, {
		washCardNo : washCardNo
	});
	$(".codediv").hide();
	$(".pay_ul").find("li").click(function() {
		$(".pay_ul").find("li").removeClass("current");
		$(".pay_ul").find("li").find("span").hide();
		$(this).addClass("current");
		$(this).find("span").show();
		if ($("#balance").hasClass("current")) {
			$(".codediv").show();
		} else {
			$(".codediv").hide();
		}
		$("#paymethod").val($(this).find("input")[0].value);
	});
	$('#payfor').click(function(){
		var method = $("#payul .current input").val();
		if(method == PayMethodEnum.ALIPAY.getValue()) {
			layer.msg("支付宝支付暂未实现");
		} else if(method == PayMethodEnum.WECHAT.getValue()) { 
			BaseUtil.get("pay/wechat/getQrCode",function(result){ 
				if(result.success){
					openWechatQrCode(result.data);
					return;
				}
				layer.msg(result.msg);
			},{
				"orderNo":washCardNo
			});
			
		}
	});
	function validOrder(){
		BaseUtil.get("eb/washCard/validStatus",function(result){ 
			if(result.success){
				location.href = basePath + '/wash/auth/washsuccess.html';
				return;
			}
			layer.msg(result.msg);
		},{"washCardNo":washCardNo});
	}
	function openWechatQrCode(data){
		//页面层
		layer.open({
			title:['请用微信扫码支付', 'text-align: center'],
			type: 1,
			skin: 'layui-layer-rim', //加上边框  
			area: ['420px', '340px'], //宽高   
			closeBtn: 0,//不显示右上角关闭
			content: '<div id="qrcode" style="width:100px; height:100px; margin-top:15px;;margin-left: 100px"></div>',
			btn: ['支付完成', '关闭'] //只是为了演示
			,yes: function(){
				validOrder();
				layer.closeAll();
			}
			,btn2: function(){
				validOrder();
				layer.closeAll();
			}
		}); 
		var qrcode = new QRCode(document.getElementById("qrcode"), {
			width : 200,
			height : 200
		});
		qrcode.makeCode(data); 
	}
});