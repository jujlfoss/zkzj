<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/assets/config/jquery-config.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>重卡之家 - 找回密码</title>
</head>
<body>
	<jsp:include page="/WEB-INF/front/views/common/head.jsp" />
	<div class="container mtb20 border clearfix wd">
		<h3 class="login_h3">找回密码</h3>
		<div class="logindiv" id="register-logindiv">
			<form class="cd-form" onsubmit="return false;">
				<p class="fieldset">
					<label class="image-replace cd-username" for="signup-username">手机号码</label>
					<input class="full-width has-padding has-border"
						id="register-signup-username" type="text" maxlength="11"
						placeholder="请输入注册的手机号码">
				</p>
				<p class="fieldset position-r">
					<label class="image-replace cd-yzm" for="signup-password">短信验证码</label>
					<input class="full-width has-padding has-border"
						id="register-signup-sms" type="text" placeholder="输入短信验证码"
						maxlength="6"> <input type="button" id="register-smsBtn"
						class="btn_mfyzm btn_mfyzm2" value="免费获取验证码" />
				</p>
				<p class="fieldset">
					<input class="full-width2" type="submit" value="下一步">
				</p>
			</form>
		</div>
	</div>
	<jsp:include page="/WEB-INF/front/views/common/foot.jsp" />
	<script type="text/javascript"
		src="<%=basePath%>/front/js/forget/forget.js"></script>
</body>
</html>
