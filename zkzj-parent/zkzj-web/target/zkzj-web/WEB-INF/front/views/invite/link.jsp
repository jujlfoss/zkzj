<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/assets/config/jquery-config.jsp"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <title></title>
    <link href="<%=basePath %>/front/views/invite/css/mui.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<%=basePath %>/front/views/invite/css/common.css"/>
    <style media="screen">
      .header{
        opacity: 0.8;
      }
      .header h1{
        font:18px/44px "微软雅黑";
        color: #f98d3d;
      }
      .header .arrow{
        color:#8c8c8c;
      }
      .mui-content{
        position: relative;
      }
      .register{
        position: absolute;
        top:68%;
        left:50%;
        margin-left: -85px;
      }
      .register img{
        height: 50px;
      }
      .bg_img{
        width: 100%;
        height: 100%;
      }
      .bg_img img{
        width: 100%;
        height: 100%;
      }
    </style>
  </head>
  <body>
<!--     <header class="mui-bar mui-bar-nav header">
      <a class="mui-action-back mui-icon mui-icon-back mui-pull-left arrow"></a>
      <h1 class="mui-title">邀请有礼</h1>
    </header> -->
    <div class="mui-content">
      <div class="bg_img">
         <img src="<%=basePath %>/front/views/invite/img/inviteFriends.jpg">
      </div>
      <div id="save" class="register">
        <img src="<%=basePath %>/front/views/invite/img/res_button.png" alt="">
      </div>
    </div>
    <script type="text/javascript">
    		require.async(['UrlUtil'],function(){
    			$('#save').click(function(){
    				location.href = basePath+'/invite/register.html?invite='+UrlUtil.getUrlParam('invite');
    			});
    		});
    </script>
  </body>
</html>
