<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
<title></title>
<link rel="stylesheet" type="text/css"
	href="<%=basePath%>/front/views/invite/css/common.css" />
<style media="screen">
.content {
	position: relative;
}

.register {
	position: absolute;
	top: 60%;
	left: 50%;
	margin-left: -85px;
}

.register img {
	height: 50px;
	margin-bottom: 20px;
}

.bg_img {
	width: 100%;
	height: 100%;
}

.bg_img img {
	width: 100%;
	height: 100%;
}
</style>
</head>
<body>
	<div class="content">
		<div class="bg_img">
			<img src="<%=basePath%>/front/views/invite/img/reg_success.png">
		</div>
		<div class="register">
			<img src="<%=basePath%>/front/views/invite/img/download_ios.png"
				alt=""> <img
				src="<%=basePath%>/front/views/invite/img/download_android.png"
				alt="">
		</div>
	</div>
</body>
</html>
