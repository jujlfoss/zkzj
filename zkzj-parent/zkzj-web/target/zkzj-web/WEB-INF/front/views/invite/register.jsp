<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/assets/config/jquery-config.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
<title></title>
<link href="<%=basePath%>/front/views/invite/css/mui.min.css"
	rel="stylesheet" />
<link rel="stylesheet" type="text/css"
	href="<%=basePath%>/front/views/invite/css/common.css" />
</head>
<style media="screen">
.header {
	opacity: 0.8;
}

.header h1 {
	font: 18px/44px "微软雅黑";
	color: #f98d3d;
}

.header .arrow {
	color: #8c8c8c;
}

.mui-content {
	position: relative;
}

.bg_img {
	width: 100%;
	height: 100%;
}

.bg_img img {
	width: 100%;
	height: 100%;
}

.photo {
	position: absolute;
	top: 15px;
	left: 50%;
	margin-left: -50px;
	width: 100px;
	height: 100px;
}

.photo img {
	width: 100%;
	height: 100%;
}

.input-list {
	position: absolute;
	width: 100%;
	top: 30%;
	left: 0;
}

.mui-input-row {
	position: relative;
}

.mui-input-row .img {
	position: absolute;
	height: 25px;
	top: 8px;
	left: 7%;
	background: white;
}

.mui-input-row input {
	width: 94%;
	margin-left: 3%;
	padding-left: 15%;
	margin-bottom: 15px;
	border: 1px solid #ccc;
	font: 14px "微软雅黑";
}

.verification {
	position: absolute;
	top: 4px;
	right: 5%;
	background: #f98d3d;
	color: white;
	font: 14px "微软雅黑";
}

.recommend {
	height: 40px;
	position: relative;
	font: 14px "微软雅黑";
}

.checkbox {
	padding-top: 5px;
	padding-left: 2%;
}

.recommender {
	position: absolute;
	left: 15%;
	top: 11px;
}

.forgetPassword {
	position: absolute;
	font: 14px "微软雅黑";
	left: 4%;
	color: #f98d3d;
	top: 10px;
}

.register {
	margin-top: 40px;
	padding: 8px;
	font: 18px "微软雅黑";
	color: white;
	background: #f98d3d;
	width: 94%;
	margin-left: 3%;
}

.clause {
	font: 12px '微软雅黑';
	position: absolute;
	left: 10%;
	top: 10px;
}

.goLogin {
	font: 12px '微软雅黑';
	position: absolute;
	right: 3%;
}

.service {
	color: #f98d3d;
	text-decoration: underline;
}
</style>
<body>
<!-- 	<header class="mui-bar mui-bar-nav header">
		<a class="mui-action-back mui-icon mui-icon-back mui-pull-left arrow"></a>
		<h1 class="mui-title">注 册</h1>
	</header> -->
	<div class="mui-content" id="register-logindiv">
		<div class="bg_img">
			<img src="<%=basePath%>/front/views/invite/img/inviteFriends2.jpg">
		</div>
		<div class="input-list">
			<div class="mui-input-row">
				<img src="<%=basePath%>/front/views/invite/img/phone.png" alt="" class="img"> <input
					type="text" class="mui-input-clear mui-input first"
					placeholder="请输入手机号码" id="register-signup-username" maxlength="11">
			</div>
			<div class="mui-input-row">
				<img src="<%=basePath%>/front/views/invite/img/passwd.png" alt="" class="img"> <input
					type="password" id="register-signup-password" class="mui-input-clear mui-input"
					placeholder="请输入密码">
			</div>
			<div class="mui-input-row">
				<img src="<%=basePath%>/front/views/invite/img/passwd.png" alt="" class="img"> <input
					type="password" id="register-signup-password-re" class="mui-input-clear mui-input"
					placeholder="请再次输入密码">
			</div>
			<div class="mui-input-row">
				<img src="<%=basePath%>/front/views/invite/img/phone.png" alt="" class="img"> <input
					type="text" class="mui-input-clear mui-input verificationInput"
					placeholder="请输入短信验证码" maxlength="6" id="register-signup-sms" >
				<button type="button" name="button" id="register-smsBtn" class="verification">获取验证码</button>
			</div>
			<button type="submit" id="save"
				class="mui-btn mui-btn-block register">注册</button>
			<div class="recommend">
				<span class="clause">点击注册表示你已阅读并同意<a id="xieyiA" class="service">《注册协议》</a>和<a id="falvA" class="service">《法律声明》</a></span>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	require.async([ 'Constant', 'ValidUtil','UrlUtil','xieyi'], function() {
		$("#xieyiA").click(function(){
			zhucexieyi('90%','90%');
		});
		$("#falvA").click(function(){
			falvshengming('90%','90%');
		});
		// 注册
		$("#register-logindiv [type=submit]").click(function() {
			var mobile = ValidUtil.trim($("#register-signup-username").val());
			if (!ValidUtil.isMobile(mobile)) {
				layer.msg('手机号码格式不正确！')
				return;
			}
			var userPwd = ValidUtil.trim($("#register-signup-password").val());
			if (!userPwd) {
				layer.msg('密码不能为空！')
				return;
			}
			var checkUserPwd = ValidUtil.trim($("#register-signup-password-re").val());
			if (userPwd !== checkUserPwd) {
				layer.msg('确认密码不正确！')
				return;
			}
			var mobileCode = ValidUtil.trim($("#register-signup-sms").val());
			if (!mobileCode) {
				layer.msg('短信验证码不能为空！')
				return;
			}
			BaseUtil.post('register', function(data) {
				if (data.success) {
					layer.msg('注册成功',{},function(){
						location.href = basePath + '/invite/success.html';
					});
					return;
				}
				layer.msg(data.msg);
			}, {
				mobile : mobile,
				userPwd : userPwd,
				mobileCode : mobileCode,
				inviteMobile : UrlUtil.getUrlParam('invite'),
				checkUserPwd : checkUserPwd
			});

		});

		var wait = 60;
		document.getElementById("register-smsBtn").disabled = false;
		document.getElementById("register-smsBtn").onclick = function() {
			var mobile = ValidUtil.trim($("#register-signup-username").val());
			if (!ValidUtil.isMobile(mobile)) {
				layer.msg('手机号码格式不正确！')
				return;
			}
			BaseUtil.post('regisCode',function(data){
				if(data.success){
					layer.msg('验证码发送成功！')
					time(document.getElementById('register-smsBtn'));
					return;
				}
				layer.msg(data.msg)
				return;
			},{
				mobile:mobile
			});
		}
		function time(o) {
			if (wait == 0) {
				$(".btn_mfyzm2").css("background", "#F98322");
				$(".btn_mfyzm2").css("color", "#fff");
				o.removeAttribute("disabled");
				$(o).text("获取验证码");
				wait = 60;
			} else {
				$(".btn_mfyzm2").css("background", "#CCCCCC");
				$(".btn_mfyzm2").css("color", "#999");
				o.setAttribute("disabled", true);
				$(o).text("重新发送(" + wait + ")");
				wait--;
				setTimeout(function() {
					time(o)
				}, 1000)
			}
		}
	});
	</script>
</body>
</html>
