<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/assets/config/jquery-config.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>重卡之家</title>
	</head>
	<jsp:include page="/WEB-INF/front/views/common/head.jsp" />
	<jsp:include page="/WEB-INF/front/views/common/search.jsp" />	
	<div class="container mtb20 clearfix wd">
		<jsp:include page="/WEB-INF/front/views/common/left.jsp" />	
		<div class="count_content right border p20">
			<div class="countbox">
				<h3 class="title">订单详情</h3>
				<div class="schedule">
					<div class="step1">
						
					</div>
				</div>
			</div>
			<div class="countbox mt20"> 
				<div class="detail_list mt20">
					<h3>订单信息</h3>
				     <div class="" id="order">
				    	<!-- <p><span class="name">洗车项目:</span><span>精致洗车套餐</span></p>
				    	<p><span class="name">洗车服务点:</span><span>凯悦华庭小区服务点</span></p>
				    	<p><span class="name">服务点地址:</span><span>四川省成都市高新区天府大道中段天府</span></p> -->
				    </div>
				</div>
					<div class="detail_list mt20">
					<h3>订单明细</h3>
				    <table class="contact_table mt20" width="100%" id="cartab">
				    	<tr><th>商品类型名</th><th>购买商品</th><th>商品金额</th><th>订单金额</th></tr>
				    </table>
				</div>
				<div class="detail_list mt20" id="pay">
					
				</div>
			</div>
		</div>
	</div>
  <jsp:include page="/WEB-INF/front/views/common/foot.jsp" />
   <script src="<%=basePath%>/front/js/user/myorderdetails.js"></script>
  
	</body>
</html>
