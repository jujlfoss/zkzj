<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/assets/config/jquery-config.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>重卡之家</title>
</head>
<body>
	<!--弹出登录-->
	<jsp:include page="/WEB-INF/front/views/common/head.jsp" />
	<jsp:include page="/WEB-INF/front/views/common/search.jsp" />
	<div class="container mtb20 clearfix wd">
		<jsp:include page="/WEB-INF/front/views/common/left.jsp" />	
		<div class="count_content right border p20">
			<div class="countbox border-b">
				<h3 class="title">我的账户</h3>
				<ul class="count_ul clearfix" id="cashAccount">

				</ul>
			</div>
			<div class="countbox mt20">
				<h3 class="title">消费记录</h3>
				<div class="box demo2 mt20">
					<ul class="tab_menu">
						<li class="current" onclick="webcashAccountHistoryList(1)">交易记录</li>
						<li onclick="webcashAccountHistoryList(2)">积分记录</li>
						<li onclick="webcashAccountHistoryList(3)">充值记录</li>
					</ul>
					<input type="hidden" value="1" id="type" />
					<div class="tab_box">
						<div>
							<ul id="cashHistory">
							</ul>
						</div>
						<div class="hide">
							<ul id="pointHistory">
							</ul>
						</div>
						<div class="hide" id="rechargeCard">
							<ul>
								<!-- <li class="clearfix">
							  <div class="left">
							  	<P class="name">充100送20套餐</P>
							  	<p class="date">2017-05-06</p>
							  </div>
							  <div class="right">
							  	 <div class="price">-299.00</div>
							  </div>
						    </li> -->
							</ul>
						</div>

					</div>
				</div>
			</div>
			<div id="pagediv"></div>
		</div>
	</div>
	<jsp:include page="/WEB-INF/front/views/common/foot.jsp" />
	<script type="text/javascript"
		src="<%=basePath%>/front/js/user/user.js"></script>


</body>
</html>
