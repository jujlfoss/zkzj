<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/assets/config/jquery-config.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>重卡之家 - 意见反馈</title>
</head>

<body>
	<jsp:include page="/WEB-INF/front/views/common/head.jsp" />
	<jsp:include page="/WEB-INF/front/views/common/search.jsp" />
	<div class="container mtb20 clearfix wd">
		<jsp:include page="/WEB-INF/front/views/common/left.jsp" />	
		<div class="count_content right border p20">
			<div class="countbox">
				<h3 class="title">意见反馈</h3>
				<div class="schedule">
					<div class="step1"></div>
				</div>
			</div>
			<div class="countbox mt20">
				<div class="detail_list mt20">
					<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;亲爱的顾客：
						感谢您一直以来对重卡之家的支持，为了提供更好的服务，您有什么意见或者建议可以在下面留言，我们会虚心听取，极力改进。</p>
					<div class="">
						<textarea placeholder=" 每一条反馈和投诉，都是我们进步的源泉！"
							style="width: 98%; height: 300px;"></textarea>
						<div class="con4" style="text-align: center;">
							<a onclick="saveSuggest();" class="a_btn">畅言一下</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/WEB-INF/front/views/common/foot.jsp" />
	<script type="text/javascript">
	$("#li8").addClass("current");
	function saveSuggest(){
		BaseUtil.post('eb/suggest/saveSuggest',function(result){
			if(result.success){
				$("textarea").val('');
				layer.alert('您的意见我们已经成功收到了，客服会火速把意见反馈给我们的工作人员，我们会在第一时间进行处理，感谢您提出的宝贵意见!',{'title':'意见反馈'});
				return;
			}
			layer.msg(result.msg)
		},{
			content:$("textarea").val()
		});
	}
	</script>
</body>
</html>
