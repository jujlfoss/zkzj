<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/assets/config/jquery-config.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>重卡之家 - 产品列表</title>
<style type="text/css">
.serviceitem p {
	color: #F98322 !important;
	border: 1px solid #F98322;
	padding: 4px 6px;
	margin-top: 4px;
}

.serviceitem .left {
	width: 100px;
	text-align: center;
	display: inline-block;
	display: -moz-box;
	display: -webkit-box;
	-moz-box-align: center;
	-webkit-box-align: center;
	-moz-box-pack: center;
	-webkit-box-pack: center;
}

.icon01Select:hover i {
	background-position: -53px -76px !important;
}

.icon02Select:hover i {
	background-position: -53px -151px !important;
}

.icon03Select:hover i {
	background-position: -53px -216px !important;
}

#goods-ul li, #couponType-ul li, #tuijian-ul-div li, #recharge-ul li,
	#washdiv li {
	margin-right: 3px;
}

#goods-ul img, #couponType-ul img, #tuijian-ul-div img, #recharge-ul img,
	#washdiv img {
	width: 100%;
	height: 100%;
}

#washdiv .yhq_txt p {
	white-space: nowrap;
	overflow: hidden;
}

.disCss {
	color: gray !important;
	border: 1px solid #EDEDED !important;
	background: #F0F0F0 !important;
}
</style>
</head>
<body>
	<jsp:include page="/WEB-INF/front/views/common/head.jsp" />
	<jsp:include page="/WEB-INF/front/views/common/search.jsp" />
	<div class="container wd clearfix ">
		<div class="location mt20" id="mylocation"></div>
		<div class="servicesbox mt30" id="mutli-box" style="display: none">
			<h3>功能菜单</h3>
			<div class=" border p20">
				<div class="box01">
					<div class="function clearfix serviceitem"></div>
				</div>
			</div>
		</div>
		<div class="servicesbox mt30" id="serve-ul-div" style="display: none">
			<h3>
				<i class="gift"></i>洗车服务
			</h3>
			<div class="orderinfo p20 border clearfix">
				<ul id="serve-ul">
				</ul>
				<div id="servePage"></div>
			</div>
		</div>
		<div class="servicesbox mt30" id="selectBox" style="display: none;">
			<div class="areabox border p20 clearfix">
				<div class="mb10 pb10 ">
					<span class="label">选择重卡之家服务点:</span>
					<select class="selectcss" id="citySelect"></select>
					<select class="selectcss" id="countySelect"></select> 
					<select class="selectcss" id="estateSelect"></select>
				</div>
				<div class="rightcontent">
					<div class="car mt20" id="standard_list">
						<span class="label">选择规格:</span>
						<a class="car_a cur" typeid="2" >轿车</a>
						<a class="car_a" typeid="1" >SUV</a>
					</div>
				</div>
			</div>
		</div>
		<div class="servicesbox mt30" id="wash-ul-div" style="display: none">
			<h3>
				<i class="discount"></i>洗车卡
			</h3>
			<div class="sale_list p20 border">
				<div class="yhq clearfix unused" id="washdiv">
				</div>
				<div id="washPage"></div>
			</div>
		</div>
		<div class="servicesbox mt30" id="goods-ul-div" style="display: none">
			<h3>
				<i class="gift"></i>超值礼品<a
					href="<%=basePath%>/search/searchlist.html?location=goods"
					id="goodsMore" style="display: none">查看更多></a>
			</h3>
			<div class="gift_list p20 border clearfix">
				<ul id="goods-ul">
				</ul>
				<div id="goodsPage"></div>
			</div>
		</div>
		<div class="servicesbox mt30" id="couponType-ul-div"
			style="display: none">
			<h3>
				<i class="discount"></i>优惠券专区<a
					href="<%=basePath%>/search/searchlist.html?location=coupon"
					id="couponMore" style="display: none">查看更多></a>
			</h3>
			<div class="sale_list p20 border">
				<div class="yhq clearfix unused">
					<ul id="couponType-ul">
					</ul>
				</div>
				<div id="couponTypePage"></div>
			</div>
		</div>
		<div class="servicesbox mt30" id="recharge-ul-div"
			style="display: none">
			<h3>
				<i class="discount"></i>优惠充值
			</h3>
			<div class="sale_list p20 border">
				<div class="yhq clearfix unused">
					<ul id="recharge-ul">
					</ul>
				</div>
				<div id="rechargePage"></div>
			</div>
		</div>
		<div class="servicesbox mt30" id="tuijian-ul-div"
			style="display: none;">
			<h3>推荐内容</h3>
			<div class="sureOrder border p20 clearfix">
				<div class="orderinfo mt20  clearfix">
					<ul id="tuijian-serve-ul">
					</ul>
				</div>
				<div class="gift_list mt20  clearfix">
					<ul id="tuijian-goods-ul">
					</ul>
				</div>
				<div class="sale_list yhq clearfix unused">
					<ul id="tuijian-couponType-ul">
					</ul>
				</div>
			</div>
		</div>
	</div>
	</div>
	<jsp:include page="/WEB-INF/front/views/common/foot.jsp" />
	<script src="<%=basePath%>/front/js/search/searchlist.js"></script>
</body>
</html>