<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%String headBasePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();%>
<link href="<%=headBasePath%>/front/css/hzw-city-picker.css" rel="stylesheet">
<link href="<%=headBasePath%>/front/css/reset.css" rel="stylesheet">
<link href="<%=headBasePath%>/front/css/main.css" rel="stylesheet">
<link href="<%=headBasePath%>/front/css/style.css" rel="stylesheet">
<style>
.page-util-a,.page-util-a-current{
	padding:2px 4px 2px 4px;
	margin-left:2px;
	background:white;
	border:1px solid lightgray;
}
.page-util-a:hover{
	background:#FAFAFA;
	border:1px solid #EAEAEA;
}
.page-util-a-current{
	background:#FF8117;
	border:1px solid #FF6600;
	color:white;
}
</style>
<div class="topbox clearfix">
	<div style="display:none">
		<span id="ajax_login"><a id="hidden_main_signin" href="javascript:;">登录</a>| <a href="javascript:;" id="hidden_main_signup" class="cd-signup">免费注册</a></span>
	</div>
	<div class="top wd clearfix">
		<div class="top_login">
			<span class="city" id="currCity"><i></i><a href="javascript:;"
				id="cityChoice"> <!-- 定位城市名称 -->
			</a></span>
			<shiro:hasRole name="customer">
				<span style="float:right">欢迎您，<a href="<%=headBasePath %>/user/auth/usercenter.html" style="color:#F98322"><shiro:principal /></a> | <a href="<%=headBasePath %>/user/auth/usercenter.html">进入会员中心</a> | <a
					href="<%=headBasePath%>/logout"> 退出</a></span>
			</shiro:hasRole>
			<shiro:lacksRole name="customer">
				<span class="login" style="float:right"><a href="<%=headBasePath%>/login.html">登录</a> | <a href="<%=headBasePath%>/register.html" class="cd-signup">免费注册</a></span>
			</shiro:lacksRole>
		</div>
		<div class="clear"></div>
		<%--<div class="logo left"></div>--%>
		<div class="nav right">
			<ul>
				<li><a href="<%=headBasePath%>/index.html">首页</a></li>
			</ul>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="cd-user-modal">
	<div class="cd-user-modal-container">
		<ul class="cd-switcher">
			<li><a href="javascript:;">用户登录</a></li>
			<li><a href="javascript:;">注册新用户</a></li>
		</ul>

		<div id="cd-login">
			<form class="cd-form" onsubmit="return false;">
				<p class="fieldset">
					<label class="image-replace cd-username" for="signin-username">手机号码</label>
					<input class="full-width has-padding has-border" maxlength="11"
						id="signin-username" type="text" placeholder="请输入手机号码">
				</p>
				<p class="fieldset">
					<label class="image-replace cd-password" for="signin-password">密码</label>
					<input class="full-width has-padding has-border"
						id="signin-password" type="password" maxlength="20"
						placeholder="输入密码，密码由6-20位字符组成">
				</p>
				<p class="fieldset" id="imageCodeP" style="display: none">
					<label class="image-replace cd-yzm" for="signin-password">验证码</label>
					<input style="width: 70%;" class="has-padding has-border" id="imageCode" type="text" maxlength="4" placeholder="请输入图片验证码" />
					<img style="float: right ;" src="data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" id="imageCodeImg" width="150" height="50"/>
				</p>
				<p class="fieldset">
					<label for="remember-me"><a href="<%=headBasePath%>/forget/forget.html">忘记密码点这里</a></label>
				</p>
				<p class="fieldset">
					<input class="full-width2" type="submit" value="登 录">
				</p>
			</form>
		</div>

		<div id="cd-signup">
			<form class="cd-form" onsubmit="return false;">
				<p class="fieldset">
					<label class="image-replace cd-username" for="signup-username">手机号码</label>
					<input class="full-width has-padding has-border"
						id="signup-username" type="text" maxlength="11"
						placeholder="请输入手机号码">
				</p>

				<p class="fieldset">
					<label class="image-replace cd-password" for="signup-password">密码</label> <input class="full-width has-padding has-border"
						id="signup-password" type="password" maxlength="20"
						placeholder="输入密码，密码由6-20位字符组成">
				</p>

				<p class="fieldset">
					<label class="image-replace cd-password" for="signup-password">密码</label>
					<input class="full-width has-padding has-border" id="signup-password-re" type="password" maxlength="20" placeholder="确认密码">
				</p>
				<p class="fieldset position-r">
					<label class="image-replace cd-yzm" for="signup-password">短信验证码</label>
					<input class="full-width has-padding has-border"
						id="signup-sms" type="text" placeholder="输入短信验证码" maxlength="6"> <input
						type="button" id="smsBtn" class="btn_mfyzm" value="免费获取验证码" />
				</p>
				<p class="fieldset">
					<label class="image-replace cd-username" for="signup-username">手机号码</label>
					<input class="full-width has-padding has-border"
						id="signup-referrerMobile" type="text" maxlength="11"
						placeholder="输入推荐人手机号码，送ta积分 (非必填)">
				</p>

				<p class="fieldset">
					<input class="full-width2" type="submit" value="立即注册"> <label
						for="accept-terms"
						style="display: block; line-height: 24px; text-align: center">点击立即注册表示您已阅读并同意
						<a href="javascript:;">《用户协议》</a>
					</label>
				</p>
			</form>
		</div>
		<a href="javascript:;" class="cd-close-form">关闭</a>
	</div>
</div>
<script src="http://api.map.baidu.com/api?v=2.0&ak=PstANbDGp7gx6L5Bah42AMOLD6QsLdQq"></script>
<script src="<%=headBasePath%>/front/js/common/head.js"></script>