<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/assets/config/jquery-config.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>重卡之家 - 积分商城</title>
<style type="text/css">
#goods-ul li,#coupon-ul li {
	margin-right: 3px;
}
</style>
</head>
<body>
	<jsp:include page="/WEB-INF/front/views/common/head.jsp" />
	<jsp:include page="/WEB-INF/front/views/common/search.jsp" />
	<div class="container wd clearfix ">
		<div class="location mt20">
			<a href="<%=basePath %>/index.html">首页</a> > <a href="javascript:;">积分商城</a>
		</div>

		<div class="servicesbox mt30">
			<h3>
				<i class="discount"></i>优惠券专区<a>查看更多></a>
			</h3>
			<div class="sale_list p20 border ">
				<div class="unused yhq clearfix">
					<ul id="coupon-ul">
					</ul>
				</div>
			</div>
		</div>
		<div class="servicesbox mt30">
			<h3>
				<i class="gift"></i>超值礼品专区<a>查看更多></a>
			</h3>
			<div class="gift_list p20 border clearfix">
				<ul id="goods-ul">
				</ul>
			</div>
		</div>
	</div>
	<jsp:include page="/WEB-INF/front/views/common/foot.jsp" />
	<script src="<%=basePath%>/front/js/point/scoremall.js"></script>
</body>
</html>
