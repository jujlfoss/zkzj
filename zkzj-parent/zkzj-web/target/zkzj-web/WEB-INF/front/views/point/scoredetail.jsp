<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/assets/config/jquery-config.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>重卡之家 - 积分商品详细</title>
<style type="text/css">
#showsum p{
	position: static !important;
}
</style>
</head>
<body>
	<jsp:include page="/WEB-INF/front/views/common/head.jsp" />
	<jsp:include page="/WEB-INF/front/views/common/search.jsp" />
	<div class="container wd clearfix ">
		<div class="location mt20">
			<a href="<%=basePath %>/index.html">首页</a> > <a href="<%=basePath%>/search/searchlist.html?location=point">积分商城</a> > <a href="javascript:location.reload()">商品详情</a>
		</div>
		<div class="orderbox border p20 mt20 clearfix">
			<div class="leftimg left">
				<div id="showbox">
				</div>
				<div id="showsum"></div>
			</div>
			<div class="rightcontent right" id="goods">

			</div>
		</div>

		<div class="servicesbox mt30" id="goodscontent">
			<h3>产品详情</h3>
			<div class="pro_detail border p20 clearfix">
				<div class="imgbox" id="imgbox">
				</div>
			</div>
		</div>
		<div class="servicesbox mt30">
			<h3>积分兑换须知：</h3>
			<div class="notice border clearfix p20">
				<p>
				• 实体商品需要到指定的服务点地址领取，可以在我的订单中查看具体地址<br> 
				• 兑换的电子券仅限兑换用户使用，兑换后不退不换。<br> 
				• 如有疑问请联系客服热线400-055-2799<br> 
				</p>
			</div>
		</div>
	</div>
	<jsp:include page="/WEB-INF/front/views/common/foot.jsp" />
	<script src="<%=basePath%>/front/js/point/scoredetail.js"></script>
</body>
</html>
