<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/assets/config/jquery-config.jsp" %> 
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>重卡之家</title>
	</head>
	<body>
		<jsp:include page="/WEB-INF/front/views/common/head.jsp" />
 	<div class="container wd clearfix ">
	    <div class="location mt20">
	    	<a href="<%=basePath%>/index.html">首页</a> > <a href="<%=basePath%>/search/searchlist.html?location=serve">洗车服务</a> > <a href="javascript:window.history.go(-1)">预约信息</a> > 
	    	<a href="javascript:location.reload()">预约服务订单</a> 
	    </div>
	    <div class="servicesbox mt30">
	    	<h3>服务流程</h3>
	    	<div class="flowbox border">
	    		<img src="<%=basePath %>/front/images/flow_bg.png">
	    	</div>
	    </div>
	 <form action="" method="post" name="bepkForm">
     <input class="datainp" id="bespeakTime" name="bespeakTime" type="hidden" />
	 <input type="hidden" name="estateId" id="estateId">
	 <input type="hidden" name="serveStandardSnapshotId" id="serveStandardSnapshotId">
	 <input type="hidden" name="totalPrice" id="serviceTotalPrice">
	 <div class="servicesbox mt30">
	    	<h3>选择服务时间</h3>
	    	<div class="select_time p20 border clearfix">
	    		<div class="time_top clearfix">
	    			<div class="left">
	    			    <input class="datainp" id="dateinfo" type="text" placeholder="请选择日期"  readonly />
	    			    <a class="looktime_btn" onclick="listBespeaktime($('#dateinfo').val())">查看服务时间</a>
	    			</div>
	    			<div class="state right">
	    				<span class=""><i class="valid"></i>可预约</span>
	    				<span class=""><i></i>不可预约</span>
	    			</div>
	    		</div>
	    		<table class="timelist_table mt20" id="bespeaktime_tab" style="width:100%;cursor: pointer;">
	    			<tr>
	    				<td colspan="5">无可预约时间信息 </td>
	    			</tr> 
	    		</table>
	    	</div>
		</div>
		<div class="servicesbox mt30">
	    	<h3>联系信息</h3>
	    	<div class="contactInfo border p20 clearfix">
	    		<div class=" mt20 clearfix">
	    			<span class="name left">联系人</span><input type="text" name="car.ownerName" id="linkman" maxlength="10" onblur="$('#linkmanText').html(this.value)" class="inputcss left"><a class="a_btn left" onclick="window.openLink()">常用联系人</a>
	    		</div>
	    		<div class="mt20 clearfix">
	    			<span class="name left">手机号</span><input type="text" name="car.ownerMobile" id="linkmobile" maxlength="11" onblur="$('#linkmobileText').html(this.value)" class="inputcss left">
	    		</div>
	    		<div class="mt20 clearfix">
	    			<span class="name left">车牌号</span><input type="text" name="car.carNo" id="carno" maxlength="10" class="inputcss left"> 
	    		</div>
	    	</div>
	    </div>
	    <div class="servicesbox mt30">
	    	<h3>确认订单信息</h3>
	    	<div class="sureOrder border p20 clearfix">
	    		<div class="surebox mb20 border-b">
	    			<p class="place" id="estateName"> </p>
	    			<p class="addr" id="estateAddress"> </p>
	    		</div>
	    		<div class="surebox mb20 border-b">
	    			<div class="pricebox clearfix">
	    				<span class="pricename left">停车地点<input type="text" id="stopPlace" name="stopPlace" maxlength="50" class="inputcss"></span>
	    			</div> 
	    		</div>
	    		<div class="surebox mb20 border-b">
	    			<div class="pricebox clearfix">
	    				<span class="pricename left" id="serveName"></span><span class="price01 right"><em>￥</em><i id="servicePrice">0.00</i></span>
	    			</div> 
	    		</div>
	    		<div class="surebox mb20 border-b">
	    			<div class="pricebox clearfix">
	    				<span class="pricename left">服务共计</span><span class="price01 right"><em>￥</em><i id="servicePriceSum">0.00</i></span>
	    			</div> 
	    		</div>
	    		<div class="surebox mb20 border-b">
	    			<div class="pricebox clearfix">
	    				<span class="pricename left">我的电子优惠券
	    				<select class="selectcss" style="font-size: 14px;" onchange="window.selectCoupon(this.value)" id="coupon">
	    				  <!-- <option selected>7元优惠券</option> -->
	    				</select></span>
	    				<span class="price01 right"><em>-￥</em><i id="couponprice">0.00</i></span>
	    			</div> 
	    		</div>
	    		<div class="surebox mb20 border-b">
	    			<div class="pricebox clearfix">
	    				<span class="pricename left">给服务人员留言 	<input type="text" id="remark" name="remark" maxlength="50" class="inputcss"></span>
	    			</div> 
	    		</div>
	    		<div class="sumbox clearfix border" style="width:400px;">
	    			<div class="sumpricebox">  
	    				<p class="sumprice">应付款：<em>￥</em><em id="totalPrice">0.00</em></p>
	    				<p>洗车时间：<i id="washtime">未选择</i></p>
	    				<p>联系人：<i id="linkmanText">未填写</i></p>
	    				<p>手机号：<i id="linkmobileText">未填写</i></p>
	    			</div>
	    		</div>
	    		<div class="surebtnbox mt20">
	    			<a class="a_btn" onclick="window.submitOrder()">提交订单</a>
	    		</div>
	    	</div>
	    </div></form>
	</div>
 		<jsp:include page="/WEB-INF/front/views/common/foot.jsp" />
		<script type="text/javascript" src="<%=basePath %>/front/js/serve/auth/servicesure.js"></script>
	</body>
</html>
