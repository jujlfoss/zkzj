<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/assets/config/jquery-config.jsp" %> 
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>重卡之家</title>
	</head>  
	<body>
		<jsp:include page="/WEB-INF/front/views/common/head.jsp" />
		<jsp:include page="/WEB-INF/front/views/common/search.jsp" /> 
	<div class="container wd clearfix ">
	    <div class="location mt20">
	    	<a href="<%=basePath%>/index.html">首页</a> > <a href="javascript:location.reload()">洗车服务列表</a>
	    </div>
	       
	     <div class="servicesbox mt30">
	    	<h3>服务信息</h3>
	    	<div class="orderinfo p20 border clearfix">
	    		<ul id="serviceUl">
	    		 <%-- <li class="clearfix"> 
	    					<div class="proimg left">
	    						<img src="<%=basePath %>/file/front/display/2017061615D9AF3BD484E8BC.jpg">
	    					</div>
	    					<div class="txt left">
	    						<span class="name">外观清洗</span>
	    						<span class="stars"><i></i><i></i><i></i></span>
	    						<span class="price">价格：88.00元</span>
	    					</div>
	    					<div class=" right">
	    						<a class="a_btn">我要预约</a>
	    					</div> 
	    			</li>  --%>
	    		</ul>
	    		<div id="pagediv"></div> 
	    	</div>
	     </div>
	    
	     </div>
	<jsp:include page="/WEB-INF/front/views/common/foot.jsp" />
	</body>
	<script type="text/javascript" src="<%=basePath%>/front/js/serve/servicelist.js"></script>
</html>
