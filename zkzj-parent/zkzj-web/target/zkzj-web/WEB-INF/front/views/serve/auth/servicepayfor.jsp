<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/assets/config/jquery-config.jsp" %> 
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>重卡之家</title>
	</head>
	<body>
		<jsp:include page="/WEB-INF/front/views/common/head.jsp" />
	<div class="container wd clearfix ">
	    <div class="location mt20">
<a href="<%=basePath%>/index.html">首页</a> > <a href="<%=basePath%>/search/searchlist.html?location=serve">洗车服务</a> > <a href="javascript:window.history.go(-2)">预约信息</a> > 
	    	<a href="javascript:window.history.go(-1)">预约服务订单</a>  >      	
<a href="javascript:location.reload()">洗车服务支付</a>
	    </div> 
	 <div class="servicesbox mt30">
	    	<h3>支付信息</h3>
	    	<div class="pay mtb20 clearfix border p20">
	    			<p id="orderno"></p>
	    			<p>订单提交成功，请尽快付款！</p>  
	    			<p>请您在<font color="red">15分钟</font>内完成支付，否则订单会被自动取消</p>
	    		<span class="left">
	    		</span>
	    		<span class="money right">应付金额:<em id="totalPrice">0.00</em>元</span>
	    	</div>
		</div>
		<div class="servicesbox mt30" id="servicesbos" style="display: none;"> 
	    	<h3>支付方式</h3>
	    	<div class="paymethod border p20 clearfix">
	    		<input type="hidden" id="paymethod" >
	    		<ul class="pay_ul clearfix" id="payul">
	    			<li class="current" id="li_wx"><input type="hidden" value="2"><i class="wx"></i>微信<span class=""></span></li>
	    			<li id="li_apay"><input type="hidden" value="1"><i class="zfb"></i>支付宝<span class=""></span></li>
	    			<li id="balance"><input type="hidden" value="0"><i class="balance"></i>账户余额<span class="" id="balanceAccount"></span></li>
	    		</ul>
	    		<div class="">
	    			<img src="">
	    		</div>
	    		<div class="code">
	    			<div class="codediv" style="margin-top: 10px">
	    				<input style="margin:0;font-size: 16px;display: none;" id="code" type="password" class="inputcss" maxlength="6" placeholder="请输入支付密码" />
	    		    	<a href="<%=basePath %>/user/auth/countsafe.html"> 忘记或设置支付密码？</a>
	    			</div> 
	    			<a onclick="window.payfor()" id="payfor" class="pay_btn">立即支付</a> 
	    		</div>
	    	</div>
	    </div>
	   
	</div>
 
	<jsp:include page="/WEB-INF/front/views/common/foot.jsp" /> 
 	<script type="text/javascript" src="<%=basePath %>/assets/front/qrcode.js"></script>
 	<script type="text/javascript" src="<%=basePath %>/front/js/serve/auth/servicepayfor.js"></script>
    <script type="text/javascript">
	$(document).ready(function () {
		$(".codediv").hide();
		$(".pay_ul").find("li").click(function(){
	    	$(".pay_ul").find("li").removeClass("current");
	    	$(".pay_ul").find("li").find("span").hide();
	    	$(this).addClass("current");
	    	$(this).find("span").show();
	    	if($("#balance").hasClass("current")){
	    		$(".codediv").show();
	    	}else{
	    		$(".codediv").hide();
	    	}
	    	$("#paymethod").val($(this).find("input")[0].value);
	   });
	}); 
	</script>
	</body>
</html>

