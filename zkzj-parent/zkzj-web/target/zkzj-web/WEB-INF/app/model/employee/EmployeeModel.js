Ext.define('App.model.employee.EmployeeModel', {
	extend : 'Ext.data.Model',
	fields : [ 'id', 'userId', 'employeeName', 'sex', 'mobile', 'nativePlace',
			'address', 'createTime', 'modifyTime', 'status', 'teamId',
			'teamName', 'cityId', 'name' ]
});