Ext.define('App.model.customer.CustomerModel', {
	extend : 'Ext.data.Model',
	fields : [ 'id','userId','customerName','avatarUrl','referrerMobile','createTime','modifyTime','status','model' ]
});