Ext.define('App.model.city.CityModel', {
	extend : 'Ext.data.Model',
	fields : [ 'id','areaName','areaType','parentId','baiduCode','cityCode','addressCode','latitude','longitude','createTime','modifyTime','status' ]
});