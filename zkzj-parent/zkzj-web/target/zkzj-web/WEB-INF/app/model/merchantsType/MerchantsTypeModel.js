Ext.define('App.model.merchantsType.MerchantsTypeModel', {
	extend : 'Ext.data.Model',
	fields : [ 'id','typeName','createTime','modifyTime','status' ]
});