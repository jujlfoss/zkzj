Ext.define('App.model.area.AreaModel', {
	extend : 'Ext.data.Model',
	fields : [ 'id','areaName','parentId','countyId','createTime','modifyTime','status','countyName','cityName','pName','pId','cityId','superName']
});