Ext.define('App.model.team.TeamCheckedModel', {
	extend : 'Ext.data.Model',
	fields : [ 'id','teamName','remark','createTime','modifyTime','status','parentId',{
		name : 'index',
		mapping : 'sort'
	},'checked','expanded']
});