Ext.define('App.lib.UploadMultiSelectWindow', {
	extend : 'App.config.BaseWindow',
	requires : [ 'App.controller.uploadFile.UploadMultiSelectController'],
	controller : 'uploadMultiSelectController',
	xtype : 'uploadMultiSelectWindow',
	id : 'uploadMultiSelectWindow',
	layout : 'anchor',
	title:"请选择图片",
	buttons:[
	         {
	        	 text:"上传",
	        	 action:"save"
	         },
	              {
	        	 text:"关闭", handler:function(btn){
	        			btn.up("window").close();
	        	 }
	        	 
	         },
	         
	         
	         ],
	items:[{
		xtype:"panel",
		itemId:"imagesPanel",
		autoScroll : true,
		layout:"anchor",
		height:200,
		listeners: {
			 'beforeadd':function(st,str,obj){
					var grid = Ext.ComponentQuery.query('panel [itemId=imagesPanel]');
					var d = grid[0].body.dom;
					d.scrollTop = d.scrollHeight - d.offsetHeight;
	          }
	        }
	},{
		xtype:"form",
		layout:"form",
		enctype : "multipart/form-data",
		items:[{
			xtype:"filefield",
			fieldLabel : "图片",
			name : "files",
			allowBlank : false,
		}]
	}
]
});
Ext.define('App.lib.UploadMultiSelect', {
	extend : 'Ext.form.FieldSet',
	requires : ['App.controller.uploadFile.UploadMultiSelectController','App.lib.SelectField'],
	controller : 'uploadMultiSelectController',
	alias : 'widget.uploadMultiSelect',
	layout : 'fit',
	border : false,
	padding:0,
	margin:0,
	config:{
		margin : 0,
		beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
		allowBlank : false,
		submitValue:'detailUrls',
		fieldLabel : '图片',
		emptyText:""
	},
	updateMargin: function(value){
		this.margin = value;
	},
    updateAllowBlank: function(value) {
    	this.items[0].allowBlank = value;
    },
    updateBeforeLabelTextTpl: function(value) {
    	this.items[0].beforeLabelTextTpl = value;
    },
    updateSubmitValue: function(value) {
    	this.items[0].name = value;
    },
    updateFieldLabel: function(value) {
    	this.items[0].fieldLabel = value;
    },
    updateEmptyText: function(value) {
    	this.items[0].emptyText = value;
    },
	defaults : {
		msgTarget : 'under',
		labelWidth : 100,
		labelAlign : 'right'
	},
	items : [ {
		xtype : 'selectfield',
		itemId:'files',
		onClick : function(me) {
			Ext.widget('uploadMultiSelectWindow', {uploadMultiSelect : me.up('uploadMultiSelect')}).show();
		}
	} ]
});
