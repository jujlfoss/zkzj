Ext.define('App.lib.UploadMultiViewSelectWindow', {
	extend : 'App.config.BaseWindow',
	requires : [ 'App.controller.uploadFile.UploadMultiViewSelectController'],
	controller : 'uploadMultiViewSelectController',
	xtype : 'uploadMultiViewSelectWindow',
	id : 'uploadMultiViewSelectWindow',
	layout : 'anchor',
	title:"查看图片",
	items:[{
		xtype:"panel",
		itemId:"imagesPanel",
		autoScroll : true,
		layout:"anchor",
		height:400,
		listeners: {
			 'beforeadd':function(st,str,obj){
					var grid = Ext.ComponentQuery.query('panel [itemId=imagesPanel]');
					var d = grid[0].body.dom;
					d.scrollTop = d.scrollHeight - d.offsetHeight;
	          }
	        }
	}
]
});
Ext.define('App.lib.UploadMultiViewSelect', {
	extend : 'Ext.form.FieldSet',
	requires : ['App.controller.uploadFile.UploadMultiViewSelectController','App.lib.SelectField'],
	controller : 'uploadMultiViewSelectController',
	alias : 'widget.uploadMultiViewSelect',
	layout : 'fit',
	border : false,
	padding:0,
	margin:0,
	config:{
		margin : 0,
		beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
		allowBlank : false,
		submitValue:'beforePictureUrls',
		fieldLabel : '图片'
	},
	updateMargin: function(value){
		this.margin = value;
	},
    updateAllowBlank: function(value) {
    	this.items[0].allowBlank = value;
    },
    updateBeforeLabelTextTpl: function(value) {
    	this.items[0].beforeLabelTextTpl = value;
    },
    updateSubmitValue: function(value) {
    	this.items[0].name = value;
    },
    updateFieldLabel: function(value) {
    	this.items[0].fieldLabel = value;
    },
	defaults : {
		msgTarget : 'under',
		labelWidth : 100,
		labelAlign : 'right'
	},
	items : [ {
		xtype : 'selectfield',
		itemId:'files',
		onClick : function(me) {
			Ext.widget('uploadMultiViewSelectWindow', {uploadMultiViewSelect : me.up('uploadMultiViewSelect')}).show();
		}
	} ]
});
