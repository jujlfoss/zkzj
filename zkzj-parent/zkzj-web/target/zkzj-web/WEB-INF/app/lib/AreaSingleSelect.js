Ext.define('App.lib.AreaSingleSelectList', {
	extend : 'App.config.BaseGrid',
	requires : [ 'Ext.selection.CheckboxModel' ],
	xtype : 'areaSingleSelectList',
	selModel: { 
        selType: 'checkboxmodel',
        mode : 'SINGLE',
		allowDeselect : true
    },
	tbar : [ {
		xtype : 'form',
		border : 0,
		layout : 'hbox',
		defaults : {
			labelWidth : 60
		},
		items : [ {
			fieldLabel : '类型名',
			name : 'areaName',
			xtype : 'textfield'
		}, {
			xtype : 'button',
			text : '查询',
			style : 'margin-left:15px',
			handler : 'tbarSearch'
		} ]
	} ],
	columns : [ {
		dataIndex : 'id',
		hidden : true
	}, {
		text : '类型名',
		dataIndex : 'areaName'
	} ],
	initComponent : function() {
		this.store = Ext.create('App.store.area.AreaStore', {
			storeId : 'area.areaSingleSelectStore',
			autoDestroy : true,
			autoLoad : true,
			listeners : {
				load : function() {
					var win = Ext.getCmp('areaSingleSelectWindow'),list = win.down('areaSingleSelectList'),store = list.getStore(),count = store.getCount(),id = win.areaSingleSelect.down('[itemId=areaId]').getValue();
					for (var i = 0; i < count; i++) {
						var record = store.getAt(i);
						if(record.get('id')==id){
							list.getSelectionModel().select(record);
							return;
						}
					}
				}
			}
		});
		this.bbar = Ext.widget('pagingtoolbar',{
			store : this.store,
			scrollable : true,
			displayInfo : true
		});
		this.callParent(arguments);
	}
});
Ext.define('App.lib.AreaSingleSelectWindow', {
	extend : 'App.config.BaseWindow',
	requires : [ 'App.controller.area.AreaSingleSelectController'],
	controller : 'areaSingleSelectController',
	xtype : 'areaSingleSelectWindow',
	id : 'areaSingleSelectWindow',
	title : '请选择类型',
	buttons : [ {
		text : '确认',
		action : 'save'
	},{
		text : '清空',
		handler : function() {
			this.up('window').down('grid').getSelectionModel().deselectAll();
		}
	},{
		text : '关闭',
		handler : function() {
			this.up('window').close();
		}
	} ],
	items : [ {
		xtype : 'areaSingleSelectList'
	} ]
});
Ext.define('App.lib.AreaSingleSelect', {
	extend : 'Ext.form.FieldSet',
	requires : ['App.controller.area.AreaSingleSelectController','App.lib.SelectField'],
	controller : 'areaSingleSelectController',
	alias : 'widget.areaSingleSelect',
	layout : 'fit',
	border : false,
	padding:0,
	margin:0,
	config:{
		margin : 0,
		beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
		allowBlank : false,
		submitName:'areaName',
		submitValue:'areaId',
		fieldLabel : '类型名'
	},
	updateMargin: function(value){
		this.margin = value;
	},
    updateAllowBlank: function(value) {
    	this.items[1].allowBlank = value;
    },
    updateBeforeLabelTextTpl: function(value) {
    	this.items[1].beforeLabelTextTpl = value;
    },
    updateSubmitName: function(value) {
    	this.items[1].name = value;
    },
    updateSubmitValue: function(value) {
    	this.items[0].name = value;
    },
    updateFieldLabel: function(value) {
    	this.items[1].fieldLabel = value;
    },
	defaults : {
		msgTarget : 'under',
		labelWidth : 100,
		labelAlign : 'right'
	},
	items : [ {
		xtype : 'hidden',
		itemId:'areaId'
	}, {
		xtype : 'selectfield',
		itemId:'areaName',
		onClick : function(me) {
            var viewXtype = me.up('areaSingleSelect').viewXtype;
			Ext.widget('areaSingleSelectWindow', {areaSingleSelect : me.up('areaSingleSelect'),viewXtype: viewXtype}).show();
		}
	} ]
});
