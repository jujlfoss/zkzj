Ext.define('App.lib.CountyCombox', {
    alias : 'widget.countyCombox',
    extend : 'Ext.form.field.ComboBox',
    width: 320,
    bodyPadding: 1,
    fieldLabel : "区/县",
    layout: 'anchor',
    queryMode : 'local',
    lazyInit : true,
    editable : false,
    triggerAction : 'all',
    store : Ext.create('Ext.data.Store', {
        fields : [ "areaId", "areaName" ],
        //autoLoad : true,
        proxy : {
            type : 'ajax',
            url : basePath+'/sys/city/getList',
            reader : {
                type : 'json',
                root : 'data'
            }
        },
    }),
    allowBlank : false,
    emptyText : '请选择区/县...',
    displayField : 'areaName',
    valueField : 'id',
    name:"countyId"

});

