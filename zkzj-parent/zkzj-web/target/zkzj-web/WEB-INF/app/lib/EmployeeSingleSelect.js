Ext.define('App.lib.EmployeeSingleSelectList', {
	extend : 'App.config.BaseGrid',
	requires : [ 'Ext.selection.CheckboxModel' ],
	xtype : 'employeeSingleSelectList',
	selModel: { 
        selType: 'checkboxmodel',
        mode : 'SINGLE',
		allowDeselect : true
    },
	tbar : [ {
		xtype : 'form',
		border : 0,
		layout : 'hbox',
		defaults : {
			labelWidth : 50
		},
		items : [ {
			fieldLabel : '名称',
			name : 'employeeName',
			xtype : 'textfield'
		}, {
			xtype : 'button',
			text : '查询',
			style : 'margin-left:15px',
			handler : 'tbarSearch'
		} ]
	} ],
	columns : [ {
		dataIndex : 'id',
		hidden : true
	}, {
		text : '名称',
		dataIndex : 'employeeName'
	}, {
		text : '状态',
		dataIndex : 'status',
		renderer : function(value) {
			return StatusEnum.getHtml(value);
		}
	} ],
	initComponent : function() {
		this.store = Ext.create('App.store.employee.EmployeeStore', {
			storeId : 'employee.EmployeeSingleSelectStore',
			autoDestroy : true,
			autoLoad : true,
			listeners : {
				load : function() {
					var win = Ext.getCmp('employeeSingleSelectWindow'),list = win.down('employeeSingleSelectList'),store = list.getStore(),count = store.getCount(),employeeId = win.employeeSingleSelect.down('[itemId=employeeId]').getValue();
					for (var i = 0; i < count; i++) {
						var record = store.getAt(i);
						if(record.get('id')==employeeId){
							list.getSelectionModel().select(record);
							return;
						}
					}
				}
			}
		});
		this.bbar = Ext.widget('pagingtoolbar',{
			store : this.store,
			scrollable : true,
			displayInfo : true
		});
		this.callParent(arguments);
	}
});
Ext.define('App.lib.EmployeeSingleSelectWindow', {
	extend : 'App.config.BaseWindow',
	requires : [ 'App.controller.employee.EmployeeSingleSelectController'],
	controller : 'employeeSingleSelectController',
	xtype : 'employeeSingleSelectWindow',
	id : 'employeeSingleSelectWindow',
	title : '选择员工',
	buttons : [ {
		text : '确认',
		action : 'save'
	},{
		text : '清空',
		handler : function() {
			this.up('window').down('grid').getSelectionModel().deselectAll();
		}
	},{
		text : '关闭',
		handler : function() {
			this.up('window').close();
		}
	} ],
	items : [ {
		xtype : 'employeeSingleSelectList'
	} ]
});
Ext.define('App.lib.EmployeeSingleSelect', {
	extend : 'Ext.form.FieldSet',
	requires : ['App.controller.employee.EmployeeSingleSelectController','App.lib.SelectField'],
	controller : 'employeeSingleSelectController',
	alias : 'widget.employeeSingleSelect',
	layout : 'fit',
	border : false,
	padding:0,
	margin:0,
	config:{
		beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
		allowBlank : false,
		submitName:'employeeName',
		submitValue:'employeeId',
		fieldLabel : '员工',
		margin : 0
	},
	updateMargin: function(value){
		this.margin = value;
	},
    updateAllowBlank: function(value) {
    	this.items[1].allowBlank = value;
    },
    updateBeforeLabelTextTpl: function(value) {
    	this.items[1].beforeLabelTextTpl = value;
    },
    updateSubmitName: function(value) {
    	this.items[1].name = value;
    },
    updateSubmitValue: function(value) {
    	this.items[0].name = value;
    },
    updateFieldLabel: function(value) {
    	this.items[1].fieldLabel = value;
    },
	defaults : {
		msgTarget : 'under',
		labelWidth : 100,
		labelAlign : 'right'
	},
	items : [ {
		xtype : 'hidden',
		itemId : 'employeeId'
	}, {
		itemId : 'employeeName',
		xtype : 'selectfield',
		onClick : function(me) {
			Ext.widget('employeeSingleSelectWindow', {employeeSingleSelect : me.up('employeeSingleSelect')}).show();
		}
	},{
		xtype : 'hidden',
		itemId : 'employeeMobile'
	}]
});