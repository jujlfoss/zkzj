Ext.define('App.lib.TeamSingleSelectList', {
	extend : 'App.config.BaseTree',
	xtype : 'teamSingleSelectList',
	mixins : [ 'App.lib.TreeFilter' ],
	tbar : [ {
		xtype : 'form',
		border : 0,
		layout : 'hbox',
		defaults : {
			labelWidth : 50
		},
		items : [ {
			fieldLabel : '名称',
			name : 'teamName',
			xtype : 'textfield'
		}, {
			xtype : 'button',
			text : '查询',
			style : 'margin-left:15px',
			handler : 'tbarSearch'
		} ]
	} ],
	columns : [ {
		dataIndex : 'id',
		hidden : true
	}, {
		text : '名称',
		xtype : 'treecolumn',
		dataIndex : 'teamName'
	}, {
		text : '状态',
		dataIndex : 'status',
		renderer : function(value) {
			return StatusEnum.getHtml(value);
		}
	} ],
	initComponent : function() {
		this.store = Ext.create('App.store.team.TeamStore', {
			storeId : 'team.TeamSingleSelectStore',
			autoDestroy : true,
			autoLoad : true,
			proxy : {
				type : 'ajax',
				url : basePath + '/hrm/team/listChecked'
			},
			listeners : {
				load : function() {
					var win = Ext.getCmp('teamSingleSelectWindow');
					var teamIdsValue = win.teamSingleSelect.down('[itemId=teamId]').getValue();
					if(ValidUtil.isNotBlank(teamIdsValue)){
						var store = win.down('teamSingleSelectList').getStore();
						store.getNodeById(teamIdsValue.split(',')[0]).set('checked', true);
					}
				}
			}
		});
		this.callParent(arguments);
	}
});
Ext.define('App.lib.TeamSingleSelectWindow', {
	extend : 'App.config.BaseWindow',
	requires : [ 'App.controller.team.TeamSingleSelectController'],
	controller : 'teamSingleSelectController',
	xtype : 'teamSingleSelectWindow',
	id : 'teamSingleSelectWindow',
	title : '选择部门',
	buttons : [ {
		text : '确认',
		action : 'save'
	}, {
		text : '关闭',
		handler : function() {
			this.up('window').close();
		}
	} ],
	items : [ {
		xtype : 'teamSingleSelectList'
	} ]
});
Ext.define('App.lib.TeamSingleSelect', {
	extend : 'Ext.form.FieldSet',
	requires : ['App.controller.team.TeamSingleSelectController','App.lib.SelectField'],
	controller : 'teamSingleSelectController',
	alias : 'widget.teamSingleSelect',
	layout : 'fit',
	border : false,
	padding:0,
	margin:0,
	config:{
		beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
		allowBlank : false,
		submitName:'teamName',
		submitValue:'teamId',
		fieldLabel : '部门',
		margin : 0
	},
	updateMargin: function(value){
		this.margin = value;
	},
    updateAllowBlank: function(value) {
    	this.items[1].allowBlank = value;
    },
    updateBeforeLabelTextTpl: function(value) {
    	this.items[1].beforeLabelTextTpl = value;
    },
    updateSubmitName: function(value) {
    	this.items[1].name = value;
    },
    updateSubmitValue: function(value) {
    	this.items[0].name = value;
    },
    updateFieldLabel: function(value) {
    	this.items[1].fieldLabel = value;
    },
	defaults : {
		msgTarget : 'under',
		labelWidth : 100,
		labelAlign : 'right'
	},
	items : [ {
		xtype : 'hidden',
		itemId : 'teamId'
	}, {
		itemId : 'teamName',
		xtype : 'selectfield',
		onClick : function(me) {
			Ext.widget('teamSingleSelectWindow', {teamSingleSelect : me.up('teamSingleSelect')}).show();
		}
	} ]
});
