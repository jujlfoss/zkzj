Ext.define('App.lib.CityCombox', {
    alias : 'widget.cityCombox',
    extend : 'Ext.form.field.ComboBox',
    width: 320,
    bodyPadding: 1,
    fieldLabel : "所属市",
    layout: 'anchor',
    queryMode : 'local',
    lazyInit : true,
    editable : false,
    triggerAction : 'all',
    store : Ext.create('Ext.data.Store', {
        fields : [ "cityId", "areaName" ],
        //autoLoad : true,
        proxy : {
            type : 'ajax',
            url : basePath+'/sys/city/getList',
            reader : {
                type : 'json',
                root : 'data'
            }
        },
    }),
    allowBlank : false,
    emptyText : '请选择市...',
    displayField : 'areaName',
    valueField : 'id',
    name:"cityId"

});

