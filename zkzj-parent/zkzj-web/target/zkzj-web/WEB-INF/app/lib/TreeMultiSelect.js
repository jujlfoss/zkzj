Ext.define('App.lib.TreeMultiSelectList', {
	extend : 'App.config.BaseTree',
	xtype : 'treeMultiSelectList',
	requires : [ 'App.model.goodsType.TreeModel' ],
	mixins : [ 'App.lib.TreeFilter' ],
	selModel: { 
        mode : 'SINGLE',
    },
	tbar : [ {
		xtype : 'form',
		border : 0,
		layout : 'hbox',
		defaults : {
			labelWidth : 50
		},
//		items : [ {
//			fieldLabel : '名称',
//			name : 'treeName',
//			xtype : 'textfield'
//		}, {
//			xtype : 'button',
//			text : '查询',
//			style : 'margin-left:15px',
//			handler : 'tbarSearch'
//		} ]
	} ],
	columns : [ {
		dataIndex : 'id',
		hidden : true
	},{
		dataIndex : 'parentId',
		hidden : true
	}, {
		text : '省/市/县',
		xtype : 'treecolumn',
		dataIndex : 'text'
	} ],
	initComponent : function() {
		this.store = Ext.create('App.store.area.TreeStore', {
			model : 'App.model.goodsType.TreeModel',
			storeId : 'tree.multiTreedStore',
			autoDestroy : true,
			autoLoad : true,
			listeners : {
				load : function() {
					var win = Ext.getCmp('treeMultiSelectWindow');
					var teamIdsValue = win.treeMultiSelect.down('[itemId=id]').getValue();
					if(ValidUtil.isNotBlank(teamIdsValue)){
						var store = win.down('treeMultiSelectList').getStore();
						store.getNodeById(teamIdsValue).set('checked', true);
					}
				}
			}
		});
		this.callParent(arguments);
	}
});
Ext.define('App.lib.TreeMultiSelectWindow', {
	extend : 'App.config.BaseWindow',
	requires : [ 'App.controller.area.TreeMultiSelectController'],
	controller : 'treeMultiSelectController',
	xtype : 'treeMultiSelectWindow',
	id : 'treeMultiSelectWindow',
	title : '选择类型',
	buttons : [ {
		text : '确认',
		action : 'save'
	},{
		text : '清空',
		action : 'reset'
	}, {
		text : '关闭',
		handler : function() {
			this.up('window').close();
		}
	} ],
	items : [ {
		xtype : 'treeMultiSelectList'
	} ]
});
Ext.define('App.lib.TreeMultiSelect', {
	extend : 'Ext.form.FieldSet',
	requires : ['App.controller.area.TreeMultiSelectController','App.lib.SelectField'],
	controller : 'treeMultiSelectController',
	alias : 'widget.treeMultiSelect',
	layout : 'fit',
	border : false,
	padding:0,
	margin:0,
	config:{
		beforeLabelTextTpl : '',
		allowBlank : true,
		submitValue:'areaId',
		fieldLabel : '请选择',
		margin : 0
	},
	updateMargin: function(value){
		this.margin = value;
	},
    updateAllowBlank: function(value) {
    	this.items[1].allowBlank = value;
    },
    updateBeforeLabelTextTpl: function(value) {
    	this.items[1].beforeLabelTextTpl = value;
    },
    updateSubmitValue: function(value) {
    	this.items[0].name = value;
    },
    updateFieldLabel: function(value) {
    	this.items[1].fieldLabel = value;
    },
	defaults : {
		msgTarget : 'under',
		labelWidth : 100,
		labelAlign : 'right'
	},
	items : [ {
		xtype : 'hidden',
		itemId : 'id'
	}, {
		itemId : 'text',
		xtype : 'selectfield',
		onClick : function(me) {
			Ext.widget('treeMultiSelectWindow', {treeMultiSelect : me.up('treeMultiSelect')}).show();
		}
	} ]
});
