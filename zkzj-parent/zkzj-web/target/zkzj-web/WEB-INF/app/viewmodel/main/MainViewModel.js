Ext.define('App.viewmodel.main.MainViewModel', {
	extend : 'Ext.app.ViewModel',
	alias : 'viewmodel.mainViewModel',
	data : {
		logoHtml : '<h1 style="color:#0D6CB2">重卡之家管理系统 '+Constant.VERSION+'</h1>'
	}
});