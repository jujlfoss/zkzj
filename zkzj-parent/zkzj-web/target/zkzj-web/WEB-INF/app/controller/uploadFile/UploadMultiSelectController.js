Ext.define('App.controller.uploadFile.UploadMultiSelectController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.uploadMultiSelectController',
	control : {
		'uploadMultiSelectWindow [action=save]' : {
			click : function(me) {
				var win = me.up("window");
				BaseUtil.submit(win.down("form"), "file/upload",
						function(image) {
							var detailUrls = win.uploadMultiSelect.down(
									"[itemId=files]").getValue();
							detailUrls = detailUrls ? detailUrls + "," : "";
							var files = image.data.split(",");
							win.uploadMultiSelect.down("[itemId=files]")
									.setValue(detailUrls + image.data);
							App.controller.uploadFile.showImages(files, null,
									win);
						});
			}
		},
		"uploadMultiSelectWindow" : {
			'afterrender' : function(me, eOpts) {
				me.down("panel").removeAll();
				var data = me.uploadMultiSelect.down("[itemId=files]")
						.getValue();
				App.controller.uploadFile.showImages(data, null, me);

			}
		}
	},
});
/***
 * 图片显示
 * @param img 图片路径
 * @param btn 当前按钮
 * @param win 窗口
 */
App.controller.uploadFile.showImages = function(image, btn, win) {
	if (image) {
		if (image.indexOf(",") > -1) {
			var files = image.split(",");
			for (var i = 0; i < files.length; i++) {
				var imgs = App.controller.uploadFile.createComponent(files[i]);
				App.controller.uploadFile.addImage(btn,win,imgs);
			}

		} else {
			var img = App.controller.uploadFile.createComponent(image);
			App.controller.uploadFile.addImage(btn,win,img);
		}

	}

}
/***
 * @param img 当前组件
 * @param  imag 图片路径
 */
App.controller.uploadFile.removeImages = function(img, imag) {
	Ext.MessageBox.confirm('提示', '你确定删除该图片?', function(btn) {
		var imgs = "";
		var subImg = [];
		if (btn == 'yes') {
			var win = Ext.getCmp("uploadMultiSelectWindow");
			var detailImages = win.uploadMultiSelect.down("[itemId=files]")
					.getValue();
			var id = img.getAttribute("id");
			var component = Ext.ComponentQuery.query("#" + id);
			var imagesPanel = Ext.ComponentQuery
					.query('panel [itemId=imagesPanel]');
			if (imagesPanel.length > 0) {
				imagesPanel[0].remove(component[0]);
				imgs = detailImages.split(",");
				for (var i = 0; i < imgs.length; i++) {
					if (imgs[i] != imag) {
						subImg.push(imgs[i]);
					}
				}
				imgs = subImg.join(",");
				win.uploadMultiSelect.down("[itemId=files]").setValue(imgs);
				Ext.Ajax.request({
					url : basePath + '/file/' + imag + '/deleteImage/',
					success : function(response, action) {
						var text = response.responseText;
						Ext.Msg.alert('系统提示', Ext.decode(text).msg);
					}
				});
			}
		}
	});
}
/**
 * 创建加载图片组件
 *@param image 图片路径
 *
 */
App.controller.uploadFile.createComponent = function(image) {
	return Ext.create('Ext.Component', {
		html : 'Hello world!',
		width : 100,
		height : 100,
		padding : 20,
		autoEl : {
			tag : "img",
			src : basePath + "/file/display/" + image,
			onClick : 'App.controller.uploadFile.removeImages(this,\'' + image
					+ '\')'
		}
	});
}
/**
 * 添加图片
 */
App.controller.uploadFile.addImage = function(btn,win,img) {
	if (!btn) {
		win.down("panel").add(img);
	} else {
		btn.up("window").down("panel").add(img);
	}
}