Ext.define('App.controller.uploadFile.UploadSingleSelectController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.uploadSingleSelectController',
	control : {
		'uploadSingleSelectWindow [action=save]' : {
			click : function(me) {
              var win = me.up("window");
      		  BaseUtil.submit(win.down("form"), "file/upload", function(image){
      			win.uploadSingleSelect.down("[itemId=files]").setValue(image.data);
      			App.controller.uploadFile.singleShowImages(image.data,me,null);
      		  });
			}
		},
		"uploadSingleSelectWindow":{
			'afterrender':function( me, eOpts ){
				me.down("panel").removeAll();
				var data=me.uploadSingleSelect.down("[itemId=files]").getValue();
				App.controller.uploadFile.singleShowImages(data,null,me);
				
			}
		}
	},
});

App.controller.uploadFile.singleShowImages=function(image,btn,win){
	if(image){
		var img=Ext.create('Ext.Component', {
		    html: 'Hello world!',
		    width: 100,
		    height: 100,
		    padding: 20,
		    autoEl:{
		    	tag:"img",
		    	src:basePath+"/file/display/"+image,
		    	//onClick:'App.controller.uploadFile.removeImages(this,\''+image+'\',\''+type+'\')'
		    }
		    
		});
			 if(!btn){
				 win.down("panel").add(img);
			 }else{
				btn.up("window").down("panel").removeAll();
				btn.up("window").down("panel").add(img); 
			 }
		 
	}
}
