Ext.define('App.controller.resource.ResourceMoveWindowController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.resourceMoveWindowController',
	control : {
		'resourceMoveWindow resourceMoveList' : {
			render : function(tree) {
				tree.getStore().reload();
			}
		},
		'resourceMoveWindow  [action=move]' : {
			click : function(me) {
				var win = me.up('window'),params = win.params,url = params.url;
				BaseUtil.post(url, function(data) {
					if (data.success) {
						App.controller.resource.util.refreshNode(url, data.data);
						BaseUtil.toast(data.msg);
						win.close();
					} else {
						BaseUtil.toast(data.msg);
					}
				},{
					'id' : params.id,
					'parentId' : win.down('resourceMoveList').getSelection()[0].get('id')
				});
			}
		}
	}
});