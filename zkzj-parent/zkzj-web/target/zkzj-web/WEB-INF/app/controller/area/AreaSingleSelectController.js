Ext.define('App.controller.area.AreaSingleSelectController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.areaSingleSelectController',
	control : {
		'areaSingleSelectWindow [action=save]' : {
			click : function(me) {
				var win = me.up('window'),id,mobile;
				var selection = win.down('areaSingleSelectList').getSelectionModel().getSelection();
				if(selection.length > 0){
					var record = selection[0];
					id = record.get('id');
					mobile = record.get('areaName');
				}
				var select = win.areaSingleSelect;
				select.down('[itemId=areaId]').setValue(id);
				select.down('[itemId=areaName]').setValue(mobile);
				win.close();
			}
		}
	},
	tbarSearch : function() {
		BaseUtil.loadStore(Ext.StoreMgr
				.get('area.areaSingleSelectStore'), Ext.getCmp(
				'areaSingleSelectWindow').down('form').getForm()
				.getFieldValues());
	}
});