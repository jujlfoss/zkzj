Ext.define('App.controller.role.RoleGrantWindowController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.roleGrantWindowController',
	control : {
		'roleGrantWindow roleGrantList' : {
			checkchange : function(node, checked, obj) {
				node.cascadeBy(function(n) {
					n.set('checked', checked);
				});
				App.controller.role.util.checkParent(node);
			}
		},
		'roleGrantWindow [action=save]' : {
			click : function(me) {
				var win = me.up('roleGrantWindow');
				var checkeds = win.down('roleGrantList').getView().getChecked();
				var resourceIds = [];
				Ext.each(checkeds, function(c) {
					resourceIds.push(c.get('id'));
				});
				var params = win.params;
				BaseUtil.post(params.url, function(data) {
					if(data.success){
						win.close();
						BaseUtil.toast(data.msg);
					}else{
						BaseUtil.toast(data.msg);
					}
				},{
					id : params.id,
					resourceIds : resourceIds
				});
			}
		}
	}
});
Ext.ns('App.controller.role.util');
App.controller.role.util.checkParent = function(node){
	node = node.parentNode;
	if (!node)
		return;
	var checkP = false;
	node.cascadeBy(function(n) {
		if (n != node) {
			if (n.get('checked')) {
				checkP = true;
			}
		}
	});
	node.set('checked', checkP);
	App.controller.role.RoleController.util.checkParent(node);
};