Ext.define('App.controller.role.RoleWindowController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.roleWindowController',
	control : {
		'roleWindow [action=save]' : {
			click : function(me) {
				var win = me.up('window'),params = win.params;
				BaseUtil.submit(win.down('form'), params.url,function(data) {
					BaseUtil.toast(data.msg);
					Ext.StoreMgr.get(params.storeId).reload();
					win.close();
				});
			}
		}
	}
});