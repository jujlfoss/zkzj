Ext.define('App.controller.merchantsType.MerchantsTypeSingleSelectController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.merchantsTypeSingleSelectController',
	control : {
		'merchantsTypeSingleSelectWindow [action=save]' : {
			click : function(me) {
				var win = me.up('window'),id,mobile;
				var selection = win.down('merchantsTypeSingleSelectList').getSelectionModel().getSelection();
				if(selection.length > 0){
					var record = selection[0];
					id = record.get('id');
					mobile = record.get('typeName');
				}
				var select = win.merchantsTypeSingleSelect;
				select.down('[itemId=merchantsTypeId]').setValue(id);
				select.down('[itemId=typeName]').setValue(mobile);
				win.close();
			}
		}
	},
	tbarSearch : function() {
		BaseUtil.loadStore(Ext.StoreMgr
				.get('merchantsType.merchantsTypeSingleSelectStore'), Ext.getCmp(
				'merchantsTypeSingleSelectWindow').down('form').getForm()
				.getFieldValues());
	}
});