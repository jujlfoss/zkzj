Ext.define('App.controller.merchantsType.MerchantsTypeViewController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.merchantsTypeViewController',
	menu : Ext.widget('menu'),
	control : {
		'merchantsTypeView merchantsTypeSearch basesearchform':{
            beforerender : function(form) {
                var buttons = {
                    'sys/merchantsType/saveMerchantsType' : function(permit) {
                      var typeName =  form.down("[name=typeName]").getValue();
                      if(!typeName){
                          BaseUtil.toast("请输入商户类型名");
                          return;
                       }
                        BaseUtil.post(permit.url,function(data){
                            BaseUtil.toast(data.msg);
                            Ext.StoreMgr.get('merchantsType.MerchantsTypeStore').reload();
                        },form.getValues());
                    },
                    'sys/merchantsType/updateMerchantsType' : function(permit) {
                        var id =  form.down("[name=id]").getValue();
                    	if(!id){
                    		return;
						}
                        var typeName =  form.down("[name=typeName]").getValue();
                        if(!typeName){
                            BaseUtil.toast("请输入商户类型名");
                            return;
                        }
                        BaseUtil.post(permit.url,function(data){
                            BaseUtil.toast(data.msg);
                            Ext.StoreMgr.get('merchantsType.MerchantsTypeStore').reload();
                        },form.getValues());
                    }
                }
                BaseUtil.createPermitBar(form, buttons);
			}
		},
		'merchantsTypeView merchantsTypeList' : {
            itemclick:function(view, record, item, index, e, eOpts){
                var search  =  Ext.getCmp('merchantsTypeView').down('merchantsTypeSearch form');
                search.reset();
                search.loadRecord(record);
            },
			itemcontextmenu : function(view, record, item, index, e) {
				var id = record.get('id');
				var menuitems = [];
				var status = record.get('status'),store = Ext.StoreMgr.get('merchantsType.MerchantsTypeStore');

					menuitems['sys/merchantsType/removeMerchantsType'] = function(permit) {
						BaseUtil.statusConfirm('确认删除此记录吗?',permit.url,id,store);
					}
				BaseUtil.createPermitMenu(view, this.menu, e, menuitems);
			},
			render : function() {
				BaseUtil.loadStore(Ext.StoreMgr.get('merchantsType.MerchantsTypeStore'),Ext.getCmp('merchantsTypeView').down('merchantsTypeSearch form').getForm().getFieldValues());
			}
		},
		'merchantsTypeView merchantsTypeSearch [action=search] ' : {
			click : function() {
				BaseUtil.loadStore(Ext.StoreMgr.get('merchantsType.MerchantsTypeStore'),Ext.getCmp('merchantsTypeView').down('merchantsTypeSearch form').getForm().getFieldValues());
			}
		}
	}
});