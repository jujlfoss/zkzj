Ext.define('App.controller.customer.CustomerSingleSelectController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.customerSingleSelectController',
	control : {
		'customerSingleSelectWindow [action=save]' : {
			click : function(me) {
				var win = me.up('window'),id,mobile;
				var selection = win.down('customerSingleSelectList').getSelectionModel().getSelection();
				if(selection.length > 0){
					var record = selection[0];
					id = record.get('id');
					mobile = record.get('mobile');
				}
				var select = win.customerSingleSelect;
				select.down('[itemId=customerId]').setValue(id);
				select.down('[itemId=mobile]').setValue(mobile);
				win.close();
			}
		}
	},
	tbarSearch : function() {
		BaseUtil.loadStore(Ext.StoreMgr
				.get('customer.CustomerSingleSelectStore'), Ext.getCmp(
				'customerSingleSelectWindow').down('form').getForm()
				.getFieldValues());
	}
});