Ext.define('App.controller.customer.CustomerWindowController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.customerWindowController',
	control : {
		'customerWindow [action=save]': {
			click : function(btn) {
				var win = btn.up('window'),params = win.params;
				BaseUtil.submit(win.down('form'), params.url,function(data) {
					BaseUtil.toast(data.msg);
					Ext.StoreMgr.get(params.storeId).reload();
					win.close();
				});
			}
		}
	}
});