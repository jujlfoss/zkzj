Ext.define('App.controller.customer.CustomerNameSingleSelectController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.customerNameSingleSelectController',
	control : {
		'customerNameSingleSelectWindow [action=save]' : {
			click : function(me) {
				var win = me.up('window'),id,mobile;
				var selection = win.down('customerNameSingleSelectList').getSelectionModel().getSelection();
				if(selection.length > 0){
					var record = selection[0];
					id = record.get('id');
					mobile = record.get('customerName');
				}
				var select = win.customerNameSingleSelect;
				select.down('[itemId=customerId]').setValue(id);
				select.down('[itemId=customerName]').setValue(mobile);
				win.close();
			}
		}
	},
	tbarSearch : function() {
		BaseUtil.loadStore(Ext.StoreMgr
				.get('customer.CustomerNameSingleSelectStore'), Ext.getCmp(
				'customerNameSingleSelectWindow').down('form').getForm()
				.getFieldValues());
	}
});