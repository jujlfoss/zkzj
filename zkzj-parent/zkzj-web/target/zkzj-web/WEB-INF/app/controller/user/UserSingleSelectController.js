Ext.define('App.controller.user.UserSingleSelectController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.userSingleSelectController',
	control : {
		'userSingleSelectWindow [action=save]' : {
			click : function(me) {
				var win = me.up('window'),id,userName;
				var selection = win.down('userSingleSelectList').getSelectionModel().getSelection();
				if(selection.length > 0){
					var record = selection[0];
					id = record.get('id');
					userName = record.get('userName');
				}
				var select = win.userSingleSelect;
				select.down('[itemId=receiveId]').setValue(id);
				select.down('[itemId=userName]').setValue(userName);
				win.close();
			}
		}
	},
	tbarSearch : function() {
		BaseUtil.loadStore(Ext.StoreMgr
				.get('user.UserSingleSelectStore'), Ext.getCmp(
				'userSingleSelectWindow').down('form').getForm()
				.getFieldValues());
	}
});