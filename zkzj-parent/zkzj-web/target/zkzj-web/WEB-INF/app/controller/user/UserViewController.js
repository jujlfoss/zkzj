Ext.define('App.controller.user.UserViewController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.userViewController',
	menu : Ext.widget('menu'),
	control : {
		'userView userList' : {
			beforerender : function(grid) {
				var buttons = {
					'sys/user/saveUser' : function(permit) {
						BaseUtil.createView('user.UserWindow',permit).show();
					}
				};
				BaseUtil.createPermitTbar(grid, buttons);
			},
			itemcontextmenu : function(view, record, item, index, e) {
				var userName = record.get('userName');
				if(userName !== Constant.ADMIN){
					var id = record.get('id');
					var menuitems = {
						'sys/user/updateUser' : function(permit) {
							var win = BaseUtil.createView('user.UserWindow',permit), viewModel = win.getViewModel();
							win.down('form').loadRecord(record);
							if(record.get('userType')==UserTypeEnum.EMPLOYEE.getValue()){
								viewModel.set({
									employeeSingleSelectHidden : false,
									employeeSingleSelectDisabled : false
								});
							}else{
								viewModel.set({
									employeeSingleSelectHidden : true,
									employeeSingleSelectDisabled : true
								});
							}
							win.getViewModel().set({
								hidden : true,
								disabled : true
							});
							win.show();
						},
						'sys/user/grant' : function(permit) {
							var win = BaseUtil.createView('user.UserGrantWindow',permit);
							win.down('[name=userId]').setValue(id);
							win.show();
						},
						'sys/user/resetPwd' : function(permit) {
							Ext.Msg.confirm('提示','确认将用户<font color=green> '+userName+' </font>的密码重置为<font color=red> '+ Constant.USER_RESETPWD +' </font>吗?',function(flag){
								if(flag==='yes'){
									BaseUtil.post(permit.url,function(data){
										BaseUtil.toast(data.msg);
									},{'id':id});
								}
							});
						}
					};
					var status = record.get('status'),store = Ext.StoreMgr.get('user.UserStore');
					if (status === StatusEnum.NORMAL.getValue()) {
						menuitems['sys/user/freeze'] = function(permit) {
							BaseUtil.statusConfirm('确认冻结<font color=red> '+userName+' </font>吗?',permit.url,id,store);
						}
					}else if (status === StatusEnum.FREEZE.getValue()) {
						menuitems['sys/user/unfreeze'] = function(permit) {
							BaseUtil.statusConfirm('确认解冻<font color=red> '+userName+' </font>吗?',permit.url,id,store);
						}
					}
					if(status === StatusEnum.DELETE.getValue()){
						menuitems['sys/user/recover'] = function(permit) {
							BaseUtil.statusConfirm('确认恢复<font color=red> '+userName+' </font>吗?',permit.url,id,store);
						}
					}else{
						menuitems['sys/user/removeUser'] = function(permit) {
							BaseUtil.statusConfirm('确认删除<font color=red> '+userName+' </font>吗?',permit.url,id,store);
						}
					}
					BaseUtil.createPermitMenu(view, this.menu, e, menuitems);
				}
			},
			render : function() {
				BaseUtil.loadStore(Ext.StoreMgr.get('user.UserStore'),Ext.getCmp('userView').down('userSearch form').getForm().getFieldValues());
			}
		},
		'userView userSearch [action=search] ' : {
			click : function() {
				BaseUtil.loadStore(Ext.StoreMgr.get('user.UserStore'),Ext.getCmp('userView').down('userSearch form').getForm().getFieldValues());
			}
		}
	}
});