Ext.define('App.controller.employee.EmployeeMuiltSelectController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.employeeMuiltSelectController',
	control : {
		'employeeMuiltSelectWindow [action=save]' : {
			click : function(me) {
				var win = me.up('window');
				var id = "";var employeeName="";
				var selection = win.down('employeeMuiltSelectList').getSelectionModel().getSelection();
				if(selection.length > 0){
					for(var i=0;i<selection.length;i++){
						var record = selection[i];
						id +=","+ record.get('id');
						employeeName +=","+ record.get('employeeName');
					}
				
				}
				var select = win.employeeMuiltSelect;
				select.down('[itemId=employeeId]').setValue(id.trim().substring(1));
				select.down('[itemId=employeeName]').setValue(employeeName.trim().substring(1));
				win.close();
			}
		}
	},
	tbarSearch : function() {
		BaseUtil.loadStore(Ext.StoreMgr
				.get('employee.EmployeeMuiltSelectStore'), Ext.getCmp(
				'employeeMuiltSelectWindow').down('form').getForm()
				.getFieldValues());
	}
});