Ext.define('App.controller.employee.EmployeeMuiltNotInCarSelectController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.employeeMuiltNotInCarSelectController',
	control : {
		'employeeMuiltNotInCarSelectWindow [action=save]' : {
			click : function(me) {
				var win = me.up('window');
				var id = "";var employeeName="";
				var selection = win.down('employeeMuiltNotInCarSelectList').getSelectionModel().getSelection();
				if(selection.length > 0){
					for(var i=0;i<selection.length;i++){
						var record = selection[i];
						id +=","+ record.get('id');
						employeeName +=","+ record.get('employeeName');
					}
				
				}
				var select = win.employeeMuiltNotInCarSelect;
				select.down('[itemId=employeeId]').setValue(id.trim().substring(1));
				select.down('[itemId=employeeName]').setValue(employeeName.trim().substring(1));
				win.close();
			}
		}
	},
	tbarSearch : function() {
		BaseUtil.loadStore(Ext.StoreMgr
				.get('employee.EmployeeMuiltNotInCarSelectStore'), Ext.getCmp(
				'employeeMuiltNotInCarSelectWindow').down('form').getForm()
				.getFieldValues());
	}
});