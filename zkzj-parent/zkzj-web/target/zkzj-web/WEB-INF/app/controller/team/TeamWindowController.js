Ext.define('App.controller.team.TeamWindowController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.teamWindowController',
	control : {
		'teamWindow [action=save]': {
			click : function(btn) {
				var win = btn.up('window'),url = win.params.url;
				BaseUtil.submit(win.down('form'), url,function(data) {
					App.controller.team.util.refreshNode(url,data.data);
					BaseUtil.toast(data.msg);
					win.close();
				});
			}
		}
	}
});