Ext.define('App.controller.team.TeamMoveWindowController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.teamMoveWindowController',
	control : {
		'teamMoveWindow teamMoveList' : {
			render : function(tree) {
				tree.getStore().reload();
			}
		},
		'teamMoveWindow  [action=move]' : {
			click : function(me) {
				var win = me.up('window');
				var params = win.params;
				var url = params.url;
				BaseUtil.post(url, function(data) {
					if (data.success) {
						App.controller.team.util.refreshNode(url, data.data);
						BaseUtil.toast(data.msg);
						win.close();
					} else {
						BaseUtil.toast(data.msg);
					}
				},{
					'id' : params.id,
					'parentId' : win.down('teamMoveList').getSelection()[0].get('id')
				});
			}
		}
	},
	tbarSearch:function(){
		BaseUtil.loadStore(Ext.StoreMgr.get('team.TeamMoveStore'), Ext.getCmp('teamMoveWindow').down('form').getForm().getFieldValues());
	}
});