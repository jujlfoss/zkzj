Ext.define('App.controller.team.TeamMultiSelectController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.teamMultiSelectController',
	control : {
		'teamMultiSelectWindow [action=save]':{
			click:function(me){
				var win = me.up('window'),teamIds = [],teamNames = [];
				var checkeds = win.down('teamMultiSelectList').getView().getChecked();
				if(checkeds.length > 0){
					Ext.each(checkeds, function(c) {
						var id = c.get('id'); 
						if(id!==1){
							teamIds.push(id);
							teamNames.push(c.get('teamName'));
						}
					});
				}
				var select = win.teamMultiSelect;
				select.down('[itemId=teamIds]').setValue(teamIds.join(','));
				select.down('[itemId=teamNames]').setValue(teamNames.join(','));
				win.close();
			}
		},
		'teamMultiSelectWindow [action=reset]':{
			click:function(me){
				var checkeds = me.up('window').down('teamMultiSelectList').getView().getChecked();
				Ext.each(checkeds, function(c) {
					c.set('checked',false);
				});
			}
		}
	},
	tbarSearch:function(){
		var tree = Ext.getCmp('teamMultiSelectWindow').down('teamMultiSelectList');
		tree.filterByTeamName(tree.down('[name=teamName]').getValue());
	}
});