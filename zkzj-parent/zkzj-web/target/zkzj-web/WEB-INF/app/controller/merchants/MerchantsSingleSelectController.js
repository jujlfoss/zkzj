Ext.define('App.controller.merchants.MerchantsSingleSelectController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.merchantsSingleSelectController',
	control : {
		'merchantsSingleSelectWindow [action=save]' : {
			click : function(me) {
				var win = me.up('window'),id,mobile;
				var selection = win.down('merchantsSingleSelectList').getSelectionModel().getSelection();
				if(selection.length > 0){
					var record = selection[0];
					id = record.get('id');
					mobile = record.get('name');
				}
				var select = win.merchantsSingleSelect;
				select.down('[itemId=merchantsId]').setValue(id);
				select.down('[itemId=name]').setValue(mobile);
				win.close();
			}
		}
	},
	tbarSearch : function() {
		BaseUtil.loadStore(Ext.StoreMgr
				.get('merchants.merchantsSingleSelectStore'), Ext.getCmp(
				'merchantsSingleSelectWindow').down('form').getForm()
				.getFieldValues());
	}
});