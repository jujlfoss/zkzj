Ext.define('App.controller.merchants.MerchantsViewController', {
	extend : 'Ext.app.ViewController',
	alias : 'controller.merchantsViewController',
	menu : Ext.widget('menu'),
	control : {
        'merchantsView merchantsSearch basesearchform':{
            beforerender : function(form) {
                var buttons = {
                    'sys/merchants/saveMerchants' : function(permit) {
                        var typeName =  form.down("[name=typeName]").getValue();
                        var areaId = form.down("baseTree").getValue();
                        var name = form.down("[name=name]").getValue();
                        if(!areaId){
                            BaseUtil.toast("请选择区域");
                            return;
                        }
                        if(!typeName){
                            BaseUtil.toast("请输入商户类型名");
                            return;
                        }
                        if(!name){
                            BaseUtil.toast("请输入商户名");
                            return;
                        }

                        BaseUtil.post(permit.url,function(data){
                            BaseUtil.toast(data.msg);
                            Ext.StoreMgr.get('merchants.MerchantsStore').reload();
                        },form.getValues());
                    },
                    'sys/merchants/updateMerchants' : function(permit) {
                        var id =  form.down("[name=id]").getValue();
                        if(!id){
                            return;
                        }
                        var typeName =  form.down("[name=typeName]").getValue();
                        var areaId = form.down("baseTree").getValue();
                        var name = form.down("[name=name]").getValue();
                        if(!areaId){
                            BaseUtil.toast("请选择区域");
                            return;
                        }
                        if(!typeName){
                            BaseUtil.toast("请输入商户类型名");
                            return;
                        }
                        if(!name){
                            BaseUtil.toast("请输入商户名");
                            return;
                        }
                        BaseUtil.post(permit.url,function(data){
                            BaseUtil.toast(data.msg);
                            Ext.StoreMgr.get('merchants.MerchantsStore').reload();
                        },form.getValues());
                    }
                }
                BaseUtil.createPermitBar(form, buttons);
            }
        },
		'merchantsView merchantsList' : {
            itemclick:function(view, record, item, index, e, eOpts){
                var search  =  Ext.getCmp('merchantsView').down('merchantsSearch form');
                search.reset();
                search.loadRecord(record);
                search.down("baseTree").setValue(record.get("areaId"));
                search.down("merchantsTypeSingleSelect").down("[itemId=merchantsTypeId]").setValue(record.get("merchantsTypeId"));
                search.down("merchantsTypeSingleSelect").down("[itemId=typeName]").setValue(record.get("typeName"));
            },
			itemcontextmenu : function(view, record, item, index, e) {
				var id = record.get('id');
				var menuitems = [];
				var status = record.get('status'),store = Ext.StoreMgr.get('merchants.MerchantsStore');
					menuitems['sys/merchants/removeMerchants'] = function(permit) {
						BaseUtil.statusConfirm('确认删除此记录吗?',permit.url,id,store);
					}
				BaseUtil.createPermitMenu(view, this.menu, e, menuitems);
			},
			render : function() {
				BaseUtil.loadStore(Ext.StoreMgr.get('merchants.MerchantsStore'),Ext.getCmp('merchantsView').down('merchantsSearch form').getForm().getFieldValues());
			}
		},
		'merchantsView merchantsSearch [action=search] ' : {
			click : function() {
				BaseUtil.loadStore(Ext.StoreMgr.get('merchants.MerchantsStore'),Ext.getCmp('merchantsView').down('merchantsSearch form').getForm().getFieldValues());
			}
		}
	}
});