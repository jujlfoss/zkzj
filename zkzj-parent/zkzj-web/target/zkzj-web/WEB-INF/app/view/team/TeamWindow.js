Ext.define('App.view.team.TeamWindow', {
	extend : 'App.config.BaseWindow',
	requires : [ 'App.controller.team.TeamWindowController'],
	controller : 'teamWindowController',
	xtype : 'teamWindow',
	id : 'teamWindow',
	buttons : [ {
		text : '保存',
		action : 'save'
	}, {
		text : '关闭',
		handler : function() {
			this.up('window').close();
		}
	} ],
	items : [ {
		xtype : 'basewindowform',
		items : [ {
			xtype : 'hidden',
			name : 'id'
		}, {
			name : 'teamName',
			beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
			fieldLabel : '名称',
			allowBlank : false
		}, {
			name : 'remark',
			fieldLabel : '备注',
		},{
			xtype : 'hidden',
			name:'parentId'
		},{
			name : 'sort',
			xtype : 'numberfield',
			fieldLabel : '排序值',
			beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
			allowBlank:false,
			value:1
		}]
	} ]
});