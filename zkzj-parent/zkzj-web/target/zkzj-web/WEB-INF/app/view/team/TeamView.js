Ext.define('App.view.team.TeamView', {
	extend : 'App.config.BasePanel',
	requires : [ 'App.controller.team.TeamViewController',
			'App.view.team.TeamSearch', 'App.view.team.TeamList' ],
	controller : 'teamViewController',
	xtype : 'teamView',
	id : 'teamView',
	items : [ {
		xtype : 'teamSearch'
	}, {
		xtype : 'teamList'
	} ]
});