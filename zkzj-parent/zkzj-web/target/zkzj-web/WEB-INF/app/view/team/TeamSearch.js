Ext.define('App.view.team.TeamSearch', {
	extend : 'App.config.BaseFieldSet',
	xtype : 'teamSearch',
	items : [ {
		xtype : 'basesearchform',
		items : [ {
			items : [ {
				name : 'teamName',
				fieldLabel : '名称'
			}]
		}, {
			items : [{
				xtype : 'button',
				text : '查询',
				action : 'search',
				columnWidth : .15,
				style : 'margin-left:15px'
			}, {
				xtype : 'button',
				text : '重置',
				columnWidth : .15,
				style : 'margin-left:15px',
				handler : function(me) {
					me.up('form').getForm().reset();
				}
			} ]
		} ]
	} ]
});
