Ext.define('App.view.merchantsType.MerchantsTypeView', {
	extend : 'App.config.BasePanel',
	requires : [ 'App.controller.merchantsType.MerchantsTypeViewController',
			'App.view.merchantsType.MerchantsTypeSearch', 'App.view.merchantsType.MerchantsTypeList' ],
	controller : 'merchantsTypeViewController',
	xtype : 'merchantsTypeView',
	id : 'merchantsTypeView',
	items : [ {
		xtype : 'merchantsTypeSearch'
	}, {
		xtype : 'merchantsTypeList'
	} ]
});