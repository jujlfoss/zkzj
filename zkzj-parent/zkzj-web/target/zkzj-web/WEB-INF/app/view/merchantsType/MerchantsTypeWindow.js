Ext.define('App.view.merchantsType.MerchantsTypeWindow', {
	extend : 'App.config.BaseWindow',
	requires : [ 'App.controller.merchantsType.MerchantsTypeWindowController'],
	controller : 'merchantsTypeWindowController',
	xtype : 'merchantsTypeWindow',
	id : 'merchantsTypeWindow',
	width:"590px",
    height:"157px",
	buttons : [ {
		text : '保存',
		action : 'save'
	}, {
		text : '关闭',
		handler : function() {
			this.up('window').close();
		}
	} ],
	items : [ {
		xtype : 'basewindowform',
		items : [{
			xtype:'hidden',
			name:'id'
		},{
			allowBlank : false,
			beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
			fieldLabel : '类型名',
			name : 'typeName'
		} ]
	} ]
});