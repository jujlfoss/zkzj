Ext.define('App.view.merchantsType.MerchantsTypeSearch', {
	extend : 'App.config.BaseFieldSet',
	xtype : 'merchantsTypeSearch',
	items : [ {
		xtype : 'basesearchform',
		items : [ {
			items : [{
				hidden:true,
				name:"id"
			}, {
				fieldLabel : '类型名',
				name : 'typeName'
			},{
                xtype : 'button',
                text : '查询',
                action : 'search',
                columnWidth : .15,
                style : 'margin-left:15px'
            }, {
                xtype : 'button',
                text : '重置',
                columnWidth : .15,
                style : 'margin-left:15px',
                handler : function(me) {
                    me.up('form').getForm().reset();
                }
            } ]
		} ]
	} ]
});
