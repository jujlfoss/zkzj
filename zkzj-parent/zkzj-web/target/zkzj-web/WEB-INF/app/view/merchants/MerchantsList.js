Ext.define('App.view.merchants.MerchantsList', {
	extend : 'App.config.BaseGrid',
	xtype : 'merchantsList',
    autoSizeXtype:"merchantsList",
	store : Ext.create('App.store.merchants.MerchantsStore',{
        groupField: 'typeName',
	}),
	bbar : {
		xtype : 'pagingtoolbar',
		store : this.store,
		scrollable : true,
		displayInfo : true
	},
    features: [{
        groupHeaderTpl: Ext.create('Ext.XTemplate',
            '商户类型:',
            '<span>{name:this.formatName}</span>',
            {
                formatName: function (name) {
                    return name;
                }
            }
        ),
        ftype: 'groupingsummary'
    }],
	columns : [ {
		xtype : 'rownumberer',
        summaryRenderer: function (value) {
            return "合计";
        }
	},{
        text : '省',
        dataIndex : 'pName'
	},{
        text : '市',
        dataIndex : 'cityName'
    },{
        text : '区/县',
        dataIndex : 'countyName'
    },{
        text : '所属区域',
        dataIndex : 'areaName'
    },{
        text : '商户类型',
        dataIndex : 'typeName'
    },{
			text : '商户名',
			dataIndex : 'name',
        summaryType: 'count',
        summaryRenderer: function (value) {

            return value ? value : 0;
        }
		}]
});