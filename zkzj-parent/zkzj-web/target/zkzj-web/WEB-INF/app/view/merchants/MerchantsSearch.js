Ext.define('App.view.merchants.MerchantsSearch', {
	extend : 'App.config.BaseFieldSet',
    requires : ['App.lib.MerchantsTypeSingleSelect','App.lib.BaseTree'],
	xtype : 'merchantsSearch',
	items : [ {
		xtype : 'basesearchform',
		items : [ {
			items : [{
				hidden:true,
				name:"id"
			}, {
                xtype: 'baseTree',
                fieldLabel: '区域',
                editable: false,
                emptyText: '--请选择--',
                columnWidth: .30,
				name:"areaId"



			},{
                    fieldLabel : '商户类型',
                    xtype:"merchantsTypeSingleSelect",
                    columnWidth: .22,
                    beforeLabelTextTpl : [ '' ],
                    allowBlank : true,
			},{
				fieldLabel : '商户名',
				name : 'name'
			} ]
		}, {
			items : [  {
				xtype : 'button',
				text : '查询',
				action : 'search',
				columnWidth : .15,
				style : 'margin-left:15px'
			}, {
				xtype : 'button',
				text : '重置',
				columnWidth : .15,
				style : 'margin-left:15px',
				handler : function(me) {
					me.up('form').getForm().reset();
                    me.up('form').down("baseTree").setValue(null);
				}
			} ]
		} ]
	} ]
});
