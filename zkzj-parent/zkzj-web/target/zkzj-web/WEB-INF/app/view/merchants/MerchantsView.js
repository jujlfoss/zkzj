Ext.define('App.view.merchants.MerchantsView', {
	extend : 'App.config.BasePanel',
	requires : [ 'App.controller.merchants.MerchantsViewController',
			'App.view.merchants.MerchantsSearch', 'App.view.merchants.MerchantsList' ],
	controller : 'merchantsViewController',
	xtype : 'merchantsView',
	id : 'merchantsView',
	items : [ {
		xtype : 'merchantsSearch'
	}, {
		xtype : 'merchantsList'
	} ]
});