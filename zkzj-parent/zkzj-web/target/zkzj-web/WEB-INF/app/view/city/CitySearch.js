Ext.define('App.view.city.CitySearch', {
	extend : 'App.config.BaseFieldSet',
	xtype : 'citySearch',
	items : [ {
		xtype : 'basesearchform',
		items : [ {
			items : [ {
				fieldLabel : '',
				name : 'areaName'
			},{
				fieldLabel : '',
				name : 'areaType'
			},{
				fieldLabel : '',
				name : 'parentId'
			},{
				fieldLabel : '',
				name : 'baiduCode'
			},{
				fieldLabel : '',
				name : 'cityCode'
			},{
				fieldLabel : '',
				name : 'addressCode'
			},{
				fieldLabel : '',
				name : 'latitude'
			},{
				fieldLabel : '',
				name : 'longitude'
			} ]
		}, {
			items : [ {
				xtype : 'basestartdate',
				columnWidth : .25,
			}, {
				xtype : 'baseenddate',
				columnWidth : .2,
				labelWidth : 10
			}, {
				xtype : 'button',
				text : '查询',
				action : 'search',
				columnWidth : .15,
				style : 'margin-left:15px'
			}, {
				xtype : 'button',
				text : '重置',
				columnWidth : .15,
				style : 'margin-left:15px',
				handler : function(me) {
					me.up('form').getForm().reset();
				}
			} ]
		} ]
	} ]
});
