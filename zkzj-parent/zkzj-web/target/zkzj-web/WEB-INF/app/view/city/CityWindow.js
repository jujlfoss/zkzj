Ext.define('App.view.city.CityWindow', {
	extend : 'App.config.BaseWindow',
	requires : [ 'App.controller.city.CityWindowController'],
	controller : 'cityWindowController',
	xtype : 'cityWindow',
	id : 'cityWindow',
	buttons : [ {
		text : '保存',
		action : 'save'
	}, {
		text : '关闭',
		handler : function() {
			this.up('window').close();
		}
	} ],
	items : [ {
		xtype : 'basewindowform',
		items : [{
			xtype:'hidden',
			name:'id'
		},{
			allowBlank : false,
			beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
			fieldLabel : '',
			name : 'areaName'
		},{
			allowBlank : false,
			beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
			fieldLabel : '',
			name : 'areaType'
		},{
			allowBlank : false,
			beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
			fieldLabel : '',
			name : 'parentId'
		},{
			allowBlank : false,
			beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
			fieldLabel : '',
			name : 'baiduCode'
		},{
			allowBlank : false,
			beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
			fieldLabel : '',
			name : 'cityCode'
		},{
			allowBlank : false,
			beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
			fieldLabel : '',
			name : 'addressCode'
		},{
			allowBlank : false,
			beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
			fieldLabel : '',
			name : 'latitude'
		},{
			allowBlank : false,
			beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
			fieldLabel : '',
			name : 'longitude'
		} ]
	} ]
});