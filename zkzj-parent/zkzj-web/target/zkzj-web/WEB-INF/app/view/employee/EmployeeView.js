Ext.define('App.view.employee.EmployeeView', {
	extend : 'App.config.BasePanel',
	requires : [ 'App.controller.employee.EmployeeViewController',
			'App.view.employee.EmployeeSearch', 'App.view.employee.EmployeeList' ],
	controller : 'employeeViewController',
	xtype : 'employeeView',
	id : 'employeeView',
	items : [ {
		xtype : 'employeeSearch'
	}, {
		xtype : 'employeeList'
	} ]
});