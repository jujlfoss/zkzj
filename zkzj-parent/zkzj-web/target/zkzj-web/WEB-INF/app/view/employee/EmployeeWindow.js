Ext.define('App.view.employee.EmployeeWindow', {
	extend : 'App.config.BaseWindow',
	requires : [ 'App.controller.employee.EmployeeWindowController','App.lib.MerchantsSingleSelect','App.lib.SexCombox',],
	controller : 'employeeWindowController',
	xtype : 'employeeWindow',
	id : 'employeeWindow',
	buttons : [ {
		text : '保存',
		action : 'save'
	}, {
		text : '关闭',
		handler : function() {
			this.up('window').close();
		}
	} ],
	items : [ {
		xtype : 'basewindowform',
		items : [{
			xtype:'hidden',
			name:'id'
		},{
			allowBlank : false,
			beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
			fieldLabel : '员工姓名',
			name : 'employeeName'
		},{
			xtype:'merchantsSingleSelect'
		},{
			xtype:'sexCombox'
		}, {
			name : 'mobile',
			beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
			fieldLabel : '手机号码',
			enforceMaxLength: true,
			maxLength:11,
			validator : function(value) {
				if (ValidUtil.isBlank(value))
					return '手机号码不能为空';
				if (!ValidUtil.isMobile(ValidUtil.trim(value)))
					return '手机号码格式错误';
				return true;
			}
		},{
			fieldLabel : '籍贯',
			name : 'nativePlace'
		},{
			allowBlank : false,
			beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
			fieldLabel : '地址',
			name : 'address'
		} ]
	} ]
});