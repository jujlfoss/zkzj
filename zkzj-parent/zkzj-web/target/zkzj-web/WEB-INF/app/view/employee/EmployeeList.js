require.sync('SexEnum');
Ext.define('App.view.employee.EmployeeList', {
	extend : 'App.config.BaseGrid',
	xtype : 'employeeList',
    autoSizeXtype:"employeeList",
	store : Ext.create('App.store.employee.EmployeeStore',{
        groupField:"name"
	}),
	bbar : {
		xtype : 'pagingtoolbar',
		store : this.store,
		scrollable : true,
		displayInfo : true
	},
    features: [{
        groupHeaderTpl: Ext.create('Ext.XTemplate',
            '所属商户:',
            '<span>{name:this.formatName}</span>',
            {
                formatName: function (name) {
                    return name;
                }
            }
        ),
        ftype: 'groupingsummary'
    }],
	columns : [ {
		xtype : 'rownumberer',
        summaryRenderer: function (value) {
            return "合计";
        }
	},{
		text : '员工姓名',
		dataIndex : 'employeeName'
	}, {
		text : '性别',
		dataIndex : 'sex',
		renderer : function(value) {
			return SexEnum.getHtml(value);
		}
	}, {
		text : '籍贯',
		dataIndex : 'nativePlace'
	}, {
		text : '地址',
		dataIndex : 'address'
	}, {
		text : '手机号码',
		dataIndex : 'mobile',
        summaryType: 'count',
        summaryRenderer: function (value) {

            return value ? value : 0;
        }
	} ]
});