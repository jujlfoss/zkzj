Ext.define('App.view.area.AreaWindow', {
	extend : 'App.config.BaseWindow',
	requires : [ 'App.controller.area.AreaWindowController','App.lib.ProviceCombox',
        'App.lib.CityCombox', 'App.lib.CountyCombox'],
	controller : 'areaWindowController',
	xtype : 'areaWindow',
	id : 'areaWindow',
	buttons : [ {
		text : '保存',
		action : 'save'
	}, {
		text : '关闭',
		handler : function() {
			this.up('window').close();
		}
	} ],
	items : [ {
		xtype : 'basewindowform',
		items : [{
			xtype:'hidden',
			name:'id'
		},{
            xtype:'hidden',
            name:'parentId'
        },{
            readOnly : true,
            fieldLabel : '上级名',
            itemId : 'parentName'
        },{
            xtype : "proviceCombox",
            listeners : {
                "select" : function(fatherType, record, index) {
                    var provinceId = fatherType.getValue();
                    var win = Ext.getCmp("areaWindow");
                    var cityCombox = win.down("cityCombox");
                    cityCombox.clearValue();
                    if (provinceId) {
                        cityCombox.getStore().load({
                            params : {
                                id : provinceId
                            }
                        });
                    }

                }
            }
        }, {
            xtype : "cityCombox",
            listeners : {
                "select" : function(fatherType, record, index) {
                    var cityId = fatherType.getValue();
                    var win = Ext.getCmp("areaWindow");
                    var countyCombox = win.down("countyCombox");
                    countyCombox.clearValue();
                    if (cityId) {
                        countyCombox.getStore().load({
                            params : {
                                id : cityId
                            }
                        });
                    }

                }
            }

        },{
            xtype:"countyCombox"
        },{
			allowBlank : false,
			beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
			fieldLabel : '区域名',
			name : 'areaName'
		}]
	} ]
});