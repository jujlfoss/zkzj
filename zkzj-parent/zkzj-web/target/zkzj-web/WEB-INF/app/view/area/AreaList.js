Ext.define('App.view.area.AreaList', {
    extend: 'Ext.tree.Panel',
    xtype: 'areaList',
    store: Ext.create('App.store.area.AreaStore'),
    expanded: true,
    rootVisible: false,
    initComponent: function () {
        this.columns = [{
            dataIndex: 'id',
            hidden: true
        }, {
            dataIndex: 'parentId',
            hidden: true
        },{
            text: '区域',
            xtype: 'treecolumn',
            dataIndex: 'areaName',
            flex: 1
        }, {
            text : '省',
            dataIndex : 'pName',
            flex: 1
        },{
            text : '市',
            dataIndex : 'cityName',
            flex: 1
        },{
            text : '区/县',
            dataIndex : 'countyName',
            flex: 1
        },]
        this.callParent(arguments);
    }
});