Ext.define('App.view.user.UserWindow', {
	extend : 'App.config.BaseWindow',
	requires : [ 'App.controller.user.UserWindowController','App.viewmodel.user.UserWindowViewModel','App.lib.EmployeeSingleSelect'],
	controller : 'userWindowController',
	xtype : 'userWindow',
	id : 'userWindow',
	viewModel : 'userWindowViewModel',
	buttons : [ {
		text : '保存',
		action : 'save'
	}, {
		text : '关闭',
		handler : function() {
			this.up('window').close();
		}
	} ],
	items : [ {
		xtype : 'basewindowform',
		items : [ {
			xtype:'hidden',
			name:'id'
		},{
			xtype : 'combobox',
			fieldLabel : '用户类型',
			beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
			fieldStyle : 'cursor:pointer',
			name : 'userType',
			editable : false,
			store : Ext.create('Ext.data.Store', {
				fields : [ 'name', 'value' ],
				data : [ {
					'name' : UserTypeEnum.EMPLOYEE.getText(),
					'value' : UserTypeEnum.EMPLOYEE.getValue()
				}, {
					'name' : UserTypeEnum.CUSTOMER.getText(),
					'value' : UserTypeEnum.CUSTOMER.getValue()
				} ]
			}),
			readOnly:true,
			queryMode : 'local',
			displayField : 'name',
			valueField : 'value',
			value : UserTypeEnum.EMPLOYEE.getValue()
		},{
	        bind: {  
	        	disabled:'{employeeSingleSelectDisabled}',
	        	hidden:'{employeeSingleSelectHidden}'
	        },
			xtype:'employeeSingleSelect'
		},{
			name : 'userName',
			beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
			fieldLabel : '用户名',
			enforceMaxLength : true,
			maxLength : 22,
			emptyText : '请输入2至22位用户名',
			validator : function(value) {
				if (ValidUtil.isBlank(value))
					return '用户名不能为空';
				if (!ValidUtil.isUserName(value))
					return '用户名应为2-22位(字母,数字)组成';
				return true;
			}
		}
//		, {
//	        bind: {  
//	        	disabled:'{disabled}',
//	        	hidden:'{hidden}'
//	        },
//			inputType : 'password',
//			name : 'userPwd',
//			beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
//			fieldLabel : '密码',
//			emptyText : '请输入密码',
//			validator : function(value) {
//				if (ValidUtil.isBlank(value))
//					return '密码不能为空';
//				if (!ValidUtil.isPwd(value))
//					return '密码长度应为6-22位';
//				return true;
//			}
//		}, {
//	        bind: {  
//	        	disabled:'{disabled}',
//	        	hidden:'{hidden}'
//	        },
//			inputType : 'password',
//			name : 'userPwdRe',
//			beforeLabelTextTpl : [ '<font color=red data-qtip=必填选项>*</font>' ],
//			fieldLabel : '确认密码',
//			emptyText : '请输入确认密码',
//			validator : function(value) {
//				if (ValidUtil.isBlank(value))
//					return '确认密码不能为空';
//				if(value!==this.up('form').getForm().findField('userPwd').getValue())
//					return '密码和确认密码不相同';
//				return true;
//			}
//		}
		, {
			name : 'mobile',
			fieldLabel : '手机号码',
			enforceMaxLength: true,
			maxLength:11,
			validator : function(value) {
				if (ValidUtil.isBlank(value))
					return true;
				if (!ValidUtil.isMobile(ValidUtil.trim(value)))
					return '手机号码格式错误';
				return true;
			}
		}, {
			name : 'email',
			fieldLabel : '电子邮箱',
			validator : function(value) {
				if (ValidUtil.isBlank(value))
					return true;
				if (!ValidUtil.isEmail(ValidUtil.trim(value)))
					return '电子邮箱格式错误';
				return true;
			}
		} ]
	} ]
});