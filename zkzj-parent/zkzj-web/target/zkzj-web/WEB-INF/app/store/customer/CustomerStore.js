Ext.define('App.store.customer.CustomerStore', {
	extend : 'Ext.data.Store',
	model : 'App.model.customer.CustomerModel',
	storeId : 'customer.CustomerStore',
	pageSize : Constant.PAGESIZE,
	proxy : {
		type : 'ajax',
		url : basePath + '/crm/customer/pageCustomer',
		reader : {
			type : 'json',
			rootProperty : 'items',
			totalProperty : 'total'
		}
	}
});