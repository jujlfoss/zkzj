Ext.define('App.store.merchantsType.MerchantsTypeStore', {
	extend : 'Ext.data.Store',
	model : 'App.model.merchantsType.MerchantsTypeModel',
	storeId : 'merchantsType.MerchantsTypeStore',
	pageSize : Constant.PAGESIZE,
	proxy : {
		type : 'ajax',
		url : basePath + '/sys/merchantsType/pageMerchantsType',
		reader : {
			type : 'json',
			rootProperty : 'items',
			totalProperty : 'total'
		}
	}
});