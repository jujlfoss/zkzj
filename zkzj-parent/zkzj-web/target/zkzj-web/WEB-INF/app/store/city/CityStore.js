Ext.define('App.store.city.CityStore', {
	extend : 'Ext.data.Store',
	model : 'App.model.city.CityModel',
	storeId : 'city.CityStore',
	pageSize : Constant.PAGESIZE,
	proxy : {
		type : 'ajax',
		url : basePath + '/sys/city/pageCity',
		reader : {
			type : 'json',
			rootProperty : 'items',
			totalProperty : 'total'
		}
	}
});