Ext.define('App.store.team.TeamStore', {
	extend : 'Ext.data.TreeStore',
	storeId : 'team.TeamStore',
	model : 'App.model.team.TeamModel',
	autoLoad : false,
	proxy : {
		type : 'ajax',
		url : basePath + '/hrm/team/list'
	},
	root : {
		id : Constant.ROOT,
		expanded : true
	}
});

