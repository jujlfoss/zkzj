Ext.define('App.store.area.AreaStore', {
	extend : 'Ext.data.TreeStore',
	model : 'App.model.area.AreaModel',
	storeId : 'area.AreaStore',
	proxy : {
		type : 'ajax',
		url : basePath + '/sys/area/areaTree',
	},
    root: {
        id: Constant.ROOT,
        expanded: true,
        rootVisible: false
    },
    listeners:{
        load:function(store){
            store.getRootNode().expand(true);
        }
    }
});