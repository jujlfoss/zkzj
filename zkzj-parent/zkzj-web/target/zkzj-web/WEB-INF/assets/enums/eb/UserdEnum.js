(function(window) {
	var UserdEnum = { 
			NOUSER : {
			getText : function() {
				return '未使用';
			},
			getValue : function() {
				return 0;
			}
		},
		USER : {
			getText : function() {
				return '已使用';
			},
			getValue : function() {
				return 1;
			}
		},
		PASS : {
			getText : function() {
				return '已接单';
			},
			getValue : function() {
				return 2;
			}
		},
		getHtml : function(value) {
			switch (value) {
			case 0:
				return '未使用';
			case 1:
				return '已使用';
			case 2:
				return '已过期';
			
			}
		}
	};
	window.UserdEnum = UserdEnum;
})(window);
