/**
 * OrderStatusEnum
 */
(function(window) {
	var OrderStatusEnum = {
		WAIT : {
			getText : function() {
				return '未领取';
			},
			getValue : function() {
				return 0;
			}
		},
		FINISH : {
			getText : function() {
				return '已完成';
			},
			getValue : function() {
				return 1;
			}
		},
		CANCEL : {
			getText : function() {
				return '已取消';
			},
			getValue : function() {
				return 2;
			}
		},
		DELETED : {
			getText : function() {
				return '客户已删除';
			},
			getValue : function() {
				return 3;
			}
		},
		EMPLOYEE_DELETE : {
			getText : function() {
				return '员工已删除';
			},
			getValue : function() {
				return 4;
			}
		},
		ALL_DELETE : {
			getText : function() {
				return '全部删除';
			},
			getValue : function() {
				return 5;
			}
		},
		getHtml : function(value) {
			switch (value) {
			case 0:
				return '未领取';
			case 1:
				return '已完成';
			case 2:
				return '已取消';
			case 3:
				return '客户已删除';
			case 4:
				return '员工已删除';
			case 5:
				return '全部删除';
			}
		}
	};
	window.OrderStatusEnum = OrderStatusEnum;
})(window);
