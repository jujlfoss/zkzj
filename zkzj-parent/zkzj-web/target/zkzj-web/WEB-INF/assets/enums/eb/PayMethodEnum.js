/**
 * PayMethodEnum
 */
(function(window) {
	var PayMethodEnum = {
			ACCOUNT : {
			getText : function() {
				return '账户';
			},
			getValue : function() {
				return 0;
			}
		},
		ALIPAY : {
			getText : function() {
				return '支付宝';
			},
			getValue : function() {
				return 1;
			}
		},
		WECHAT : {
			getText : function() {
				return '微信';
			},
			getValue : function() {
				return 2;
			}
		},
		POINT : {
			getText : function() {
				return '积分';
			},
			getValue : function() {
				return 3;
			}
		},
		getHtml : function(value) {
			switch (value) {
			case 0:
				return '账户';
			case 1:
				return '支付宝';
			case 2:
				return '微信';
			case 3:
				return '积分';
			}
		}
	};
	window.PayMethodEnum = PayMethodEnum;
})(window);
