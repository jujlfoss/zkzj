/**
 * StatusEnum
 */
(function(window) {
	var BespeakEnum = { 
		PAYWAIT : {
			getText : function() {
				return '待支付';
			},
			getValue : function() {
				return 0;
			}
		},
		PAYSUCCESS : {
			getText : function() {
				return '已支付';
			},
			getValue : function() {
				return 1;
			}
		},
		ORDERS : {
			getText : function() {
				return '已接单';
			},
			getValue : function() {
				return 2;
			}
		},
		FINISH : {
			getText : function() {
				return '已完成';
			},
			getValue : function() {
				return 3;
			}
		},
		CANCEL : {
			getText : function() {
				return '已取消';
			},
			getValue : function() {
				return 4;
			}
		},
		CUSTOMER_DELETED : {
			getText : function() {
				return '客户已删除';
			},
			getValue : function() {
				return 1;
			}
		},
		EMP_DELETED : {
			getText : function() {
				return '员工已删除';
			},
			getValue : function() {
				return 2;
			}
		},
		ALL_DELETED : {
			getText : function() {
				return '全部已删除';
			},
			getValue : function() {
				return 0;
			}
		},
		getHtml : function(value) {
			switch (value) {
			case 0:
				return '待支付';
			case 1:
				return '已支付';
			case 2:
				return '已接单';
			case 3:
				return '已完成';
			case 4:
				return '已取消';
			}
		},
		getDelHtml : function(value) {
			switch (value) {
			case 1:
				return '客户已删除';
			case 2:
				return '员工已删除';
			case 0:
				return '全部已删除';
			}
		}
	};
	window.BespeakEnum = BespeakEnum;
})(window);
