(function(window) {
	var RechargeCardBindStatusEnum = {
		WAIT : {
			getText : function() {
				return '待绑定';
			},
			getValue : function() {
				return 0;
			}
		},
		WAIT_PAY : {
			getText : function() {
				return '待支付';
			},
			getValue : function() {
				return 1;
			}
		},
		PAYED : {
			getText : function() {
				return '已支付';
			},
			getValue : function() {
				return 2;
			}
		}
	};
	window.RechargeCardBindStatusEnum = RechargeCardBindStatusEnum;
})(window);