/**
 * TradeTypeEnum
 */
(function(window) {
	var TradeTypeEnum = {
		BESPEAK : {
			getText : function() {
				return '服务订单';
			},
			getValue : function() {
				return 0;
			}
		},
		RECHARGE : {
			getText : function() {
				return '充值卡';
			},
			getValue : function() {
				return 1;
			}
		},
		REFUND : {
			getText : function() {
				return '退款';
			},
			getValue : function() {
				return 2;
			}
		}
	};
	window.TradeTypeEnum = TradeTypeEnum;
})(window);
